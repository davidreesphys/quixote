import numpy as np

def machsfr():
    """
        MACHSFR returns the safe numerical range (a tiny positive machine
    value e0 for which -e0, 1/e0, -1/e0 are all computable).
    This routine may contain system dependencies.

        This code is tricky. The parameter lshort is intended to indicate
    whether real*8 variables are four-byte or eight-byte. In the case
    of four-byte reals it is assumed that we have IEEE arithmetic,
    and the real*8 value mchsfr is used (mchsfr was taken from code for
    the subroutine r1mach in the core section on netlib, April 1992).
    In the case of eight-byte reals the NAG library routine x02amf is
    employed.

        It was found that if mchsfr is set exactly as in r1mach, i.e.
    using small=8388608, then the expression exp(log(mchsfr)) raises
    an underflow exception on the Sparc (f77 with libraries SC1.0).
    Using a slightly larger value, small=8388614, avoids that problem.
    The decimal equivalent value is about 1.1755e-38 in either case.
    """
    #Not correctly implemented
    if (1.0 + 1e-12) == 1.0:
        return 1e-12
    else:
        return 1e-15

def dim(x,y):
    if isinstance(x,int) or isinstance(x,float):
        x = np.array([x])
        y = np.array([y])
    tmp = x-y
    tmp[tmp<0] = 0.0
    return tmp

def expu(t0):
    cutll = np.full_like(t0, fill_value = np.log(machsfr()))
    cutlo = np.maximum(np.full_like(t0, fill_value= machsfr()), np.exp(cutll))
    return dim(np.exp(np.maximum(t0,cutll)),cutlo)

def b2sqel(mother):
    """
        B2SQEL computes on all cells of the mesh the rate coefficients
    for ionisation, recombination, electron heat loss and radiation.

        This routine employs the log-log linear representation provided
    in the arrays rlsa, rlra, rlqa, rlrd and rlbr to evaluate on all cells
    of the mesh the rate coefficients for ionisation, recombination,
    electron heat loss, and radiation.

        In case a rate coefficient vanishes, the representation of the
    log of the rate coefficient will provide a large negative value,
    for which the exponential function should underflow. The present
    code tests for this condition and explicitly sets those rate
    coefficients to 0.

    Input variables are created in sources/b2spel.py

    matchsfr(), from utility/machsfr.F, returns the smaller value for
    which x, -x, 1/x and -1/x can be computed safely.

    x02amf is a NAG fortran function that returns the smaller value
    """


    # Unpacking inputs for less verbose code
    ny = mother.ny
    nx = mother.nx
    ns = mother.ns
    ev = mother.ev
    ismain = 1 #WRONG! It should be the main ion species,not hardwired.

    te = mother.te.copy()*ev

    rlsa = mother.rlsa.copy()
    rlra = mother.rlra.copy()
    rlqa = mother.rlqa.copy()
    rlrd = mother.rlrd.copy()
    rlbr = mother.rlbr.copy()
    rlza = mother.rlza.copy()
    rlz2 = mother.rlz2.copy()
    rlpt = mother.rlpt.copy()
    rlpi = mother.rlpi.copy()


    #import ipdb; ipdb.set_trace()
    zamin = mother.zamin.copy()
    zamin = np.repeat(zamin[np.newaxis], ny, axis=0)
    zamin = np.repeat(zamin[np.newaxis], nx, axis=0)
    zamax = mother.zamax.copy()
    zamax = np.repeat(zamax[np.newaxis], ny, axis=0)
    zamax = np.repeat(zamax[np.newaxis], nx, axis=0)

    # Defining arrays of outputs
    rsa = np.zeros((nx,ny,ns))
    rra = np.zeros((nx,ny,ns))
    rqa = np.zeros((nx,ny,ns))
    rrd = np.zeros((nx,ny,ns))
    rbr = np.zeros((nx,ny,ns))
    rza = np.zeros((nx,ny,ns))
    rz2 = np.zeros((nx,ny,ns))
    rpt = np.zeros((nx,ny,ns))
    rpi = np.zeros((nx,ny,ns))


    ## Get values for switches
    phm0 = mother.read_switch('b2sqel_phm0', default=1.0)
    phm1 = mother.read_switch('b2sqel_phm1', default=1.0)
    phm2 = mother.read_switch('b2sqel_phm2', default=1.0)
    art_rad = mother.read_switch('b2sqel_artificial_radiation', default=0.0)



    #Compute coefficients
    wrk0 = np.log(te/ev)
    for si in range(ns):
        for yi in range(ny):
            for xi in range(nx):
                rsa[xi,yi,si] = phm0*expu(
                        rlsa[xi,yi,0,si] +  rlsa[xi,yi,1,si]*wrk0[xi,yi])
                rra[xi,yi,si] = phm1*expu(
                        rlra[xi,yi,0,si] + rlra[xi,yi,1,si]*wrk0[xi,yi])
                rqa[xi,yi,si] = phm2*ev*expu(
                        rlqa[xi,yi,0,si] + rlqa[xi,yi,1,si]*wrk0[xi,yi])
                rrd[xi,yi,si] = phm2*ev*expu(
                        rlrd[xi,yi,0,si] + rlrd[xi,yi,1,si]*wrk0[xi,yi])
                rbr[xi,yi,si] = phm2*ev*expu(
                        rlbr[xi,yi,0,si] + rlbr[xi,yi,1,si]*wrk0[xi,yi])
                rza[xi,yi,si] = np.maximum(
                        zamin[xi,yi,si], np.minimum(zamax[xi,yi,si],
                        rlza[xi,yi,0,si] + rlza[xi,yi,1,si]*wrk0[xi,yi]))
                rz2[xi,yi,si] = np.maximum(
                        np.power(zamin[xi,yi,si],2), np.minimum(
                        np.power(zamax[xi,yi,si],2), rlz2[xi,yi,0,si] +
                        rlz2[xi,yi,1,si]*wrk0[xi,yi]))
                rpt[xi,yi,si] = rlpt[xi,yi,0,si] + rlpt[xi,yi,1,si]*wrk0[xi,yi]
                rpi[xi,yi,si] = rlpi[xi,yi,0,si] + rlpi[xi,yi,1,si]*wrk0[xi,yi]

    #Compute artificial radiation
    if art_rad != 0.0:
        art_rad = np.abs(art_rad)
        mother.log.info("Artificial radiation model active" +
                "art_rad = {}".format(art_rad))
        tnorm = np.maximum(np.full_like(te,fill_value=0.000001), te/(15.0*ev))
        ftnorm = np.power(tnorm,1.5) + 1.0/np.power(tnorm,3)
        rqa[...,ismain] = rqa[...,ismain] + art_rad*2e-31/ftnorm
        rrd[...,ismain] = rrd[...,ismain] + art_rad*2e-31/ftnorm


    return {
        'rsa': rsa,
        'rra': rra,
        'rqa': rqa,
        'rrd': rrd,
        'rbr': rbr,
        'rza': rza,
        'rz2': rz2,
        'rpt': rpt,
        'rpi': rpi,
    }






























