import os
import numpy as np
import glob

#from quixote.tools import SimpleNamespace, create_log, extend, get
from . import SimpleNamespace, create_log, extend, get
import scipy.io as io

#def read_adas_adf15_file(file):


log = create_log('ATOMIC')


def elements(ident):
    """ If Zn given, returns name.  If name given, returns Zn."""
    tmp = {1:'h',2:'he',3:'li',4:'be',5:'b',6:'c',7:'n',8:'o',9:'f',
        10:'ne',11:'na',12:'mg',13:'al',14:'si',15:'p',16:'s',
        17:'cl',18:'ar',19:'k',20:'ca',21:'sc',22:'ti',23:'v',
        24:'cr',25:'mn',26:'fe',27:'co',28:'ni',29:'cu',30:'zn',
        31:'ga',32:'ge',33:'as',34:'se',35:'br',36:'kr',37:'rb',
        38:'sr',39:'y',40:'zr',41:'nb',42:'mo',43:'tc',44:'ru',
        45:'rh',46:'pd',47:'ag',48:'cd',49:'in',50:'sn',51:'sb',
        52:'te',53:'i',54:'xe',55:'cs',56:'ba',57:'la',58:'ce',
        59:'pr',60:'nd',61:'pm',62:'sm',63:'eu',64:'gd',65:'tb',
        66:'dy',67:'ho',68:'er',69:'tm',70:'yb',71:'lu',72:'hf',
        73:'ta',74:'w',75:'re',76:'os',77:'ir',78:'pt',79:'au',
        80:'hg'}
    try:
        return tmp[int(ident)]
    except:
        return {y: x for x, y in tmp.items()}[ident.lower()]


def ionization_potentials(ident):
    """ Returns ionization potentials, given Zn or name."""
    data = []
    with open('ionization_potentials.dat', 'r') as file:
        for line in file:
            data.append([float(ele) for ele in line.split()])
    try:
        return data[elements(ident.lower())-1]
    except:
        return data[int(ident)-1]






class ADAS(object):
    def __init__(self, path, edition='f15'):
        self.path = path


    ## ATTENTION: Not working for now.
    def read_element(self, element):
        """ Reads files associated with element.
        Returns
        -------
        Dict which keys are the series.
        """
        element = element.lower()
        element_str = "*#{}".format(element)
        path_files = os.path.join(self.path, element_str)
        drs = glob.glob(path_files)
        series = [] #Balmer, paschen, etc.
        data = {}
        #import ipdb; ipdb.set_trace() ## Problem in 183
        for dr in drs:
            for file in glob.glob(os.path.join(dr, "*.dat")):
                name = os.path.basename(file).split("#")[1]
                if name not in series:
                    series.append(name)
            for name in series:
                data[name] = []
                files = glob.glob(os.path.join(dr, "*{}*.dat".format(name)))
                inds = []
                tmp = []
                for file in files:
                    inds.append(int(os.path.basename(file).split("#")[2]
                        .split(element)[1].split('.')[0]))
                    tmp.append(parse_adasf15(file))
                for i in range(len(inds)):
                    data[name].append(tmp[inds.index(i)])
        return data











## ATTENTION: For now only for structured
def linearize_rates(mother, mingrad=-20, maxgrad=20):
    """ Based on Jeremy's Matlab/linearize_rates.m
    Try to generalize to 308, 310, and 320.
    For now, results equivalent to b25_src/sources/b2spel
    but much faster (factor of 7 or 42)
    """
    data = {}

    logte = np.clip(np.log(mother.te), mother.rtlt[0], mother.rtlt[-1])
    logne = np.clip(np.log(mother.ne), mother.rtln[0], mother.rtln[-1])

    ite = np.full_like(mother.te, -9999, dtype=int)
    ine = np.full_like(mother.ne, -9999, dtype=int)
    

    ## Find the indices of the table
    if mother.grid_type == 'structured':
        for iy in range(mother.ny):
            for ix in range(mother.nx):
                ite[ix,iy] = np.where(mother.rtlt >=logte[ix,iy])[0][0] -1
                ine[ix,iy] = np.where(mother.rtln >=logne[ix,iy])[0][0] -1 
    elif mother.grid_type == 'unstructured':
        for ici in range(mother.nci):
            ite[ici] = np.where(mother.rtlt >=logte[ici])[0][0] -1
            ine[ici] = np.where(mother.rtln >=logne[ici])[0][0] -1 
    else:
        mother.log.exception(
            "'grid_type' is neither 'structured' nor 'unstructured'")


    fxte = (logte-mother.rtlt[ite])/(mother.rtlt[ite+1]-mother.rtlt[ite])
    fxne = (logne-mother.rtln[ine])/(mother.rtln[ine+1]-mother.rtln[ine])

    for label in ['r{}lsa', 'r{}lra', 'r{}lqa', 'r{}lrd',
        'r{}lbr', 'r{}lza', 'r{}lz2', 'r{}lpt', 'r{}lpi']:

        rnew = label.format('')
        rold = label.format('t')

        if mother.grid_type == 'structured':
            tmp = np.full((mother.nx, mother.ny, 2, mother.ns), np.nan)
        elif mother.grid_type == 'unstructured':
            tmp = np.full((mother.ncv, 2, mother.ns), np.nan)

        coeff3d = get(mother, rold)
        for si in range(mother.ns):
            t0 = ((1.0 - fxne)*coeff3d[ite,ine,si] +
                         fxne* coeff3d[ite,ine+1,si])
            t1 = ((1.0 - fxne)*coeff3d[ite+1,ine,si] +
                         fxne* coeff3d[ite+1,ine+1,si])
            tt = np.clip(
                (t1-t0)/(mother.rtlt[ite+1] - mother.rtlt[ite]),
                mingrad, maxgrad)

            tmp[...,1,si] = tt
            tmp[...,0,si] = (1.0 - fxte)*t0 + fxte*t1 - tt*logte

        data[rnew] =  tmp
    
    return data







def evaluate_rates(mother):
    """
        B2SQEL computes on all cells of the mesh the rate coefficients
    for ionisation, recombination, electron heat loss and radiation.
    Compared to Jeremy's matlab tool.

    Surprisingly, 
    data[rnew] = factor*np.exp(rold[...,0,:].T + rold[...,1,:].T*logte.T).T
    is a factor of 2 slower than just iterating.
    It seems to be mostly due to np.exp for some reason.
    """

    data = {}
    ev = mother.ev

    phm0 = mother.read_switch('b2sqel_phm0', default=1.0)
    phm1 = mother.read_switch('b2sqel_phm1', default=1.0)
    phm2 = mother.read_switch('b2sqel_phm2', default=1.0)
    art_rad = mother.read_switch('b2sqel_artificial_radiation', default=0.0)

    logte = np.log(mother.te)

    for rname, factor in zip(
        ['r{}sa', 'r{}ra', 'r{}qa', 'r{}rd', 'r{}br'],
        [phm0, phm1*ev, phm2*ev, phm2*ev, phm2*ev]):

        rnew = rname.format('')
        rold = get(mother, rname.format('l'))

        if mother.grid_type=='structured':
            tmp = np.zeros((mother.nx, mother.ny, mother.ns))
        elif mother.grid_type=='unstructured':
            tmp = np.zeros((mother.ncv, mother.ns))
        for si in range(mother.ns):
            tmp[...,si] = factor*np.exp(
                    rold[...,0,si] + rold[...,1,si]*logte)
        data[rnew] = tmp


    
    for rname in ['r{}za', 'r{}z2']:
        rnew = rname.format('')
        rold = get(mother, rname.format('l'))

        tmp = np.zeros((mother.nx, mother.ny, mother.ns))
        for si in range(mother.ns):
            tmp[...,si] = np.clip(
                rold[...,0,si] + rold[...,1,si]*logte,
                mother.zamin[si], mother.zamax[si])
        data[rnew] = tmp

    for rname in ['r{}pt', 'r{}pi']:
        rnew = rname.format('')
        rold = get(mother, rname.format('l'))

        tmp = np.zeros((mother.nx, mother.ny, mother.ns))
        for si in range(mother.ns):
            tmp[...,si] = rold[...,0,si] + rold[...,1,si]*logte
        data[rnew] = tmp


    return data
        


    #from IPython import embed; embed()






                #rza[xi,yi,si] = np.maximum(
                #        zamin[xi,yi,si], np.minimum(zamax[xi,yi,si],
                #        rlza[xi,yi,0,si] + rlza[xi,yi,1,si]*wrk0[xi,yi]))
                #rz2[xi,yi,si] = np.maximum(
                #        np.power(zamin[xi,yi,si],2), np.minimum(
                #        np.power(zamax[xi,yi,si],2), rlz2[xi,yi,0,si] +
                #        rlz2[xi,yi,1,si]*wrk0[xi,yi]))
                #rpt[xi,yi,si] = rlpt[xi,yi,0,si] + rlpt[xi,yi,1,si]*wrk0[xi,yi]
                #rpi[xi,yi,si] = rlpi[xi,yi,0,si] + rlpi[xi,yi,1,si]*wrk0[xi,yi]













def b2sqel(mother):
    """
        B2SQEL computes on all cells of the mesh the rate coefficients
    for ionisation, recombination, electron heat loss and radiation.
    Compared to Jeremy's matlab tool.
    """


    # Unpacking inputs for less verbose code
    ny = mother.ny
    nx = mother.nx
    ns = mother.ns
    ismain = 1 #WRONG! It should be the main ion species,not hardwired.

    te = mother.te.copy()*ev

    rlsa = mother.rlsa.copy()
    rlra = mother.rlra.copy()
    rlqa = mother.rlqa.copy()
    rlrd = mother.rlrd.copy()
    rlbr = mother.rlbr.copy()
    rlza = mother.rlza.copy()
    rlz2 = mother.rlz2.copy()
    rlpt = mother.rlpt.copy()
    rlpi = mother.rlpi.copy()


    #import ipdb; ipdb.set_trace()
    zamin = mother.zamin.copy()
    zamin = np.repeat(zamin[np.newaxis], ny, axis=0)
    zamin = np.repeat(zamin[np.newaxis], nx, axis=0)
    zamax = mother.zamax.copy()
    zamax = np.repeat(zamax[np.newaxis], ny, axis=0)
    zamax = np.repeat(zamax[np.newaxis], nx, axis=0)

    # Defining arrays of outputs
    rsa = np.zeros((nx,ny,ns))
    rra = np.zeros((nx,ny,ns))
    rqa = np.zeros((nx,ny,ns))
    rrd = np.zeros((nx,ny,ns))
    rbr = np.zeros((nx,ny,ns))
    rza = np.zeros((nx,ny,ns))
    rz2 = np.zeros((nx,ny,ns))
    rpt = np.zeros((nx,ny,ns))
    rpi = np.zeros((nx,ny,ns))


    ## Get values for switches
    phm0 = mother.read_switch('b2sqel_phm0', default=1.0)
    phm1 = mother.read_switch('b2sqel_phm1', default=1.0)
    phm2 = mother.read_switch('b2sqel_phm2', default=1.0)
    art_rad = mother.read_switch('b2sqel_artificial_radiation', default=0.0)



    #Compute coefficients
    logte = np.log(mother.te/mother.ev)






    for si in range(ns):
        for yi in range(ny):
            for xi in range(nx):
                rsa[xi,yi,si] = phm0*expu(
                        rlsa[xi,yi,0,si] +  rlsa[xi,yi,1,si]*wrk0[xi,yi])

    #Compute artificial radiation
    if art_rad != 0.0:
        art_rad = np.abs(art_rad)
        mother.log.info("Artificial radiation model active" +
                "art_rad = {}".format(art_rad))
        tnorm = np.maximum(np.full_like(te,fill_value=0.000001), te/(15.0*ev))
        ftnorm = np.power(tnorm,1.5) + 1.0/np.power(tnorm,3)
        rqa[...,ismain] = rqa[...,ismain] + art_rad*2e-31/ftnorm
        rrd[...,ismain] = rrd[...,ismain] + art_rad*2e-31/ftnorm


    return {
        'rsa': rsa,
        'rra': rra,
        'rqa': rqa,
        'rrd': rrd,
        'rbr': rbr,
        'rza': rza,
        'rz2': rz2,
        'rpt': rpt,
        'rpi': rpi,
    }







if __name__ == '__main__':
    create_log('ATOMIC', level='info')

    ## This works
    hbalmer = parse_adasf15( 
    '/mnt/SOLPS/solps-iter-3.0.8/modules/adas/adf15/pec12#h/pec12#h_balmer#h0.dat')
    ## This works
    harg = parse_adasf15( 
    '/mnt/SOLPS/solps-iter-3.0.8/modules/adas/adf15/pec40#ar/pec40#ar_ca#ar1.dat')

    path ='/mnt/SOLPS/solps-iter-3.0.8/modules/adas/adf15'
    data = ADAS(path)
    hdata = data.read_element('H')
    #ardata = data.read_element('Ar')


    from IPython import embed; embed()
