import os
import re
import sys
import gzip
import itertools
import subprocess
import numpy as np
from scipy.interpolate import RegularGridInterpolator as RGI
from scipy.interpolate import interp2d
from functools import wraps
import collections
from collections import OrderedDict, deque, defaultdict
from collections.abc import Callable
import logging
import copy
import yaml




from .utils import *
from .decorators import *

slog = logging.getLogger('quixote')





def read_config(include_local=True):
    """ Reads default and local configuration files and merge them together
    with appropiate priority order.
    Improved so now it modifies a deep dict just fine.
    include_local = False to be used on test cases.
    """

    config = {}
    local  = {}

    # Read 'default.yaml' file:
    yaml_file = os.path.join(module_path(),'config/default.yaml')
    try:
        config = yaml_loader(yaml_file)
    except IOError:
        slog.exception(
        "Error opening 'default.yaml' file '{0}'".format(yaml_file))
    except:
        slog.exception(
        "Yaml file contains unparseable statement '{0}'".format(yaml_file))

    # Read 'local.yaml' file:
    if include_local:
        yaml_file = os.path.join(module_path(),'config/local.yaml')
        try:
            local = yaml_loader(yaml_file)
        except IOError as e:
            slog.info("No local Yaml file '{0}' is provided".format(yaml_file))
        except Exception as e:
            slog.exception(
            "Yaml file contains unparseable statement '{0}'".format(yaml_file))

    return update(config, local)



def module_path():
    dirpath = os.path.abspath(sys.modules['quixote'].__file__)
    return dirpath.rstrip(os.path.basename(dirpath))










## ATTENTION: Only really relevant for Grid Plots.
## Move to tools/plots.py??
def vessel_square_unstructured(mother):
    east = np.max(mother.grid[0][:,0])
    west = np.min(mother.grid[0][:,0])
    north = np.max(mother.grid[0][:,1])
    south = np.min(mother.grid[0][:,1])
    for ici in range(1, mother.nci):
        tmp = np.max(mother.grid[ici][:,0])
        if tmp > east:
            east = tmp
        tmp = np.min(mother.grid[ici][:,0])
        if tmp < west:
            west = tmp
        tmp = np.max(mother.grid[ici][:,1])
        if tmp > north:
            north = tmp
        tmp = np.min(mother.grid[ici][:,1])
        if tmp < south:
            south = tmp
    vessel = np.zeros((4,4))
    vessel[:,0] = west,north,east,north
    vessel[:,1] = east,north,east,south
    vessel[:,2] = east,south,west,south
    vessel[:,3] = west,south,west,north
    return vessel

def vessel_square_structured(mother):
    east = np.max(mother.grid[0,0][:,0])
    west = np.min(mother.grid[0,0][:,0])
    north = np.max(mother.grid[0,0][:,1])
    south = np.min(mother.grid[0,0][:,1])
    for ix in range(1, mother.nx):
        for iy in range(1, mother.ny):
            tmp = np.max(mother.grid[ix,iy][:,0])
            if tmp > east:
                east = tmp
            tmp = np.min(mother.grid[ix,iy][:,0])
            if tmp < west:
                west = tmp
            tmp = np.max(mother.grid[ix,iy][:,1])
            if tmp > north:
                north = tmp
            tmp = np.min(mother.grid[ix,iy][:,1])
            if tmp < south:
                south = tmp
    vessel = np.zeros((4,4))
    vessel[:,0] = west,north,east,north
    vessel[:,1] = east,north,east,south
    vessel[:,2] = east,south,west,south
    vessel[:,3] = west,south,west,north
    return vessel





## ATTENTION: To be moved to solps.py?
def intface(mother, centre):
    """ Interpolates centered quantity centre to all the faces.
    See utility/intface.F
    """
    tmp = np.full(mother.nfc, np.nan)
    fcvol = mother.fcvol
    fccv = mother.fccv
    for ifc in range(nfc):
        dv1 = fcvol[ifc,0]*centre[fccv[ifc,1]]
        dv2 = fcvol[ifc,1]*centre[fccv[ifc,0]]
        dvt = fcvol[ifc,0] + fcvol[ifc,1]
        tmp[ifc] = (dv1 + dv2) / dvt
    return tmp





## ATTENTION: This doesn-t seem core functionality.
## Move to other file???

##ATTENTION: ONLY works for rectangular grids.

## ATTENTION: Names are wrong, and it seems too specific to be here.
## Maybe in a new filed called tools_class.py or something like that.
## Also wrong for unstructured simulations.
def generate_mask(mother, **kwargs):
    """ Generates mask with given xrange and yrange, and extra dimensions.
    To generate more complex masks, use np.logical_or or np.logical_and
    on two basic masks.

    Parameters
    ----------
    xrange, yrange: int or list(int)
        Beginning and end of the range of the masked
        poloidal, radial indices.
        It follows the range() conventions on indices.
        Default to [0,nx], [0,ny] if ix=[], iy=[]

    ix, iy: int or list(int)
        Selected poloidal/radial indices to be
        included in the mask.
        xrange= [0,3] == ix=[0,1,2]
        Default to [].

    extra_dims : int or iterable(int)
        Expands mask from (nx,ny) to (nx,ny,extra_dims).
        Default to None.

    Returns
    -------
    mask: numpy.array with dtype=numpy.bool
        Array with True in the cells that dwell inside the desired
        subdivision of the grid.
        It has dimensions (nx,ny,extra_dims)
    """
    
    nx = mother.nx
    ny = mother.ny

    ix = priority('ix', kwargs, [])
    iy = priority('iy', kwargs, [])
    xrange = priority('xrange', kwargs, [])
    yrange = priority('yrange', kwargs, [])
    extra_dims = priority(['extra_dims', 'extra_dim', 'dims', 'dim'],
        kwargs, None)

    if not isinstance(ix, collections.abc.Iterable): ix = [ix]
    if not isinstance(ix, collections.abc.Iterable): iy = [iy]
    try:
        xrange = set(range(xrange[0], xrange[1]))
    except:
        pass
    try:
        yrange = set(range(yrange[0], yrange[1]))
    except:
        pass

    try:
        xrange = set(itertools.chain(xrange,ix))
    except:
        self.log.exception("xrange and ix could not be combined")
        return
        
    try:
        yrange = set(itertools.chain(yrange,iy))
    except:
        self.log.exception("yrange and iy could not be combined")
        return

    if not xrange: xrange = set(range(0, nx))
    if not yrange: yrange = set(range(0, ny))


    mask1 = np.zeros((nx,ny), dtype=bool)
    mask2 = np.zeros((nx,ny), dtype=bool)
    for i in range(nx):
        for k in range(ny):
            if (i in xrange):
                mask1[i,k] = True
            if (k in yrange):
                mask2[i,k] = True

    mask = np.logical_and(mask1, mask2)

    if extra_dims:
        if '__iter__' not in extra_dims.__class__.__dict__:
            extra_dims = [extra_dims]

        for dm in extra_dims:
            mask = np.repeat(mask.T[np.newaxis], dm, axis=0).T

    return mask
