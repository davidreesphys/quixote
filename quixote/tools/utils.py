"""
    Basic tools for general python programming, including dictionaries,
    list, strings, etc handling and manipulation.
"""
import os
import re
import sys
import gzip
import inspect
import itertools
import subprocess
import numpy as np
from functools import wraps
import collections
from collections import OrderedDict, deque, defaultdict
from collections.abc import Callable
from scipy.interpolate import RegularGridInterpolator as RGI
from scipy.interpolate import interp2d
import logging
import copy
import yaml
import json

slog = logging.getLogger('quixote')


class SimpleNamespace(object):
    """
    Rough equivalent to types.SimpleNamespace class for Python 3.3+.
    One could also use the empty class:
                 def SimpleNamespace:
                     pass
    but this one provides some perks, and looks cooler.
    __repr__ provides what is see when it the object is called without purpose
    and in the "official" case, it shows all the keys and their types
        def __repr__(self):
            keys = sorted(self.__dict__)
            items = ("{}={!r}".format(k, self.__dict__[k]) for k in keys)
            return "{}({})".format(type(self).__name__, ", ".join(items))
    However, I found it a bit annoying, so mine just gives back the keys.
    When moving the code to Python 3, this will not be necessary anymore.
    """
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __repr__(self):
        keys = sorted(self.__dict__)
        items = ("{}".format(k) for k in keys)
        return "SimpleNamespace({})".format(", ".join(items))

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

class snv(SimpleNamespace):
    """
    Equal to SimpleNamespace but in verbose mode
    """
    def __repr__(self):
        keys = sorted(self.__dict__)
        items = ("{} = {!r}".format(k, self.__dict__[k]) for k in keys)
        return "snv({})".format("   ".join(items))


class DefaultOrderedDict(OrderedDict):
    # Source: http://stackoverflow.com/a/6190500/562769
    def __init__(self, default_factory=None, *a, **kw):
        if (default_factory is not None and
               not isinstance(default_factory, Callable)):
            raise TypeError('first argument must be callable')
        OrderedDict.__init__(self, *a, **kw)
        self.default_factory = default_factory
        
        def __getitem__(self, key):
            try:
                return OrderedDict.__getitem__(self, key)
            except KeyError:
                return self.__missing__(key)
                
        def __missing__(self, key):
            if self.default_factory is None:
                raise KeyError(key)
            self[key] = value = self.default_factory()
            return value
            
        def __reduce__(self):
            if self.default_factory is None:
                args = tuple()
            else:
                args = self.default_factory,
            return type(self), args, None, None, self.items()
            
        def copy(self):
            return self.__copy__()
            
        def __copy__(self):
            return type(self)(self.default_factory, self)
            
        def __deepcopy__(self, memo):
            import copy
            return type(self)(self.default_factory,
                              copy.deepcopy(self.items()))
            
        def __repr__(self):
            return 'OrderedDefaultDict(%s, %s)' % (self.default_factory,
                    OrderedDict.__repr__(self))





def priority(names, *args):
    """
    Inputs:
    ------
        Names:  List of possible names of the property. List order implies
                priority order. I.e. if two or more names are present, the
                value of the first name will be accepted.

        Args:   List of all the possible dicts in which the property can
                be found.
                The last provided argument is the default.

    Outputs:
    -------
        Return: Value of the property, named with priority according to names order,
                of the highest priority source.
    """
    #If names is just a string, list create a list of characters
    #if isinstance(names, basestring):
    if isinstance(names, str):
        names = [names]
    else:
        names = list(names)
    for a in args[:-1]:
        for b in names:
            if b in a: #Substitute by try so that things like functions can raise and thus be skipped???
                return a[b]
    return args[-1]




def extend(*args):
    """ Extend multiple arguments into single list and flatten internal lists.
    It respects dicts for now, adding them to the list as dicts, not just keys.
    It translates 1D numpy arrays to list.
    """
    def listify(tmp):
        if isinstance(tmp, np.ndarray):
            if tmp.ndim==1:
                return list(tmp)
            else:
                return [tmp]
        if tmp != 0 and not tmp:
            #return tmp
            return []
        if not isinstance(tmp, list) and not isinstance(tmp, tuple):
            return [tmp]
        return list(itertools.chain.from_iterable([tmp]))
    lst = []
    for arg in args:
        arg = listify(arg)
        lst.extend(arg)
    return lst





## ATTENTION: create an equivalent set function??
## Or can you set(data, 'out.te', te)
def get(self, varname):
    """ Get shallow and deep variables from self.
    E.g: get(data, 'te') and  get(data, 'eqout.sna_ion')
    """
    elements = varname.split('.')
    tmp = self
    for element in elements:
        tmp = getattr(tmp, element)
    return tmp



def update(d, u=None):
    """ Returns updated version of D overwritten by U, without modification
    of D and U.
    """
    if u is None:
        return d

    tmp = copy.deepcopy(d)
    try:
        mapping = collections.abc.Mapping
    except:
        mapping = collections.Mapping
    for k, v in list(u.items()):
        ##if isinstance(v, collections.Mapping):
        #if isinstance(v, collections.abc.Mapping):
        if isinstance(v, mapping):
            tmp[k] = update(tmp.get(k, {}), v)
        else:
            tmp[k] = v
    return tmp

def purge(d, values=None):
    """ Purge dictionary d from values v.
    Allow value=list and remove for all values.
    Maybe allow rtol and atol for isclose removal.
    """
    if not values and values !=0:
        values = [values]
    else:
        values = extend(values)
    tmp = copy.deepcopy(d)
    for k, v in list(tmp.items()):
        if v in values:
            del tmp[k]
    return tmp


def intersection(dict1, dict2=None, rtol=1.e-5, atol=1.e-8):
    """
    It returns a dict with keys and values exactly present in both
    dicts.
    If values differ, they are not included.
    Self intersection is full.
    Kinda similar to np.where(np.isclose(dict1,dict2))
    """
    if dict2 is None:
        return dict1
    if dict1 == {} or dict2 == {}:
        return {}

    mdic = {} 
    for k1,v1 in list(dict1.items()):
        try:
            if ((isinstance(v1,float) or isinstance(v1,int))
                and np.isclose(v1,dict2[k1],rtol=rtol,atol=atol)):
                mdic[k1] = v1
            elif (isinstance(v1,np.ndarray)
                and np.allclose(v1,dict2[k1],rtol=rtol,atol=atol)):
                mdic[k1] = v1
            elif isinstance(v1,str) and (v1 == dict2[k1]):
                mdic[k1] = v1
        except:
            pass
    return mdic



###ATTENTION: Change name to missing and accept also lists, e.g., keys()?
#def exclusion(dict1, dict2=None, rtol=1.e-5, atol=1.e-8):
#    """
#    Return the dict of keys and values that present in dict2 but not in dict1.
#    So the values "missing" from dict1.
#    rtol and atol are used to define "close enough" for numerical comparison.
#
#    At the moment is more like "remove using dict"
#    """
#    if dict2 is None:
#        return dict1
#    if dict1 == {} or dict2 == {}:
#        return {}
#
#    mdic = {} 
#    for k1,v1 in list(dict1.items()):
#        try:
#            if ((isinstance(v1,float) or isinstance(v1,int))
#                and np.isclose(v1,dict2[k1],rtol=rtol,atol=atol)):
#                continue
#            elif isinstance(v1,str) and (v1 == dict2[k1]):
#                continue
#        except:
#            pass
#        mdic[k1] = v1
#    return mdic



def missing(obj1, obj2=None, rtol=1.e-5, atol=1.e-8):
    """ Returns elements missing from obj1 but present in obj2.
    For now for simplicity use only lists, but in the future
    list and dict should match keys, dict and dict should match
    keys and values, etc.
    """
    if obj2 is None:
        return []

    mlist = []
    for ele in obj2:
        if ele not in obj1:
            mlist.append(ele)
    return mlist
    


## ATTENTION: Use np.testing.assert_equal and assert_array_equal
## to assert the validity when nan are present!!
def equal(obj1, obj2, rtol=1.e-5, atol=1.e-8, strict= False):
    """
    equal tests equality not only the shallow equality,
    but deeply compares all the elements with possibly 
    rtol and atol tolerances.

    Notes
    -----
      - Dicts do not need to be ordered to be equal.

      - OrderedDicts are enforced to be in order.

    """
    if strict and type(obj1) != type(obj2):
        return False

    def validity(obj1, obj2, rtol, atol):
        #try:
        #    obj1 = np.asarray(obj1)
        #    obj2 = np.asarray(obj1)
        #    m = ~(np.isnan(obj1) & np.nan(obj2))
        #    #m = np.isfinite(obj1) & np.isfinite(obj2)
        #    return np.allclose(obj1[m], obj2[m], rtol=rtol, atol=atol)
        #except:
        #    pass
        try:
            return np.allclose(
                obj1, obj2, rtol=rtol, atol=atol, equal_nan=True)
        except:
            pass

        try:
            return (obj1 == obj2).all() ## Check for nans?
        except:
            pass
        try:
            return bool(obj1 == obj2)
        except:
            return False

    if isinstance(obj1, dict) and isinstance(obj2, dict):
        try:
            for k in obj1.keys(): 
                valid = equal(obj1[k], obj2[k], rtol, atol, strict)
                if not valid:
                        return False
            return True

        except:
            return False
    elif isinstance(obj1, OrderedDict) and isinstance(obj2, OrderedDict):
        try:
            for d1,d1 in zip(obj1.items(), obj2.items()):
                valid = (d1[0] == d2[0])
                valid = np.logical_and(valid,
                    equal(d1[1], d2[2], rtol, atol, strict))
                if not valid:
                        return False
            return True

        except:
            return False

    elif ((isinstance(obj1, list) and isinstance(obj2, list)) or
        (isinstance(obj1, tuple) and isinstance(obj2, tuple))):# or
        #((isinstance(obj1, np.ndarray) and ob1.dtype=='O') and
        # (isinstance(obj2, np.ndarray) and ob1.dtype=='O'))):
        try:
            for v1, v2 in zip(obj1, obj2):
                valid = equal(v1, v2, rtol, atol, strict)
                if not valid:
                        return False
            return True
        except:
            return False
    else:
        return validity(obj1, obj2, rtol, atol)



def unique(sequence):
    """ Returns a list of unique elements of sequence, in order,
    unlike set (and apparently np.unique) by itself.
    """
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]



def repeat(array, extra_dims):
    """ Numpy repeat, but given possibly multiple extra dimensions.
    """
    tmp = array.copy()
    for dim in extend(extra_dims):
        tmp = np.repeat(tmp[...,np.newaxis], dim, axis=-1)
    return tmp




def null(item):
    """ Returns the null version of the given item, so that it can
    be summed without issues or checks.
    """
    if isinstance(item, str):
        return ''
    elif isinstance(item,float):
        return 0.0
    elif isinstance(item,int):
        return 0
    elif isinstance(item,list):
        return []
    elif isinstance(item,dict):
        return {}
    elif isinstance(item,tuple):
        return ()
    elif isinstance(item,np.ndarray):
        return np.zeros_like(item)







### ATTENTION: Use np.testing.assert_equal and assert_array_equal
### to assert the validity when nan are present!!
#def equal(obj1, obj2, rtol=1.e-5, atol=1.e-8, strict= False):
#    """ equal tests equality not only the shallow equality,
#    but deeply compares all the elements with possibly 
#    rtol and atol tolerances.
#    """
#    if strict and type(obj1) != type(obj2):
#        return False
#
#    def validity(obj1, obj2, rtol, atol):
#        try:
#            m = np.isfinite(obj1) & np.isfinite(obj2)
#            return np.allclose(obj1[m], obj2[m], rtol=rtol, atol=atol)
#        except:
#            pass
#
#        try:
#            return (obj1 == obj2).all() ## Check for nans?
#        except:
#            pass
#        try:
#            return bool(obj1 == obj2)
#        except:
#            return False
#
#    if isinstance(obj1, dict) and isinstance(obj2, dict):
#        try:
#            for k in obj1.keys():
#                valid = equal(obj1[k], obj2[k], rtol, atol, strict)
#                if not valid:
#                        return False
#            return True
#
#        except:
#            return False
#
#    elif (isinstance(obj1, list) and isinstance(obj2, list) or
#        isinstance(obj1, tuple) and isinstance(obj2, tuple)):
#        try:
#            for v1, v2 in zip(obj1, obj2):
#                valid = equal(v1, v2, rtol, atol, strict)
#                if not valid:
#                        return False
#            return True
#        except:
#            return False
#    else:
#        return validity(obj1, obj2, rtol, atol)

















#    #If objects are iterables, one need to check the iteartions
#    if isinstance(obj1, dict) and isinstance(obj2, dict):
#        try:
#            for k, v in obj1.keys():
#                v1 = obj1[k]
#                v2 = obj2[k]
#                if (isinstance(v1, list) and isinstance(v2, list) or
#                    isinstance(v1, tuple) and isinstance(v2, tuple) or
#                    isinstance(v1, dict) and isinstance(v2, dict)):
#                    valid = equal(v1, v2, rtol, atol)
#                else: 
#                    valid = validity(v1, v2, rtol, atol)
#                if not valid:
#                        return False
#            return True
#
#        except:
#            return False
#
#    if (isinstance(obj1, list) and isinstance(obj2, list) or
#        isinstance(obj1, tuple) and isinstance(obj2, tuple)):
#        try:
#            for v1, v2 in zip(obj1, obj2):
#                if (isinstance(v1, list) and isinstance(v2, list) or
#                    isinstance(v1, tuple) and isinstance(v2, tuple) or
#                    isinstance(v1, dict) and isinstance(v2, dict)):
#                    valid = equal(v1, v2, rtol, atol)
#                else: 
#                    valid = validity(v1, v2, rtol, atol)
#                if not valid:
#                        return False
#            return True
#        except:
#            return False
#
#    return validity(obj1, obj2, rtol, atol)





def nested_depth(d):
    """ Returns the deepest point of the given dict.
    Maybe allow lists and tuples too."""
    if not isinstance(d, dict):
        return 0
    if not d:
        return 1
    return 1 + max(nested_depth(v) for v in list(d.values()))






def create_log(name, adapter=None, **kwargs):
    form = priority(['formatter', 'form'], kwargs, None)
    if form is None:
        if adapter and 'ident' in adapter:
            form = '%(name)s(%(ident)s) -- %(levelname)s: %(message)s'
        else:
            form = '%(name)s -- %(levelname)s: %(message)s'
    log = logging.getLogger(name)
    if not log.handlers: #Prevents from adding repeated streamhandlers
        formatter = logging.Formatter(form)
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        log.addHandler(ch)
        log.propagate = False #Otherwise, root logger also prints
    if adapter:
        log = logging.LoggerAdapter(log, adapter)
    if 'level' in kwargs:
        log.setLevel(getattr(logging, kwargs['level'].upper()))
    return log


def modify_log(name='root', level='warning', logfile=None, silent=False):
    """ Set the alert level of a logger or file to write.
    If 'root', it will affect all loggers.
    If named, it will affect only the corresponding logger.
    """
    logger = logging.getLogger(name)
    logger.setLevel(getattr(logging, level.upper()))

    #It does not print to file yet, but the other thing works.
    if logfile:
        fh = logging.FileHandler(logfile)
        fh.setLevel(getattr(logging, level.upper()))
        logger.addHandler(fh)
        if silent:
            #Remove stream handler and only use file handler.
            logger.removeHandler(ch)
    return



def whichclass(obj, attr, name='base'):
    """ Return the name of class which last defines an attribute.
    """
    if inspect.isclass(obj):
        mro = list(obj.__mro__)
    else:
        mro = list(obj.__class__.__mro__)
    for tmp in mro:
        if attr in tmp.__dict__:
            if name == 'base':
                return tmp.__name__
            else: 
                return tmp
    if hasattr(obj, attr):
        return 'Instance Attr'
    else:
        return 'Unknown Attr'




def latex_unit(unformatted, braces=True):
    """
    Based on convert_to_latex_unit
     Create latex output for units in plots.
     It simplifies the units when possible, e.g., m^3*m^-2 = m
    
     Example:
      input: '10^19 m^3 eV s^-1 m^-2 * photons*MW eV^-1'
      returns:
      -> '[$10^{19}\mathrm{m}\mathrm{s}^{-1}\mathrm{photons}\mathrm{MW}$]'
    """

    #if unformatted  == '' or input == '[]': return ''
    if  unformatted == '' or unformatted == '[]':
        return ''
    elif not unformatted:
        return ''

    inputs = unformatted.replace('*',' ').replace('[','').replace(']','').split()
    outputs = []
    for i in inputs:
        outputs.append(i.split('^'))
        if '^' not in i:
            outputs[-1].append(1)

    output = OrderedDict()
    for o in outputs:
        output[o[0]] = output.get(o[0],0) + int(o[1])

    unit = r''
    for o in output:
        if output[o] == 0: continue
        try:
            int(o)
            unit += o
        except:
            unit += r'\mathrm{' + o + r'}'
        if output[o] != 1:
            unit += r'^{' + str(output[o]) + r'}'
        if o != list(output.keys())[-1]:
            unit += r'\,'
    unit  = unit.replace('\mathrm{%}','\%')

    if braces:
        form = ['[',']', unit]
    else:
        form = ['','', unit]

    return '{0}${2}${1}'.format(*form)








def yaml_loader(name, safe=False):
    """
    Loads yaml file with 'name' safely and takes into account correctly
    the scientific notation (no point before e/E and/or sign afterwards needed)
    Taken from Stackoverflow.

    Parameters
    ----------
    name: str
        Path and name of the YAML file.

    safe: bool.
        Safe=False allows for the evaluation of limited lambda functions.
        They must start by either 'lambda x: x.' or 'lambda x: np.(' and
        they must not contain ';'.
        This is still unsafe, since the string is passed to eval.
        Use this only on trusted YAML DB.
        Safe=True replaces the lambda function by None.
        Default: False
    """
    
    loader = yaml.SafeLoader
    # I think that this replaces the tag of float in loader
    # to allow scientific notation.
    loader.add_implicit_resolver(
        'tag:yaml.org,2002:float',
        re.compile('''^(?:
         [-+]?(?:[0-9][0-9_]*)\\.[0-9_]*(?:[eE][-+]?[0-9]+)?
        |[-+]?(?:[0-9][0-9_]*)(?:[eE][-+]?[0-9]+)
        |\\.[0-9_]+(?:[eE][-+][0-9]+)?
        |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\\.[0-9_]*
        |[-+]?\\.(?:inf|Inf|INF)
        |\\.(?:nan|NaN|NAN))$''', re.X),
        list('-+0123456789.'))

    def lambda_unsafe_constructor(loader, node):
        value = loader.construct_scalar(node)
        if value.find(';')==-1 and (value.startswith("lambda x: x.") or
                                    value.startswith("lambda x: np.")):
            return eval(value)
        else:
            return None
    def lambda_safe_constructor(loader, node):
        return None

    def module_path_constructor(loader, node):
        value = loader.construct_scalar(node)
        if value:
            return os.path.join(module_path(), value)
        else:
            return module_path()


    if not safe:
        loader.add_constructor(u'!Lambda', lambda_unsafe_constructor)
    else:
        loader.add_constructor(u'!Lambda', lambda_safe_constructor)

    loader.add_constructor(u'!module_path', module_path_constructor)

    with open(name, 'r') as fr:
        data = yaml.load(fr, Loader=loader)
    if data is None:
        data = {}
    return data


def format_e(n, lower_case=True):
    if lower_case:
        a = '%e' % n
        return a.split('e')[0].rstrip('0').rstrip('.') + 'e' + a.split('e')[1]
    else:
        a = '%E' % n
        return a.split('E')[0].rstrip('0').rstrip('.') + 'E' + a.split('E')[1]







class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

def json_numpy(data):
    return json.dumps(data, cls=NumpyEncoder)





def mpsi(*data):
    """
    Returns a bicubic spline interpolation, so that mpsi(R,z) is correct.

    Notes
    -----
    The reason behind the transpose of mpsi is because everything is a mess.
    In this way, we can evaluate mpsi as expected mpsi(R,z),
    otherwise, it is wrong.
    """
    if isinstance(data[0], dict):
        try:
            r = data[0]['r']
            z = data[0]['z']
            psi = data[0]['mpsi']
        except:
            slog.exception("Dictionary needs: 'r', 'z', and 'mpsi'")
            raise
    elif len(data) == 3:
        r = data[0]
        z = data[1]
        psi = data[2]
    else:
        slog.exception("Input required: r, z, and mpsi.")
        raise


    try:
        return RGI((r, z), psi.T, method = 'cubic')
    except:
        slog.warning("RGI of older scipy does not accept cubic splines.")
        slog.warning("Using deprecated interp2d instead.")
        return interp2d(r, z, psi, kind='cubic') ##Also .T??










### ================= PROBLEMATIC???? =========================================


#ATTENTION: Not clear what is the purpose
##ATTENTION : modify naming to remove defaults.
def sort_lists(lists,reverse=False):
    """Takes a list of lists as argument, and sorts all lists with respect
    to the values in the first list."""

    size = len(lists[0])
    for l in lists:
        if size != len(l):
            #print("Error in sort_lists(): inconsistent size of lists.")
            slog.error("In sort_lists(): inconsistent size of lists.")
            return

    dict = {}
    for i in range(size):
        key = lists[0][i]
        if key not in dict:
            dict[key] = [[]]
        else:
            dict[key].append([])
        for l in lists:
            dict[key][-1].append(l[i])

    retval = ()
    for l in range(len(lists)):
        retval += ([],)
        for key in sorted(dict,reverse=reverse):
            for sublist in dict[key]:
                retval[-1].append(sublist[l])

    return retval




##ATTENTION : modify naming to remove defaults.
def open_file(file,dir='.'):
    """Opens .png, .pdf, .(e)ps or text files in the run directory."""
    file = os.path.join(dir,file)
    if not os.path.isfile(file):
        #print('File does not exist:',file)
        slog.error("File does not exist: {}".format(file))
        return

    if file.endswith('.pdf'):
       #prog = 'acroread'
        prog = 'gv'
    elif file.endswith('.ps') or file.endswith('.eps'):
        prog = 'gv'
    elif file.endswith('.png'):
        prog = 'display'
    else:
        prog = 'emacs'

    FNULL = open(os.devnull, 'w')
    subprocess.Popen([prog,file],stdout=FNULL,stderr=subprocess.STDOUT)
    FNULL.close()




## re.findall(regex_numbers, string) = list of all the numbers in many different forms.
regex_numbers = r"[+-]? *(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][+-]?\d+)?"



#@profile
def grep(regex=None, file=None, flags=None):
    """
        Search for regex in file and return a list tuples with first the first
        index indicating the line incident and the second index providing a list
        of lines that the context flags required

        TOO SLOW for parsing of run.log.gz
    """

    if file.endswith('.gz'):
        rlog = gzip.open(file,'r')
    else:
        rlog = open(file,'r')

    if flags is None:
        flags = {}

    mylen1=1
    mylen2=0
    flaga=False
    try:
        mylen1 = flags['c'] + 1
        mylen2 = flags['c']
    except:
        if 'b' in flags:
            mylen1 = flags['b'] + 1
        if 'a' in flags:
            mylen2 = flags['a']

    blocks = []
    stackb = deque(maxlen = mylen1)
    stacka = deque(maxlen = mylen2)
    stackc = deque(maxlen = mylen1+mylen2)
    #stackc = []

    regex = re.compile(regex)
    #import pdb; pdb.set_trace()
    while True:
        last_pos = rlog.tell()
        line = rlog.readline()

        if line == '':
            break
        stackb.append(line)

        if re.findall(regex,line):
            for k in range(mylen2):
                ln = rlog.readline()
                if ln == '':
                    break
                stacka.append(ln)

            stackc.extend(stackb)
            stackc.extend(stacka)
            blocks.append(stackc.__copy__())

            stackc.clear()
            stacka.clear()
            rlog.seek(last_pos)
            if rlog.readline() == '':
                break

    return blocks



# ----------------------- DEVELOPMENT ----------------------------------


# ----------------------- DEVELOPMENT ----------------------------------
#if __name__ == "__main__":






