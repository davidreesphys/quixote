import os
import numpy as np
import copy
import functools
import logging

#from . import utils
import quixote.tools.utils as utils

#log = utils.create_log('ExperimentalTools')
#utils.modify_log(level='debug')
#utils.modify_log(level='debug', logfile='debug.log')

log = utils.create_log('ExperimentalTools', level='debug')
#log = utils.create_log('ExperimentalTools')


#def parser_f90namelist(filename, namelist):
def parser_f90namelist(namelist):
    """ It works, but it doesn't like things like
    userfluxparm(1,1) = bunch of numbers.
    It wants userfluxparm(:,1) = bunch of numbers"""
    import f90nml
    parser = f90nml.Parser()
    #parser.default_start_index = 1 ## Default, but maybe zero??
    return parser.read(namelist)




def solps_property3(func=None, *, documented=True, autoread=True):
    """
    """
    ## Will this mess up func naming???
    if func is None:
        return functools.partial(solps_property3,
                documented=documented, autoread=autoread)

    @functools.wraps(func)
    def wrapped(self, *args, **kwargs): ##Args and Kwargs not used, should I pass them to func??
        try:
            return getattr(self, '_'+func.__name__)
        except:
            try:
                func(self) #Func should create self._variable. Needs args and kwargs??
                return getattr(self, '_'+func.__name__)
            except:
                pass
            if autoread:
                try:
                    self._read_signal('_'+func.__name__, func.__name__)
                    return getattr(self, '_'+func.__name__)
                except:
                    pass
            self.log.exception("'{}' could not be set".format(func.__name__))
            raise Exception

    if documented:
        return DocumentedProperty(wrapped)
    else:
        return property(wrapped)








def docstringtemplate_test():

### Try again with Documenter Decorator
### In case of property, it should go before, no?
### it just changes the docstring, which property will simply copy afterewards
    def docstringtemplate(f):
        """Treat the docstring as a template, with access to globals and defaults"""
        from inspect import getargspec
        #The problem is that this cannot get MRO and not class variables.
        spec = getargspec(f)
        defaults = {} if not spec.defaults else dict(
            zip(spec.args[-len(spec.defaults):], spec.defaults))
        f.__doc__ = f.__doc__ and f.__doc__.format(
            **dict(f.__globals__, **defaults))
        return f
















## ATTENTION: When property is attached into new class, whichclass will
## stop working. Make some sort of dictionary that stores that information
## before it is destroyed.
## -----------------------------------------------------------------------
def metaclass_property_test():
    #import ipdb; ipdb.set_trace()

    #Maybe make it inherit from property, so that everything is replaced, but
    #it is still listed as property?? Probalby doesn't work because of __slots__
    #or BS like that.
    class DocumentedProperty(object):
        """
        Emulate PyProperty_Type() in Objects/descrobject.c
        by Raymond Hettinger.
        Adapted to accept og_doc.
        """
        import copy
        def __init__(self, fget=None, fset=None, fdel=None, doc=None, og_doc=None):
            self.fget = fget
            self.fset = fset
            self.fdel = fdel
            if doc is None and fget is not None:
                doc = copy.copy(fget.__doc__)
            if og_doc is None and fget is not None:
                og_doc = copy.copy(fget.__doc__)
            self.__doc__ = doc
            self.__og_doc__ = og_doc
    
        def __get__(self, obj, objtype=None):
            if obj is None:
                return self
            if self.fget is None:
                raise AttributeError("unreadable attribute")
            return self.fget(obj)
    
        def __set__(self, obj, value):
            if self.fset is None:
                raise AttributeError("can't set attribute")
            self.fset(obj, value)
    
        def __delete__(self, obj):
            if self.fdel is None:
                raise AttributeError("can't delete attribute")
            self.fdel(obj)
    
        def getter(self, fget):
            return type(self)(fget, self.fset, self.fdel,
                self.__doc__, self.__og_doc__)
    
        def setter(self, fset):
            return type(self)(self.fget, fset, self.fdel,
                self.__doc__, self.__og_doc__)
    
        def deleter(self, fdel):
            return type(self)(self.fget, self.fset, fdel,
                self.__doc__, self.__og_doc__)



    def mark(func=None, *dummy, **deckws):
        if func is None:
            return functools.partial(mark, **deckws)
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            return func(*args, **kwargs)
        wrapped = Property(wrapped)
        #It gets marked by the provided decorator kwargs
        setattr(wrapped, 'deckws', deckws)
        return wrapped


    def solps_property3(func=None, *, documented=True):
        """
        """
        ## Will this mess up func naming???
        #log.debug("Beginning of solps_property3 for '{}'".format(func))
        try:
            log.debug("Beginning of solps_property3 for '{}'".format(func.__name__))
        except:
            pass
        if func is None:
            log.debug("Func was None")
            return functools.partial(solps_property3, documented=documented)
        @functools.wraps(func)
        def wrapped(self, *args, **kwargs): ##Args and Kwargs not used, should I pass them to func??
            try:
                return getattr(self, '_'+func.__name__)
            except:
                try:
                    func(self) #Func should create self._variable. Needs args and kwargs??
                    return getattr(self, '_'+func.__name__)
                except:
                    pass
                try:
                    self._read_signal('_'+func.__name__, func.__name__)
                    return getattr(self, '_'+func.__name__)
                except:
                    pass
                self.log.exception("'{}' could not be set".format(func.__name__))
                raise Exception
        log.debug("Begin check of documented for '{}'".format(func.__name__))
        if documented:
            log.debug("Documented wrapped named '{}'\n".format(wrapped.__name__))
            return DocumentedProperty(wrapped)
        else:
            log.debug("Normal property wrapped named '{}'\n".format(wrapped.__name__))
            return property(wrapped)


    ##ATTENTION: create too a dict in which the original class that gave
    ## the property to be documented is collected for purposes of
    ## tools.whichclass, otherwise they will all be assigned to the
    ## first class, no??
    ## TEST THIS!
    ## Maybe something like for each class, get the name space and save it
    ## as og_namespace, and then whcihclass checks if og_namespace exists
    ## when going down the MRO, instead of creating the name space, and then
    ## checking the variable.
    ## Maybe do this last thing if og_namespace does not exist !!
    class MetaDocumenter(type):
        """ Expands out the entire class_namespace taking into account
        the MRO, and then updates the doctrings of attributes not starting
        with '__' with all the available class attributes.
        """
        #@profile
        def __new__(meta, class_name, class_bases, class_namespace):

            log.debug("META: Begin class creation of '{}'".format(class_name))
            og_cls =  type.__new__(meta, class_name, class_bases, class_namespace)
            mro = list(og_cls.__mro__)
            mro.reverse()
            del(mro[0])
            
            log.debug("Begin creation full_namespace and docparams")
            full_namespace = {}
            docparams = {}
            for tmp in mro:
                if '__DOCPARAMS__' in dir(tmp):
                    docparams.update(getattr(tmp, '__DOCPARAMS__'))
                full_namespace.update(tmp.__dict__)
            log.debug("End creation full_namespace and docparams")
            log.debug("DOCPARAMS: {}".format(docparams))


            log.debug(
            "Begin creation of update of __doc__ and/or creation of __og_doc__")

            for k, v in full_namespace.items():
                if k.startswith('__'):
                    continue
                else:
                    log.debug("  For '{}'".format(k))
                    try:
                        #log.debug("    Initial v.__doc__ = {}".format(v.__doc__))
                        if '__og_doc__' not in dir(v):
                            log.debug("    No __og_doc__")
                            v.__og_doc__ = copy.copy(v.__doc__)
                            log.debug("    __og_doc__ created")
                        else:
                            # New IparProperty needed to avoid cross-class mutation
                            if isinstance(v, DocumentedProperty):
                                log.debug("    '{}' is a DocumentedProperty"
                                    .format(k))
                                if v.__og_doc__ != v.__doc__.format(**docparams):
                                    log.debug("    '{}' is being updated".format(k))
                                    v = DocumentedProperty(
                                        fget=v.fget, fset=v.fset, fdel=v.fdel,
                                        doc=v.__doc__, og_doc=v.__og_doc__)
                                    full_namespace[k] = v
                                    log.debug("    '{}' Added to full_namespace again".format(k))

                        #log.debug("    v.__og_doc__ = {}".format(v.__og_doc__))
                        log.debug("    Updating v.__doc__ ")
                        v.__doc__ = v.__og_doc__.format(**docparams)
                        log.debug("    Updated v.__doc__ ")
                        #log.debug("    Final v.__doc__ = {}".format(v.__doc__))
                    except:
                        log.debug("    Something went wrong, EXCEPTION called !!!")
                        continue
            log.debug("End creation of update of __doc__ and/or creation of __og_doc__")

            log.debug("Set full_namespace['__DOCPARAMS__'] = docparams")
            full_namespace['__DOCPARAMS__'] = docparams
            cls =  type.__new__(meta, class_name, class_bases, full_namespace)
            log.debug("META: Finish class creation of '{}'\n".format(class_name))
            return cls




    class MyClass(metaclass=MetaDocumenter):
        """ I hope this does not translate
        """
        log.debug("Reached 'MyClass'")
        z = 200
        __DOCPARAMS__ = {
            'GRID_DIMS': 'nx, ny',
            'FACE_DIMS': 'nx, ny',
            }

        def __init__(self, name):
            self.name = name

        @DocumentedProperty
        def x(self):
            """ This has dimensions ({GRID_DIMS})"""
            return 100

        @DocumentedProperty
        def y(self):
            """ This has dimensions ({GRID_DIMS})"""
            return 200

        @solps_property3
        def beta(self):
            """({GRID_DIMS}): Test for solps-property."""
            self._beta = 'Beta in MyClass'

        def _read_signal(self, signal_name, var_name):
            setattr(self, signal_name, 'set with _read_signal')

        @classmethod
        def help(cls, var):
            """
            Returns docstring of requested property
            """
            try:
                print(getattr(cls, var).__doc__)
            except:
                print("No docstring yet.")



        @solps_property3
        def gamma(self):
            """({FACE_DIMS}): Test for solps-property, with automatic reading."""

        log.debug("END OF  'MyClass'\n")




    class DerivedClass(MyClass):
        log.debug("Reached 'DerivedClass'")
        __DOCPARAMS__ = {
            'GRID_DIMS': 'ncv',
            }

        @property
        def alpha(self):
            """ This is just a normal property, with no weird stuff in doc"""
            return 'Normal'

        @DocumentedProperty
        def y(self):
            """ This has altered ({GRID_DIMS}) +2"""
            return 300

        def read_signal(self, x):
            """ Returns name with ({FACE_DIMS})."""
            return  self.name

        @solps_property3(documented=False)
        def omega(self):
            self._omega = 'undocumented solpspy property3'


        @solps_property3
        def beta(self):
            """({GRID_DIMS}): Test for solps-property."""
            self._beta = 'Beta in DerivedClass'

        log.debug("END OF  'DerivedClass'\n")



    cla = MyClass('Pepe')
    der = DerivedClass("Jose")
    print(der.gamma)
    from IPython import embed; embed()



















## -----------------------------------------------------------------------







##Now think about __init_subclass__

def study_subclass_init():
    class MyClass:
        """ I hope this does not translate
        """
        z = 200
        __DOCPARAMS__ = {
            'GRID_DIMS': 'nx, ny',
            'FACE_DIMS': 'nx, ny',
            }
        def __init__(self, name):
            self.name = name

        def __init_subclass__(cls, **kwargs): ## datatype and gridtype
            from IPython import embed; embed()

        @property
        def x(self):
            """ This has dimensions ({GRID_DIMS})"""
            return 100

        @property
        def y(self):
            """ This has dimensions ({GRID_DIMS})"""
            return 200

        #@property
        #def beta(self):
        #    """({GRID_DIMS}): Test for solps-property."""
        #    self._beta = 'Beta in MyClass'

        def _read_signal(self, signal_name, var_name):
            setattr(self, signal_name, 'set with _read_signal')

        @classmethod
        def help(cls, var):
            """
            Returns docstring of requested property
            """
            try:
                print(getattr(cls, var).__doc__)
            except:
                print("No docstring yet.")



        #@solps_property3
        #def gamma(self):
        #    """({FACE_DIMS}): Test for solps-property, with automatic reading."""


    class DerivedClass(MyClass, dataype='rundir', gridtype='structured'):
        __DOCPARAMS__ = {
            'GRID_DIMS': 'ncv',
            }

        @property
        def alpha(self):
            """ This is just a normal property, with no weird stuff in doc"""
            return 'Normal'

        @DocumentedProperty
        def y(self):
            """ This has altered ({GRID_DIMS}) +2"""
            return 300

        def read_signal(self, x):
            """ Returns name with ({FACE_DIMS})."""
            return  self.name

        #@solps_property3(documented=False)
        #def omega(self):
        #    self._omega = 'undocumented solpspy property3'


        #@solps_property3
        #def beta(self):
        #    """({GRID_DIMS}): Test for solps-property."""
        #    self._beta = 'Beta in DerivedClass'


    cla = MyClass('Pepe')
    der = DerivedClass("Jose")
    #from IPython import embed; embed()












































if __name__ == '__main__':
    metaclass_property_test()
    #docstringtemplate_test()
