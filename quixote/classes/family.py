

### TODO
# Allow include and exclude logic to use inequalities.
# This allows people to use more complex labels such as 1.356e19.
# Also, create $$ labels, which correspond to parameters that are linked
# AFTER the runs have been loaded $$self.ne[self.omp,self.sep]
# Maybe replace $$self by a lambda function of self.
# Together with the previous improvement one could include first to parse out
# a huge chunk of the database but then, once loaded, include and exclude
# again to really be able to use shot parameters.
# E.g.:
# include = {'dens': 'low'}
# Create family, loads all the cases labelled as low.
# Family.parse({'nesepm':'1e18<var<1e20'})
# Where var is the keyword for the quantity that will be obtained.
# For most databases, loading all the shots is not too much (usually 10-20).

# Also allow for experiments to accept 'all', as in load all the experiments.

# Common modules
import os
import types
import numpy as np
import pandas as pd
from collections import OrderedDict # as od
import itertools
import matplotlib.pyplot as plt

# Special modules

# Quixote modules
from .factory import SolpsData
from ..tools import SimpleNamespace
from ..tools import priority, read_config
from ..tools import parse_dataframe, module_path
from ..tools import yaml_loader, exp_cross_references, yaml_xrefs
from ..tools import equal, extend, update, get, unique
from ..tools import create_log
from ..tools import initialize_canvas


try:
    from tqdm import tqdm
    _forced_no_progress_bar = False
except:
    _forced_no_progress_bar = True



# =============================================================================


##ATTENTION: Allow for passing db and then skipping the whole thing except maybe
## Run creation. If runs are passed in db or in vector, check with the other
## to make sure taht they are in the correct form.
## In this way, Filter can call a Family class with db and work better than
## Namespace
class Family(object):
    """
    Parameters
    ----------
    default_index: bool or str, optional
        If True, then look for default index in root_path_to_index.
        If path, then look for index at that path.
        if False, then no default index is used.
        Default: False.
    
    include: list(dicts), optional
        List of including criteria. Each criterion works as an
        'and' statement, thus only the cases that specifically suffice
        at least one criterion completely are included.
        Default: [].
    
    exclude: list(dicts), optional
        List of excluding criteria. Each criterion works as an
        'and' statement, thus only the cases that specifically suffice
        at least one criterion completely are excluded.
        Default: [].
    
    order: list(str, dict), optional
        From left to right, increasing level of grouping priority.
        Default: ['experiment'].
    
    autobuild: bool, optional
        If True, then database is automatically built during __init__
        phase.  Default: True.
    
    autoload: bool, optional
        If True, SOLPS runs are automatically loaded during __init__
        phase.  Default: True.
    
    progress_bar: bool, optional
        If True, a progress bar will be shown during the (possibly long) loops.
        Default: True.
    

    Notes
    -----

    Rework the idea of default index.

    order should be either a list or an ordered dict.
    Maybe a list of dicts too? ['experiment', {'ip':'descending'}, 'pow']


    26/07/2022: Maybe if DataFrame is provided, skip building DataFrame, etc.
    and if runs are provided, skip autobuild, etc.
    In that way, filter can create a df and just initialize a family with it.
    
    """
    def __init__(self, **kwargs):
        self.log = create_log('Family')

        config = priority(['config','cfg'], kwargs, None)
        if config is None or not isinstance(config, dict):
            config = read_config()
        self.config = config['Family']
        self._full_config = config ##For Uncertainty


        kwargs_runs = priority('kruns', kwargs, {})
        if not kwargs_runs:
            kwargs['kruns'] = {}
        if 'config' not in kwargs_runs:
            kwargs['kruns']['config'] = config

        self.module_path = module_path()
        self.samples = SimpleNamespace()
        self.built =  priority('built', kwargs, False)
        self.loaded = priority('loaded', kwargs, False)
        data = priority('data', kwargs, None)


        default_index = priority('default_index', kwargs,
                self.config['default_index'])
        index = priority('index', kwargs, None)
        db = priority(['database','db','dbfile'], kwargs, None)
        self.include = priority('include', kwargs, self.config['no_include'])
        self.exclude = priority('exclude', kwargs, self.config['no_exclude'])
        self._order   = priority('order',   kwargs, self.config['no_order'])
        self._order = self.parse_order(self._order, default=True)
        autobuild = priority('autobuild', kwargs, self.config['autobuild'])
        autoload  = priority('autoload',  kwargs, self.config['autoload'])
        if _forced_no_progress_bar:
            self.progress_bar = False
        else:
            self.progress_bar = priority(['progress_bar', 'bar'], kwargs,
                    self.config['progress_bar'])



        if data is None:
            ## A list, even if of just one element, is needed down the line
            if self.include == 'all':
                self.include = [{'experiment':'all'}]
            elif isinstance(self.include, dict):
                self.include = [self.include]

            if isinstance(self.exclude, dict):
                self.exclude = [self.exclude]


            ## INDICES and DBs ----------
            self.path_index = []
            if index:
                if isinstance(index, str):
                    index = [index]
                for ind in index: 
                    self.add_index(ind)
            # Maybe create the posibility of having a list as default_index.
            if default_index:
                for tmp in [default_index, os.path.join(module_path(), default_index)]:
                    try:
                        self.add_index(tmp)
                        break
                    except:
                        continue
                else:
                        self.log.error(
                        "Default index '{0}' provided but not found.".format(default_index))

            self.path_db = []
            if db:
                if isinstance(db, str):
                    db = [db]
                for ind in db: 
                    self.add_db(ind)


            ## EXPERIMENTS ----------------
            #Allow to accept a kwargs['database'] (list(db) or db) and deal
            #with it as if it were the main (only?) "index" for avail_experiments.
            # Also allow to pass a .yaml file with the database itself.
            # Order of experiments may vary due to multiple dbs containing the same exp.
            self.avail_experiments = self._avail_experiments()
            


            ## BUILD AND LOAD DBs ----------
            if not self.built and autobuild:
                self.build_db()
                self.experiments = []
                for exp in list(self.db['experiment']):
                    if exp not in self.experiments:
                        self.experiments.append(exp)

            if not self.loaded and autobuild and autoload:
                self.load_runs(**kwargs) ##Pasing kruns with config speeds up x10.

            
            ## ORDER -----------------------
            self._impose_order()

        #data could be a db or the whole  family thing.
        else:
            if isinstance(data, pd.DataFrame) or isinstance(data, pd.DataSeries):
                self.db = data
            else:
                self.db = data.db
            if self.loaded:
                self.runs = []
                self.info = []
                for row in self.db.iterrows():
                    self.runs.append(row[1]['run'])
                    self.info.append(row[1].drop('run'))

        #elif isinstance(data, quixote.Family)





## ------------ PROPERTIES ----------------------------------------


    @property
    def order(self):
        """list(str, dict or OrderedDict: Order for the entries
        of the database.
        When dict, the value is True for ascending order and
        False for descending order.
        Lists will default to ascending (True).
        """
        return self._order
    @order.setter
    def order(self, od):
        if isinstance(od, list) or isinstance(od, OrderedDict):
            self._order = self.parse_order(od, default=True)
            self._impose_order(self._order)
        else:
            self.log.error(
                "Order must be eiter list(str, dict or OrderedDict.")
            raise TypeError
    #@order.setter
    #def order(self, od):
    #    if isinstance(od, list):
    #        self._order = od
    #        self.impose_order()
    #    elif isinstance(od, str):
    #        self._order = [od]
    #        self.impose_order()
    #    else:
    #        self.log.error(
    #            "Order must be eiter list(str, dict or OrderedDict.")
    #        raise TypeError





    def add_index(self, path):
        """Creates abspath to index, either form relative or
        abs path in config
        """
        if (os.path.isfile(path) and os.stat(path).st_size > 0): 
            self.path_index = list(itertools.chain(self.path_index, [path]))
            for i, pth in enumerate(self.path_index):
                self.path_index[i] = os.path.abspath(pth)
        else:
            self.log.error("'{0}' is not a non-zero size file.".format(path))

    def rm_index(self,ind):
        if type(ind) is int:
            ind = [ind]
        elif type(ind) is not list:
            self.log.error("Argument must be either int or list(int)")
            return
        tmp = []
        for i, cond in enumerate(self.path_index):
            if i not in ind:
                tmp.append(cond)
        self.path_index = tmp


    def add_db(self, path):
        """Creates abspath to db, either form relative or abspath.
        """
        if (os.path.isfile(path) and os.stat(path).st_size > 0): 
            self.path_db = list(itertools.chain(self.path_db, [path]))
            for i, pth in enumerate(self.path_db):
                self.path_db[i] = os.path.abspath(pth)
        else:
            self.log.error("'{0}' is not a non-zero size file.".format(path))

    def rm_db(self,ind):
        if type(ind) is int:
            ind = [ind]
        elif type(ind) is not list:
            self.log.error("Argument must be either int or list(int)")
            return
        tmp = []
        for i, cond in enumerate(self.path_db):
            if i not in ind:
                tmp.append(cond)
        self.path_db = tmp






    def add_include(self, inc):
        """
        Parameters
        ----------
        inc : list(dict)
            New conditions to be added to self.include. Same format as
            include kwarg.

        Returns
        -------
        None
            The new criteria are automatically recalculated upon inclusion.
        """
        self.include = list(itertools.chain(self.include, [inc]))
        self._criteria()

    def rm_include(self, ind):
        """
        Parameters
        ----------
        ind : int or list(int)
            Indexes of self.include conditions to be removed.

        Returns
        -------
        None
            The new criteria are automatically recalculated upon removal.
        """
        if type(ind) is int:
            ind = [ind]
        elif type(ind) is not list:
            self.log.error("Ind must be either int or list(int)")
            return
        tmp = []
        for i, cond in enumerate(self.include):
            if i not in ind:
                tmp.append(cond)
        self.include = tmp
        self._criteria()


    def add_exclude(self, exc):
        """
        Parameters
        ----------
        exc : list(dict)
            New conditions to be added to self.exclude. Same format as
            exclude kwarg.

        Returns
        -------
        None
            The new criteria are automatically recalculated upon inclusion.
        """
        self.exclude = list(itertools.chain(self.exclude, [exc]))
        self._criteria()

    def rm_exclude(self, ind):
        """
        Parameters
        ----------
        ind : int or list(int)
            Indexes of self.exclude conditions to be removed.

        Returns
        -------
        None
            The new criteria are automatically recalculated upon removal.
        """
        if type(ind) is int:
            ind = [ind]
        elif type(ind) is not list:
            self.log.error("Ind must be either int or list(int)")
            return
        tmp = []
        for i, cond in enumerate(self.include):
            if i not in ind:
                tmp.append(cond)
        self.include = tmp
        self._criteria()




    def _extract_cases(self, expname):
        yaml_exp=yaml_loader(
                self.avail_experiments[expname])[expname]
        self._explist.append(expname)
        cases = yaml_exp['cases'][:]
        try:
            defaults = yaml_exp['defaults'].copy()
        except:
            defaults = {}
        try:
            description = yaml_exp['description']
        except:
            description = ''

        #Case it in try except to raise better Exception when path
        # to case does not exist?
        for case in cases:
            exp_dict = defaults.copy()
            exp_dict.update({'experiment':expname})
            exp_dict.update({'description': description})
            exp_dict.update(case)
            exp_dict['abspath'] = self._case_abspath(exp_dict)
            exp_dict.update(yaml_xrefs(exp_dict))
            case.update(exp_dict)

        return cases


    def build_db(self):
        """ Ensambles the database using all the available databases, filtered
        by the include and the exclude criteria.

        It uses the available databases in databases/index.yaml or given index
        according to config/local.yaml preferences.

        Parameters
        ----------
        None
            Internal parameters self.include and self.exclude are used.

        Returns
        -------
        None
            self.db is created, containing the corresponding ordered database.


        Notes
        -----
        fulldb is a list of ALL cases for each experiment name.
        If include says {'experiment': 'all'} then all the names should
         be used. I guess that this step should be done before expanding
         include_all and exclude_all
         Maybe instead adding experiment to the include all, this step
        should be skipped and fulldb should be created without parsing.
         and then experiment simply does not appear in include all, thus
         it is not a matter of the parser, unless specifically stated.
         For example include all, but exclude Exp1.
         Exclude all with some extra parameters would remove those parameters.
         from all the experiments.

        Improve Exception handling to better assess problems.
        """

        self._criteria()

        fulldb = []
        self._explist = []
        for crit in self._include_all:
            # if 'experiment': 'all' include all the experiments
            # all experiemnts is self.avail_experiments.keys()
            try:
                expname = crit['experiment']
            except:
                expname = 'all'



#            if expname == 'all':
#                for tmp in self.avail_experiments.keys():
#                    try:
#                        cases = self._extract_cases(tmp)
#                        fulldb = list(
#                            itertools.chain.from_iterable([fulldb, cases]))
#                    except:
#                        self.log.exception(
#                        "Experiment '{}' not found in catalogue.".format(tmp))
#                    
#            elif expname not in self._explist:
#                try:
#                    #self._extract_cases(expname)
#                    cases = self._extract_cases(expname)
#                    fulldb = extend(fulldb, cases) 
#
#                #ATTENTION: Improve exception selection..
#                except:
#                    self.log.exception(
#                    "Experiment '{}' not found in catalogue.".format(expname))
            if expname == 'all':
                explist = self.avail_experiments.keys()
            elif expname not in self._explist: 
                explist = [expname]
            else:
                explist = []
            for tmp in  explist:
                try:
                    cases = self._extract_cases(tmp)
                    fulldb = extend(fulldb, cases) 
                except:
                    self.log.exception(
                    "Experiment '{}' not found in catalogue.".format(tmp))

        fulldb = pd.DataFrame(fulldb)


        df = pd.DataFrame()

        #Include all the include_all criteria
        for criterion in self._include_all:
            if 'experiment' in criterion and criterion['experiment'] == 'all':
                df = fulldb
                break
            tmp = parse_dataframe(fulldb, criterion)
            for _, new_entry in tmp.iterrows():
                inc = True
                for _, entry in df.iterrows():
                    if entry.equals(new_entry):
                        inc = False
                        break
                if inc:
                    #df = df.append(new_entry, ignore_index=True) #Deprecation warning??
                    ## Ah, yes, "same behaviour", right, pandas devs??
                    #df = pd.concat([df, new_entry.to_frame().transpose()], ignore_index=True, axis=0)
                    df = pd.concat([df, pd.DataFrame([new_entry])],
                        ignore_index=True, axis=0)

        #Substract all the exclude_all criteria
        for criterion in self._exclude_all:
            ## Parsing {} results in all. Maybe not needed if []
            if criterion != {}:
                tmp = parse_dataframe(df, criterion)
                df = df.drop(tmp.index.values)
                df = df.reset_index(drop=True)
        #self.db = df.reset_index(drop=True) #Why was this removed?
        self.db = df
        self.built = True

        return


    def _avail_experiments(self):
        """ Gather all available experiments from databases in either
        json or yaml formats.

        Parameters
        ----------
        None

        Returns
        -------
        OrderedDict
            Contains experiments as keys and paths to corresponding json/yaml
            files.

        Notes
        -----
            Individual DBs are prioritized above those found in indices.

            The intended use consist in allowing to choose a version of the same
            experiment over some other version in a different database.
            In that sense, {'my-experiment': '/path/to/my/database.yaml'} will
            provide the source to read 'my-experiment'.

            It can also be used to check the available experiments in a dynamic
            session, as 'my-experiment' in self.avail_experiments is bool.
        """
        # ATTENTION: This still did not make the order of exp correct.
        #experiments = {}
        experiments = OrderedDict()

        rpath = self.path_index[:]
        rpath.reverse() #Top update lasts, thus top priority.
        for index in rpath:
            try:
                #rlist = yaml_loader(index)[:]
                #To prevent weird spaces
                rlist = [tmp.replace(" ","") for tmp in yaml_loader(index)]
                rlist.reverse() #Keep same order: Top = highest priority
                folderindexpath = index.rstrip(os.path.basename(index))
            except IOError:
                self.log.exception(
                    "Index '{}' could not be opened.".format(index))
            except Exception:
                self.log.exception(
                        "Index '{}' could not be found/read".format(index))

            for db in rlist:
                if type(db) is dict:
                    dbpath = os.path.join(folderindexpath,list(db.keys())[0])
                    if not os.path.exists(dbpath):
                        self.log.error(
                                "DB file '{}' could not be found/read".format(
                                    list(db.keys())[0]))
                        continue
                    dummy = list(db.values())[0]
                    for exp in dummy:
                        experiments.update({exp:dbpath})
                if type(db) is str:
                    dbpath = os.path.join(folderindexpath,db)
                    try:
                        dbcontent = yaml_loader(dbpath)
                    except:
                        self.log.error(
                            "DB file '{}' could not be found/read".format(db))
                        continue
                    for exp in list(dbcontent.keys()):
                        ##  Save relative path or absolute path?? Try abs for now.
                        #experiments.update({exp:dbpath})
                        experiments.update({exp:os.path.abspath(dbpath)})

        ## Add individual given databases
        rpath = self.path_db[:]
        rpath.reverse() #Top update lasts, thus top priority.
        for dbpath in rpath:
            try:
                dbcontent = yaml_loader(dbpath)
            except:
                self.log.error(
                    "DB file '{}' could not be found/read".format(db))
                continue
            for exp in list(dbcontent.keys()):
                experiments.update({exp:os.path.abspath(dbpath)})

        return experiments








    def parse_db(self, dicty):
        """ Alias for parse_dataframe which already provides self.db as
        the corresponding database.

        Parameters
        ----------
        dicty : dict
            Dict to be used as filter to parse self.db

        Returns
        -------
        pd.DataFrame
            Contains all the occurrences meeting the filter criteria.
        """
        return parse_dataframe(self.db,dicty)




    #@staticmethod
    def parse_order(self, order, default=None):
        """
        Parameters
        ----------
        order: list(str, dict) or OrderedDict
            It indicates the labels to order and also how to order them,
            e.g., ['experiment',{'ip':False},'fhecore']
            Internal OrderedDicts may contain more than one key, but
            normal dicts should only contain a single  key or order might
            be random in some Python versions.

        Returns
        -------
        OrderedDict
            The str will be expanded to key:None entries.
            
        """
        parsed = OrderedDict()
        if isinstance(order,(list, str)):
            order = extend(order)
            for ele in order:
                if isinstance(ele, str):
                    parsed[ele] = default
                elif isinstance(ele, dict):
                    parsed.update(ele)
            return parsed
        elif isinstance(order, OrderedDict):
            return order
        else:
            self.log.error("Order must be list(str, dict) or OrderedDict")
            raise TypeError

#    ## ATTENTION 2021: Read Note for public impose_order
#    ## ATTENTION 2021: Read Note for list(str) for str parameters
#    def _impose_order(self, order=None, replace=False):
#        """ Use list to order columns of db, creating a boxing effect.
#
#        Parameters
#        ----------
#        order : list(str), optional.
#            List to be used as ordering criterion.
#            If order is None, then self.order is used.
#            If order is not None, then order is used and self.order
#            is updated.
#
#
#        Notes
#        -----
#
#        Create a new impose_order to be used publicly.
#        It should not mutate self, but return a df object which
#        is a copy. Then, it can be used by users who want to temporarily
#        change the order
#
#        Then reassign _impose_order to call that function and update
#        self.db, self.runs and self.info.
#        However, that would create an extra object even if temporarily.
#        Is that too much?? Most likely not??
#        But wait until later, when tests have been created
#
#        Additionally, str parameters should be allowed to pass a list of
#        str as their value, so that str values are ordered according
#        to that lits intead of alphabetical order (or whatever is done
#        internally)
#        This might be achiveable with creating categories, e.g. exp rank,
#        automatically for any column str and assigning the rank value
#        based on either position (if nothing is given) or on given list.
#        In fact, this could be done for ANYTHING.
#        And it could, in fact solve the problem of experiment ordering.
#        
#        """
#        if order is None:
#            order = self.order.copy()
#        else:
#            order = self.parse_order(order, default=True)
#
#        #For experiment, create a temporary column with numerical indexes.
#        for i, exp in enumerate(self.experiments):
#            self.db.loc[self.db['experiment'] == exp, 'exp rank'] = i
#
#
#        #Substitute experiment by exp rank in order
#        try:
#            if isinstance(order, list):
#                order[order.index('experiment')] = 'exp rank'
#            elif isinstance(order, OrderedDict):
#                order = OrderedDict( [('exp rank', v) if k == 'experiment'
#                    else (k, v) for k, v in order.items()])
#        except:
#            pass
#
#        # Order database according to order
#        try:
#            #self.db = self.db.sort_values(by=order)
#            self.db = self.db.sort_values(
#                    by=list(order.keys()), ascending=list(order.values()))
#        except KeyError:
#            self.log.error("Ordering key not present as database labels")
#        del self.db['exp rank']
#
#        #Ordering index. Will help to re-order mds list
#        self._last_odin = np.array(self.db.index.astype(int))
#        self.db = self.db.reset_index(drop=True)
#        try:
#            self.runs = [self.runs[i] for i in self._last_odin]
#            self.info = [self.info[i] for i in self._last_odin]
#        except:
#            self.log.exception("No loaded runs, therefore ordering skipped.")



    def _impose_order(self, order=None, replace=False, cleanup=True):
        """ Use list to order columns of db, creating a boxing effect.

        Parameters
        ----------
        order : list(str), optional
            List to be used as ordering criterion.
            If order is None, then self.order is used.
            If order is not None, then order is used and self.order
            is updated.

        cleanup : bool, optional
            If True (default) then ranking columns are deleted.
            False maybe used for debugging.


        Notes
        -----

        Create a new impose_order to be used publicly.
        It should not mutate self, but return a df object which
        is a copy. Then, it can be used by users who want to temporarily
        change the order

        Then reassign _impose_order to call that function and update
        self.db, self.runs and self.info.
        However, that would create an extra object even if temporarily.
        Is that too much?? Most likely not??
        But wait until later, when tests have been created

        
        """
        if order is None:
            order = self.order.copy()
        else:
            order = self.parse_order(order, default=True)

        # Create ranking for each parameter for which list was given.
        ranked = []
        for param, od_param in order.items():
            if isinstance(od_param, list):
                placeholder = param+' rank'
                ranked.append(param)
                for i, itm in enumerate(od_param):
                    self.db.loc[self.db[param] == itm,
                            placeholder] = od_param.index(itm)
            #Experiment is special,  by default order is input order.
            elif param == 'experiment':
                ranked.append('experiment')
                for i, exp in enumerate(self.experiments):
                    self.db.loc[self.db['experiment'] == exp,
                        'experiment rank'] = i

        #Substitute experiment by exp rank in order
        try:
            for param in ranked:
                if param == 'ip':
                    import pdb; pdb.set_trace()
                if isinstance(order, list):
                    order[order.index(param)] = param+' rank'
                elif isinstance(order, OrderedDict):
                    order = OrderedDict([
                        (param+' rank', True) if k in ranked else (k, v)
                        for k, v in order.items()])
        except:
            pass

        # Order database according to order
        try:
            #self.db = self.db.sort_values(by=order)
            self.db = self.db.sort_values(
                    by=list(order.keys()), ascending=list(order.values()))
        except KeyError:
            self.log.error("Ordering key not present as database labels")

        # Clean up auxiliary ranking columns.
        if cleanup:
            for param in ranked:
                del self.db[param+' rank']

        #Ordering index. Will help to re-order other elements.
        self._last_odin = np.array(self.db.index.astype(int))
        self.db = self.db.reset_index(drop=True)

        # Reassign previously created lists and arrays with new order.
        self.experiments = list(self.db['experiment'].unique())
        if self.loaded:
            try:
                self.runs = [self.runs[i] for i in self._last_odin]
                self.info = [self.info[i] for i in self._last_odin]
            except AttributeError:
                #self.log.warning("No loaded runs, therefore ordering of runs skipped.")
                pass
            except:
                self.log.exception("Runs could not be ordered.")
        #try:
        #    self.runs = [self.runs[i] for i in self._last_odin]
        #    self.info = [self.info[i] for i in self._last_odin]
        #except AttributeError:
        #    self.log.warning("No loaded runs, therefore ordering of runs skipped.")
        #except:
        #    self.log.exception("Runs could not be ordered.")

        return





    def load_runs(self, **kwargs):
        """ Ensambles a list with mds and rundir shots in self.db.
        self.db is created automatically.
        After loading, evaluates all the !Lambda parameters.
        The self.info and the self.runs[:].info is updated too.

        Parameters
        ----------
        load_by: str, optional.
            Label of the parameter to be used as input for SolpsData.
            Default: 'id'

        kwargs_runs: dict, optional.
            kwargs to pass to all the runs when initialized.



        Returns
        -------
        None
            self.runs and self.info are created and attached directly
            to self. Also, individual runs have now a .info with the labels.

        Notes
        -----
        In the future, accept various load_by so that they are consecutively
        tried after failure.

        Maybe think about including this in the future instead:
        load_as: str, optional.
            Load the solps simulation as 'mds' or 'rundir', using
            nshot or abspath, respectively.
            Default: Loaded using 'id'

        """
        self.runs = []
        self.info = []

        load_by = priority(['by', 'load_by'], kwargs, self.config['load_by'])

        progress_bar = priority(
                ['progress_bar','bar'], kwargs, self.progress_bar)

        #kwargs_runs = priority(['kwargs_runs', 'kruns', 'ksims'], kwargs, {})
        kwargs_runs = priority('kruns', kwargs, {})

        if progress_bar and not _forced_no_progress_bar:
            rowgen = tqdm(self.db.iterrows(), total=self.db.shape[0],
                    desc='Loading', unit='runs')
        else:
            rowgen = self.db.iterrows()

        try:
            for i,row in rowgen:
                lb = row[load_by]
                if isinstance(lb, float) or isinstance(lb,int):
                    lb = int(lb)
                sim = SolpsData(lb, **kwargs_runs)

                ## Evaluate all the entries in row which are functions.
                ## They are assumed to be functions of sim(SolpsData).
                for column, itm in row.items():
                    if isinstance(itm, types.FunctionType):
                        row[column] = itm(sim) #Not sure if mutation, but...
                        self.db.at[i, column] = itm(sim)
                sim.info = row
                self.runs.append(sim)
                self.info.append(row)

            ## Append self.runs into the database
            #import IPython ; IPython.embed()
            self.db['run'] = self.runs
            self.loaded = True

        except:
            self.log.error("Loading of DB could not be completed.")
            self.loaded = False



    
        ## Atempt in case we do not mind some beign None, but it is probably better
        ## to fail and know it.
        #for i,row in rowgen:
        #    try:
        #        lb = row[load_by]
        #        if isinstance(lb, float) or isinstance(lb,int):
        #            lb = int(lb)
        #        #tmp = SolpsData(row[load_by])
        #        tmp = SolpsData(lb)
        #        tmp.info = row
        #        self.runs.append(tmp)
        #        self.info.append(row)
        #    except:
        #        self.log.error(
        #           "Load_by {} did not yield a correct SolpsData instance.".format(lb))
        #        self.runs.append(None)
        #        self.info.append("Not loaded")
        #self.loaded = True

    #In the future allow include and exclude, but for now
    def indices(self, dicty):
        return self.parse_dataframe(dicty).index



    def sample(self, func, args=[], kwargs={}, name=None, use_nan=True):
        """ Samples each run of the database in order given a function.

        Parameters
        ----------

        func: function or lambda function
            It must accept a SolpsData object as its parameter and can accept
            a list of additional positional arguments and a dictionary of
            optional key arguments.
            Run already contains .info

        args: list, optional
            List of additional positional arguments for func.
            The SolpsData MUST NOT be included in this list, since it is always
            the default first argument.
            Default: []

        kargs: dict, optional
            List of optional key arguments of func.
            Default: {}


        name: str, optional
            If name is given, then sample is saved in self.samples with that
            name instead of being returned.
            Default: None.

        Returns
        -------
        (n entries, dims)

            If name is None, then returns a numpy array of dimensions:
            (number of database entries, dims) where dims is the dimensions
            of the returned variable of func.
            If name is not None, then that numpy array is attached to
            self.samples under the given name.

        """
        if not callable(func):
            self.log.error("Func must be callable.")
            raise TypeError

        if not hasattr(self,'runs'):
            self.log.info("No runs were loaded, thus loading them now.")
            self.load_runs()

        tmp= []
        ## Keep using self.runs, or move to db['run']?
        if self.progress_bar:
            enu = tqdm(enumerate(self.runs), total = len(self.runs),
                    desc='Sampling', unit='samples')
        else:
            enu = enumerate(self.runs)

        for i,run in enu:
            try:
                ## This works
                #tmp.append(func(run, *args, **kwargs))

                tmp.append(func(run, *args, **kwargs))
            except:
                if use_nan:
                    self.log.exception(
                    "Function could not be correctly applied to runs[{}]".format(i))
                    tmp.append(np.nan)
                else:
                    raise Exception
        tmp = np.array(tmp)


        if name and isinstance(name,str):
            setattr(self.samples, name, tmp)
        else:
            return tmp


#    def iterslice(self, func):
#        """
#        Generator
#
#        Allow to accept order as an argument to overwrite temporarily the
#        default order, self.order.
#        """
#        if not callable(func):
#            self.log.error("Func must be callable with run as parameter.")
#            raise TypeError
#
#
#        labels = self.order[:-1]
#        basis =  self.order[-1]
#
#
#
#        df = self.db.copy()
#        # JUST FOR CHECKING
#        tmp = lambda x: os.path.join(x['basepath'], x['path'])
#        check = df[['basepath','path']].apply(tmp,axis=1)
#        embed()
#        # JUST FOR CHECKING
#
#
#        #for label in labels:
#            #Get unique values for each label
#
#
#        #for run in tmp_list:
#        #    slice.append(func(run))
#




    def iterdb(self):
        """
        Generator. NON GENERAL CASE.

        Allow to accept order as an argument to overwrite temporarily the
        default order, self.order.
        """
        try:
            for index, row in self.db.iterrows():
                yield self.runs[index], dict(row)
        except:
            for index, row in self.db.iterrows():
                yield None, dict(row)

    def iterbar(self):
        return tqdm(self.iterdb(), total=len(self.runs),
                    desc='Iterating', unit='runs')

    def unique(self,label):
        return self.df_unique(self.db, label)

    @staticmethod
    def df_unique(df, label):
        """
        Maybe improve logic of what is consider 'unique' (0.59999 vs 0.6, etc)
        Accept unique(df, label, tol=None, rtol=None) like in numpy.
        Also, move to tools for a more general use?
        self.unique is already replacing it anyway.
        """
        unique = []
        for index,row in df.iterrows():
            if row[label] not in unique:
                unique.append(row[label])
        return unique




    ## ATTENTION: Improve logic, update to modern tools, make tests, etc.
    ## ATTENTION: IT ONLY WORKS FOR TWO QUANTITIES IN ORDER
    #@profile
    def iterslice(self, func, order=None):
        """
        Generator

        Allow to accept order as an argument to overwrite temporarily the
        default order, self.order.

        Yields 
        ------
        x, y, dict
            x: deep-most ordering parameter or basis.

            y: value of func.

            dict: self.info, labels (ordering hierarchy), and prettify.

        Notes
        -----
        For the future, allow to call and alraedy created sample to save time,
        using info to gather the relevant information on each interation.

        ATTENTION: Users should be allowed to use real sim data as ordering
        since this is done after loading.

        """
        self.log.warning("This function is still in a building phase.")

        if not hasattr(self,'runs'):
            self.log.info("No runs were loaded, thus loading them now.")
            self.load_runs()

        df = self.db.copy()
        if not callable(func):
            self.log.error("Func must be callable.")
            raise TypeError

        if not order:
            order = self.order

        labels = order[:-1]
        basis =  order[-1]

        ## Create labels and basis out of order
        #if isinstance(order,list):
        #    labels = []
        #    for label in order[:-1]:
        #        try:
        #            label.append(label.key)
        #            label.append(label)

        #    #labels = 
        #    basis =  order[-1]
        #elif isinstance(order, OrderedDict):
        #    labels = order.keys()[:-1]
        #    basis =  order.keys()[-1]
            

        rdict = {}
        if len(labels) == 2:
            if self.progress_bar:
                genu = tqdm(df[labels[0]].unique(),
                    total = len(df[labels[0]].unique()),
                    desc='Iterating', unit='iterations')
            else:
                genu = df[labels[0]].unique()

            #genu = df[labels[0]].unique()

            #ATTENTION: UNIQUE MIGHT NOT NECESSARILY MEAN IN ORDER.
            for val1 in genu:
                rdict[labels[0]] = val1
                if isinstance(val1, float):
                    rdf = df[np.isclose(df[labels[0]], val1)]
                elif isinstance(val1, int):
                    rdf = df[np.isclose(df[labels[0]], val1)]
                elif isinstance(val1, str):
                    rdf = df[(df[labels[0]] == val1)]

                for val2 in rdf[labels[1]].unique():
                    rdict[labels[1]] = val2
                    if isinstance(val1, float):
                        rrdf = rdf[np.isclose(rdf[labels[1]], val2)]
                    elif isinstance(val1, int):
                        rrdf = rdf[np.isclose(rdf[labels[1]], val2)]
                    elif isinstance(val1, str):
                        rrdf = rdf[(rdf[labels[1]] == val2)]

                    indices = rrdf.index
                    basis_uniques = rrdf[basis].unique()
                    variable = np.full(len(indices), np.nan)
                    x = np.full(len(indices), np.nan)
                    mimdic = {}
                    for i, ind in enumerate(indices):
                        variable[i] = (func(self.runs[ind]))
                        x[i] = basis_uniques[i]
                        mimdic = self.minimum_dict(dict(df.iloc[ind]),mimdic)

                    mimdic['basis'] = basis
                    mimdic['labels'] = labels
                    yield x, variable, mimdic

        elif len(labels) == 1:
            for val1 in df[labels[0]].unique():
                rdict[labels[0]] = val1
                if isinstance(val1, float):
                    rdf = df[np.isclose(df[labels[0]], val1)]
                elif isinstance(val1, int):
                    rdf = df[np.isclose(df[labels[0]], val1)]
                elif isinstance(val1, str):
                    rdf = df[(df[labels[0]] == val1)]

                indices = rdf.index
                basis_uniques = rdf[basis].unique()
                variable = np.full(len(indices), np.nan)
                x = np.full(len(indices), np.nan)
                mimdic = {}
                for i, ind in enumerate(indices):
                    variable[i] = (func(self.runs[ind]))
                    x[i] = basis_uniques[i]
                    mimdic = self.minimum_dict(dict(df.iloc[ind]),mimdic)

                mimdic['basis'] = basis
                mimdic['labels'] = labels
                yield x, variable, mimdic






    ## The heck is this??
    ##Replace by quixote.tools.intersection??
    @staticmethod
    def minimum_dict(dict1, dict2):
        """ minimum_dict(A,{}) = A
        It should be called dict_intersection.
        It returns a dict with keys and values exactly present in both
        dicts.
        If values differ, they are not included.
        2022: copied into tools.intersection
        """
        if dict1 == {}:
            return dict2
        if dict2 == {}:
            return dict1

        mdic = {}
        for k1,v1 in list(dict1.items()):
            try:
                if ((isinstance(v1,float) or isinstance(v1,int))
                    and np.isclose(v1,dict2[k1])):
                    mdic[k1] = v1
                elif isinstance(v1,str) and (v1 == dict2[k1]):
                    mdic[k1] = v1
            except:
                pass
        return mdic





    def plot_slice(self, func, **kwargs):
        pass








    def _expand_criteria(self, criteria=None):
        """
        """
        if criteria is None:
            criteria = [{}]
        elif not isinstance(criteria, list):
            criteria = [criteria]

        criteria_all = []
        for criterion in criteria:
            criteria_all.append(self._expand_combinations(criterion))
        return list(itertools.chain.from_iterable(criteria_all))
        #Probably replaceable by 
        #return extend(criteria_all)








    # IDEA: Just use __getitem__ of DF
    ## FIlter to find the indices and then self[indices] and it will return either
    ## a series or a dataframe.
    ## IDEALLY IT WOULD RETURN A FAMILY TOO.

    #26/07/2022: Maybe if DataFrame is provided, skip building DataFrame, etc.
    #and if runs are provided, skip autobuild, etc.
    #In that way, filter can create a df and just initialize a family with it.
    
    ## ATTENTION: This does not work at the moment
    def filter(self,include=None,exclude=None, as_type=None):
        """ Filter takes self and returns a "family" proxy with only
        the required cases.
        Runs and info are the same objects as the original.
        If one wants copies, one has to reload the runs.
        """

        if include is None:
            include = ['all']
        elif not isinstance(include, list):
            include = [include]

        if exclude is None:
            exclude = [{}]
        elif not isinstance(exclude, list):
            exclude = [exclude]


        #fam = Family(default_index=False, index=None, db = None,
        #        include=include, exclude=exclude, order=self.order,
        #        autobuild=False, autload=False, bar=self.progress_bar)

        ##Works for now but.
        #fam = SimpleNamespace(include=include, exclude=exclude, order=self.order,
        #        bar=self.progress_bar)


        df = pd.DataFrame()

        includes = self._expand_criteria(include)
        excludes = self._expand_criteria(exclude)

        for criterion in includes:
            tmp = parse_dataframe(self.db, criterion)
            for _, new_entry in tmp.iterrows():
                inc = True
                for _, entry in df.iterrows():
                    if entry.equals(new_entry):
                        inc = False
                        break
                if inc:
                    ## Check this whole thing before deprecation to make sure
                    ## That the behaviour is the same
                    ##df = df.append(new_entry, ignore_index=True)
                    #df = df.append(new_entry)

                    ## Ah, yes, "same behaviour", right, pandas devs??
                    #df = pd.concat([df, new_entry.to_frame().transpose()], axis=0)
                    df = pd.concat([df, pd.DataFrame([new_entry])], axis=0)

        ## Problem!! parse_dataframe does not work with index not starting on zero.
        ## For exclude, a reset in the index is required.
        ## But saving the included indices and mapping the droped by excluded,
        ## I can still know which ones are which.

        save_ind = df.index.to_list()
        df = df.reset_index(drop=True)

        #Substract all the exclude_all criteria
        for criterion in excludes:
            if criterion != {}:
                tmp = parse_dataframe(df, criterion)

                # Drop indices in positions tmp.index.values
                for ind in tmp.index.values:
                    del save_ind[ind]

                df = df.drop(tmp.index.values)
                df = df.reset_index(drop=True)



        ## This works.
        #fam = self.__class__(default_index=False, index=None, db = None,
        #        include=include, exclude=exclude, order=self.order,
        #        autobuild=False, autload=False, bar=self.progress_bar,
        #        built = self.built, loaded=self.loaded, data=df)

        if as_type and as_type.lower() == 'family':
            fam = Family(default_index=False, index=None, db = None,
                include=include, exclude=exclude, order=self.order,
                autobuild=False, autload=False, bar=self.progress_bar,
                built = self.built, loaded=self.loaded, data=df)

        elif as_type and as_type.lower() == 'uncertainty':
            fam = Uncertainty(default_index=False, index=None, db = None,
                include=include, exclude=exclude, order=self.order,
                autobuild=False, autload=False, bar=self.progress_bar,
                built = self.built, loaded=self.loaded, data=df)
        else:
            fam = self.__class__(default_index=False, index=None, db = None,
                include=include, exclude=exclude, order=self.order,
                autobuild=False, autload=False, bar=self.progress_bar,
                built = self.built, loaded=self.loaded, data=df)

        return fam


    def as_type(self, dtype):

        if dtype and dtype.lower() == 'family':
            fam = Family(default_index=False, index=None, db = None,
                include=self.include, exclude=self.exclude, order=self.order,
                autobuild=False, autload=False, bar=self.progress_bar,
                built = self.built, loaded=self.loaded, data=self.db)

        elif dtype and dtype.lower() == 'uncertainty':
            fam = Uncertainty(default_index=False, index=None, db = None,
                include=self.include, exclude=self.exclude, order=self.order,
                autobuild=False, autload=False, bar=self.progress_bar,
                built = self.built, loaded=self.loaded, data=self.db)
        else:
            self.log.exception("Must be 'family' or 'uncertainty'")

        return fam




    #def map(self, column, d, name=None):
    #    """
    #        The provided dictionary is applied to the provided column.
    #        If a name is given, then the column is created in place.
    #    """
    #    if name:
    #        self.db[name] = self.db[column].map(d)
    #    else:
    #        return self.db[column].map(d)


    def map(self, column, functional, name=None, na_action='ignore'):
        """
            The provided functional is applied to the provided column.
            If a name is given, then the column is created in place.
            Functionl or dict must ONLY depend on the value of the columns,

        """
        if name:
            self.db[name] = self.db[column].map(functional, na_action=na_action)
        else:
            return self.db[column].map(functional, na_action=na_action)


    #def apply(self, column, functional, name=None):
    #    """
    #    """
    #    if name:
    #        self.db[name] = self.db[column].map(functional)
    #    else:
    #        return self.db[column].map(functional)





    ## ATTENTION: Make manual part of legend a util/plot function??
    ## Then legend of family.legend provides some defaults, but 
    ## legend can be used separately.
    def legend(self, **kwargs):
        """
            if done with set_style, pairs is generated automatically
        if not given by the user.
        """
        spaced = priority(['blank','space','blanks','spaced'], kwargs, True)
        order = priority('order', kwargs, ['color', 'ls', 'marker', 'lw'])

        lines = []
        names = []

        for pretty in order:
            if pretty in kwargs:

                if isinstance(kwargs[pretty], str):
                    col = kwargs[pretty]
                    ##Create dictionary
                    uniques = self[col].unique()
                    for uniq in uniques:
                        tmp = self.filter({col:uniq}).unique(pretty)
                        if len(tmp) !=1:
                            self.log.exception(
                            "'{}' is not injective to '{}'".format(col, pretty))
                        else:
                            lines.append(
                            plt.Line2D([0], [0], **{pretty:tmp[0]}))
                            names.append(uniq)

                ## Rushed, check corner cases.
                elif isinstance(kwargs[pretty], dict):
                    tmp = kwargs[pretty]
                    for name, uniq in tmp.items():
                        lines.append(plt.Line2D([0], [0],
                            **update({pretty:uniq})))
                        names.append(name)

                elif isinstance(kwargs[pretty], list):
                    tmp = kwargs[pretty]
                    for name, uniq in tmp[0].items():
                        lines.append(plt.Line2D([0], [0],
                            **update(tmp[1],{pretty:uniq})))
                        names.append(name)

                if spaced:
                    lines.append(plt.Line2D([0], [0], color='w', alpha=0.0))
                    names.append(' ')


        if names[-1] == ' ':
            lines = lines[:-1]
            names = names[:-1]
        return [lines, names]








#    ## ATTENTION: Call self.map to prettify stuff
#    def set_style(self, style):
#        """ Not yet usable.
#        """
#        if isinstance(style,list):
#            pass
#        elif isinstance(style,dict):
#            pass






    ## Also allow prettify by order!
    ## last level, second to last, etc.
    ## In that way, order can be changed while prettify remains.
    ## Maybe if pretties are lists instead of dicts.
    def prettify(self, color=None, ls=None, marker=None, lw=None, alpha=None):
        """
            For each, provide exhaustive dict or list (Not yet).
            If dict, label:value is assumed.
            For now only dict

            In the future:
            If list, np.uniques are called and assigned
        """
        for pretty,name in zip([color, ls, marker, lw, alpha],
            ['color', 'ls', 'marker', 'lw', 'alpha']):
            if isinstance(pretty, dict):
                for label, values in pretty.items():
                    if isinstance(values, dict):
                        self.map(label, values, name=name)
                    #elif isinstance(values, list):
                    #    uniques = self[
            elif isinstance(pretty, list):
                pass
            ## Assume list of lists, first element being bottom level, etc.








# =============== PRIVATE METHODS =============================================
    def _read_fulldb(self):
        """ Read all available databases, in either json or yaml format.
        """


    def _criteria(self):
        """ Creates a list containing the criteria to be used for the selection
        of the SOLPS simulations in the various databases.
        The final criteria are a mixture of the expanded include minus the
        expanded exclude.

        Parameters
        ----------
        None
            It uses self.include and self.exclude.

        Returns
        -------
        None
            It creates self._include_all and self._exclude_all, which contain the
            respective list with all the requested combinations given by
            self.include and self.exclude.

        Notes
        -----
        It needs to remove exact copies.
        """
        include_all = []
        for criterion in self.include:
            include_all.append(self._expand_combinations(criterion))
        self._include_all =  list(itertools.chain.from_iterable(include_all))
        exclude_all = []
        for criterion in self.exclude:
            exclude_all.append(self._expand_combinations(criterion))
        self._exclude_all =  list(itertools.chain.from_iterable(exclude_all))


    ## This could perfectly work in tools for general purposes.
    @staticmethod
    def _expand_combinations(dicty):
        """ Expands the dictionary by creating all valid combinations of
        the values of all the keys, which values can be lists.

        It takes a dict which values might or might not be list with multiple
        possibilities and gives back a list of "flattened" dicts, which keys
        only contain a single value, float, str, etc.

        Parameters
        ----------
        dicty : dict
            Dictionary to be expanded. The values for each key, which may be
            a list of values, will be combined.

        Returns
        -------
        list(dict)
            List containing all possible combinations with keys and values
            of dicty.
        """
        tmp = []
        ditems = list(dicty.items())
        dnames = [item[0] for item in ditems]
        for key, values in ditems:
            if type(values) != list:
                values = [values]
            tmp.append(values)
        combinations = list(itertools.product(*tmp))
        a = [dict(list(zip(dnames, values))) for values in combinations]
        return a



    @staticmethod
    def _shorter_str(tmp, ln):
        """ Return shortened string when it is longer than certain value.

        Parameters
        ----------
        tmp : str
            String to be tested and, if required, shortened.
        ln : int
            Number of characters above which the string tmp should be trimmed.

        Returns
        -------
        str
            It equals tmp, if it was within accepted lenght, or trimmed tmp.
        """
        tmp = str(tmp)
        ln = int(ln)
        if len(tmp) > ln:
            return '...'+tmp[-ln:]
        else:
            return tmp



    @staticmethod
    def _case_abspath(case):
        try:
            return case['abspath']
        except:
            pass

        try:
            rootpath = case['rootpath']
        except:
            rootpath = ''

        try:
            basepath = case['basepath']
        except:
            basepath = ''

        try:
            path = case['path']
        except:
            path = ''

        #return os.path.abspath(os.path.join(rootpath,basepath,path))
        total_path = os.path.abspath(os.path.join(rootpath,basepath,path))
        if os.path.exists(total_path):
            return total_path
        else:
            #import ipdb; ipdb.set_trace()
            raise Exception(
                "'{}' is not a valid directory path".format(total_path))



    ## Allow index = True, and then order gives indices, etc
    ## Think about making it prettier with MultiIndex (hierarchical index)
    ## Not to organize db, but to show it.
    def _get_db_view(self, extra_cols=None, db ='db',
            show_always=['id'], show_order=True, only=None):
        """
        Notes
        -----
            The return works.. somehow. I dont't want to impose order in
            id, though.
            Does it acknowledge it correctly?
            Return without ordering would be:
                return tmp.to_string()
        """
        #tmp = self.db.copy()
        tmp = get(self, db).copy()

        # Global column width, if any
        try:
            gwidth= int(self.config['repr']['global_column_width'])
            for col in list(tmp):
                try:
                    tmp[col] = tmp[col].apply(
                            lambda x: self._shorter_str(x, gwidth))
                except:
                    pass
        except:
            pass
        # Column width per column name
        if 'column_width' in self.config['repr']:
            for col, width in list(self.config['repr']['column_width'].items()):
                try:
                    tmp[col] = tmp[col].apply(
                            lambda x: self._shorter_str(x, width))
                except:
                    pass


        show = []
        if only is not None:
           show = extend(only, show)
        else:
            show = extend(show_always, show)
            if show_order:
                show.extend(self.order)
            if extra_cols:
                show.extend(extend(extra_cols))


        return tmp[unique(show)]
        #Create a MultiIndex using order, and return that?
        # Basically, for each entry, make the array of the values of the
        # labels in order, and use that as index in the multiindex







    def __str__(self):
        return self._get_db_view().to_string()


    ## Allow index = True, and then order gives indices, etc
    def show(self, cols=None, **kwargs):
        """ Maybe accept str or list
        and add those columns to the standard print(self)
        eg. tmp = self.__str__ and concat axis=1 the column
        """
        if not self.db.empty:
            print(self._get_db_view(extra_cols = cols,**kwargs).to_string())
        else:
            print('[]')
        return


## Experimental
    def __getitem__(self, *args):
        if not args:
            return None
        elif not self.built:
            self.log.error("DB not built yet")
        else:
            args = extend(*args)
            if isinstance(args[0],str):
                return self.db[args].squeeze()
            else:
                return self.db.iloc[args].squeeze()




















##=============================================================================
##=============================================================================
##=============================================================================





#no_main_values: average
#no_main_labels: common

## ----------------------------------------------------------------------------
class Uncertainty(Family):
    def __init__(self, *args, **kwargs):
        """
        """
        super().__init__(*args, **kwargs)

        self.config = update(self.config, self._full_config['Uncertainty'])
        self._build_mdb()



#    def _build_mdb(self):
#
#        import ipdb; ipdb.set_trace()
#        ## If no main, "construct" main out of all common tags and NaN
#        ## to others
#        ## And then decide what will be the number returned when sampled
#        ## Whether the .
#        self.mdb = self.db.dropna(subset='main').reset_index(drop=True)
#
#        uniques = self.db['group'].unique()
#        
#        #import ipdb; ipdb.set_trace()
#        #self.mdb['entries'] = [len(
#        #    parse_dataframe(self.db, {'group': tag})) for tag in uniques]
#        tmp = []
#        for tag in uniques:
#            if isinstance(tag, (int, float)) and np.isnan(tag):
#                continue
#            tmp.append(len(parse_dataframe(self.db, {'group': tag})))
#        self.mdb['entries'] = tmp

        
    ##ATTENTION: Revised method
    def _build_mdb(self):

        self.mdb = pd.DataFrame()

        groups = self.db['group'].unique()
        
        for tag in groups:
            if isinstance(tag, (int, float)) and np.isnan(tag):
                continue
            group_entries = parse_dataframe(self.db, {'group': tag})
            try:
                entry = group_entries.dropna(subset='main').to_dict()
            except:
                labels = {}
                entry = {}
                for col in group_entries:
                    tmp = group_entries[col].unique()
                    if len(tmp)!=1:
                        entry[col] = np.nan
                    else:
                        entry[col] = tmp[0]
                entry['main'] = tag
            entry['entries'] = len(group_entries)
            entry = pd.DataFrame(entry, index=[0])
            self.mdb = pd.concat([self.mdb, entry], ignore_index=True)

        return

        #self.mdb = self.db.dropna(subset='main').reset_index(drop=True)

    #df.append(df2, ignore_index=True)

    ##Apply order to mains??? It doesn't make sense that the order should only be applied
    ## to the underlying invisible entries. Althought probably good enough in most cases.


    def __getitem__(self, *args):
        if not args:
            return None
        elif not self.built:
            self.log.error("DB not built yet")
        else:
            args = extend(*args)
            if isinstance(args[0],str):
                return self.mdb[args].squeeze()
            else:
                return self.mdb.iloc[args].squeeze()



    def show(self, cols=None, **kwargs):
        db = priority(['db','database'], kwargs, 'mdb')
        always = priority(['always', 'show_always', 'always_show'],
            kwargs, ['group', 'entries'])
        print(self._get_db_view(extra_cols=cols, db=db,
            show_always=always, **kwargs).to_string())
        return




    def sample(self, function, args=[], kwargs={},
            use_nan=True, squeeze=True, **kws):
        """
        In the future, add a way to sample cases in different cores/threads.
        """
        ## no_main_values is now config
        no_main_values = priority(['no_main', 'nomain'],
            kws, self.config['no_main_values'])

        groups = self.db['group'].unique()
        samples = super().sample(function,
            args=args, kwargs=kwargs, use_nan=use_nan)
        dims = extend(len(groups), 3, samples.shape[1:])
        uncertainty = np.full(dims, np.nan)
        for i, tag in enumerate(groups):
            inds = parse_dataframe(self.db, {'group': tag}).index
            try:
                mind = parse_dataframe(self.db, {'group': tag, 'main': tag}).index
                if len(mind)==1:
                    uncertainty[i,0] = samples[mind]
                else:
                    self.log.exception("Only one 'main' per group allowed.")
            except KeyError:
                if no_main_values.lower() in ['average','mean']:
                    uncertainty[i,0] = np.nanmean(samples[inds], axis=0)
                elif no_main_values.lower() == 'median':
                    uncertainty[i,0] = np.nanmedian(samples[inds], axis=0)
                else:
                    self.log.exception(
                        "'{}' not implemented for no_main_values"
                        .format(no_main_values))
            except:
                self.log.exception(
                    "'main' could not be correctly established.")
            uncertainty[i,1] = np.nanmin(samples[inds], axis=0)
            uncertainty[i,2] = np.nanmax(samples[inds], axis=0)
        #from IPython import embed; embed()
        if squeeze:
            return uncertainty.squeeze()
        else:
            return uncertainty


    ## ATTENTION!
    ## This include does not expand to allow for things like 
    ## 'puff': [4e21, 6e21]
    ## Correct this!
    def indices(self, include, db='mdb'):
        return list(parse_dataframe(get(self, db), include).index)

    
    ##ATTENTION: Wfrong
    #def plot_sample(self, x, obj=None, **kwargs):
    ## ATTENTION: Labels????
    ##ATTENTION: Replace defaults by rcParams
    ## Could be done outside of this!! Move to plot class???
    def plot_1dsample(self, x, obj, **kwargs):
        """
        """
        canvas = priority(['canvas', 'ax'], kwargs, None)
        fargs = priority('args', kwargs, [])
        fkwargs = priority('kwargs', kwargs, [])
        use_nan = priority('use_nan', kwargs, [])
        #ptype = priority(['plot','ptype'], kwargs, 'errorbar')
        ptype = priority(['plot','ptype'], kwargs, 'area')

        pretty=  {}
        prettyline=  {}
        prettyerror=  {}
        prettyarea={}
        color = priority(['color','colour','c'], kwargs, None)
        if color is not None:
            pretty['color'] = color
        prettyerror['ecolor'] = priority(['ecolor','ecolour','ec'], kwargs, None)
        #prettyline['lw'] = priority(['lw','linewidth'], kwargs, 1.0)
        prettyline['lw'] = priority(['lw','linewidth'],
                kwargs, plt.rcParams['lines.linewidth'])
        prettyerror['elinewidth'] = priority(['elw','elinewidth'], kwargs, None)
        prettyline['ls'] = priority(['ls','linestyle'], kwargs, '-')
        prettyerror['capsize'] = priority('capsize', kwargs, 0.0)
        prettyerror['capthick'] = priority('capthick', kwargs, 0.0)

        prettyline['ms'] = priority(['ms','markersize'], kwargs, 0.0)

        pretty['alpha'] = priority(['alpha'], kwargs, 1.0)
        #prettyarea['alpha'] = priority(['alpha_area'], kwargs, 1.0)
        prettyarea['alpha'] = priority(['alpha_area'], kwargs, 0.3)


        if canvas is None:
            #fig, canvas = plt.subplots()
            fig, canvas = initialize_canvas(**kwargs)

        ##FOR NOW
        ##Obj can either be function for sampling or a sample
        #if callable(obj):
        #    y = self.sample(obj,
        #        args=fargs, kwargs=fkwargs, use_nan=use_nan)
        #else:
        #    y = obj
        #FOR NOW

        ## So that only one thing is passed and the other is assumed
        ## to be just range of first index, the not 3 one.
        #if obj = None:
        #    obj = x
        #    x = obj.shape[0]
        if obj.ndim != 2:
            self.log.exception(
                "Obj ndim must be 2, and either one must be of size 3.")
        if obj.shape[0] == 3:
            y = obj.T
        else:
            y = obj

        if y[0,0]<y[0,1] or y[0,0]>y[0,2]:
            y = y.T ## For the case of 3 by 3.



        if ptype=='fill' or ptype=='area':
            artist = []
            artist.append(canvas.fill_between(x, y[:,1], y[:,2],
                **update(pretty, prettyarea)))
            artist.append(canvas.plot(x, y[:,0], **update(pretty, prettyline)))

        else:
            ##Does NOT work with NaN correctly
            prettify= update(pretty, update(prettyline, prettyerror))
            artist =  canvas.errorbar(x, y[:,0],
                yerr=(np.abs(y[:,1:].T-y[:,0])), **prettify)

        return canvas, artist








