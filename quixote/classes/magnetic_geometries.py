# Common modules
import numpy as np

# Special modules
from .mds import MdsData
from .rundir import RundirData
from .structured import StructuredDataRundir, StructuredDataMds

from ..tools import solps_property
from ..tools import create_log

from .timetraces import TimeTraces


alog = create_log('quixote')


### =========== SINGLE NULL CONFIGURATIONS ====================================
class LSN(object):
    magnetic_geometry = 'lsn'
    def __init__(self, *args, **kwargs):
        alog.debug("> Entering LSN __init__")
        super().__init__(*args,**kwargs)
        self.target1 = TimeTraces(self,'target1')
        self.target4 = TimeTraces(self,'target4')

        self.inn = self.target1
        self.out = self.target4

        self.li = self.target1
        self.lo = self.target4
        alog.debug("< Leaving  LSN __init__")

    @solps_property
    def nncut(self):
        self._nncut = np.array([1])

    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = self.nx-2

    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = 1


    #@property
    #def iimp(self):
    #    return max(self.jxi, self.jxa)
    #
    #@property
    #def iomp(self):
    #    return min(self.jxi, self.jxa)

    ## ATTENTION:
    ## Change them to functions so that
    ## cvnames('core') == 1
    ## cvnames(1) == 'core'
    @property
    def cvregions(self):
        return {
            'core':1,
            'sol': 2,
            'inn': 3,
            'out': 4,
            'div': [3,4],
            }

    @property
    def xregions(self):
        return {
            'inn': 1,
            'ixp': 2,
            'oxp': 3,
            'out': 4,
            }

    @property
    def yregions(self):
        if self.grid_type == 'unstructured':
            fcx = 6
        else:
            fcx = 0
        return {
            'core':2+fcx,
            'sep': 4+fcx,
            }




class USN(object):
    magnetic_geometry = 'usn'
    def __init__(self, *args, **kwargs):
        alog.debug("> Entering USN __init__")
        super().__init__(*args,**kwargs)
        self.target1 = TimeTraces(self,'target1')
        self.target4 = TimeTraces(self,'target4')

        self.out = self.target1
        self.inn = self.target4

        self.uo = self.target1
        self.ui = self.target4
        alog.debug("< Leaving  USN __init__")

    @solps_property
    def nncut(self):
        self._nncut = np.array([1])

    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = 1

    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = self.nx-2


    #@property
    #def iimp(self):
    #    return max(self.jxa, self.jxi)
    #
    #@property
    #def iomp(self):
    #    return min(self.jxi, self.jxa)

    @property
    def cvregions(self):
        return {
            'core':1,
            'sol': 2,
            'out': 3,
            'inn': 4,
            'div': [3,4],
            }

    @property
    def xregions(self):
        return {
            'out':1,
            'oxp':2,
            'ixp':3,
            'inn':4,
            'coreconn':5,
            'pfrconn':6,
            }

    @property
    def yregions(self):
        if self.grid_type == 'unstructured':
            fcx = 6
        else:
            fcx = 0
        return {
            'core':2+fcx,
            'sep':4+fcx,
            'pfr':[1+fcx,3+fcx],
            'mcw':[5+fcx,6+fcx,7+fcx],
            'pfrout':1+fcx,
            'pfrinn':3+fcx,
            'mcwout':5+fcx,
            'mcwsol':6+fcx,
            'mcwinn':7+fcx,
            }









class CDN(object):
    magnetic_geometry = 'cdn'
    def __init__(self, *args, **kwargs):
        alog.debug("> Entering CDN __init__")
        super().__init__(*args,**kwargs)

        self.target1 = TimeTraces(self,'target1')
        self.target2 = TimeTraces(self,'target2')
        self.target3 = TimeTraces(self,'target3')
        self.target4 = TimeTraces(self,'target4')

        self.inn  = self.target1
        self.inn2 = self.target2
        self.out2 = self.target3
        self.out  = self.target4

        self.li = self.target1
        self.ui = self.target2
        self.uo = self.target3
        self.lo = self.target4

        alog.debug("< Leaving  CDN __init__")

    @property
    def cvregions(self):
        return {
            'hfs': [1,2,3,4],
            'lfs': [5,6,7,8],

            'core':[1,5],
            'sol': [2,6],
            'out': [8,7],
            'inn': [3,4],

            'div': [3,8],
            'ldiv':[3,8],
            'lout':[8],
            'out': [8],
            'inn': [3],
            'linn':[3],

            'div2':[4,7],
            'udiv':[4,7],
            'uout':[7],
            'out2':[7],
            'uinn':[4],
            'inn2':[4],
            }

    @property
    def xregions(self):
        return {
            'inn': [1],
            'linn':[1],
            'ixp': [2],
            'lixp':[2],
            'ixp2':[3],
            'uixp':[3],
            'inn2':[4],
            'uinn':[4],

            'out2':[5], 
            'uout':[5], 
            'oxp2':[6],
            'uoxp':[6],
            'oxp': [7],
            'loxp':[7],
            'out': [8], 
            'lout':[8], 
            }

    @property
    def yregions(self):
        if self.grid_type == 'unstructured':
            fcx = 12
        else:
            fcx = 0
        return {
            'core':[2+fcx, 9+fcx],
            'sep': [4+fcx, 11+fcx],
            'ipfr': [1+fcx, 3+fcx],
            'opfr': [8+fcx, 10+fcx],
            'imcw': [5+fcx, 6+fcx, 7+fcx],
            'omcw': [12+fcx, 13+fcx, 14+fcx],
            }

    @property
    def xsigns(self):
        return {
            'inn': -1,
            'linn':-1,
            'ixp': -1,
            'lixp':-1,
            'ixp2':+1,
            'uixp':+1,
            'inn2':+1,
            'uinn':+1,

            'out2':-1, 
            'uout':-1, 
            'oxp2':-1,
            'uoxp':-1,
            'oxp': +1,
            'loxp':+1,
            'out': +1, 
            'lout':+1, 
            }

    @property
    def xsign(self):
        return self.xsigns



class Linear(object):
    magnetic_geometry = 'linear'
    def __init__(self, *args, **kwargs):
        alog.debug("> Entering Linear __init__")
        super().__init__(*args,**kwargs)
        #self.target1 = TimeTraces(self,'target1')
        #self.target4 = TimeTraces(self,'target4')

        #self.target = self.target4???
        #self.upstream = self.target1???
        alog.debug("< Leaving  Linear __init__")


    @solps_property
    def nncut(self):
        self._nncut = np.array([1])


    @property
    #def iups(self):
    #def iupst(self):
    def iupstream(self):
        """ For now, I guess"""
        return 1

    @property
    #def itrg(self):
    #def itarg(self):
    def itarget(self):
        """int: Index of the main target (not guarding cell)."""
        return self.nx-2


    @property
    def cvregions(self):
        return {
            'plasma': 1,
            }

    @property
    def xregions(self):
        return {
            'upstream':1,
            'target':2,
            }

    @property
    def yregions(self):
        if self.grid_type == 'unstructured':
            fcx = 2
        else:
            fcx = 0
        return {
            'core':1+fcx,
            'sol':2+fcx,
            }








class Limiter(object):
    magnetic_geometry = 'limiter'
    def __init__(self, *args, **kwargs):
        alog.debug("> Entering LSN __init__")
        super().__init__(*args,**kwargs)
        #self.target1 = TimeTraces(self,'target1')
        #self.target4 = TimeTraces(self,'target4')

        #self.upper = self.target1???
        #self.lower = self.target4???

        alog.debug("< Leaving  LSN __init__")

    @solps_property
    def nncut(self):
        self._nncut = np.array([1])


    @property
    def iupper(self):
        """ int: """
        return 1

    @property
    def ilower(self):
        """ int: """
        return self.nx-2


    @property
    def cvregions(self):
        return {
            'core': 1,
            'sol': 2,
            }

    @property
    def xregions(self):
        return {
            'upper':1,
            'lower':2,
            'pfr-conn':3,
            }

    @property
    def yregions(self):
        if self.grid_type == 'unstructured':
            fcx = 3
        else:
            fcx = 0
        return {
            'core':1+fcx,
            'sep':2+fcx,
            'north':3+fcx,
            }









### =========== RUNDIR MAGNETIG CONFIGURATIONS ================================
# =============================================================================
class RundirDataLSN(RundirData, StructuredDataRundir, LSN):
    """
    Rundir class for Lower Single-null configurations.
    """
    class_type = 'RundirDataLSN'
    def __init__(self, *args, **kwargs):
        alog.debug(">>> Entering Rundir LSN __init__")
        super().__init__(*args,**kwargs)
        alog.debug("<<< Leaving  Rundir LSN __init__")



    @solps_property
    def rlcl(self):
        tmp = np.ones((self.nx, self.ny), dtype=bool)
        tmp[0] = False
        tmp[-1] = False
        tmp[:,0] = False
        tmp[:,-1] = False
        self._rlcl = tmp




#    @solps_property
#    def iixp(self):
#        """int: Index of the inner side of the x-point.
#        First poloidal index inside SOL/Core.
#
#        Notes
#        -----
#        Should it be changed to first in div?
#        """
#        self.log.warning("Untested.")
#        self._iixp = self.rightcut[0]
#
#    @solps_property
#    def ioxp(self):
#        """int: Index of the outer side of the x-point.
#        First poloidal index inside SOL/Core.
#
#        Notes
#        -----
#        Should it be changed to first in div?
#        """
#        self.log.warning("Untested.")
#        self._ioxp = self.leftcut[0]+1








class RundirDataUSN(RundirData, StructuredDataRundir, USN):
    """
    Rundir class for Upper Single-null configurations.
    """
    class_type = 'RundirDataUSN'

    def __init__(self, *args, **kwargs):
        alog.debug(">>> Entering Rundir USN __init__")
        super().__init__(*args,**kwargs)
        alog.debug("<<< Leaving  Rundir USN __init__")


    @solps_property
    def iixp(self):
        """int: Index of the inner side of the x-point.
        First poloidal index inside SOL/Core.

        Notes
        -----
        Should it be changed to first in div?
        """
        self.log.warning("Untested.")
        self._iixp = self.rightcut[0]

    @solps_property
    def ioxp(self):
        """int: Index of the outer side of the x-point.
        First poloidal index inside SOL/Core.

        Notes
        -----
        Should it be changed to first in div?
        """
        self.log.warning("Untested.")
        self._ioxp = self.leftcut[0]+1



    @solps_property
    def masks(self):
        """Class containing different grid masks."""
        self._masks = masks.MasksUSN(self)


    @solps_property
    def rlcl(self):
        tmp = np.ones((self.nx, self.ny), dtype=bool)
        tmp[0] = False
        tmp[-1] = False
        tmp[:,0] = False
        tmp[:,-1] = False
        self._rlcl = tmp

        ## Include 'internal' guarding cells too
        #in1 = np.where(self.region[...,1] == 4)[0][0]
        #in2 = np.where(self.region[...,1] == 5)[0][0]
        #new_field[in1] = new_field[in1-1]
        #new_field[in2-1] = new_field[in2]




class RundirDataLinear(RundirData, StructuredDataRundir, Linear):
    """
    Rundir class for Linear configurations.
    """
    class_type = 'RundirDataLinear'
    def __init__(self, *args, **kwargs):
        alog.debug(">>> Entering Rundir Linear __init__")
        super().__init__(*args,**kwargs)
        alog.debug("<<< Leaving  Rundir Linear __init__")



    @solps_property
    def rlcl(self):
        tmp = np.ones((self.nx, self.ny), dtype=bool)
        tmp[0] = False
        tmp[-1] = False
        tmp[:,0] = False
        tmp[:,-1] = False
        self._rlcl = tmp

    @solps_property
    def isep(self):
        """int: Separatrix is not really defined here, so just the center."""
        self._isep = 1




class RundirDataLimiter(RundirData, StructuredDataRundir, Limiter):
    """
    Rundir class for Limiter configurations.
    """
    class_type = 'RundirDataLimiter'
    def __init__(self, *args, **kwargs):
        alog.debug(">>> Entering Rundir Limiter __init__")
        super().__init__(*args,**kwargs)
        alog.debug("<<< Leaving  Rundir Limiter __init__")



    @solps_property
    def rlcl(self):
        tmp = np.ones((self.nx, self.ny), dtype=bool)
        tmp[0] = False
        tmp[-1] = False
        tmp[:,0] = False
        tmp[:,-1] = False
        self._rlcl = tmp

    @solps_property
    def isep(self):
        """int: Index of the LCFS"""
        self._isep = self.ny//2

    @property
    def ilcfs(self):
        return self.isep








class MdsDataUSN(MdsData, StructuredDataMds, USN):
    """
    Rundir class for Upper Single-null configurations.
    """
    class_type = 'RundirDataUSN'

    def __init__(self, *args, **kwargs):
        alog.debug(">>> Entering Rundir USN __init__")
        super().__init__(*args,**kwargs)
        alog.debug("<<< Leaving  Rundir USN __init__")


    @solps_property
    def iixp(self):
        """int: Index of the inner side of the x-point.
        First poloidal index inside SOL/Core.

        Notes
        -----
        Should it be changed to first in div?
        """
        self.log.warning("Untested.")
        self._iixp = self.rightcut[0]

    @solps_property
    def ioxp(self):
        """int: Index of the outer side of the x-point.
        First poloidal index inside SOL/Core.

        Notes
        -----
        Should it be changed to first in div?
        """
        self.log.warning("Untested.")
        self._ioxp = self.leftcut[0]+1



    @solps_property
    def masks(self):
        """Class containing different grid masks."""
        self._masks = masks.MasksUSN(self)



    @solps_property
    def leftcut(self):
        self._leftcut = np.array(
            [np.where(self.region[...,1]==2)[0][0].astype(int) -1])

    @solps_property
    def rightcut(self):
        self._rightcut = np.array(
           [np.where(self.region[...,1]==3)[0][0].astype(int) -1])

    @solps_property
    def topcut(self):
        self._topcut = np.array(
            [np.where(self.region[...,2]==4)[1][0].astype(int) -1])

    @solps_property
    def bottomcut(self):
        self._bottomcut = np.array([-1])









## Make class DDN!
class DDN(object):
    magnetic_geometry = 'ddn'
    def __init__(self, *args, **kwargs):
        alog.debug(">>> Entering DDN __init__")
        super().__init__(*args,**kwargs)
        self.target1 = TimeTraces(self,'target1')
        self.target2 = TimeTraces(self,'target2')
        self.target3 = TimeTraces(self,'target3')
        self.target4 = TimeTraces(self,'target4')

        self.inn  = self.target1
        self.inn2 = self.target2
        self.out2 = self.target3
        self.out  = self.target4

        self.li = self.target1
        self.ui = self.target2
        self.uo = self.target3
        self.lo = self.target4
        alog.debug(">>> Leaving DDN __init__")

    ## CHECK THESE ARE CDN
    @property
    def cvregions(self):
        return {
            'hfs': [1,2,3,4],
            'lfs': [5,6,7,8],

            'core':[1,5],
            'sol': [2,6],
            'out': [8,7],
            'inn': [3,4],

            'div': [3,8],
            'ldiv':[3,8],
            'lout':[8],
            'out': [8],
            'inn': [3],
            'linn':[3],

            'div2':[4,7],
            'udiv':[4,7],
            'uout':[7],
            'out2':[7],
            'uinn':[4],
            'inn2':[4],
            }

    ## CHECK THESE ARE CDN
    @property
    def xregions(self):
        return {
            'inn': [1],
            'linn':[1],
            'ixp': [2],
            'lixp':[2],
            'ixp2':[3],
            'uixp':[3],
            'inn2':[4],
            'uinn':[4],

            'out2':[5], 
            'uout':[5], 
            'oxp2':[6],
            'uoxp':[6],
            'oxp': [7],
            'loxp':[7],
            'out': [8], 
            'lout':[8], 
            }

    ## CHECK THESE ARE CDN
    @property
    def yregions(self):
        if self.grid_type == 'unstructured':
            fcx = 12
        else:
            fcx = 0
        return {
            'core':[2+fcx, 9+fcx],
            'sep': [4+fcx, 11+fcx],
            'pfr': [1+fcx, 3+fcx],
            'mcw': [5+fcx, 6+fcx, 7+fcx],
            }






class DDNU(object):
    magnetic_geometry = 'ddnu'
    def __init__(self, *args, **kwargs):
        alog.debug(">>> Entering DDNU __init__")
        super().__init__(*args,**kwargs)
        self.target1 = TimeTraces(self,'target1')
        self.target2 = TimeTraces(self,'target2')
        self.target3 = TimeTraces(self,'target3')
        self.target4 = TimeTraces(self,'target4')

        self.inn2 = self.target1
        self.inn  = self.target2
        self.out  = self.target3
        self.out2 = self.target4

        self.li = self.target1
        self.ui = self.target2
        self.uo = self.target3
        self.lo = self.target4
        alog.debug(">>> Leaving DDNU __init__")

    @solps_property
    def nncut(self):
        self._nncut = np.array([2])

    ### ATTENTION: ONLY FOR STRUCTURED???
    @solps_property
    def isep(self):
        """int: Index of the first flux surface in the inner SOL"""
        self._isep = np.min(self.topcut) + 1

    ### ATTENTION: ONLY FOR STRUCTURED???
    @solps_property
    def isep2(self):
        """int: Index of the first flux surface in the outer SOL"""
        self._isep2 = np.max(self.topcut) + 1

    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = np.where(self.region[...,1] == 5)[0][0]

    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = np.where(self.region[...,1] == 4)[0][0]-1


    @solps_property
    def iout2(self):
        """int: Index of the secondary outer target (not guarding cell)."""
        self._iout2 = np.where(self.region[...,1] == 8)[0][0]-1

    @solps_property
    def iinn2(self):
        """int: Index of the secondary inner target (not guarding cell)."""
        self._iinn2 = np.where(self.region[...,1] == 1)[0][0]

    @property
    def ioxp(self):
        #""" Outer index of main x-point, first pol. cell inside divertor."""
        """ Outer index of main x-point,
        first pol. cell inside SOL/Core."""
        #return self.rightcut[1] #In div NO
        return self.rightcut[1]+1 #In SOL YES

    @property
    def ioxp2(self):
        #""" Outer index of secondary x-point, first pol. cell inside divertor."""
        """ Outer index of secondary x-point,
        first pol. cell inside SOL/Core."""
        #return self.rightcut[0]+1 #In div NO
        return self.rightcut[0] #In SOL YES

    @property
    def iixp(self):
        #""" Inner index of main x-point, first pol. cell inside divertor."""
        """ Inner index of main x-point,
        first pol. cell inside SOL/Core."""
        #return self.leftcut[1]+1 #In div NO
        return self.leftcut[1] #In SOL YES

    @property
    def iixp2(self):
        #""" Inner index of secondary x-point, first pol. cell inside divertor."""
        """ Inner index of secondary x-point,
        first pol. cell inside SOL/Core."""
        #return self.leftcut[0] #In div NO
        return self.leftcut[0]+1 #In SOL YES


    #In test phase yet:
    @solps_property
    def masks(self):
        """Class containing different grid masks."""
        self._masks = masks.MasksDDNU(self)


    #ATTENTION: Improve docstrig
    #ATTENTION: Review logic
    def _extend(self, field):
        """
        Similar to b2plot/extend.F but only for grid dimensions.
        Should be similar to, but not implemented (why waste time right now?)
        For now, just use it for the standard case of connected spaces
        """
        # As for SN configurations
        ndim = list(field.shape)
        ndim[0], ndim[1] = ndim[0]+2, ndim[1]+2
        ndim = tuple(ndim)
        new_field = np.zeros(ndim)
        new_field[1:-1,1:-1] = field
        new_field[0] = new_field[1]
        new_field[-1] = new_field[-2]
        new_field[:,0] = new_field[:,1]
        new_field[:,-1] = new_field[:,-2]

        # Include 'internal' guarding cells too
        in1 = np.where(self.region[...,1] == 4)[0][0]
        in2 = np.where(self.region[...,1] == 5)[0][0]
        new_field[in1] = new_field[in1-1]
        new_field[in2-1] = new_field[in2]

        return new_field





# =============================================================================
class RundirDataDDNU(RundirData, StructuredDataRundir, DDNU):
    """
    Rundir class for Disconnected Double Null Up configurations.
    """
    class_type = 'RundirDataDDNU'

    def __init__(self, *args, **kwargs):
        alog.debug(">>> Entering Rundir DDNU __init__")
        super().__init__(*args,**kwargs)

        alog.debug("<<< Leaving  Rundir DDNU __init__")


# =============================================================================

class RundirDataDDN(RundirDataDDNU):
    """
    Rundir class for Disconnected Double Null configurations
    """


# =============================================================================








##################### Connected Double-Null ###################################


# =============================================================================
class RundirDataCDN(RundirData, StructuredDataRundir, CDN):
    """
    Rundir class for Connected Double Null configurations.
    """
    class_type = 'RundirDataCDN'
    #magnetic_geometry = 'cdn'

    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        ##ATTENTION: This should be before super, but gives problems!

        #self.target1 = rundirtt.TargetData(self,'target1')
        #self.target2 = rundirtt.TargetData(self,'target2')
        #self.target3 = rundirtt.TargetData(self,'target3')
        #self.target4 = rundirtt.TargetData(self,'target4')

        #self.inn  = self.target1
        #self.inn2 = self.target2
        #self.out2 = self.target3
        #self.out  = self.target4

        #self.li = self.target1
        #self.ui = self.target2
        #self.uo = self.target3
        #self.lo = self.target3

        ##Are these names correct for CDN?
        #self._find_file('dstr')
        #self._find_file('dstl')

    @solps_property
    def nncut(self):
        self._nncut = np.array([2])

    @solps_property
    def isep(self):
        """int: Index of the first flux surface in the inner SOL"""
        self._isep = np.min(self.topcut) + 1


    ## Main or lower??
    @solps_property
    def iout(self):
        """int: Index of the main outer target (not guarding cell)."""
        self._iout = np.where(self.region[...,1] == 8)[0][0]-1

    ## Main or lower??
    @solps_property
    def iinn(self):
        """int: Index of the main inner target (not guarding cell)."""
        self._iinn = np.where(self.region[...,1] == 1)[0][0]


    ## Secondary or upper??
    @solps_property
    def iout2(self):
        """int: Index of the secondary outer target (not guarding cell)."""
        self._iout2 = np.where(self.region[...,1] == 5)[0][0]

    ## Secondary or upper??
    @solps_property
    def iinn2(self):
        """int: Index of the secondary inner target (not guarding cell)."""
        self._iinn2 = np.where(self.region[...,1] == 4)[0][0]-1

    @property
    def ioxp(self):
        """ Outer index of main x-point, first pol. cell inside divertor."""
        return self.rightcut[0]+1

    @property
    def ioxp2(self):
        """ Outer index of secondary x-point, first pol. cell inside divertor."""
        return self.rightcut[1]

    @property
    def iixp(self):
        """ Inner index of main x-point, first pol. cell inside divertor."""
        return self.leftcut[0]

    @property
    def iixp2(self):
        """ Inner index of secondary x-point, first pol. cell inside divertor."""
        return self.leftcut[1]+1

    ## ATTENTION: masks not implemented yet?
#    @solps_property
#    def masks(self):
#        """Class containing different grid masks."""
#        self._masks = masks.MasksCDN(self)

    @solps_property
    def rlcl(self):
        tmp = np.ones((self.nx, self.ny), dtype=bool)
        tmp[0] = False
        tmp[-1] = False
        tmp[:,0] = False
        tmp[:,-1] = False
        in1 = np.where(self.region[...,1] == 4)[0][0]
        in2 = np.where(self.region[...,1] == 5)[0][0]
        tmp[in1] = False
        tmp[in2-1] = False
        self._rlcl = tmp

# =============================================================================

