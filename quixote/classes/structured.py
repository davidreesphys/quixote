# Common modules:
import os
import numpy as np
import itertools


## Quixote modules:
from .base import BaseData
from ..tools import solps_property
from ..tools import update
from ..tools import create_log
from ..tools import try_block
from ..tools import solps_property2

#Extensions


alog = create_log('quixote')

class StructuredDataBase(BaseData):
    """
    """

    grid_type = 'structured'
    ns_axis = 2 #Usual axis of species index
    __DOCPARAMS__ ={
        'GRID_DIMS': 'nx, ny',
        'FACE_DIMS': 'nx, ny',
        }
    
    def __init__(self, *args, **kwargs):
        alog.debug(">> Entering Structured __init__")
        super().__init__(*args, **kwargs)
        alog.debug("<< Leaving  Structured __init__")



##  EXTENSIONS ================================================================
    ###ATTENTION: WRong?? LSN??
    #@solps_property
    #def masks(self):
    #    """C.f. :class:`classes.masks.MasksLSN`"""
    #    self._masks = masks.MasksLSN(self)

    ###ATTENTION: Keep? Maybe data.GridDataPlot('ne') is also cool.
    #@solps_property
    #def plot(self):
    #    """C.f. :class:`plots.core.Plots`"""
    #    self._plot = Plots(self)

    #@solps_property
    #def chords(self):
    #    #"""A dictionary containing lines of sight as Chords() objects."""
    #    """C.f. :class:`extensions.chords.Chords`"""
    #    self._chords = Chords(self)

    ###ATTENTION: Does this even work?? The answer seems to be no.
    #@solps_property
    #def spectroscopy(self):
    #    #"""An object for calculation & plotting of spectroscopic ADAS data."""
    #    """C.f. :class:`extensions.spectroscopy.Spectroscopy`"""
    #    self._spectroscopy = Spectroscopy(self)




##  IDENT PROPERTIES ==========================================================





##  GEOMETRY ==================================================================

    # -------------------- Dimensions -----------------------------------------
    # nx, ny, ns, natm, nmol and nion are read with self._get_basic_dimensions()


    # -------------------- Structure ------------------------------------------






    ## TODO: Use geometry file first to see the cuts and calculate the xpoint better.
    ## This only shows ONE of the xpoints, not the two of them.
    @solps_property
    def xpoint(self):
        """(float, float): R and z coordinates of the x-point of B2 grid."""
        # The x-point is the only grid vertex where 8 lines converge, i.e.
        # which is shared by 8 grid cells. => Count the occurrence of all
        # vertices, the one which appears 8 times is the x-point.
        allvertices = {}
        for nx in range(self.nx):
            for ny in range(self.ny):
                cell = self.grid[nx,ny] # [nvertex,R|z]
                for v in range(len(cell)):
                    vertex = str(cell[v,0]) + ' ' + str(cell[v,1])
                    allvertices[vertex] = allvertices.get(vertex,0) + 1

        xpoint = list(allvertices.keys())[list(allvertices.values()).index(8)]
        xpoint = np.array([float(xpoint.split()[0]),
                           float(xpoint.split()[1])])
        self._xpoint = xpoint



    #ATTENTION: Create test.
    #ATTENTION: Is the definition correct?
    #ATTENTION: Maybe make this a function and accept ix='iomp' as parameter?
    @solps_property
    def pedestal_top(self):
        """int: Radial index of the midplane pedestal top position calculated
        as the minimum of the  second derivative of the plasma pressure."""
        ddyp = self.gradient(self.gradient(self.ptot)[...,1])[...,1][self.iomp,:]
        self._pedestal_top = np.argmin(ddyp)





    @solps_property
    def grid(self):
        """(nx, ny, 4, 2): Corners of B2 grid cells.

        Notes
        -----
        [grid] = ix, iy, vertex, 0:R or 1:z.


        2 ---T--- 3
        |         |
        L         R
        |         |
        0 ---B--- 1


        """
        self._grid = np.zeros((self.nx,self.ny,4,2))
        for i in range(self.nx):
            for k in range(self.ny):
                self._grid[i,k,:,0] = np.array([self.r[i,k,0], self.r[i,k,1],
                                                self.r[i,k,3],self.r[i,k,2]])
                self._grid[i,k,:,1] = np.array([self.z[i,k,0], self.z[i,k,1],
                                                self.z[i,k,3],self.z[i,k,2]])

#    #ATTENTION: Improve docstring
#    #ATTENTION: Needs revision?? Is it even needed? No logic will be used here
#    @solps_property
#    def magnetic_geometry(self):
#        """
#        This function check if the current case is a LSN or an USN.
#        In the future, DN in all the forms should be checked too.
#        It requires certain knowledge about the case:
#        self.z or self.cz
#        self.sep (maybe not required)
#        It will default to None, so users are aware that the check was not
#        successful.
#        If magnetic geometry is passed as a kwarg with creation, then set
#        the _ manually during initialization to prevent the check.
#
#        Another check could be to test self.b[:,:,2] which is Bt:
#        The self.sep target with the smaller abs strengh is the lfs
#        The self.sep target with the higher  abs strengh is the hfs
#
#        Then, if targ1 is hfs and targ4 is hfs, then USN
#
#        This could also serve for more complex DN:
#        Get the four strike points, two lfs two hfs, two up two down.
#
#        Probably, an exclusive and explicit way to obtain cz and z only to
#        test is needed, as try_block will now use LSN or USN information.
#        Also, a new generating function will be needed because it cannot
#        be a try_block property
#
#
#        DDN and DDNU must be valid outputs of magnetic geometry too
#        """
#        try:
#            pos = self.cz[:,self.sep]
#        except:
#            try:
#                pos = self.z[:,self.sep,0]
#            except:
#                self._magnetic_geometry = None
#                return
#
#        if (pos.argmin() == 0) or (pos.argmin() == len(pos)-1):
#            self._magnetic_geometry = 'lsn'
#        else:
#            self._magnetic_geometry = 'usn'





    @solps_property
    def hx(self):
        """(nx, ny): Scale factors in the x-direction [m].

        Notes
        -----
            Corresponds to the lenght of the cell.
        """
        self._read_signal('_hx', 'hx')

    @solps_property
    def hy(self):
        """(nx, ny): Scale factors in the y-direction [m].

        Notes
        -----
            Corresponds to the (uncorrected) width of the cell.
            :py:attr:`~hy1` should be used instead.
            """
        self._read_signal('_hy', 'hy')

    @solps_property
    def hy1(self):
        """(nx,ny): Corrected scale factors in the y-direction [m]."""
        self._hy1 = self.hy*self.qz[:,:,1]



    @solps_property
    def sx(self):
        """(nx, ny): Area of the x-face between cell (ix,iy) and its left
        neighbour (ix-1,iy) [m^2].
        """
        self._sx = self.gs[...,0]

    @solps_property
    def sy(self):
        """(nx, ny): Area of the y-face between cell (ix,iy) and its bottom
        neighbour (ix,iy-1) [m^2].
        """
        self._sy = self.gs[...,1]

    @solps_property
    def gs(self):
        """(nx, ny, 3): Area of faces between cell (ix,iy) and its left and
        bottom neighbours [m^2].

        gs[...,0] left x-face (left face).

        gs[...,1] bottom y-face (bottom face).

        Notes
        -----
        gs[...,2] corresponding to SZ is still missing!
        But:
            >>> np.allclose(shot.gs[...,:-1], run.gs[...,:-1])
            >>> True
        """
        self._read_signal('_gs','gs')



    @solps_property2
    def pbs(self):
        """ ({GRID_DIM}, 2): Parallel contact area [m^2]
        0: (bx/bb)*sx
        1: (by/bb)*sy
        """


    @property
    def pbsx(self):
        return self.pbs[...,0]

    @property
    def pbsy(self):
        return self.pbs[...,1]





    @solps_property
    def dsrad(self):
        """(nx,ny): Radial distance wrt separatrix [m].

        Notes
        -----
            Somewhat different results than self implemented
            :py:attr:`~ds`.
        """
        self._read_signal('_dsrad', 'dsrad')




    #ATTENTION: should this stay?
    #ATTENTION: Add docstrig
    @solps_property
    def ipardspar(self):
        """(nx,ny): Parallel distance with respect to first poloidal cell
        in the computational grid layout [m].

        Notes
        -----
        This is a manual implementation to be used for cases in which
        dspar is missing or wrongly calculated.
        """
        self._ipardspar = np.zeros((self.nx,self.ny))
        for k in range(self.ny):
          self._ipardspar[0,k]=self.hx[0,k]/2
          for j in range(1,self.nx):
              self._ipardspar[j,k] = (self._ipardspar[j-1,k]+
                  (self.hx[j-1,k]*self.invpitch[j-1,k]+
                   self.hx[j,k]*self.invpitch[j,k])/2)

    @solps_property
    def ds(self):
        """(nx,ny) : Radial distance from the separatrix [m].

        Notes
        -----
        This is a manual implementation.
        """
        self._ds = np.zeros((self.nx,self.ny))
        proxy = np.zeros((self.nx,self.ny))
        for i in range(self.nx):
            proxy[i,0] = 0
            for k in range(1,self.ny):
                proxy[i,k] = (proxy[i,k-1] +
                              np.sqrt((self.cr[i,k] - self.cr[i,k-1])**2 +
                                      (self.cz[i,k] - self.cz[i,k-1])**2))
            ds_offset = (proxy[i,self.isep-1] + proxy[i,self.isep])/2
            self._ds[i,:] = proxy[i,:] - ds_offset


    #ATTENTION: Required? Wouldn't it be better to have poloidal distance?
    @solps_property
    def domp(self):
        """(nx,ny) : Poloidal distance from the outer midplane
                     along x-direction [m]."""
        # I think this is both more correct and much
        # simpler/more elegant than the old version:
        self.log.warning("Deprecation Warning")
        self._domp = np.array([np.sum(self.hx[0:nx,:],0) + self.hx[nx,:]/2
                               for nx in range(self.nx)])
        self._domp = self._domp - self._domp[self.omp,:]
#   @solps_property
#   def domp_old(self):
#       """Distance from the outer midplane along x-direction."""
#       self._domp_old = np.zeros((self.nx,self.ny))
#       proxy = np.zeros((self.nx,self.ny))
#       for ny in range(self.ny):
#           proxy[0,ny] = 0
#           for nx in range(1,self.nx):
#               a = np.array([self.cr[nx  ,ny],self.cz[nx  ,ny]])
#               b = np.array([self.cr[nx-1,ny],self.cz[nx-1,ny]])
#               d = np.linalg.norm(b-a)
#               proxy[nx,ny] = proxy[nx-1,ny] + d
#           domp_offset = proxy[self.omp,ny]
#           self._domp_old[:,ny] = proxy[:,ny] - domp_offset

    #ATTENTION: Required? Wouldn't it be better to have poloidal distance?
    @solps_property
    def domp_parallel(self):
        """(nx,ny) : Parallel distance from the outer midplane
                     along x-direction (i.e. parallel to B) [m]."""
        self.log.warning("Deprecation Warning")
        hx_par = self.hx*np.abs(self.bb[...,3]/self.bb[...,0])
        dp = np.array([np.sum(hx_par[0:nx,:],0) + hx_par[nx,:]/2
                       for nx in range(self.nx)])
        self._domp_parallel = dp - dp[self.omp,:]





    #ATTENTION: which one is the most correct one??
    #ATTENTION: Add docstrig
    @solps_property
    def pitch(self):
        self._pitch = np.ones((self.nx,self.ny))
        bm = (self.b[:,:,2] != 0)
        for i in range(self.nx):
          for j in range(self.ny):
            if bm[i,j]:
              self._pitch[i,j] = np.arctan(np.abs(self.b[i,j,0]/self.b[i,j,2]))

    #ATTENTION: which one is the most correct one??
    #ATTENTION: Add docstrig
    @solps_property
    def pitch_pt(self):
        self._pitch_pt = np.ones((self.nx,self.ny))
        bm = (self.b[:,:,2] != 0)
        for i in range(self.nx):
          for j in range(self.ny):
            if bm[i,j]:
              self._pitch_pt[i,j] = np.abs(self.b[i,j,0]/self.b[i,j,2])

    #ATTENTION: which one is the most correct one??
    #ATTENTION: Add docstrig
    @solps_property
    def pitch_tot(self):
        self._pitch_tot = np.ones((self.nx,self.ny))
        bm = (self.b[:,:,3] != 0)
        for i in range(self.nx):
          for j in range(self.ny):
            if bm[i,j]:
              self._pitch_tot[i,j] = np.abs(self.b[i,j,0]/self.b[i,j,3])

    #ATTENTION: Add docstrig
    @solps_property
    def invpitch(self):
        """(nx,ny): Inverse of pitch angle (1/alpha)."""
        self._invpitch = 1./self.pitch #Add protection against 1/0

    #ATTENTION: Necessary?
    #ATTENTION: Add docstrig
    @solps_property
    def parallel_surface_m(self):
        """(nx, ny): Parallel surface at cell center [m^2]."""
        self._parallel_surface_m = (self.dv/self.hx)*self.pitch_tot

    @solps_property
    def parallel_surface(self):
        """(nx, ny): Parallel surface at cell center [m^2]."""
        self._parallel_surface = (self.dv/self.hx)*self.pitch_tot



    @solps_property
    def qz(self):
        """(nx, ny, 2): Complementary angle, t, between x- and y-directions.

        qz[...,0]: sin(t) => inner product of x and y basis vectors.
                    It vanishes for orthogonal geometries.
        qz[...,1]: cos(t).
        """
        self._read_signal('_qz', 'qz')

    @solps_property
    def vol(self):
        """(nx,ny): Cell volume [m^3]."""
        self._read_signal('_vol', 'vol')


    # -------------------- Indices and locators -------------------------------






## SNAPSHOT ===================================================================
    # -------------------------------------------------------------------------

    # -------------------- Plasma Characterization ----------------------------
    @solps_property
    def za(self):
        """(ns): Atom charge [e-]."""
        self._read_signal('_za', 'za')


    @solps_property
    def bb(self):
        """(nx, ny, 4): Magnetic field components [T].

        bb[...,0]: x component (Poloidal)

        bb[...,1]: y component (Radial)

        bb[...,2]: z component (Toroidal)

        bb[...,3]: Total ( =sqrt(x^2+y^2+z^2) )
        """
        self._read_signal('_bb', 'bb')

    @property
    def b(self):
        #""" Alias for self.bb"""
        """ Alias for :py:attr:`~bb`"""
        return self.bb


    @solps_property2
    def fpsi(self):
        """({GRID_DIMS}, 4): Potential magnetic field components bx,by.
        """

    @solps_property
    def psisep(self):
        """ Shouldn't it be the fpsi of the vertex in the separatrix?
        I think that because jsep is isep-1
        """
        #self._psisep = np.average(self.fpsi[self.iomp, self.isep])
        #self._psisep = 0.5*(self.fpsi[self.iomp, self.isep,2]+
        #    self.fpsi[self.iomp, self.isep,3])
        self._psisep = 0.5*(self.fpsi[self.iomp, self.isep,0]+
            self.fpsi[self.iomp, self.isep,1])

    @solps_property
    def npsi(self):
        """ Normalized PSI []
        Why do I need the - sign???
        With it, it works.
        Should it be defined at the OMP and then have only dimensions
        of iy??
        """
        psi = np.average(self.fpsi,axis=2)-self.psisep
        psign = np.sign(psi[self.iomp,-1]-psi[self.iomp,0])
        poffset = 1.0
        #pnorm = self.psisep
        pnorm = np.abs(self.psisep)

        self._npsi = (psi*psign/pnorm)+poffset

        

    @solps_property
    def rhopol(self):
        """({GRID_DIMS}): I think one can calculate this from fpsi and ffbz,
        procedure maybe in preprocessing/b2agbf.f
        Actually maybe in b2plot/fy.f since mpsi is norm psi
        """
        self._rhopol = np.sqrt(self.npsi)


    @solps_property2
    def ffbz(self):
        """({GRID_DIMS}, 4): Potential magnetic field components bz.
        """


    # -------------------- Densities ------------------------------------------

    ##ATTENTION: Doesn't seem to exist??
    @solps_property2
    def ne2(self):
        """({GRID_DIMS}):
        """

    
    # -------------------- Temperatures ---------------------------------------
    

    # -------------------- Collisionalities -----------------------------------

    ## Adapt to be used by Classical too
    def collisionality(self, upstream, downstream, species='e'):
        """ Provides the stimation of the collisoinality (nu*) nL/T^2
        using the parallel distance between upstream and downstream.

        Parameters
        ----------
            upstream: int
                Upstream poloidal index.
            downstream: int
                Upstream poloidal index.
            species: {'e', 'i'} or list(int)
                Species for which to calculate the collisionality.
                Default to electrons, 'e'

        Returns
        -------
        (ny):
            Collisionality nu* for each flux tube.
        """
        dspar = np.abs(self.ipardspar[upstream]-self.ipardspar[downstream])
        
        if isinstance(species, str) and species=='e':
            return self.ne[upstream]*dspar/np.power(
                self.te[upstream],2)*1e-16

        #in the future, allow list(int) to sum over species in the list.
        #While i is all the ions.
        elif isinstance(species, str) and species=='i':
            return self.na[upstream,:,1]*dspar/np.power(
                self.ti[upstream],2)*1e-16
        else:
            return np.zeros((self.nx, self.ny))






    # -------------------- Pressures ------------------------------------------


    @solps_property
    def pressure_gradient_force(self):
        """(nx, ny, ns): Parallel pressure gradient force acting on
        the whole cell volume element [N].

        Notes
        -----
        For the comparison of forces force-densities (force/self.vol)
        should be used since the total force scales with the volume of
        the considered volume element (i.e. with the cell volume)
        """

        # F = M * a
        #   = M * (-1/sum(mi*na) * grad p)
        #   = vol*mi*na * (-1/sum(mi*na) * grad p)
        #   = - vol*mi*na*grad p / sum(mi*na)
        # (with the total mass density rho = sum(mi*na))

        # Only the static pressure has to be used here:
        # (see, e.g., Stangeby (2000), Chapter 1.8.1 and Fig. 1.21)
        dp  = self.gradient(self.pstot)[...,2]     # (nx,ny)
        dp  = np.expand_dims(dp,2)                 # (nx,ny,1)
        vol = np.expand_dims(self.vol,2)           # (nx,ny,1)
        mi  = self.mi                              # (nx,ny,ns)
        # Only works for not b2standalone
        na  = self.na#_eirene                      # (nx,ny,ns)
        rho = np.expand_dims(np.sum(mi*na,2),2)    # (nx,ny,1)
        self._pressure_gradient_force = -vol*mi*na*dp/rho

    # -------------------- Velocities -----------------------------------------

    @solps_property2
    def vedia(self):
        """({FACE_DIMS}, 2) : Electrons effective drift velocity [m/s]"""


    @solps_property2
    def veecrb(self):
        """({FACE_DIMS}, 2) : Electrons ExB drift velocity [m/s]"""

    @solps_property2
    def vadia(self):
        """({FACE_DIMS}, 2, ns) : Ions effective drift velocity [m/s]"""


    @solps_property2
    def vaecrb(self):
        """({FACE_DIMS}, 2, ns) : Ions ExB drift velocity [m/s]"""

    # -------------------- Fluxes and energies --------------------------------

    @solps_property2
    def fhet(self):
        """({GRID_DIMS}, 2): Total electron energy flux [??]
        """

    @solps_property2
    def fhit(self):
        """({GRID_DIMS}, 2): Total ion energy flux [??]
        """

#    @solps_property
#    def fet(self):
#    def fhint(self): ##Better name??
#        """ I found this in modules/b2mod_b2mwti.F:3351
#        It is a way to talculate the total internal energy,
#        as opposed to FT, the total energy.
#        """



    # -------------------- Coefficients ---------------------------------------
    @solps_property2
    def dna0(self):
        """ Dperp"""

    @property
    def dperp(self):
        """Alias for :py:attr:`~dna0`."""
        return self.dna0

    @solps_property2
    def hce0(self):
        """ ({GRID_DIMS}): kye*ne[1/sm]"""

    @property
    def kyeperp(self):
        """ ({GRID_DIMS}): Electron heat diffusivity [m^2/s]"""
        return self.hce0/self.ne


    @solps_property2
    def hci0(self):
        """ Sum of all kyi*na"""

    @solps_property2
    def hcib(self):
        """ kyi*na"""

    @property
    def kyiperp(self):
        """ ({GRID_DIMS}, ns): Ion heat diffusivity [m^2/s]"""
        return self.hcib/self.na

    @solps_property
    def rpt(self):
        """({GRID_DIMS}, ns): Cumulative ionisation potential to stage (is) [eV].
        """
        self._read_signal('_rpt',  'rpt')

    # -------------------- Rates, losses/sources and residuals ----------------


    @solps_property2
    def sne(self):
        """ ({GRID_DIMS}, 2):  Components of the electron
        particle source [1/s].
        """

    @solps_property2
    def electron_source(self):
        """ ({GRID_DIMS}): Electron source per species [1/s].
        Corresponding to sne(...,0,:)+ne*sne(...,1,:)
        """
        self._electron_source = self.sne[...,0] + self.sne[...,1]*self.ne




    #ATTENTION: Are they correct??
    @solps_property
    def rqahe(self):
        """({GRID_DIMS}, ns) : Electron cooling rate [W]"""
        self._read_signal('_rqahe', 'rqahe')

    @solps_property
    def rqbrm(self):
        """({GRID_DIMS}, ns) : Bremsstrahlung radiation rate [W]"""
        self._rqbrm = (self.ne.T
            *self.na.T
            #*self.naeir.T ##Alternative?
            *self.vol.T
            *self.rlcl.T
            *self.rbr.T).T

    @solps_property
    def rqrad(self):
        """({GRID_DIMS}, ns) : B2 line radiation rate [W]

        Notes
        -----
        Use :py:attr:`~line_radiation` for runs with Eirene.
        """
        #tmp = self.read_switch('b2xxxx_phm0', default=1.0)
        self._rqrad = (self.ne.T
            *self.na.T
            #*self.naeir.T #Alternative for better approx?
            *self.vol.T
            *self.rlcl.T
            *self.rrd.T).T

    ##ATTENTION: Might not be right anymore.
    @solps_property
    def line_radiation(self):
        """({GRID_DIMS}, ns): Corrected line radiation rate
        (:py:attr:`~rqrad`) [W].

        Notes
        -----
        Manual stimation using Eirene atomic densities instead
        of B2 neutral fluids.
        """
        self._line_radiation = self.rqrad.copy()
        natm = 0
        for i, z in enumerate(self.za):
            if z == 0:
                self._line_radiation[:,:,i] = ( self.rqrad[:,:,i] /
                                                self.na[:,:,i] *
                                                self.dab2[:,:,natm] )
                natm += 1

    @solps_property
    def rrahi(self):
        """ ({GRID_DIMS}, ns): Recombination ion energy losses [W]"""
        self._read_signal('_rrahi', 'rrahi')

    @solps_property
    def rramo(self):
        """ ({GRID_DIMS}, ns): Recombination ion momentum losses [ms^-2]"""
        self._read_signal('_rramo', 'rramo')

    @solps_property
    def rrana(self):
        """ ({GRID_DIMS}, ns): Recombination ion particle losses [1/s]"""
        self._read_signal('_rrana', 'rrana')

    @solps_property
    def rsahi(self):
        """ ({GRID_DIMS}, ns): Ionization ion energy losses [W]"""
        self._read_signal('_rsahi', 'rsahi')

    @solps_property
    def rsamo(self):
        """ ({GRID_DIMS}, ns): Ionization ion momentum losses [ms^-2]"""
        self._read_signal('_rsamo', 'rsamo')

    @solps_property
    def rsana(self):
        """ ({GRID_DIMS}, ns): Ionization ion particle losses [1/s]"""
        self._read_signal('_rsana', 'rsana')

    @solps_property
    def rsana_b2stbr(self):
        """ ({GRID_DIMS}, ns) : Ionization ion and neutral particle losses [1/s].

        Notes
        -----
        For the Ionization rates of the neutrals the recycling sources of
        the singly ionized charge states :py:attr:`~b2stbr_sna` are used.
        """
        # This should be the correct thing to use I think. Also
        # the particle balance works well with this quantity.
        self._rsana_b2stbr = self.rsana.copy()
        for ns in range(self.ns):
            if (self.za[ns] == 0 
                and ns+1 < self.ns
                and self.zn[ns] == self.zn[ns+1]):
                self._rsana_b2stbr[:,:,ns] = self.b2stbr_sna[:,:,ns+1]

    ### ATTENTION: Wrong?? It says rescaled neutrals but they are not.
    @solps_property
    def rsana_rna(self):
        """ ({GRID_DIMS}, ns) : Rescaled ionization ion particle losses [1/s].

        Notes
        -----
        The neutrals are rescaled using the EIRENE neutral density:
        rsana_rna = rsana*dab2/na for the neutrals.
        No, they are not. Should they?
        """
        self._rsana_rna = self.rsana.copy()
        natm = 0
        for i, z in enumerate(self.za):
            if z == 0:
                self._rsana_rna[:,:,i] = ( self.rsana[:,:,i] /
                                           self.na[:,:,i] *
                                           self.dab2[:,:,natm] )
                natm += 1

    ### ATTENTION: Review??
    @solps_property
    def rsana_rns(self):
        """ ({GRID_DIMS}, ns): Rescaled ionization ion particle losses [1/s].

        Notes
        -----
        This is rescaled via the 'self._rescale_neutrals' method using the
        scaling factor specified in the 'b2mndr_rescale_neutrals_sources'
        switch in b2mn.dat.
        """
        self._rsana_rns = self._rescale_neutrals(self.rsana)

    @solps_property
    def rcxmo(self):
        """({GRID_DIMS}, ns): Ion momentum neutral losses [ms^-2].
        
        Notes
        -----
        According to the manual it is in [1/s], but probably that's a typo.
        """
        self._read_signal('_rcxmo','rcxmo')

    #They do not appear on SOLPS manual??
    @solps_property
    def smo(self):
        """(nx, ny, 4, ns): Coefficients of the linear expression of the
        parallel momentum source of species is [N].
        """
        self._read_signal('_smo', 'smo')

    @solps_property
    def smq(self):
        """(nx, ny, 4, ns): Coefficients of the linear expression of the
        total momentum source of species is [N].

        Notes
        -----
        Apparently, from atomic physics.
        """
        self._read_signal('_smq', 'smq')

    @solps_property
    def smaf(self):
        """(nx, ny, 4, ns): Coefficients of the linear expression of the
        parallel momentum source of species (is) from friction [N].
        """
        self._read_signal('_smaf', 'b2npmo_smaf')

    @solps_property
    def smag(self):
        """(nx, ny, 4, ns): Coefficients of the linear expression of the
        parallel momentum source of species (is) from pressure gradient [N].
        """
        self._read_signal('_smag', 'b2npmo_smag')

    @solps_property
    def smav(self):
        """(nx, ny, 4, ns): Coefficients of the linear expression of the
        parallel momentum source of species (is) from additional viscosity [N].
        """
        self._read_signal('_smav', 'b2npmo_smav')



    @solps_property
    def resmo(self):
        """(nx, ny, ns): Residuals of the parallel momentum equation of
        species (is) [N*s].
        """
        self._read_signal('_resmo', 'resmo')
        if (self._resmo is not None and
            self._resmo.shape ==(self.nx,self.ny-1,self.ns)):
              self._resmo = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                           self._resmo), axis = 1)

    @solps_property
    def respo(self):
        """(nx, ny): Potential equation residuals [V]"""
        self._read_signal('_respo','respo')



    @solps_property2
    def sne(self):
        """ ({GRID_DIMS}, 2): Linear approximation to the assumed
        non-ambipolar electron source [??]
        """





    # -------------------- Grid and vessel geometry ---------------------------

    # -------------------- EIRENE triangular grid -----------------------------















    # -------------------- Names and texts ----------------------------------
    ## Heavily improve this section with more feedback.

    #ATTENTION: Maybe change to fluid_species
    def b2_species(self, ns):
        """ Returns index of species if given a name or name for a given
        index of the B2 fluid species.
        """
        if type(ns) != str:
            return self.species_names[ns]
        else:
            return self.species_names.index(ns)

    #ATTENTION: This only works for atoms, not molecules or test ions.
    #ATTENTION: Maybe change to atom_species.
    #ATTENTION: And add other like mol_species
    def eirene_species(self, ns):
        """ Returns index of species if given a name or name for a given
        index of EIRENE atomic species.
        """
        if type(ns) != str:
            return self.eirene_names[ns]
        else:
            return self.eirene_names.index(ns)


    ## ---------------- REVISION    warning! ------------------------
    #ATTENTION: It gives back H, not D.
    #ATTENTION: change name to eirene_names? neutral_names?
    @solps_property
    def atom_names(self):
        """list(str) : B2 neutrals names."""
        self._atom_names = []
        zn = -1
        for ns in range(self.ns):
            if zn != self.zn[ns]:
                zn = self.zn[ns]
                self._atom_names.append(elements[zn].title())

    #ATTENTION: Add docstrig
    #ATTENTION: Better name?
    @property
    def eirene_names(self):
        return self.atom_names


    #ATTENTION: It gives back H, not D.
    #ATTENTION: change name to b2_names?
    @solps_property
    def species_names(self):
        """list(str): B2 species names."""
        self._species_names = []
        for ns in range(self.ns):
            name = elements[self.zn[ns]].title()
            if   self.za[ns] == 1: name += '+'
            elif self.za[ns] == 2: name += '++'
            elif self.za[ns] >= 3: name += str(int(self.za[ns])) + '+'
            self._species_names.append(name)

    #ATTENTION: Create function instead: It returns name if given an index and
    # index if given a name
    # Maybe insert the logic here completely with the one of species_names
    #Add the logic of species_index and eirene_index too
    # Maybe add alias of fluid_species
    def b2_species(self, id):
        if type(id) != str:
            return self.species_names[id]
        else:
            return self.species_names.index(id)
    def eirene_species(self, id):
        if type(id) != str:
            return self.eirene_names[id]
        else:
            return self.eirene_names.index(id)





    # -------------------- Others (for now) ---------------------------------
    #ATTENTION: Keep?
    @property
    @try_block
    def rqrad_natm(self):
        """(nx,ny,natm) : Total radiation per isonuclear sequence [W]."""
        self._rqrad_natm = self._sum_cs(self.line_radiation)

    @property
    @try_block
    def lz(self):
        """(nx,ny,natm) : Radiation efficiency per isonuclear sequence [Wm^3].
        """

#       This is wrong:
        self._lz = self._sum_cs(self.lz_cs)

#       This is correct (according to Ralph Dux):
        self._lz = np.zeros((self.nx,self.ny,self.natm))
        pv = self.rqrad_natm / np.expand_dims(self.vol,2)
        for natm in range(self.natm):
            self._lz[:,:,natm] = pv[:,:,natm] / self.na_atom[:,:,natm] / self.ne

    @solps_property
    def lz_cs(self):
        """(nx,ny,ns) : Radiation efficiency for every charge state [Wm^3].
        """
        self._lz_cs = np.zeros((self.nx,self.ny,self.ns))
        pv = self.line_radiation / np.expand_dims(self.vol,2)
        for ns in range(self.ns):
            self._lz_cs[:,:,ns] = pv[:,:,ns] / self.na_eirene[:,:,ns] / self.ne
        self._lz_cs[np.where(np.isnan(self._lz_cs))] = 0

    @property
    def lz_adas(self):
        """(nx,ny,natm) : Expected L_z values for each cell according to their
        density and temperature values [Wm^3].
        Calculated according to the the ADAS database.

        Notes
        -----
        Alias of :py:attr:`~.spectroscopy.lz_adas`.
        """
        return self.spectroscopy.lz_adas

    @property
    @try_block
    def lz_deviation(self):
        """(nx,ny,natm) : Deviation of 'real' lz values from expectation [%]."""
        self._lz_deviation = np.zeros((self.nx,self.ny,self.natm))
        for natm in range(self.natm):
            for nx in range(self.nx):
                for ny in range(self.ny):
                    if self.lz_adas[nx,ny,natm] == 0: continue
                    self._lz_deviation[nx,ny,natm] = ((self.lz[nx,ny,natm] -
                                                       self.lz_adas[nx,ny,natm])
                                                      /self.lz_adas[nx,ny,natm])


    @solps_property
    def electric_field(self):
        """(nx, ny, 3): Electric field calculated as the gradient of
        the electric potential in x-, y- and parallel direction [V/m].

        electric_field[...,0]:  electric_field in x direction.

        electric_field[...,1]:  electric_field in y direction.

        electric_field[...,2]:  electric_field in parallel direction (to B).
        """
        self._electric_field = -self.gradient(self.po)

    @solps_property
    def electric_force(self):
        """(nx, ny, ns): Parallel electrostatic force acting on the whole
        cell volume [N].

        Notes
        -----
        For the comparison of forces force-densities (force/self.vol)
        should be used since the total force scales with the volume of
        the considered volume element (i.e. with the cell volume)
        """
        e = self.masks.rlcl*self.electric_field[...,2]   # (nx,ny)
        e = np.expand_dims(e,2)                          # (nx,ny, 1)
        z = np.expand_dims(np.expand_dims(self.za,0),0)  # ( 1, 1,ns)
        n = np.expand_dims(self.vol,2)*self.na           # (nx,ny,ns)
        q = self.qe*n*z # Charge per species in the whole cell volume
        self._electric_force = q*e                       # (nx,ny,ns)

    # -------------------- Regions -----------------------------------------
    # -------------------- Globals -----------------------------------------
















#Create an avg function: self.avg('dab2', 'outdiv', along/dim='x'/'y'/'all')
#   # ONLY TEMPORAL
    @property
    @try_block
    def outdiv_nolim(self):
        self._outdiv_nolim = self.masks.outdiv.copy()
        self._outdiv_nolim[:,0] = np.zeros(self.nx, dtype=bool)
        self._outdiv_nolim[:,-1] = np.zeros(self.nx, dtype=bool)

    @property
    def dab2_avg(self):
        return self.average(self.dab2, self.outdiv_nolim)

    @property
    def na_avg(self):
        return self.average(self.na, self.outdiv_nolim)

    @property
    def dmb2_avg(self):
        return self.average(self.dmb2, self.outdiv_nolim)

    @property
    def dab2_line(self):
        return self.average(self.dab2, self.outdiv_nolim, dimension='x')

    @property
    def na_line(self):
        return self.average(self.na, self.outdiv_nolim, dimension='x')

    @property
    def dmb2_line(self):
        return self.average(self.dmb2, self.outdiv_nolim, dimension='x')



#    @property
#    @try_block
#    def dab2_line(self):
#        self._dab2_line = np.zeros((self.ny,self.natm))
#        for k in xrange(self.ny):
#          if self.outdiv_nolim[:,k].any():
#            for l in xrange(self.natm):
#                self._dab2_line[k,l] = (
#                    np.sum(self.dab2[self.outdiv_nolim[:,k],k,l]*
#                               self.dv[self.outdiv_nolim[:,k],k])/
#                    np.sum(self.dv[self.outdiv_nolim[:,k],k]))
#    @property
#    @try_block
#    def na_line(self):
#        self._na_line = np.zeros((self.ny,self.ns))
#        for k in xrange(self.ny):
#          if self.outdiv_nolim[:,k].any():
#            for l in xrange(self.ns):
#                self._na_line[k,l] = (
#                    np.sum(self.na[self.outdiv_nolim[:,k],k,l]*
#                               self.dv[self.outdiv_nolim[:,k],k])/
#                    np.sum(self.dv[self.outdiv_nolim[:,k],k]))
#    @property
#    @try_block
#    def dmb2_line(self):
#        self._dmb2_line = np.zeros((self.ny,self.nmol))
#        for k in xrange(self.ny):
#          if self.outdiv_nolim[:,k].any():
#            for l in xrange(self.nmol):
#                self._dmb2_line[k,l] = (
#                    np.sum(self.dmb2[self.outdiv_nolim[:,k],k,l]*
#                               self.dv[self.outdiv_nolim[:,k],k])/
#                    np.sum(self.dv[self.outdiv_nolim[:,k],k]))
#



#                   ============== EXTENSIONS ================




##  B2 INPUT AND RUN.LOG ======================================================


##  FUNCTIONS =================================================================

    def read_signal(self, signal_name, signal_link, attach_to = None):
        """ Retrieve data from the MDS+, given tree link and signal name.

        read_signal does not return any value. It automatically attach
        the signal as an attribute with a given name to the given object
        or self.

        Parameters
        ----------
        signal_name : str
            Storage name for the retrieve signal.
        signal_link : str
            SOLPS tree link to signal in the MDS+ directory.
            It is described in the SOLPS* user manuals.
        attach_to : object, optional
            signal_name will be attached as attribute to object
            attach_to. Default to self.

        Returns
        -------
        None
        """
        if not self.conn_open:
            self.conn.openTree('solps', self.shot)
            self.conn_open=True
        if not attach_to:
            attach_to = self

        try:
            setattr(attach_to, signal_name, self.conn.get(signal_link).value.T)
        except:
            self.log.warning("error reading '{1}' for '{0}'".format(signal_name,
                                                                 signal_link))
            setattr(attach_to, signal_name, None)

        if self.conn_open:
            self.conn.closeTree('solps', self.shot)
            self.conn_open = False




    # -------------------- Operations -----------------------------------------
    ## ATTENTION: Check the logic and compare to default avg. Then create
    ## tests and finally delete the default avg. for clean up.

    def average(self, var, gridmask=None, dimension='xy', volume_average=True,
            full_array=False):
        """Calculates the average value of var in the region defined
        by the grid mask.

        Parameters
        ----------

        var : (nx, ny) or (nx, ny, n3) array
            Quantity to be averaged.

        gridmask : (nx, ny) boolean like array, optional
            Mask defining the average region.
            Default: Entire grid.

        dimension : {'xy', 'x', 'y'}, optional
            Dimension in which the average is applied.
            'x' makes the average in the poloidal direction (at least n2).
            'y' in the radial direction (at least n1).
            'xy' makes the average over the entire grid section.
            Default: 'xy'.

        volume_average: bool, optional
            If volume_average is True, then the average is weighted by the
            cell volume. Otherwise, a simple average is used.
            Default: True.

        full_array: bool, optional
            If full_array is True, then the return value is always (n1,n2)
            or (nx,ny,n3). If False, then it only returns indexes which had
            at least one True in the direction of average.
            Default: False.


        Returns
        -------
        (n3), (nx, n3) or (ny, n3) array, depending on the dimension parameter.


        TODO
        ----
        Think about returning a proper array instead of a list when n3>1.
        """
        nx = var.shape[0]
        ny = var.shape[1]
        if gridmask is None:
            gridmask = np.ones((nx, ny), dtype=bool)
        gmask = gridmask.astype(np.int)


        #import pdb; pdb.set_trace()
        if len(var.shape) == 3:
            # If dimensions are not (nx,ny) then use recursivity.
            ns = var.shape[2]
            avr = []
            for n in range(ns):
                avr.append(self.average(var[:,:,n],
                    gridmask, dimension, volume_average))
            return avr

        if volume_average:
            # Average weighted with the cell volume:
            if dimension == 'xy':
                return np.sum(gmask*var*self.vol) / np.sum(gmask*self.vol)

            elif dimension == 'x':
                tmp = np.full(ny, np.nan)
                msk = np.any(gridmask,0)
                for iy in range(ny):
                    if msk[iy] == False:
                        continue
                    tmp[iy] = (np.sum(gmask[:,iy]*var[:,iy]*self.vol[:,iy])/
                               np.sum(gmask[:,iy]*self.vol[:,iy]))
                return tmp

            elif dimension == 'y':
                tmp = np.full(nx, np.nan)
                msk = np.any(gridmask,1)
                for ix in range(nx):
                    if msk[ix] == False:
                        continue
                    tmp[ix] = (np.sum(gmask[ix]*var[ix]*self.vol[ix])/
                               np.sum(gmask[ix]*self.vol[ix]))
                return tmp


        else:
            # Only averaged over cell amount:
            if dimension == 'xy':
                return np.sum(gmask*var) / np.sum(gmask)

            elif dimension == 'x':
                tmp = np.full(ny, np.nan)
                msk = np.any(gridmask,0)
                for iy in range(ny):
                    if msk[iy] == False:
                        continue
                    tmp[iy] = np.sum(gmask[:,iy]*var[:,iy])/np.sum(gmask[:,iy])
                return tmp

            elif dimension == 'y':
                tmp = np.full(nx, np.nan)
                msk = np.any(gridmask,1)
                for ix in range(nx):
                    if msk[ix] == False:
                        continue
                    tmp[ix] = np.sum(gmask[ix]*var[ix])/np.sum(gmask[ix])
                return tmp



    def sum(self, var, gridmask=None, dimension='xy', volume_average=True,
            full_array=False):
        """Calculates the sum value of var in the region defined
        by the grid mask.

        Parameters
        ----------

        var : (n1, n2) or (n1, n2, n3) array
            Quantity to be averaged.

        gridmask : (n1,n2) boolean like array, optional
            Mask defining the average region.
            Default: Entire grid.

        dimension : {'xy', 'x', 'y'}, optional
            Dimension in which the average is applied.
            'x' makes the average in the poloidal direction (at least n2).
            'y' in the radial direction (at least n1).
            'xy' makes the average over the entire grid section.
            Default: 'xy'.

        full_array: bool, optional
            If full_array is True, then the return value is always (n1,n2)
            or (n1,n2,n3). If False, then it only returns indexes which had
            at least one True in the direction of average.


        Returns
        -------
        (n3), (n1, n3) or (n2, n3) array, depending on the dimension parameter.


        TODO
        ----
        Think about returning a proper array instead of a list when n3>1.
        """
        nx = var.shape[0]
        ny = var.shape[1]
        if gridmask is None:
            gridmask = np.ones((nx, ny), dtype=bool)
        gmask = gridmask.astype(np.int)


        #import pdb; pdb.set_trace()
        if len(var.shape) == 3:
            # If dimensions are not (nx,ny) then use recursivity.
            ns = var.shape[2]
            avr = []
            for n in range(ns):
                avr.append(self.sum(var[:,:,n],
                    gridmask, dimension))
            return avr

        if True:
            if dimension == 'xy':
                return np.sum(gmask*var)

            elif dimension == 'x':
                tmp = np.full(ny, np.nan)
                msk = np.any(gridmask,0)
                for iy in range(ny):
                    if msk[iy] == False:
                        continue
                    tmp[iy] = (np.sum(gmask[:,iy]*var[:,iy]))
                return tmp

            elif dimension == 'y':
                tmp = np.full(nx, np.nan)
                msk = np.any(gridmask,1)
                for ix in range(nx):
                    if msk[ix] == False:
                        continue
                    tmp[ix] = (np.sum(gmask[ix]*var[ix]))
                return tmp




    #ATTENTION: DO NOT USE YET.
    #ATTENTION: Complete when understood the logic of the matlab indexing
    #At least it is known that giving x and y or just x give same result.
    def divergence(self, flux_x, flux_y=None):
        """ Calculates the divergences of different fluxes for every grid cell,
        following $SOLPSTOP/scrits/matlab/calc_divergence.m.

        Parameters
        ----------

        flux_x: (nx,ny) or (nx,ny,ns) or (nx,ny,2) or (nx,ny,2,ns)
            x component of the flux which divergence is wanted.
            It can also be the merge version of both x and y fluxes.
            In that case, it will be separated inside the function.

        flux_y: (nx,ny) or (nx,ny,ns), optional
            y component of the flux which divergence is wanted.

        Returns
        -------

        (div_x, div_y, div):  (nx, ny, 3) or (nx, ny, ns,3)

            div_x: (nx,ny) or (nx,ny,ns)
                Divergence in the x-direction.
            div_y:
                Divergence in the y-direction.
            div:
                Total divergence in the cell.
        """

        self.log.error("self.divergence is still not suitable for usage")

        if flux_y is None:
            flux_y = flux_x[:,:,1]
            flux_x = flux_x[:,:,0]

        if flux_x.shape != flux_y.shape:
            self.log.error("Dimensions of flux_x and flux_y must be the same.")

        div_x =  np.zeros_like(flux_x)
        div_y =  np.zeros_like(flux_x)
        div   =  np.zeros_like(flux_x)
        tmp   =  np.zeros(list(itertools.chain(flux_x.shape,[3])))

        for ix in range(self.nx):
            for iy in range(self.ny):
                div_x[ix,iy] = -flux_x[ix,iy]
                div_y[ix,iy] = -flux_y[ix,iy]
                ## ATTENTION: The following doesn't make sense!!!
                #if self.topiy[ix,iy] != self.ny:
                #    div_y[ix,iy] += flux_y[self.topiy[ix,iy],iy]
                #if self.rightix[ix,iy] != self.nx:
                #    div_x[ix,iy] += flux_x[ix, self.rightix[ix,iy]]

        tmp[...,0] = div_x
        tmp[...,1] = div_y
        tmp[...,2] = div_x + div_y
        return tmp









#    #ATTENTION: Improve docstring
#    def region_average(self,var,gridmask,volume_average=True):
#        """Calculates the average value of var in the region defined
#        by the grid mask.
#        """
#
#        if len(var.shape) == 3:
#            avr = []
#            for n in range(var.shape[2]):
#                avr.append(self.region_average(var[:,:,n],
#                                               gridmask,
#                                               volume_average))
#            return avr
#
#        if volume_average:
#            # Average weighted with the cell volume:
#            return np.sum(gridmask*var*self.vol) / np.sum(gridmask*self.vol)
#        else:
#            # Only averaged over cells:
#            return np.sum(gridmask*var) / np.sum(gridmask)
#


    #ATTENTION: Better name?
    def map_cell_faces_to_center(self,var,direction='xy'):
        """Calculates the average value of 'var' in the cell center,
        e.g. for fluxes (which are stored on the cell faces) as it
        is also done in B2Plot.

        Parameters
        ----------

            var : (nx,ny,...)
                Quantity to be mapped.

            direction : {'xy', 'x', 'y'}
                Specifies the direction of the mapping; for 'xy' does the
                mapping in both directions if var.shape == (...,2,...)

        Returns
        -------
            (nx,ny,...)
        """

        if var.shape[0:2] != (self.nx,self.ny):
            self.log.error('Invalid shape of variable array: %s',var.shape)
            return None

        retval = np.zeros(var.shape)

        if direction == 'xy':
            if var.shape[-2] == 2:
                retval[...,0,:]=self.map_cell_faces_to_center(var[...,0,:],'x')
                retval[...,1,:]=self.map_cell_faces_to_center(var[...,1,:],'y')
            elif var.shape[-1] == 2:
                retval[...,0] = self.map_cell_faces_to_center(var[...,0],'x')
                retval[...,1] = self.map_cell_faces_to_center(var[...,1],'y')
            else:
                self.log.error("Invalid shape of variable array for"
                               + " direction 'xy': %s",var.shape)
                return None
        elif direction == 'x':
            for nx in range(self.nx):
                for ny in range(self.ny):
                    if nx == 0:
                        retval[nx,ny,...] = var[int(self.rightix[nx,ny]),
                                                int(self.rightiy[nx,ny]),...]
                    elif nx == self.nx-1:
                        retval[nx,ny,...] = var[nx,ny,...]
                    else:
                        retval[nx,ny,...] = 0.5*(var[nx,ny,...] +
                                                 var[int(self.rightix[nx,ny]),
                                                     int(self.rightiy[nx,ny]),...])
        elif direction == 'y':
            for nx in range(self.nx):
                for ny in range(self.ny):
                    if ny == 0:
                        retval[nx,ny,...] = var[int(self.topix[nx,ny]),
                                                int(self.topiy[nx,ny]),...]
                    elif ny == self.ny-1:
                        retval[nx,ny,...] = var[nx,ny,...]
                    else:
                        retval[nx,ny,...] = 0.5 * (var[nx,ny,...] +
                                                   var[int(self.topix[nx,ny]),
                                                       int(self.topiy[nx,ny]),...])
        else:
            self.log.error('Invalid direction: %s',direction)
            return None

        return retval

    #ATTENTION: Improve docstring
    def gradient(self,var):
        """ Returns the gradient of 'var' for all cells. The last dimension 
        of the output distinguishes the gradients in x-, y- and in parallel
        direction.

        Parameters
        ----------
            var : np.array(nx,ny,...)

        Return Value
        ------------
            gradient : np.array(nx,ny,...,3)

        Notes
        -----
            gradient[...,0] is the gradient in x direction (poloidal)
            gradient[...,1] is the gradient in y direction (radial)
            gradient[...,2] is the gradient in parallel direction (to B)
        """

        if var.shape[0:2] != (self.nx,self.ny):
            self.log.error('Invalid shape of variable array: %s',var.shape)
            return

        # Shape + (3,) for x/y/parallel direction:
        grad = np.zeros(var.shape+(3,))

        # Use the 'corrected' cell width as used in b2plot
        # for the calculation of the y-cell distances:
        hy1 = self.hy*self.qz[:,:,1]
        hx  = self.hx
        hp  = self.hx*np.abs(self.bb[...,3]/self.bb[...,0])

        # Calculate gradients:
        for y in range(self.ny):
            for x in range(self.nx):
                xl = max(int(self.leftix  [x,y]),0)         # (Lower  x-value)
                yl = max(int(self.bottomiy[x,y]),0)         # (Lower  y-value)
                xh = min(int(self.rightix [x,y]),self.nx-1) # (Higher x-value)
                yh = min(int(self.topiy   [x,y]),self.ny-1) # (Higher y-value)
                # x Distance:
                dx = hx[xl,y]/2 + hx[xh,y]/2
                if x != xl and x != xh: dx += hx[x,y]
                # y Distance:
                dy = hy1[x,yl]/2 + hy1[x,yh]/2
                if y != yl and y != yh: dy += hy1[x,y]
                # Parallel Distance:
                dp = hp[xl,y]/2 + hp[xh,y]/2
                if x != xl and x != xh: dp += hp[x,y]
                # Gradients in x, y and parallel direction:
                grad[x,y,...,0] = (var[xh,y,...] - var[xl,y,...]) / dx
                grad[x,y,...,1] = (var[x,yh,...] - var[x,yl,...]) / dy
                grad[x,y,...,2] = (var[xh,y,...] - var[xl,y,...]) / dp

        return grad



    #ATTENTION: Not obvious. Poor naming
    #Should be called eirene_names and eirene_index?
    def atom_index(self,atom):
        """Returns the natm index for the specified atom."""
        if atom == 'D':
            atom = 'H'
        return self.atom_names.index(atom.title())

    #ATTENTION: Not obvious. Poor naming
    #Should be called b2_names and b2_index?
    def species_index(self,species):
        """Returns a list of species indizes calculated from the parameter
        'species'. This 'species' variable can be a list of species names
        (like ['N6+,N7+]') or a range of species, like 'N+-N5+' or a single
        species string (like 'N++')."""

        def sti(string): # string_to_index
            if type(string) == int: return string
            s = string.title()
            if s == 'D':  s = 'H'
            if s == 'D+': s = 'H+'
            if s in self.species_names: return self.species_names.index(s)
            s = s.replace('2+','++')
            if s in self.species_names: return self.species_names.index(s)
            # Else:
            self.log.error("Invalid species '%s'",string)

        if species == 'all':
            return list(range(self.ns))
        elif type(species) == int:
            return [species]
        elif type(species) == str:
            if '-' in species:
                sr = species.split('-')
                return list(range(sti(sr[0]),sti(sr[1])+1))
            else:
                return [sti(species)]
        elif type(species) == list:
            rlist = []
            for item in species:
                rlist.append(sti(item))
            return rlist


    #ATTENTION: Better name?
    @staticmethod
    def iy_closer_to(ds, dist=0):
        """ Calculates the closest radial index to a certain distance.

        Parameters
        ----------

        ds : (ny) array
            Values of the distance from the separatrix, ds

        dist : float, optional
            Value of the desired distance from the separatrix of which
            the index is to be yielded.
            Default: 0

        Returns
        -------
        int
            Index of ds which contains the value which
            minimizes abs(ds-dist).

        """
        same_sign = (np.sign(ds) == np.sign(dist))
        min_dist = np.inf
        ind = None
        for i in range(ds.shape[0]):
            if same_sign[i]:
                if np.abs(ds[i]-dist)< min_dist:
                    min_dist = np.abs(ds[i]-dist)
                    ind = i
        return ind


    # ATTENTION: This should be replaced in the final version
    def momentum_balance(self, **kwargs):
        """ This works. Use this for now"""
        import solpspy.extensions.mockup_balance as blc
        blc.momentum_balance(self, **kwargs)







    # -------------------- Private methods ------------------------------------
    #ATTENTION: Improve docstrig
    #ATTENTION: Review logic
    def _extend(self, field):
        """
        Similar to b2plot/extend.F but only for grid dimensions.
        Should be similar to, but not implemented (why waste time right now?)
        For now, just use it for the standard case of connected spaces
        """
        ndim = list(field.shape)
        ndim[0], ndim[1] = ndim[0]+2, ndim[1]+2
        ndim = tuple(ndim)
        new_field = np.zeros(ndim)
        new_field[1:-1,1:-1] = field
        new_field[0] = new_field[1]
        new_field[-1] = new_field[-2]
        new_field[:,0] = new_field[:,1]
        new_field[:,-1] = new_field[:,-2]

        if self.magnetic_geometry.lower() in ['cdn','ddn','ddnu']:
            in1 = np.where(self.region[...,1] == 4)[0][0]
            in2 = np.where(self.region[...,1] == 5)[0][0]
            new_field[in1] = new_field[in1-1]
            new_field[in2-1] = new_field[in2]

        return new_field


    ## ATTENTION: Honestly, knowing nx and ny and having na, one can
    ## extract ns
    def _get_basic_dimensions(self):
        """
        Retrieve basic dimensions: nx, ny, ns, natm, nmol, nion
        """
        #Get nx, ny and ns from b2fstat*
        if 'b2fstate' in self.avail or 'b2fstati' in self.avail:
            try:
                gdir = self.avail['b2fstate']
            except:
                gdir = self.avail['b2fstati']
            with open(gdir) as fg:
                for _ in range(2):
                    fg.readline()
                tmp = fg.readline().split()
                self.nx = int(tmp[0]) + 2
                self.ny = int(tmp[1]) + 2
                self.ns = int(tmp[2])

        #Or get nx, ny from b2fgmtry and ns by other means
        elif ('b2fgmtry' in self.avail
            or 'b2fpardf' in self.avail
            or 'b2fparam' in self.avail
            or 'b2fplasmf' in self.avail):

            if 'b2fgmtry' in self.avail:
                gdir = self.avail['b2fgmtry']
                with open(gdir) as fg:
                    for _ in range(2):
                        fg.readline()
                    tmp = fg.readline().split()
                    self.nx = int(tmp[0]) + 2
                    self.ny = int(tmp[1]) + 2
            else:
                self.log.warning("nx, ny could not be retrieved.")

            if 'b2fpardf' in self.avail:
                pdir = self.avail['b2fpardf']
                with open(pdir) as fp:
                    for _ in range(2):
                        fp.readline()
                    self.ns = int(fp.readline().split()[0])
            elif 'b2fparam' in self.avail:
                pdir = self.avail['b2fparam']
                with open(pdir) as fp:
                    for _ in range(2):
                        fp.readline()
                    self.ns = int(fp.readline().split()[0])
            elif 'b2fplasmf' in self.avail:
                try:
                    pdir = self.avail['b2fplasmf']
                    with open(pdir) as fp:
                        for line in fp:
                            elements = line.split()
                            if elements[-1].strip().lower() == 'na':
                                self.ns = int(
                                    int(elements[-2])/(self.nx*self.ny))
                                break
                except:
                    pass
            else:
                self.log.error("ns could not be retrieved")

        else:
            self.log.error("nx,ny,ns could not be retrieved.")


        #Read EIRENE dims: natm, nmol and nion
        try:
            fdir = self.avail['fort.44']
            with open(fdir,'r') as fd:
                line = [int(x) for x in fd.readline().split()[:3]]
                if (line[0]+2 != self.nx) or (line[1]+2 != self.ny):
                    self.log.error("Faulty fort.44")
                    self.natm = None
                    self.nmol = None
                    self.nion = None
                    self.jvft44 = None
                    return
                self.jvft44 = line[2]
                line = [int(x) for x in fd.readline().split()]
                self.natm = line[0]
                self.nmol = line[1]
                self.nion = line[2]
                self.atm = []
                self.mol = []
                self.ion = []
                for _ in range(self.natm):
                    self.atm.append(fd.readline().strip())
                for _ in range(self.nmol):
                    self.mol.append(fd.readline().strip())
                for _ in range(self.nion):
                    self.ion.append(fd.readline().strip())
        except:
            self.log.warning("natm, nmol and nion could not be retrieved.")
            self.natm = None
            self.nmol = None
            self.nion = None
            self.jvft44 = None
            self.log.warning("Assuming b2 standalone run.")
            self._b2standalone=True


        #Read Rates dims: rtnt, rtnn, rtns
        try:
            fdir = self.avail['b2frates']
            with open(fdir,'r') as fd:

                # Go to 'label':
                while ( True ):
                    line = fd.readline()
                    if line.split()[-1] == 'label':
                        break

                # Read out rates table, which is 2 words after the timestamp (containing ':'):
                try:
                    self.rates_table = fd.readline().split(':')[1].split()[2]
                except:
                    self.log.warning("Name of atomic rates table"+
                                     " could not be retrieved")
                    self.rates_table = None

                # Go to 'rtnt,rtnn,rtns':
                while ( True ):
                    line = fd.readline()
                    if line.split()[-1] == 'rtnt,rtnn,rtns':
                        break

                # Read 'rtnt,rtnn,rtns':
                line = [int(x) for x in fd.readline().split()]
                self.rtnt = line[0] + 1
                self.rtnn = line[1] + 1
                self.rtns = line[2]
        except:
            self.log.warning("rtnt, rtnn and rtns could not be retrieved")
            self.rtnt = None
            self.rtnn = None
            self.rtns = None
























class StructuredDataRundir(StructuredDataBase):
    """
    """

    def __init__(self, *args, **kwargs):
        alog.debug(">> Entering StructuredRundir __init__")
        super().__init__(*args, **kwargs)
        alog.debug("<< Leaving  StructuredRundir __init__")


    # -------------------- Indices --------------------------------------------

    @solps_property
    def nnreg(self):
        self._read_signal('_nnreg','nnreg')
        self._nnreg = self._nnreg.astype(int)

    @solps_property
    def region(self):
        """(nx,ny,3):   Index of corresponding region
        0:  Volume region
        1:  x-region
        2:  y-region
        """
        self._read_signal('_region','region')
        self._region = self._region.astype(int)

    #Read convert/process.F
    @solps_property
    def jxi(self):
        """Logic can be found in the definition of jxi in the documentation"""
        dummy = self.read_switch('b2mwti_jxi')
        if dummy:
            self._jxi = int(dummy) + 1
        else:
            if   self.nnreg[0] == 1:
                self._jxi = self.nx//4
            elif self.nnreg[0] == 4 or self.nnreg[0] == 5:
                self._jxi = (self.leftcut[0]+
                    (self.rightcut[0]-self.leftcut[0])//4 + 1)
            elif self.nnreg[0] == 8:
                self._jxi = (self.leftcut[0]+self.leftcut[1])/2
            self._jxi = int(self._jxi)

    @solps_property
    def jxa(self):
        """Logic can be found in the definition of jxa in the documentation"""
        dummy = self.read_switch('b2mwti_jxa')
        if dummy:
            self._jxa = int(dummy) + 1
        else:
            if   self.nnreg[0] == 1:
                self._jxa = 3*self.nx//4
            elif self.nnreg[0] == 4 or self.nnreg[0] == 5:
                self._jxa = (self.rightcut[0]-
                    (self.rightcut[0]-self.leftcut[0])//4 - 1)
            elif self.nnreg[0] == 8:
                self._jxa = (self.rightcut[0]+self.rightcut[1])/2
            self._jxa = int(self._jxa)

    
    @solps_property
    def iimp(self):
        bi = np.abs(np.average(self.bb[self.jxi,:,2]))
        ba = np.abs(np.average(self.bb[self.jxa,:,2]))
        if bi > ba:
            self._iimp = self.jxi
            self._iomp = self.jxa
        else:
            self._iimp = self.jxa
            self._iomp = self.jxi

    @solps_property
    def iomp(self):
        bi = np.abs(np.average(self.bb[self.jxi,:,2]))
        ba = np.abs(np.average(self.bb[self.jxa,:,2]))
        if bi > ba:
            self._iimp = self.jxi
            self._iomp = self.jxa
        else:
            self._iimp = self.jxa
            self._iomp = self.jxi


    @solps_property
    def isep(self):
        """Equals first flux surface in SOL"""
        if   self.nnreg[0] == 1:
            #self._isep = self.ny//2
            self._isep = 1
        elif   self.nnreg[0] == 2:
            self._isep = self.ny//2
        elif self.nnreg[0] == 4 or self.nnreg[0] == 5:
            self._isep = self.topcut[0] + 1


    # -------------------- Neighbours and cuts --------------------------------
    @solps_property
    def leftix(self):
        """Index in base 0, Python-like"""
        self._read_signal('_leftix','leftix')
        self._leftix +=1
        self._leftix = self._leftix.astype(int)

    @solps_property
    def leftiy(self):
        """Index in base 0, Python-like"""
        self._read_signal('_leftiy','leftiy')
        self._leftiy +=1
        self._leftiy = self._leftiy.astype(int)

    @solps_property
    def rightix(self):
        """Index in base 0, Python-like"""
        self._read_signal('_rightix','rightix')
        self._rightix +=1
        self._rightix = self._rightix.astype(int)

    @solps_property
    def rightiy(self):
        """Index in base 0, Python-like"""
        self._read_signal('_rightiy','rightiy')
        self._rightiy +=1
        self._rightiy = self._rightiy.astype(int)

    @solps_property
    def bottomix(self):
        """Index in base 0, Python-like"""
        self._read_signal('_bottomix','bottomix')
        self._bottomix +=1
        self._bottomix = self._bottomix.astype(int)

    @solps_property
    def bottomiy(self):
        """Index in base 0, Python-like"""
        self._read_signal('_bottomiy','bottomiy')
        self._bottomiy +=1
        self._bottomiy = self._bottomiy.astype(int)

    @solps_property
    def topix(self):
        """Index in base 0, Python-like"""
        self._read_signal('_topix','topix')
        self._topix +=1
        self._topix = self._topix.astype(int)

    @solps_property
    def topiy(self):
        """Index in base 0, Python-like"""
        self._read_signal('_topiy','topiy')
        self._topiy +=1
        self._topiy = self._topiy.astype(int)

    @solps_property
    def leftcut(self):
        self._read_signal('_leftcut','leftcut')
        self._leftcut = self._leftcut.astype(int)

    @solps_property
    def rightcut(self):
        self._read_signal('_rightcut','rightcut')
        self._rightcut = self._rightcut.astype(int)

    @solps_property
    def topcut(self):
        self._read_signal('_topcut','topcut')
        self._topcut = self._topcut.astype(int)

    @solps_property
    def bottomcut(self):
        self._read_signal('_bottomcut','bottomcut')
        self._bottomcut = self._bottomcut.astype(int)
    


    # -------------------- Structure ------------------------------------------

    @solps_property
    def crx(self):
        """({GRID_DIMS}, 4): R-coordinate of the corners of the B2 grid [m]."""
        self._read_signal('_crx','crx')

    @property
    def r(self):
        """Alias for crx"""
        return self.crx

    @solps_property
    def cry(self):
        """({GRID_DIMS}, 4): Z-coordinate of the corners of the B2 grid [m]."""
        self._read_signal('_cry','cry')

    @property
    def z(self):
        """Alias for cry"""
        return self.cry


    @solps_property
    def cr(self):
        """(nx,ny) : R-coordinate of the cell centre [m]"""
        self._cr = 0.25*np.sum(self.crx, 2)

    @solps_property
    def cr_x(self):
        self._cr_x = (self.crx[1:,:,0]+self.crx[1:,:,2])/2.0

    @solps_property
    def cr_y(self):
        self._cr_y = (self.crx[:,1:,0]+self.crx[:,1:,1])/2.0

    @solps_property
    def cz(self):
        """(nx,ny) : Z-coordinate of the cell centre [m]"""
        self._cz = 0.25*np.sum(self.cry, 2)

    @solps_property
    def cz_x(self):
        self._cz_x = (self.cry[1:,:,0]+self.cry[1:,:,2])/2.0

    @solps_property
    def cz_y(self):
        self._cz_y = (self.cry[:,1:,0]+self.cry[:,1:,1])/2.0



    ## ATTENTION: Improve with logic of Unstructured, just using
    ## These dimensions
    def where(self, var):
        """
          Locate the (best) file which contains a certain variable and
        provides the name of the file and the dimensions of the variable
        """
        signal = var.lower()

        s = (self.ns)
        xy = (self.nx, self.ny)
        reg = (3) # Should be shot.nnreg
        xys = (self.nx, self.ny, self.ns)
        xy2 = (self.nx, self.ny, 2)
        xy22 = (self.nx, self.ny, 2, 2)
        xy3 = (self.nx, self.ny, 3)
        xy4 = (self.nx, self.ny, 4)
        xy2s = (self.nx, self.ny, 2, self.ns)
        xy22s = (self.nx, self.ny, 2, 2, self.ns)
        xy4s = (self.nx, self.ny, 4, self.ns)
        xyreg = (self.nx, self.ny, 3) #Should be shot.nnreg

        #Eirene stores them in matix without guarding cells
        fxy = (self.nx-2,self.ny-2)
        fxya = (self.nx-2,self.ny-2, self.natm)
        fxym = (self.nx-2,self.ny-2, self.nmol)
        fxyi = (self.nx-2,self.ny-2, self.nion)

        # For rates
        rtnt = self.rtnt
        rtnn = self.rtnn
        rtns = self.rtns
        rt = (self.rtnt, self.rtnn, self.rtns)



        in_b2f_common = {
            'fch':xy2,
            'fna':xy2s,
            'fhe':xy2,
            'fhi':xy2,
            'na':xys,
            'ne':xy,
            'po':xy,
            'te':xy,
            'ti':xy,
            'ua':xys,
            'uadia':xy2s,
            'fchdia':xy2,
            'fna_32':xy2s,
            'fna_52':xy2s,
            'fni_32':xy2,
            'fni_52':xy2,
            'fne_32':xy2,
            'fne_52':xy2,
            'wadia':xy2s,
            'vaecrb':xy2s,
            'fchvispar':xy2,
            'fchvisper':xy2,
            'fchin':xy2,
            'fna_nodrift':xy2s,
            'fna_mdf':xy2s,
            'fhe_mdf':xy2,
            'fhi_mdf':xy2,
            'fnapsch':xy2s,
            'fhepsch':xy2,
            'fhipsch':xy2,
            'fna_fcor':xy2s,
            'fna_he':xy2s,
            'fchvisq':xy2,
            'fchinert':xy2}

        if self.branch=='3.1.0':
            in_b2f_common.update({
            'fch': xy22s,
            'fna': xy22s,
            'fhe':xy22,
            'fhi':xy22,
            'uadia':xy22s,
            'fchdia':xy22,
            'fna_32':xy22s,
            'fna_52':xy22s,
            'fni_32':xy22,
            'fni_52':xy22,
            'fne_32':xy22,
            'fne_52':xy22,
            'wadia':xy22s,
            'vaecrb':xy22s,
            'fchvispar':xy22,
            'fchvisper':xy22,
            'fchin':xy22,
            'fna_nodrift':xy22s,
            'fna_mdf':xy22s,
            'fhe_mdf':xy22,
            'fhi_mdf':xy22,
            'fnapsch':xy22s,
            'fhepsch':xy22,
            'fhipsch':xy22,
            'fna_fcor':xy22s,
            'fna_he':xy22s,
            'fchvisq':xy22,
            'fchinert':xy22})



        in_b2fstate = {
            'zamin':s,
            'zamax':s,
            'zn':s,
            'am':s,
            'fch_32':xy2,
            'fch_52':xy2,
            'kinrgy':xys,
            'time':1,
            'fch_p':xy,
            'fna_eir':xy2s,
            'fne_eir':xy2,
            'fhe_eir':xy2,
            'fhi_eir':xy2,
            'vadia':xy2s,
            'veecrb':xy2,
            'vedia':xy2,
            'floe_noc':xy2,
            'floi_noc':xy2}
        in_b2fstate = update(in_b2f_common, in_b2fstate)

        if self.branch=='3.1.0':
            in_b2fstate.update({
            'fch_32': xy22,
            'fch_52':xy22,
            'fch_p':xy22,
            'fna_eir':xy22s,
            'fne_eir':xy22,
            'fhe_eir':xy22,
            'fhi_eir':xy22,
            'vadia':xy22s,
            'veecrb':xy22,
            'vedia':xy22,
            'floe_noc':xy22,
            'floi_noc':xy22})


        in_b2fplasmf = {
            'bb':xy4,
            'crx':xy4,
            'cry':xy4,
            'ffbz':xy4,
            'fpsi':xy4,
            'gs':xy3,
            'hx':xy,
            'hy':xy,
            'qz':xy2,
            'qc':xy,
            'vol':xy,
            'pbs':xy2,
            'fch0':None,
            'fchp':None,
            'fhe0':None,
            'fhep':None,
            'fhet':xy2,
            'fhi0':None,
            'fhip':None,
            'fhit':xy2,
            'fna0':None,
            'fnap':None,
            'fne':xy2,
            'fni':None,
            'na0':None,
            'nap':None,
            'ne0':None,
            'ne2':xy,
            'nep':None,
            'ni':xy2,
            'ni0':None,
            'pb':None,
            'po0':None,
            'pop':None,
            'te0':None,
            'tep':None,
            'ti0':None,
            'tip':None,
            'ua0':None,
            'uap':None,
            #'uadia':xy2s,
            'fchdia':None,
            'fmo':xy2s,
            'facdrift':None,
            'fac_ExB':None,
            'fac_vis':None,
            'fht':xy2, #Only for >3.0.5?
            'fhj':xy2, #Only for >3.0.5?
            'fhm':xy2s, #Only for >3.0.5?
            'fhp':xy2s,
            'resco':xys,
            'reshe':xy,
            'reshi':xy,
            'resmo':xys,
            'resmt':xy,
            'respo':xy,
            'sch':None,
            'she':xy4,
            'shi':xy4,
            'smo':xy4s,
            'smq':xy4s,
            'sna':xy2s,
            'sne':xy2,
            'rsana':xys,
            'rsahi':xys,
            'rsamo':xys,
            'rrana':xys,
            'rrahi':xys,
            'rramo':xys,
            'rqahe':xys,
            'rqrad':xys,
            'rqbrm':xys,
            'rcxna':None,
            'rcxhi':None,
            'rcxmo':xys,
            'b2stbr_sna':xys,
            'b2stbr_smo':None,
            'b2stbr_she':xy,
            'b2stbr_shi':xy,
            'b2stbr_sch':None,
            'b2stbr_sne':None,
            'b2stbc_sna':None,
            'b2stbc_smo':None,
            'b2stbc_she':xy,
            'b2stbc_shi':xy,
            'b2stbc_sch':None,
            'b2stbc_sne':None,
            'b2stbm_sna':None,
            'b2stbm_smo':None,
            'b2stbm_she':None,
            'b2stbm_shi':None,
            'b2stbm_sch':None,
            'b2stbm_sne':None,
            'b2sihs_divue':None,
            'b2sihs_divua':None,
            'b2sihs_exbe':None,
            'b2sihs_exba':None,
            'b2sihs_visa':None,
            'b2sihs_joule':None,
            'b2sihs_fraa':None,
            'b2sihs_str':None,
            'b2npmo_smaf':xy4s,
            'b2npmo_smag':xy4s,
            'b2npmo_smav':xy4s,
            'b2npmo_smat':xy4s, #Only for >3.0.5?
            'smpr':xys,
            'smpt':xys,
            'smfr':xys,
            'smcf':None, #Only 3.0.8?
            'ext_sna':None,
            'ext_smo':None,
            'ext_she':None,
            'ext_shi':None,
            'ext_sch':None,
            'ext_sne':None,
            'calf':None,
            'cdna':None,
            'cdpa':None,
            'ceqp':None,
            'chce':None,
            'chci':None,
            'chve':None,
            'chvemx':None,
            'chvi':None,
            'chvimx':None,
            'csig':None,
            'cvla':None,
            'cvsa':None,
            'cthe':None,
            'cthi':None,
            'csigin':None,
            'cvsa_cl':None,
            'fllime':None,
            'fllimi':None,
            'fllim0fna':None,
            'fllim0fhi':None,
            'fllimvisc':None,
            'f_luc_ke': None, #Only 3.0.8?
            'f_luc_ki': None, #Only 3.0.8?
            'f_luc_etc_ki': None, #Only 3.0.8?
            'f_luc_sgc_ki': None, #Only 3.0.8?
            'f_luc_alc_ki': None, #Only 3.0.8?
            'fllim_kec_ki': None, #Only 3.0.8?
            'fllim_kic_ki': None, #Only 3.0.8?
            'fllim_alc_ki': None, #Only 3.0.8?
            'fllim_al_c': None, #Only 3.0.8?
            'sig0':None,
            'hce0':xy,
            'alf0':None,
            'hci0':xy,
            'hcib':xys,
            'dpa0':xys,
            'dna0':xys,
            'vsa0':None,
            'vla0':None,
            'csig_an':None,
            'calf_an':None,
            'nstra':None,
            'sclstra':None,
            'sclrtio':None}
        in_b2fplasmf = update(in_b2f_common, in_b2fplasmf)


        if self.branch=='3.1.0':
            in_b2fplasmf.update({
            'fch0':None,
            'fchp':None,
            'fhe0':None,
            'fhep':None,
            'fhet':xy22,
            'fhi0':None,
            'fhip':None,
            'fhit':xy22,
            'fna0':None,
            'fnap':None,
            'fne':xy22,
            'fni':None,
            'fchdia':None,
            'fmo':xy22s,
            'facdrift':None,
            'fac_ExB':None,
            'fac_vis':None,
            'fht':xy22,
            'fhj':xy22,
            'fhm':xy22s,
            'fhp':xy22s})





        in_b2fgmtry = {
            'crx':xy4,
            'cry':xy4,
            'fpsi':xy4,
            'ffbz':xy4,
            'bb':xy4,
            'vol':xy,
            'hx':xy,
            'hy':xy,
            'qz':xy2,
            'qc':xy,
            'gs':xy3,
            'nlreg':1,
            'nlxlo':1,
            'nlxhi':1,
            'nlylo':1,
            'nlyhi':1,
            'nlloc':None,
            'nncut':1,
            'leftcut':self.nncut,
            'rightcut':self.nncut,
            'topcut':self.nncut,
            'bottomcut':self.nncut,
            'leftix':xy,
            'rightix':xy,
            'topix':xy,
            'bottomix':xy,
            'leftiy':xy,
            'rightiy':xy,
            'topiy':xy,
            'bottomiy':xy,
            'region':xyreg,
            'nnreg':reg,
            'resignore':None,
            'periodic_bc':None,
            'pbs':xy2,
            'parg':None}

        in_b2fpardf = {'zamin':None, 'zamax':None, 'zn':None, 'am':None,
           'cbir':None,  'cbnr': None, 'cbrbrk':None,'cbsna':None, 'cbsmo':None,
           'cbshi':None, 'cbshe':None, 'cbsch':None, 'cbrec':None, 'cbmsa':None,
           'cbmsb':None, 'cbsna':None, 'cbsmo':None, 'cbshi':None, 'cbshe':None,
           'cbsch':None, 'cbrec':None, 'cbmsa':None, 'cbmsb':None, 'cbsna':None,
           'cbsmo':None, 'cbshi':None, 'cbshe':None, 'cbsch':None, 'cbrec':None,
           'cbmsa':None, 'cbmsb':None, 'cbsna':None, 'cbsmo':None, 'cbshi':None,
           'cbshe':None, 'cbsch':None, 'cbrec':None, 'cbmsa':None, 'cbmsb':None,
           'cbsna':None, 'cbsmo':None, 'cbshi':None, 'cbshe':None, 'cbsch':None,
           'cbrec':None, 'cbmsa':None, 'cbmsb':None, 'cbsna':None, 'cbsmo':None,
           'cbshi':None, 'cbshe':None, 'cbsch':None, 'cbrec':None, 'cbmsa':None,
           'cbmsb':None, 'cfdf0':None, 'cfdna':None, 'cfdpa':None, 'cfvla':None,
           'cfvsa':None, 'cfhci':None, 'cfhce':None, 'cfsig':None, 'cfalf':None,
           'cflim':None}

        in_b2frates = {
            'ppout':None,
            'rtnt':None,
            'rtnn':None,
            'rtns':None,
            'rtzmin':rtns,
            'rtzmax':rtns,
            'rtzn':rtns,
            'rtt':rtnt,
            'rtn':rtnn,
            'rtlt':rtnt,
            'rtln':rtnn,
            'rtlsa':rt,
            'rtlra':rt,
            'rtlqa':rt,
            'rtlcx':rt,
            'rtlrd':rt,
            'rtlbr':rt,
            'rtlza':rt,
            'rtlz2':rt,
            'rtlpt':rt,
            'rtlpi':rt}


        in_fort44 = {
            'dab2':fxya,
            'tab2':fxya,
            'dmb2':fxym,
            'tmb2':fxym,
            'dib2':fxyi,
            'tib2':fxyi,
            'eneutrad':fxya,
            'emolrad':fxym,
            'eionrad':fxyi}


        in_d= {'b2fplasmf': in_b2fplasmf, 'b2fstate': in_b2fstate,
                'b2fstati': in_b2fstate, 'b2fgmtry': in_b2fgmtry,
                'b2fpardf': in_b2fpardf, 'b2frates': in_b2frates,
                'fort.44': in_fort44}

        for k, d in in_d.items():
            try:
                d.update(to_update[k])
            except:
                pass

        #files = read_config()['Files']
        files = self.config['Files']
        for loc in files['location']:
            for prio in files['priority']:
                if (prio in self.avail and os.path.dirname(
                    os.path.normpath(self.avail[prio]))
                    .startswith(self.rundir_rel(loc))):
                    if prio in in_d and signal in in_d[prio]:
                        #if loc != '.':
                        #    self.log.warning(
                        #        "'{0}' signal read from '{1}'.".format(signal, loc))
                        if prio == 'b2fstati':
                            self.log.warning(
                            "'{0}' signal read from 'b2fstati'.".format(signal))

                        return (prio, in_d[prio][signal])
        self.log.error("'{0}' could not be found in the".format(var)+
                    " implemented description of files.")
        return None






























class StructuredDataMds(StructuredDataBase):
    """
    """

    def __init__(self, *args, **kwargs):
        alog.debug(">> Entering StructuredMds __init__")
        super().__init__(*args, **kwargs)
        alog.debug("<< Leaving  StructuredMds __init__")

    # -------------------- Indices and locators -------------------------------

    @solps_property
    def regvol(self):
        """(nx, ny): Volume region indices. """
        self._read_signal('_regvol','regvol')

    @solps_property
    def regflx(self):
        """(nx, ny): x-directed flux region indices. """
        self._read_signal('_regflx','regflx')

    @solps_property
    def regfly(self):
        """(nx, ny): y-directed flux region indices. """
        self._read_signal('_regfly','regfly')

    @solps_property
    def region(self):
        """(nx, ny, 3): Index of corresponding region.

        [...,0]:  Volume region.

        [...,1]:  x-region.

        [...,2]:  y-region.
        """
        self._region = np.zeros((self.nx, self.ny, 3), dtype=np.int32)
        self._region[...,0] = self.regvol
        self._region[...,1] = self.regflx
        self._region[...,2] = self.regfly

    @solps_property
    def nnreg(self):
        """(3): Number of volumetric, x- and y-regions. """
        self._nnreg = np.zeros(3, dtype=np.int32)
        self._nnreg[0] = self.region[...,0].max()
        self._nnreg[1] = self.region[...,1].max()
        self._nnreg[2] = self.region[...,2].max()




    @solps_property
    def jxi(self):
        """int: SOLPS inner midplane index."""
        self.read_signal('_jxi', r'\SNAPSHOT::TOP.DIMENSIONS:IMP')

    @solps_property
    def jxa(self):
        """int: SOLPS outer midplane index."""
        self.read_signal('_jxa', r'\SNAPSHOT::TOP.DIMENSIONS:OMP')



    @solps_property
    def isep(self):
        """int : Separatrix index (B2 grid).

        Notes
        -----
        Defined as first flux surface in the SOL.
        It means that there a +1 of difference with standard definition.
        Distance from separatrix defined by this index should always be >0.
        """
        self.read_signal('_sep', r'\SNAPSHOT::TOP.DIMENSIONS:SEP')
        self._isep+= 1

    @solps_property
    def targ1(self):
        """int: Index of 'targ1'."""
        self._read_signal('_targ1', 'targ1')

    @solps_property
    def targ2(self):
        """int: Index of 'targ2'."""
        self._read_signal('_targ2', 'targ2')

    @solps_property
    def targ3(self):
        """int: Index of 'targ3'."""
        self._read_signal('_targ3', 'targ3')

    @solps_property
    def targ4(self):
        """int: Index of 'targ4'."""
        self._read_signal('_targ4', 'targ4')

    # ATTENTION: This is wrong for USN.
    @solps_property
    def iixp(self):
        """int: Index of the inner side of the x-point.
        First poloidal index inside SOL/Core.

        Notes
        -----
        Should it be changed to first in div?
        """
        try:
            self._iixp = self.leftcut[0]+1
        except:
            gradte = np.array([self.te[i+1,1]-self.te[i,1]
                               for i in range(self.nx-1)])
            self._iixp = np.argmax(gradte) + 1 #In SOL YES
            #self._ixp = np.argmax(gradte) # In div NO

    @solps_property
    def ioxp(self):
        """int: Index of the outer side of the x-point.
        First poloidal index inside SOL/Core.

        Notes
        -----
        Should it be changed to first in div?
        """
        try:
            self._ioxp = self.rightcut[0]
        except:
            gradte = np.array([self.te[i+1,1]-self.te[i,1]
                               for i in range(self.nx-1)])
            self._ioxp = np.argmin(gradte) #In SOL YES
            #self._ioxp = np.argmin(gradte) + 1 #In div NO 



    @solps_property
    def iout(self):
        """int: Index of the main outer target (index above guarding cell)."""
        self._iout = np.where(self.region[...,1] == 4)[0][0]-1

    @solps_property
    def iinn(self):
        """int: Index of the main inner target (index above guarding cell)."""
        self._iinn = np.where(self.region[...,1] == 1)[0][0]



    ##ATTENTION: To be removed? Is it still useful?
    @solps_property
    def oul(self):   # Outer divertor upstream limit
        self._oul = self.oxp + 1
    ##ATTENTION: To be removed? Is it still useful?
    @solps_property
    def odl(self):   # Outer divertor downstream limit
        self._odl = self.nx - 1
    ##ATTENTION: To be removed? Is it still useful?
    @solps_property
    def iul(self):   # Outer divertor upstream limit
        self._iul = self.ixp - 1
    ##ATTENTION: To be removed? Is it still useful?
    @solps_property
    def idl(self):   # Outer divertor downstream limit
        self._idl = 1

    # -------------------- Neighbours and cuts --------------------------------
    #ATTENTION: Improve docstrings
    @solps_property
    def leftix(self):
        """int: X index of the left neighbour (ix-1,iy).

        Notes
        -----
            Index in base 0, Python-like."""
        self._read_signal('_leftix','leftix')

    @solps_property
    def leftiy(self):
        """int: Y index of the left neighbour (ix-1,iy).

        Notes
        -----
            Index in base 0, Python-like."""
        self._read_signal('_leftiy','leftiy')

    @solps_property
    def rightix(self):
        """int: X index of the right neighbour (ix+1,iy).

        Notes
        -----
            Index in base 0, Python-like."""
        self._read_signal('_rightix','rightix')

    @solps_property
    def rightiy(self):
        """int: Y index of the right neighbour (ix+1,iy).

        Notes
        -----
            Index in base 0, Python-like."""
        self._read_signal('_rightiy','rightiy')

    @solps_property
    def bottomix(self):
        """int: X index of the bottom neighbour (ix,iy-1).

        Notes
        -----
            Index in base 0, Python-like."""
        self._read_signal('_bottomix','bottomix')

    @solps_property
    def bottomiy(self):
        """int: Y index of the bottom neighbour (ix,iy-1).

        Notes
        -----
            Index in base 0, Python-like."""
        self._read_signal('_bottomiy','bottomiy')

    @solps_property
    def topix(self):
        """int: X index of the top neighbour (ix,iy+1).

        Notes
        -----
            Index in base 0, Python-like."""
        self._read_signal('_topix','topix')

    @solps_property
    def topiy(self):
        """int: Y index of the top neighbour (ix,iy+1).

        Notes
        -----
            Index in base 0, Python-like."""
        self._read_signal('_topiy','topiy')


    # -------------------- Grid and vessel geometry ---------------------------
    #ATTENTION: In rundir it is read from input.dat most likely,
    #but in mds, it is read from mesh.extra.
    @solps_property
    def vessel(self):
        """(4, nsurfaces): Vessel structure."""
        self._read_signal('_vessel', 'vessel')

    @property
    def vessel_mesh(self):
        """ Alias for :py:attr:`~vessel`, as in MDS+ only
        vessel_mesh is saved.
        """
        return self.vessel

    #ATTENTION: Add docstrig
    @solps_property
    def cr(self):
        """(nx, ny): R-coordinate of the cell centre [m]"""
        self._read_signal('_cr', 'cr')
    #ATTENTION: Add docstrig
    @solps_property
    def cr_x(self):
        """(nx, ny): R-coordinate of the cell left x-face [m]"""
        self._read_signal('_cr_x', 'cr_x')
    #ATTENTION: Add docstrig
    @solps_property
    def cr_y(self):
        """(nx, ny): R-coordinate of the cell bottom y-face [m]"""
        self._read_signal('_cr_y', 'cr_y')

    @solps_property
    def cz(self):
        """(nx, ny): Z-coordinate of the cell centre [m]"""
        self._read_signal('_cz', 'cz')

    @solps_property
    def cz_x(self):
        """(nx, ny): Z-coordinate of the cell left x-face [m]"""
        self._read_signal('_cz_x', 'cz_x')

    @solps_property
    def cz_y(self):
        """(nx, ny): Z-coordinate of the cell bottom y-face [m]"""
        self._read_signal('_cz_y', 'cz_y')

    @solps_property
    def r(self):
        """(nx, ny, 4): R-coordinate of the corners of the B2 grid [m]."""
        self._read_signal('_r', 'r')

    @property
    def crx(self):
        return self.r

    @solps_property
    def z(self):
        """(nx, ny, 4): Z-coordinate of the corners of the B2 grid [m]."""
        self._read_signal('_z', 'z')

    @property
    def cry(self):
        return self.z


    @solps_property
    def dspar(self):
        """(nx,ny): Parallel distance with respect to first poloidal cell
        in the computational grid layout [m].

        Notes
        -----
            Almost equal to self calculated :py:attr:`~ipardspar`.
            Also defined for closed flux surfaces.
        """
        self._read_signal('_dspar', 'dspar')



    @solps_property
    def sx(self):
        """(nx, ny): Area of the x-face between cell (ix,iy) and its left
        neighbour (ix-1,iy) [m^2].
        """
        self._read_signal('_sx','sx')
        self._sx = np.concatenate((np.zeros((1,self.ny)),
                                        self._sx), axis = 0)

    @solps_property
    def sy(self):
        """(nx, ny): Area of the y-face between cell (ix,iy) and its bottom
        neighbour (ix,iy-1) [m^2].
        """
        self._read_signal('_sy','sy')
        self._sy = np.concatenate((np.zeros((self.nx,1)),
                                        self._sy), axis = 1)

    @solps_property
    def gs(self):
        """(nx, ny, 3): Area of faces between cell (ix,iy) and its left and
        bottom neighbours [m^2].

        gs[...,0] left x-face (left face).

        gs[...,1] bottom y-face (bottom face).

        Notes
        -----
        gs[...,2] corresponding to SZ is still missing!
        But:
            >>> np.allclose(shot.gs[...,:-1], run.gs[...,:-1])
            >>> True
        """
        self._gs = np.zeros((self.nx,self.ny,3))
        self._gs[...,0] = self.sx
        self._gs[...,1] = self.sy




    @solps_property
    def fnax(self):
        """(nx, ny, ns): Poloidal particle flux of species is through the x-face
        between the cell (ix, iy) and its left neighbour (ix-1,iy) [s^-1].

        Notes
        -----
        fnax[0] = np.zeros(ny, ns) to complete the dimensions.
        """
        self._read_signal('_fnax', 'fnax')
        self._fnax = np.concatenate((np.zeros((1,self.ny,self.ns)),
                                    self._fnax), axis = 0)

    @solps_property
    def fnax32(self):
        """(nx, ny, ns): Convective poloidal particle flux of species is
        through the x-face between the cell (ix, iy) and its left
        neighbour (ix-1, iy) [s^-1].

        Notes
        -----
        fnax[0] = np.zeros(ny, ns) to complete the dimensions.
        """
        self._read_signal('_fnax32', 'fnax_32')
        self._fnax32 = np.concatenate((np.zeros((1,self.ny,self.ns)),
                                    self._fnax32), axis = 0)

    @solps_property
    def fnax52(self):
        """(nx, ny, ns): Conductive poloidal particle flux of species is
        through the x-face between the cell (ix, iy) and its left
        neighbour (ix-1, iy) [s^-1].

        Notes
        -----
        fnax[0] = np.zeros(ny, ns) to complete the dimensions.
        """
        self._read_signal('_fnax52', 'fnax_52')
        self._fnax52 = np.concatenate((np.zeros((1,self.ny,self.ns)),
                                    self._fnax52), axis = 0)

    @solps_property
    def fnay(self):
        """(nx, ny, ns): Radial particle flux of species is through the y-face
        between the cell (ix, iy) and its bottom neighbour (ix, iy-1) [s^-1].

        Notes
        -----
        fnay[:,0,:] = np.zeros(nx, ns) to complete the dimensions.
        """
        self._read_signal('_fnay', 'fnay')
        self._fnay = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                    self._fnay), axis = 1)

    @solps_property
    def fnay32(self):
        """(nx, ny, ns): Convective radial particle flux of species is
        through the y-face between the cell (ix, iy) and its bottom
        neighbour (ix, iy-1) [s^-1].

        Notes
        -----
        fnay[:,0,:] = np.zeros(nx, ns) to complete the dimensions.
        """
        self._read_signal('_fnay32', 'fnay_32')
        self._fnay32 = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                    self._fnay32), axis = 1)

    @solps_property
    def fnay52(self):
        """(nx, ny, ns): Conductive radial particle flux of species is
        through the y-face between the cell (ix, iy) and its bottom
        neighbour (ix, iy-1) [s^-1].

        Notes
        -----
        fnay[:,0,:] = np.zeros(nx, ns) to complete the dimensions.
        """
        self._read_signal('_fnay52', 'fnay_52')
        self._fnay52 = np.concatenate((np.zeros((self.nx,1,self.ns)),
                                    self._fnay52), axis = 1)

    @solps_property
    def fna(self):
        """(nx, ny, 2, ns): Particle flux through cell faces [s^-1].

        fna[...,0]: poloidal flux through left face.
        Alias of :py:attr:`~fnax`.

        fna[...,1]: radial flux through bottom face.
        Alias of :py:attr:`~fnay`.
        """
        self._fna = np.zeros((self.nx,self.ny,2,self.ns))
        self._fna[:,:,0,:] = self.fnax
        self._fna[:,:,1,:] = self.fnay


    @solps_property
    def fna32(self):
        """(nx, ny, 2, ns): Convective particle flux through cell faces [s^-1].

        fna32[...,0]: poloidal flux through left face.
        Alias of :py:attr:`~fnax32`.

        fna32[...,1]: radial flux through bottom face.
        Alias of :py:attr:`~fnay32`.
        """
        self._fna32 = np.zeros((self.nx,self.ny,2,self.ns))
        self._fna32[:,:,0,:] = self.fnax32
        self._fna32[:,:,1,:] = self.fnay32

    @solps_property
    def fna52(self):
        """(nx, ny, 2, ns): Conductive particle flux through cell faces [s^-1].

        fna52[...,0]: poloidal flux through left face.
        Alias of :py:attr:`~fnax52`.

        fna52[...,1]: radial flux through bottom face.
        Alias of :py:attr:`~fnay52`.
        """
        self._fna52 = np.zeros((self.nx,self.ny,2,self.ns))
        self._fna52[:,:,0,:] = self.fnax52
        self._fna52[:,:,1,:] = self.fnay52





    ##ATTENTION: Add docstring
    #@solps_property
    #def fnex(self):
    #    """ Definition of the manual, not the implemented b2 src code.
    #    This uses za instead of rza.
    #    Not equal in values to the one from rundir"""
    #    self._fnex = np.sum(self.za*self.fnax,2) + self.fchx/self.qe

    ##ATTENTION: Add docstring
    #@solps_property
    #def fney(self):
    #    """ Definition of the manual, not the implemented b2 src code.
    #    This uses za instead of rza.
    #    Not equal in values to the one from rundir"""
    #    self._fney = np.sum(self.za*self.fnay,2) + self.fchy/self.qe

    ##ATTENTION: Add docstring
    #@solps_property
    #def fne(self):
    #    """ Definition of the manual, not the implemented b2 src code.
    #    This uses za instead of rza.
    #    Not equal in values to the one from rundir"""
    #    self._fne = np.zeros((self.nx,self.ny,2))
    #    self._fne[...,0] = self.fnex
    #    self._fne[...,1] = self.fney






## ATTENTION: 2024, commented, I think that found in rundir?
#    #ATTENTION STILL NOT WORKING
#    @solps_property
#    def fne(self):
#        """Also not the same as rundir"""
#        from solpspy.b25_src.b2aux.b2xpfe import b2xpfe
#        self._fne = b2xpfe(self)
#
#    @solps_property
#    def fnex(self):
#        """Also not the same as rundir"""
#        self._fnex = self.fne[...,0]
#
#    @solps_property
#    def fney(self):
#        """Also not the same as rundir"""
#        self._fney = self.fne[...,1]
