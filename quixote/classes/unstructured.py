## TODO:
## Copy utility/intcell whcih is apparently the function that maps quantities to cell centers

## 26/07/2022 ideas:
## What if I move the read_signal, parser, etc out of the class so that it can be
## called by the class factory, and then I just make
## def _read_signal(self,...) 
##  return read_signal(self,...)
## Read signal needs paths and dimensions, so probably something like
## get basic dimensions would be more appropiate for a class factory, depending
## on what is needed.
## Maybe even create unstructured class within class factory, and return that if
## unstructured is correct, or use the parameters to create classic and return that
## instead.
## It creates the class twice, but it is pretty fast anyway, so it might not be
## a problem.


# Common modules:
import os
import numpy as np

# Quixote modules:
from .base import BaseData
from ..tools import solps_property
from ..tools import update, get
from ..tools import extend
from ..tools import create_log

from ..tools import solps_property2

#Extensions
#from .timetraces import TargetData, MidplaneData
#from ..extensions.unstructured_eqout import EquationsOutput
from ..extensions.masks import UnstructuredMasks


alog = create_log('quixote')

class UnstructuredData(BaseData):
    """
    """

    grid_type = 'unstructured'
    ns_axis = 1 #Usual axis of species index
    __DOCPARAMS__ ={
        'GRID_DIMS': 'ncv',
        'FACE_DIMS': 'nfc',
        }

    def __init__(self, *args, **kwargs):
        alog.debug(">> Entering Unstructured __init__")
        super().__init__(*args, **kwargs)
        alog.debug("<< Leaving  Unstructured __init__")


        self.chords.add_chord('SELF OMP',
            extend(self.rzomp[0],0.0), extend(self.rzomp[1],0.0))

        try:
            self.chords.add_chord('SELF IMP',
                extend(self.rzimp[0],0.0), extend(self.rzimp[1],0.0))
        except:
            self.log.warning("IMP not defined in b2.users.params")

##  EXTENSIONS ================================================================


    #@solps_property
    #def eqout(self):
    #    self._eqout = EquationsOutput(self)

    @solps_property
    def masks(self):
        self._masks = UnstructuredMasks(self)

##  IDENT PROPERTIES ==========================================================


    @solps_property
    def gitversion(self):
        if 'b2fplasmf' in self.avail:
            fd = self.avail['b2fplasmf']
        elif 'b2fstate' in self.avail:
            fd = self.avail['b2fstate']
        elif 'b2fstati' in self.avail:
            fd = self.avail['b2fstati']
        elif 'b2fgmtry' in self.avail:
            fd = self.avail['b2fgmtry']
        with open(fd,'r') as b2p:
            title = b2p.readline()
            self._gitversion = title[18:].strip()




##  GEOMETRY ==================================================================

    # -------------------- Structure ------------------------------------------


    # -------------------- Dimensions -----------------------------------------
    #Basic dimensions are read with get_basic_dimensions() 


    # -------------------- Indices and locators -------------------------------
    ## ATTENTION: I will probably have to create a function called linefield
    ## that returns the indices of cells along a given line field.
    ## Maybe also something similar for poloidal profiles, given a line return
    ## indices of cells containing that line.
    ## Maybe allow for the creation of a LOS file with
    ## name : x,y,x,y and then you can call self.los[name]


    @property
    def ftregions(self):
        return {
            'core': 1,
            'sol': 2,
            'pfr': 3,
            1: 'core',
            2: 'sol',
            3: 'pfr'
            }

    ## ATTENTION: These should be fsreg and the other ones ifsreg
    @property
    def fsregions(self):
        return {
            'sep': -1,
            'core': 1,
            'sol': 2,
            'pfr': 3,
            -1: 'sep', 
            1: 'core',
            2: 'sol',
            3: 'pfr'
            }



#    @solps_property
#    def nfcx(self):
#        """ Based on Flux Tubes.
#        From 0 to nfcx-1, self.icvft[self.ifccv[ind]] should
#        show that both cells belong to the same FT.
#
#        Notes
#        -----
#        Only really valid for classical-like grids. Fully unstructured
#        grids won't preserve this characteristic.
#        """
#
#        for ifc in range(self.nfc):
#            fts = self.cvft[self.ifccv[ifc]]
#            if fts[0] != fts[1]:
#                self._nfcx = ifc
#                break
#
#    @solps_property
#    def nfcy(self):
#        self._nfcy = self.nfc-self.nfcx


    #def cvOnClosedSurface(self, icv):
    @solps_property
    def cfs(self):
        """ Recreation of mpg%cvOnClosedSurface.

        Notes
        -----
        Line 562-564 of modules/b2us_map.F
        m%cvOnClosedSurface = mod(m%cvReg,4).eq.1
        """
        self._cfs = (self.cvreg%4 ==1)


    @solps_property
    def rzomp(self):
        """(2,2): Beginning [0,:] and end [1,:] points' R[:,0] and z[:,1] values
        of OMP.
        """
        self._rzomp = self._read_b2file_param('b2.user.parameters','rzomp',(2,2)).T

    @solps_property
    def rzimp(self):
        """(2,2): Beginning [0,:] and end [1,:] points' R[:,0] and z[:,1] values
        of IMP.
        """
        self._rzimp = self._read_b2file_param('b2.user.parameters','rzimp',(2,2)).T


    @solps_property
    def iomp(self):
        """ String of indices of the interseting
            grid cells with the OMP LOS, rzomp.
        """
        #self.chords.add_chord('SELF OMP',
        #    extend(self.rzomp[0],0.0), extend(self.rzomp[1],0.0))
        self._iomp = self.chords['SELF OMP'].indices

    @solps_property
    def iimp(self):
        """ String of indices of the interseting
            grid cells with the IMP LOS, rzomp.
        """
        #self.chords.add_chord('SELF IMP',
        #    extend(self.rzimp[0],0.0), extend(self.rzimp[1],0.0))
        self._iimp = self.chords['SELF IMP'].indices


    ##ATTENTION: change to list??
    @solps_property
    def icvfc(self):
        """(nci, 4): Indices of faces for each cell.
        """
        self._icvfc = np.full((self.ncv,4), -999999, dtype=int)
        for icv in range(self.ncv): # For each internal cell.
            fc = []
            for i, ifc in enumerate(range(self.cvfcp[icv,1])):
                self._icvfc[icv, i] = self.cvfc[self.cvfcp[icv, 0]+ifc]


    @property
    def ifccv(self):
        """ From face index return the two corresponding cells.
        [:,0]: left/bottom for x/y face.
        [:,1]: right/top for x/y face.
        """
        return self.fccv



    @solps_property
    def icvvx(self):
        """(ncv, nvert): Unique indices of vertices for each internal cell.
        """
        self._icvvx = []
        for icv in range(self.ncv):
            vx=[]
            faces = self.icvfc[icv]
            for face in faces:
                try:
                    vx.append(self.fcvx[face])
                except:
                    pass
            ## Order them with arctan2??
            self._icvvx.append(np.unique(vx))


    ##ATTENTION: Transform into numpy array??
    @solps_property
    def ivxcv(self):
        """ (nvx): From vertex index return the corresponding amount of cells.
        """
        self._ivxcv = []
        for ivx in range(self.nvx):
            tmp =[]
            for icv in range(self.vxcvp[ivx,1]):
                tmp.append(int(self.vxcv[self.vxcvp[ivx, 0]+icv]))
            self._ivxcv.append(tmp)

    ##ATTENTION: Transform into numpy array??
    @solps_property
    def ivxfc(self):
        """ (nvx, ifc): Faces which contain given vertex.
        """
        self._ivxfc = []
        for ivx in range(self.nvx):
            self._ivxfc.append(np.where(self.ifcvx == ivx)[0])

    @property
    def ifcvx(self):
        """ (nfc, 2): Vertices forming a given surface.
        """
        return self.fcvx


    @solps_property
    def ivxfs(self):
        """
        """
        vxfs = []
        #import ipdb; ipdb.set_trace()
        for ivx in range(self.nvx):
            tmp = np.unique(self.ifcfs[self.ivxfc[ivx]])[1:]
            if tmp.shape[0]==1:
                vxfs.append(tmp[0])
            else:
                vxfs.append(tmp)
        self._ivxfs = np.array(vxfs, dtype=object)




    ##ATTENTION: Transform into numpy array??
    @solps_property
    def icifc(self):
        """(ncv, 4): Indices of faces for each internal cell.
        """
        self._icifc = np.full((self.nci,4), np.nan, dtype=int)
        for ici in range(self.nci): # For each internal cell.
            fc = []
            for i, ifc in enumerate(range(self.cvfcp[ici,1])):
                self._icifc[ici, i] = self.cvfc[self.cvfcp[ici, 0]+ifc]

    ##ATTENTION: Transform into numpy array??
    @solps_property
    def icivx(self):
        """(nci, nvert): Unique indices of vertices for each internal cell.
        """
        self._icivx = []
        for ici in range(self.nci):
            vx=[]
            faces = self.icifc[ici]
            for face in faces:
                try:
                    vx.append(self.fcvx[face])
                except:
                    pass
            ## Order them with arctan2??
            self._icivx.append(np.unique(vx))


    @solps_property
    def iftcv(self):
        """ (nft, icv): Indices of ordered cells along a flux tube.

        Notes
        -----
        Are the indices always in order to begin with?
        For now, just a list of ordered indices, check validity.
        """
        self._iftcv = []
        for ift in range(self.nft):
            self._iftcv.append(np.arange(self.ncv)[self.cvft == ift])


    @property
    def icvft(self):
        """ (ncv): Corresponding Flux Tube to each cell.
        Alias of cvft
        """
        return self.cvft


    @solps_property
    def ifsfc(self):
        """ (nfs, ifc): Unique indices of cell (radial) faces in a
        given flux surface.
        """
        self._ifsfc = []
        for ifs in range(self.nfs):
            fc=[]
            iface = self.fsfcp[ifs,0]
            for ifc in range(self.fsfcp[ifs,1]):
                try:
                    fc.append(self.fsfc[iface+ifc])
                except:
                    pass
            ## Order them with arctan2??
            self._ifsfc.append(np.array(fc))
        self._ifsfc = np.array(self._ifsfc, dtype=object)


    ##ATTENTION: Complete!
    @solps_property
    def ifcfs(self):
        """ (nfc): Return the corresponding flux surface of a
        given face.

        Notes
        -----
        -9999 means that the face is a poloidal face and thus
        does not correspond to any flux surface.
        """
        self._ifcfs = np.full((self.nfc), -999999, dtype=int)
        for ifs in range(self.nfs):
            faces = self.ifsfc[ifs]
            for face in faces:
                self._ifcfs[face] = ifs



#    ##ATTENTION: Complete!
#    @solps_property
#    def ifsft(self):
#        """ (nfs, 2): Indices of the two flux tubes surrounding 
#        the corresponding flux surface.
#        """
#        self._ifsft = np.full((self.nfs,2), -999999, dtype=int)
#        for ifs in range(self.nfs):
#            pass
#
#
#    ##ATTENTION: Complete!
#    @solps_property
#    def iftfs(self):
#        """ (nft, ifs): 
#        Flux tubes k
#        """
#        self._iftfs = []
#        for ift in range(self.nft):
#            pass


    @solps_property
    def ifscv(self):
        """ (nfs, icv, 2):
        """
        tmp = []
        for ifs in range(self.nfs):
            tmp.append(self.ifccv[self.ifsfc[ifs]]) 
        self._ifscv = np.array(tmp, dtype=object)

    @solps_property
    def icvfs(self):
        """ (ncv, 2)
        """
        tmp = []
        for icv in range(self.ncv):
            inds = np.where(self.ifcfs[self.icvfc[icv]]>0)
            tmp.append(self.ifcfs[self.icvfc[icv]][inds])
        self._icvfs = np.array(tmp, dtype=object)




    @solps_property
    def isepfs(self):
        """ Flux surface index of separatrix.
        With ifsfc that provides all faces of the separatrix.

        Notes
        -----
        For multiple separatrices, list, or separate isep2 with [2]??
        """
        isepfs = []
        for ifs in range(self.nfs):
            if np.any(np.in1d(self.fsfcreg[ifs], -1)):
                isepfs.append(ifs)
        if len(isepfs) ==1:
            self._isepfs = isepfs[0]
        else:
            self._isepfs = np.array(isepfs)



    @solps_property
    def isepft(self):
        """ Flux tube index of first ring in SOL.
        """
        self._isepft = np.where(self.ftreg==2)[0][0]
        if (self.magnetic_geometry == 'ddn' or
            self.magnetic_geometry == 'ddnu' or
            self.magnetic_geometry == 'cdn'):
            self._isepft = np.array([self._isepft, self._isepft+1], dtype=int)

    def is_isep(self, cv):
        cv = extend(cv)
        tmp = np.zeros_like(cv, dtype=bool)
        for i, icv in enumerate(cv):
            for isep in self.isepft:
                inds = self.iftcv[isep]
                tmp[i]= np.logical_or(tmp[i], icv in inds)
        if tmp.shape[0]==1:
            return tmp[0]
        else:
            return tmp

    @solps_property
    def isepcv(self):
        """(icv, separatrices): """
        self._isepcv = []
        for isep in extend(self.isepft):
            self._isepcv.append(self.iftcv[isep])
        if len(self._isepcv) == 1:
            self._isepcv = self._isepcv[0]




    @solps_property
    def ixpoint(self):
        """ Indices of all the x-points (vertices shared by 8 cells).
        Only tested for single-null so far.
        """
        self._ixpoint = []
        for i,vert in enumerate(self.ivxcv):
            if len(vert)== 8:
                self._ixpoint.append(i)





    # -------------------- Grid and vessel geometry ---------------------------
    @solps_property
    def grid(self):
        """(nci, vertices, 2): Corners of the internal cells [m].

        Notes
        -----
        [grid] = nci, vertex, R or z
        """
        self._grid = []
        for ici in range(self.nci): # For each internal cell.
            verts = self.icivx[ici]
            poly = np.zeros((len(verts),2))
            for i, vert in enumerate(verts):
                poly[i,0] = self.vxx[vert]
                poly[i,1] = self.vxy[vert]
            x = poly[:,0]
            y = poly[:,1]
            order = np.argsort(np.arctan2(y-y.mean(), x-x.mean()))
            poly[:,0] = x[order]
            poly[:,1] = y[order]
            self._grid.append(poly)


    @solps_property
    def crx(self):
        self._crx = np.array([np.average(g[:,0]) for g in self.grid])

    @solps_property
    def cry(self):
        self._cry = np.array([np.average(g[:,1]) for g in self.grid])


# ---------------------------- Geometry: cell quantities
    @solps_property
    def cvfcp(self):
        """ (ncv, 2): pointing for every cell to the first index number
        in the cvFc list and its number of faces.
        [:,0]: index of the first cell.
        [:,1]: number of faces.
        """
        self._read_signal('_cvfcp','cvfcp')
        self._cvfcp = self._cvfcp.astype(int)
        self._cvfcp[:,0] = self._cvfcp[:,0] -1  #Python indexing.

    @solps_property
    def cvfc(self):
        """ (ncmxfc): listing for every cell the corresponding
        face numbers. Disabled cells get -1, set to -9999
        """
        self._read_signal('_cvfc','cvfc')
        self._cvfc = self._cvfc.astype(int) -1 #Python indexing.
        self._cvfc[self._cvfc==-1] = -999999


    @solps_property
    def cvvxp(self):
        """ (ncv, 2): pointing for every cell to the first index number in
        the cvVx list and its number of vertices.
        """
        self._read_signal('_cvvxp','cvvxp')
        self._cvvxp = self._cvvxp.astype(int)
        self._cvvxp[:,0] = self._cvvxp[:,0] -1  #Python indexing.

    @solps_property
    def cvvx(self):
        """ (ncv): listing for every cell the corresponding vertex numbers."""
        self._read_signal('_cvvx','cvvx')
        self._cvvx = self._cvvx.astype(int) -1 #Python indexing.

    @solps_property2
    def cvft(self):
        """ (ncv): Corresponding Flux Tube for each cell. """
        self._read_signal('_cvft','cvft')
        self._cvft = self._cvft.astype(int) -1 #Python indexing.




    @solps_property
    def cvbb(self):
        """ (ncv, 4): Magnetic field [T]"""
        self._read_signal('_cvbb','cvbb')

    @property
    def bb(self):
        """ alias of cvbb"""
        return self.cvbb

    @property
    def b(self):
        """ alias of cvbb"""
        return self.cvbb


    @solps_property
    def cvx(self):
        """ (ncv): X-coordinate of cell center [m]"""
        self._read_signal('_cvx','cvx')

    @solps_property
    def cvy(self):
        """ (ncv): Y-coordinate of cell center [m]"""
        self._read_signal('_cvy','cvy')

    @solps_property
    def cvsz(self):
        """ (ncv): Area perp. to Z [m^2]"""
        self._read_signal('_cvsz','cvsz')

    @solps_property
    def cvhz(self):
        """ (ncv): Cell lenght in Z [m^2]"""
        self._read_signal('_cvhz','cvhz')

    @solps_property
    def cvhx(self):
        """ (ncv): Cell lenght in X: characteristic poloidal lenght [m^2]
        Note
        ----
        Only temporary, because meaning unclear in WG context.
        """
        self._read_signal('_cvhx','cvhx')

    @solps_property2
    def cvqgam(self):
        """ (ncv, 2): complementary angle between poloidal
        and radial directions [ ]

        Note
        ----
        Probably Gamma angle.
        Probably Cosine [:,0] and Sine [:,0].
        """

    @solps_property
    def cvvol(self):
        """ (ncv): Cell volume [m^3]"""
        self._read_signal('_cvvol','cvvol')

    @property
    def vol(self):
        """ Alias for :py::attr:`~cvvol`"""
        return self.cvvol

    @solps_property
    def cvconn(self):
        """ (ncv): Connection length for each cell [m]"""
        self._read_signal('_cvconn','cvconn')

#    ##ATTENTION: No where??
#    @solps_property
#    def cvonedbsq(self):
#        """ (ncv): 1/B**2 in cell center [T^-2]"""
#        self._read_signal('_cvonedbsq','cvonedbsq')

#    ##ATTENTION: No where??
#    @solps_property
#    def cvbzb(self):
#        """ (ncv): Factor for P-S/mdf flows [??]"""
#        self._read_signal('_cvbzb','cvbzb')

    @solps_property
    def cveb(self):
        """ (ncv, 3): Unit vector pointing along magnetic field
        (Cartesian X,Y,Z) [??]

        Notes
        -----
        These unit vectors are normalized (as expected by the name).
        """
        self._read_signal('_cveb','cveb')


    @solps_property2
    def cvreg(self):
        """(ncv): Corresponding volumetric region to icv.
        """
        self._read_signal('_cvreg','cvreg')
        self._cvreg = self._cvreg.astype(int)


    @solps_property
    def cvlbl(self):
        self._read_signal('_cvlbl','cvlbl')
        self._cvlbl = self._cvlbl.astype(int)


# ---------------------------- Geometry: face quantities



    @solps_property
    def fcvx(self):
        """ (nfc, 2): Index of vertices for each face."""
        self._read_signal('_fcvx','fcvx')
        self._fcvx = self._fcvx.astype(int) -1

    @solps_property
    def fccv(self):
        """ (nfc, 2): Index of cell for each face."""
        self._read_signal('_fccv','fccv')
        self._fccv = self._fccv.astype(int) -1

    @solps_property2
    def fclbl(self):
        """ (nfc): Label for face-structure contact.
        0: no contact with structure
        else: index of the structure, e.g., in b2.boundary.parameters.
        """
        self._read_signal('_fclbl', 'fclbl')
        self._fclbl = self._fclbl.astype(int)

    @solps_property
    def fcbb(self):
        """ (nfc, 4): Magnetic field  recomputed at cell face [T]"""
        self._read_signal('_fcbb','fcbb')

    @solps_property
    def fcs(self):
        """ (nfc): Cell face area [m^2]"""
        self._read_signal('_fcs','fcs')

    @solps_property
    def fchc(self):
        """ (nfc): Lengths between cell face center and neighboring
        cell centers ("connector" direction) [m]
        """
        self._read_signal('_fchc','fchc')

    @solps_property
    def fcht(self):
        """ (nfc): Lengths along cell face ("tangential" direction) [m]"""
        self._read_signal('_fcht','fcht')

    @solps_property
    def fchz(self):
        """ (nfc): Length in Z direction [m]"""
        self._read_signal('_fchz','fchz')

    @solps_property
    def fcvol(self):
        """ (nfc): Part of volume of neighboring cell centers associated
        with the face; for interpolation [m^3]
        """
        style = self.read_switch('b2mndr_fcVol_style', default=0)
        if style==0:
            self._fcvol = np.full((self.nfc, 2), np.nan)
            for ifc in range(self.nfc):
                self._fcvol[ifc] = 0.5*self.cvvol[self.ifccv[ifc]]
        elif style==1:
            self._fcvol = np.full((self.nfc, 2), 0.5)


    @solps_property
    def fcqgam(self):
        """ (nfc, 2): Cosine (:,0) and sine (:,1) of gamma."""
        self._read_signal('_fcqgam','fcqgam')

    @property
    def gamma(self):
        """ Alias of fcqgam"""
        return self.fcqgam

    @solps_property
    def fcqalf(self):
        """ (nfc, 2): Cosine (:,0) and sine (:,1) of alfa."""
        self._read_signal('_fcqalf','fcqalf')

    @property
    def alpha(self):
        """ Alias of fcqalf"""
        return self.fcqalf

    @solps_property
    def fcqbet(self):
        """ (nfc, 2): Cosine (:,0) and sine (:,1) of beta."""
        self._read_signal('_fcqbet','fcqbet')

    @property
    def beta(self):
        """ Alias of fcqbet"""
        return self.fcqbet

    @solps_property
    def fcpbs(self):
        """ (nfc): Magnetic contact area [m^2]"""
        self._read_signal('_fcpbs','fcpbs')

    @property
    def pbs(self):
        return self.fcpbs

    @solps_property
    def fcpbshz(self):
        """ (nfc): Magnetic contact area * hz [m^3]"""
        self._read_signal('_fcpbshz','fcpbshz')

    @solps_property
    def fcbzb(self):
        """ (ncv): Factor for P-S/mdf flows [??]"""
        self._read_signal('_fcbzb','fcbzb')

     ##ATTENTION: Not in b2fgmtry??
#    @solps_property
#    def fceb(self):
#        """ (ncv): Unit vector pointing along magnetic field
#        (Cartesian X,Y,Z) [??]
#        """
#        self._read_signal('_fceb','fceb')



    ##ATTENTION: Improve?
    ##ATTENTION: Create region definition in MagGeom classes
    @solps_property2
    def fcreg(self):
        """ (nfc): x/y region of the x/y faces.
        For now, keep as the original, but ideally, (???, 2).
        Maybe needed to be done in mix classes, like
        UnstructuredUSN, since reg definition changes.
        Unless I just create a reg definition in each class
        and that helps resolve the probelm
        """
        self._read_signal('_fcreg','fcreg')
        self._fcreg = self._fcreg.astype(int)






    def labels(self, ident):
        """ If int given, returns name.  If name given, returns int."""
        
        defaults = {
            -13: 'out',

            -21: 'core',


            -25: 'core',

            -34: 'inn',

            -42: 'mcwsol',
            -43: 'mcwpfrout',
            -44: 'mcwpfrinn',
            'mcw': [-42, -43, -44],

        }

        ##Read file called labels.dat or something like that.
        ## So that the users can define what does the different
        ## non default labels mean for them.

        #tmp = {1:'h',2:'he',3:'li',4:'be',5:'b',6:'c',7:'n',8:'o',9:'f',
        #    10:'ne',11:'na',12:'mg',13:'al',14:'si',15:'p',16:'s',
        #    17:'cl',18:'ar',19:'k',20:'ca',21:'sc',22:'ti',23:'v',
        #    24:'cr',25:'mn',26:'fe',27:'co',28:'ni',29:'cu',30:'zn',
        #    31:'ga',32:'ge',33:'as',34:'se',35:'br',36:'kr',37:'rb',
        #    38:'sr',39:'y',40:'zr',41:'nb',42:'mo',43:'tc',44:'ru',
        #    45:'rh',46:'pd',47:'ag',48:'cd',49:'in',50:'sn',51:'sb',
        #    52:'te',53:'i',54:'xe',55:'cs',56:'ba',57:'la',58:'ce',
        #    59:'pr',60:'nd',61:'pm',62:'sm',63:'eu',64:'gd',65:'tb',
        #    66:'dy',67:'ho',68:'er',69:'tm',70:'yb',71:'lu',72:'hf',
        #    73:'ta',74:'w',75:'re',76:'os',77:'ir',78:'pt',79:'au',
        #    80:'hg'}
        try:
            if isinstance(ident, str):
                return tmp[ident]
            else:
                return tmp[int(ident)]
        except:
            return {y: x for x, y in tmp.items()}[ident.lower()]


    def ilblfc(self, labels):
        """ Given a label or a list of them,
        provide faces with such label.
        """
        labels = extend(labels)
        tmp = []
        for label in labels:
            if isinstance(label, str):
                label = self.labels(label)
            tmp.append(np.where(self.fclbl==label)[0])
        return tmp








# ---------------------------- Geometry: vertex quantities
    @solps_property
    def vxbb(self):
        """ (nvx, 4): Magnetic field recomputed at vertices [T]"""
        self._read_signal('_vxbb','vxbb')

    @solps_property
    def vxx(self):
        """ (nvx): X-coordinate of vertex [m]"""
        self._read_signal('_vxx','vxx')

    @solps_property
    def vxy(self):
        """ (nvx): Y-coordinate of vertex [m]"""
        self._read_signal('_vxy','vxy')

    @solps_property
    def vxhz(self):
        """ (nvx): Cell lenght in Z [m^2]"""
        self._read_signal('_vxhz','vxhz')

    @solps_property
    def vxvol(self):
        """ (nvx): Part of volume of surrounding cell centers
         associated with the vertex; for interpolation [m^3]
        """
        style = self.read_switch('b2mndr_vxVol_style', default=2)
        if style == 0:
            ## Cells divide volume equally among vertices.
            tmp = []
            for ivx in range(self.nvx):
                pass
        elif style == 1:
            ## Fraction of cell volume to vertices.
            tmp = []
            for ivx in range(self.nvx):
                pass
        elif style == 2:
            ## 1/4 except at guard cell.
            tmp = []
            for ivx in range(self.nvx):
                pass
        elif style==3:
            ## Equal weight for each cell around vertices.
            tmp = []
            for ivx in range(self.nvx):
                pass

    @solps_property
    def vxffbz(self):
        """ (nvx): [??]"""
        self._read_signal('_vxffbz','vxffbz')

    @solps_property
    def vxfpsi(self):
        """ (nvx): Full magnetic flux ? [??]"""
        self._read_signal('_vxfpsi','vxfpsi')

    #@property
    #def fpsi(self):
    #    """ (nvx): Full magnetic flux ? [??]"""
    #    return self.vxfpsi

    @solps_property
    def vxonedbsq(self):
        """ (nvx): 1/B**2 at vertices [T^-2]"""
        self._read_signal('_vxonedbsq','vxonedbsq')

    @solps_property
    def vxbzb(self):
        """ (nvx): Factor for P-S/mdf flows [??]"""
        self._read_signal('_vxbzb','vxbzb')


    @solps_property2
    def vxcv(self):
        """ (nmxcv): Indices of vertices or something"""
        self._read_signal('_vxcv','vxcv')
        self._vxcv = self._vxcv.astype(int) -1

    @solps_property
    def vxcvp(self):
        """ (nvx, 2): [??]"""
        self._read_signal('_vxcvp','vxcvp')
        self._vxcvp = self._vxcvp.astype(int)
        self._vxcvp[:,0] = self._vxcvp[:,0] -1  #Python indexing.


# ---------------------------- Geometry: flux tube  quantities
    ##ATTENTION: Not in files??
    #@solps_property
    #def ftconn(self):
    #    """ (nft): Connection length for each flux tube [m]"""
    #    self._read_signal('_ftconn','ftconn')

    @solps_property
    def ftreg(self):
        """ (nft): Corresponding region to each flux tube.

        This is not certain at all, but I think that:
            1: Core
            2: SOL
            3: PFR
        """
        self._read_signal('_ftreg','ftreg')
        self._ftreg = self._ftreg.astype(int)

    @solps_property
    def iftcore(self):
        """ Indices of flux surfaces in core.
        """
        self._iftcore = np.arange(self.nft)[self.ftreg==1]

    @solps_property
    def iftsol(self):
        """ Indices of flux surfaces in SOL.
        """
        self._iftsol = np.arange(self.nft)[self.ftreg==2]

    @solps_property
    def iftpfr(self):
        """ Indices of flux surfaces in PFR.
        """
        self._iftpfr = np.arange(self.nft)[self.ftreg==3]

    @solps_property
    def iftcv(self):
        """ Return a list of indices of ordered cells along a flux
        tube. Are the indices always in order to begin with?
        For now, just a list of ordered indices, check validity.
        """
        self._iftcv = []
        for ift in range(self.nft):
            self._iftcv.append(np.arange(self.ncv)[self.cvft == ift])


    @solps_property
    def ftlbl(self):
        """ (nft): iy label for each flux tube??"""
        self._read_signal('_ftlbl', 'ftlbl')
        self._ftlbl = self._ftlbl.astype(int) -1 #Python indexing.


    @solps_property
    def fteps(self):
        """ (nft): Inverse aspect ratio [ ]"""
        self._read_signal('_fteps','fteps')

    ### ATTENTION: What?
    #@solps_property
    #def ftbbav2(self):
    #    """ (nft): Average B**2 on the flux tube [T]"""
    #    self._read_signal('_ftbbav2','ftbbav2')



#    ##ATTENTION Not working yet? isep does not exist
#    ## What is the logic here.
#    ## Should be self.isepfs???
#    @solps_property
#    def ftpsi(self):
#        """ (nft): Psi on each flux tube """
#        ##How??
#        self._ftpsi= np.full((self.nft), np.nan)
#        icore = 0
#        ipfr = 0
#        isol = 0
#        core = np.append(self.ifscore, self.isep)
#        pfr = np.append(self.ifspfr, self.isep)
#        sol = np.append(self.isep, self.ifssol)
#        #import ipdb; ipdb.set_trace()
#        for ift in range(2, self.nft-1):
#            if self.ftreg[ift] == 1:
#                tmp = 0.5*(
#                     self.fspsi[core[icore]]
#                    +self.fspsi[core[icore+1]])
#                icore += 1
#            elif self.ftreg[ift] == 3:
#                tmp = 0.5*(
#                     self.fspsi[pfr[ipfr]]
#                    +self.fspsi[pfr[ipfr+1]])
#                ipfr+=1
#            elif self.ftreg[ift] == 2:
#                tmp = 0.5*(
#                     self.fspsi[sol[isol]]
#                    +self.fspsi[sol[isol+1]])
#                isol+=1
#
#            self._ftpsi[ift] = tmp

        
#    ## ATTENTION: Completely wrong!
#    ## Should be self.isepfs???
#    @solps_property
#    def ftrho(self):
#        """ (nft): """
#        self._ftrho = np.full((self.nft), np.nan)
#        seppsi = self.fspsi[self.isep]
#        axpsi = self.fspsi[self.ifscore[0]] # WRONG!!!!
#        for ift in range(2, self.nft-1):
#            #self._ftrho[ift] = np.sqrt(
#            #    (self.ftpsi[ift]-seppsi)/(seppsi-axpsi))
#            self._ftrho[ift] = (self.ftpsi[ift]-seppsi)/(seppsi-axpsi)
#            
#




# ---------------------------- Geometry: flux surface  quantities
    @solps_property
    def fsfcp(self):
        """ (nfs, 2): Relationship matrix between flux surfaces and faces.
        [...,0] = first index in list of faces.
        [...,1] = Number of face cells in the list.
        """
        self._read_signal('_fsfcp','fsfcp')
        self._fsfcp = self._fsfcp.astype(int) 
        self._fsfcp[...,0] -= 1 #Python index.

    @solps_property
    def fsfc(self):
        """ (nfc): 
        Indices with -999999 are faces which do not correspond 
        to a flux surface, i.e., the external surfaces of guardinc cells.
        """
        self._read_signal('_fsfc','fsfc')
        self._fsfc = self._fsfc.astype(int) -1
        self._fsfc[self._fsfc==-1] = -999999


    @solps_property
    def fsfcreg(self):
        """ (nfs, ifc): Corresponding region to each flux surface's faces.

            >0: cv regions.
            -1: Separatrix between core and sol.
            ...
        """
        self._fsfcreg = []
        for ifs in range(self.nfs):
            fsfcreg = []
            regs = self.cvreg[self.ifccv[self.ifsfc[ifs]]]
            for pair in regs:
                if pair[0] == pair[1]:
                    fsfcreg.append(pair[0])
                else:
                    fsfcreg.append(-1)
            self._fsfcreg.append(np.array(fsfcreg))
        self._fsfcreg = np.array(self._fsfcreg, dtype=object)


    ## ATTENTION: Only works for SN, since for DN there should be
    ## A distinction between the two halves's pfr, sol, etc.
    @solps_property
    def fsreg(self):
        """ (nfs): Same naming criterion as ftreg.
            1: Core
            2: SOL
            3: PFR
           -1: Separatrix

        Maybe ifsreg, iftreg would distinguish between both
        halves of a DN's SOL, PFR and core?
        Maybe secondary sep has label -2?

        """
        self._fsreg = np.full((self.nfs), -999999, dtype=int)
        for ifs in  range(self.nfs):
            regs = self.fsfcreg[ifs]
            if np.any(regs == -1):
                self._fsreg[ifs] = -1
            elif np.any(np.in1d(regs, self.cvregions['core'])):
                self._fsreg[ifs] = 1
            elif np.any(np.in1d(regs, self.cvregions['sol'])):
                self._fsreg[ifs] = 2
            else:
                self._fsreg[ifs] = 3


    @property
    def ifssep(self):
        return self.isepfs

    @solps_property
    def ifscore(self):
        """ Indices of flux surfaces in core.
        """
        self._ifscore = np.arange(self.nfs)[self.fsreg == 1]

    @solps_property
    def ifssol(self):
        """ Indices of flux surfaces in SOL.
        """
        self._ifssol = np.arange(self.nfs)[self.fsreg == 2]

    @solps_property
    def ifspfr(self):
        """ Indices of flux surfaces in PFR.
        """
        self._ifspfr = np.arange(self.nfs)[self.fsreg == 3]


    @solps_property
    def fspsi(self):
        """ (nfs): Psi on each flux surface []"""
        self._read_signal('_fspsi','fspsi')


    @solps_property
    def psisep(self):
        """ float: Psi at the separatrix []"""
        #self._psisep =  0.5*(self.fspsi[self.ifscore[-1]]+
        #    self.fspsi[self.isepfs])

        #self._psisep = np.average(self.fspsi[self.isepfs])

        self._psisep = self.fspsi[self.isepfs]
        if self.magnetic_geometry=='cdn':
            self._psisep = self._psisep[1]
        elif self.magnetic_geometry in ['ddn', 'ddnu']:
            self._psisep = self._psisep[0]



    @solps_property
    def fsnpsi(self):
        psi = self.fspsi-self.psisep
        psign = np.sign(psi[self.ifssol[-1]]-psi[self.ifscore[0]])
        poffset = 1.0
        #pnorm = self.psisep
        pnorm = np.abs(self.psisep)

        self._fsnpsi = (psi*psign/pnorm)+poffset


    @solps_property
    def fsrhopol(self):
        self._fsrhopol = np.sqrt(self.fsnpsi)



# ---------------------------- Geometry: Misc.

    @solps_property2
    def intcellp(self):
        """ (nfc, 2): Factors for interpolation from faces to centers in
        poloidal direction.

        Notes
        -----
        Description from b2us_map.F:61
        """
        
    @solps_property2
    def intcellr(self):
        """ (nfc, 2): Factors for interpolation from faces to centers in
        radial direction.

        Notes
        -----
        Description from b2us_map.F:62
        """




    # -------------------- EIRENE triangular grid -----------------------------








## SNAPSHOT ===================================================================

    # -------------------- Unidentified ---------------------------------------

    @solps_property2
    def kt(self):
        """ ({GRID_DIMS}): [??]
        """

    @solps_property2
    def zt(self):
        """ ({GRID_DIMS}): [??]
        """


    # -------------------- Plasma Characterization ----------------------------

    # -------------------- Densities ------------------------------------------
    
    # -------------------- Temperatures ---------------------------------------

    @solps_property2
    def tn(self):
        """ ({GRID_DIMS}): Fluid neutral temperature [eV]??
        """
    
    # -------------------- Pressures ------------------------------------------
    
    # -------------------- Velocities -----------------------------------------

    @solps_property2
    def vedia(self):
        """({FACE_DIMS}, 2) : Electrons effective drift velocity [m/s]"""


    @solps_property2
    def veecrb(self):
        """({FACE_DIMS}, 2) : Electrons ExB drift velocity [m/s]"""

    @solps_property2
    def vadia(self):
        """({FACE_DIMS}, 2, ns) : Ions effective drift velocity [m/s]"""


    @solps_property2
    def vaecrb(self):
        """({FACE_DIMS}, 2, ns) : Ions ExB drift velocity [m/s]"""
    




    # -------------------- Fluxes and energies --------------------------------

    @solps_property2
    def fhn(self):
        """ ({FACE_DIMS}, 2): Fluid neutral energy flux [??]
        """

    @solps_property2
    def fkt(self):
        """ ({FACE_DIMS}, 2): Something something flux [??]
        """

    @solps_property2
    def fzt(self):
        """ ({FACE_DIMS}, 2): Something something flux [??]
        """

    @solps_property2
    def fchviskt(self):
        """ ({FACE_DIMS}, 2): Something something current due to 
        viscosity?? [A??]
        """

    @solps_property2
    def fchanml(self):
        """ ({FACE_DIMS}, 2): Something something anomalous current ?? [A??]
        """


    ##ATTENTION: Wrong for DN?
    ##ATTENTION: Wrong for DN? Also because they are not yet Classical DDN
    @solps_property
    def psep(self):
        """ Total amount of power crossing the separatrix [W]
        Power increases at the separatrix??

        Notes
        -----
        Both give the same result for SN, but for DN yregions might need
        a loop to add both sides, whereas fsfcreg probably do it already

        self._psep = np.sum(qt[self.fcreg == self.yregions['sep']]


        """
        fht = self.fhey+self.fhiy ##Correct total power for this case??
        qt = fht*self.fcs
        ## In DN there are two IFS for a single separatrix.
        inds = [True if i == -1 else False for i in self.fsfcreg[self.isep]]
        self._psep = np.sum(qt[self.ifsfc[self.isep]][inds])


    ##ATTENTION: Temporary?? Put it in masks?
    @solps_property
    def isepcore(self):
        self._isepcore = []
        for face, reg in zip(self.ifsfc[self.isep], self.fsfcreg[self.isep]):
            if reg == -1:
                self._isepcore.append(face)
        self._isepcore = np.array(self._isepcore)




    ##ATTENTION: Wrong for DN? Also because they are not yet Classical DDN
    @solps_property
    def pin(self):
        """ Total amount of power crossing the core [W]
        """
        fht = self.fhey+self.fhiy ##Correct total power for this case??
        qt = fht*self.fcs
        #from IPython import embed; embed()
        if isinstance(self.yregions['core'], list) and len(
            self.yregions['core']) >1:
            inds = np.logical_or(
                self.fcreg == self.yregions['core'][0],
                self.fcreg == self.yregions['core'][1])
        else:
            inds = self.fcreg == self.yregions['core']
        self._pin = np.sum(qt[inds])


## Maybe do the same for fnasep and fnain, as integrated values.
## It already exists in b2tallies, but I do not have that for wg yet, no?


    # -------------------- Coefficients ---------------------------------------
    @solps_property2
    def hci0(self):
        """
        """

    @solps_property2
    def hcib(self):
        """
        """

    @solps_property2
    def hce0(self):
        """
        """

    @solps_property
    def kyeperp(self):
        """ ({GRID_DIMS}): Electron heat diffusivity [m^2/s]"""
        #self._read_signal('_kyeperp',  'hce0')
        return self.hce0/self.ne

    @solps_property
    def kyiperp(self):
        """ ({GRID_DIMS}, ns): Ion heat diffusivity [m^2/s]"""
        #self._read_signal('_kyiperp',  'hci0')
        return self.hcib/self.na

    @solps_property2
    def dna0(self):
        """dna"""

    @solps_property2
    def cdkt(self):
        """ ({FACE_DIMS}, 2):  [??]
        """

    @solps_property2
    def cdzt(self):
        """ ({FACE_DIMS}, 2):  [??]
        """

    @solps_property2
    def dkt0(self):
        """ ({GRID_DIMS}):  [??]
        """

    @solps_property2
    def dzt0(self):
        """ ({GRID_DIMS}):  [??]
        """

    @solps_property2
    def dnaExB(self):
        """ ({GRID_DIMS}):  [??]
        """
        self._read_signal('_dnaExB', 'dna_ExB')

    @solps_property2
    def hceExB(self):
        """ ({GRID_DIMS}):  [??]
        """
        self._read_signal('_hceExB', 'hce_ExB')

    @solps_property2
    def hciExB(self):
        """ ({GRID_DIMS}):  [??]
        """
        self._read_signal('_hciExB', 'hci_ExB')




    # -------------------- Rates, losses/sources and residuals ----------------
    @solps_property2
    def reshn(self):
        """ ({GRID_DIMS}): Residuals of hn? [??]
        """

    @solps_property2
    def reskt(self):
        """ ({GRID_DIMS}): Residuals of kt? [??]
        """

    @solps_property2
    def reszt(self):
        """ ({GRID_DIMS}): Residuals of zt? [??]
        """


    @solps_property2
    def shn(self):
        """ ({GRID_DIMS}, 4): Source of hn?? [??]
        """

    @solps_property2
    def szt(self):
        """ ({GRID_DIMS}, 4): Source of zt?? [??]
        """

    @solps_property2
    def skt(self):
        """ ({GRID_DIMS}, 4): Source of kt?? [??]
        """

    @solps_property2
    def skt_prod(self):
        """ ({GRID_DIMS}, 4): Source of kt prod?? [??]
        """

    @solps_property2
    def skt_diss(self):
        """ ({GRID_DIMS}, 4): Source of kt diss?? [??]
        """
 
 



##  B2 INPUT AND RUN.LOG ======================================================





##  FUNCTIONS =================================================================

    def where(self, var, to_update=None):
        """
          Locate the (best) file which contains a certain variable and
        provides the name of the file and the dimensions of the variable
        """
        signal = var.lower()


        s = (self.ns)
        x = (self.nx+2)
        xy = (self.nx+2, self.ny+2)

        nvx = (self.nvx)
        nvx2 = (self.nvx, 2)
        nvx4 = (self.nvx, 4)

        ncv  = (self.ncv)
        ncv2  = (self.ncv, 2)
        ncv3  = (self.ncv, 3)
        ncv4 = (self.ncv, 4)
        ncv2s  = (self.ncv, 2, self.ns)
        ncv4s  = (self.ncv, 4, self.ns)
        ncvs = (self.ncv, self.ns)
        #ncvs2 = (self.ncv, self.ns, 2)
        #ncvs4 = (self.ncv, self.ns, 4)

        nfc = (self.nfc)
        nfcs = (self.nfc, self.ns)
        nfc2 = (self.nfc, 2)
        nfc2s = (self.nfc, 2, self.ns)
        nfc2ss = (self.nfc, 2, self.ns, self.ns)
        nfc4 = (self.nfc, 4)

        nfs = (self.nfs)
        nfs2 = (self.nfs, 2)

        nft = (self.nft)
        nft2 = (self.nft, 2)

        ncmxfc = (self.ncmxfc)
        ncmxvx = (self.ncmxvx)
        nvmxfc = (self.nvmxfc)
        nvmxcv = (self.nvmxcv)


        #Eirene stores them in matix without guarding cells
        fncv  = (self.ncv)
        fncva = (self.ncv, self.natm)
        fncvm = (self.ncv, self.nmol)
        fncvi = (self.ncv, self.nion)

        # For rates
        rtnt = self.rtnt
        rtnn = self.rtnn
        rtns = self.rtns
        rt = (self.rtnt, self.rtnn, self.rtns)


        in_b2f_common = {
            # Plasma state
            'na': ncvs,
            'ne': ncv,
            'ua': ncvs,
            'uadia': nfc2s,
            'te': ncv,
            'ti': ncv,
            'tn': ncv,
            'po': ncv,
            'kt': ncv,
            'zt': ncv,

            # Fluxes
            'fna': nfc2s,
            'fhe': nfc2,
            'fhi': nfc2,
            'fhn': nfc2,
            'fch': nfc2,
            'fch_32': nfc2,
            'fch_52': nfc2,
            'kinrgy': ncvs,
            'fkt': nfc2,
            'fzt': nfc2,

            # Parallel current
            'fch_p': nfc2,

            # Advanced fluxes
            'fna_mdf': nfc2s,
            'fhe_mdf': nfc2,
            'fhi_mdf': nfc2,
            'fna_fcor': nfc2s,
            'fna_nodrift': nfc2s,
            'fna_he': nfc2s,
            'fnaPSch': nfc2s,
            'fhePSch': nfc2,
            'fhiPSch': nfc2,
            'fna_eir': nfc2s,
            'fne_eir': nfc2,
            'fhe_eir': nfc2,
            'fhi_eir': nfc2,
            'fna_32': nfc2s,
            'fna_52': nfc2s,
            'fni_32': nfc2,
            'fni_52': nfc2,
            'fne_32': nfc2,
            'fne_52': nfc2,
            'fchdia': nfc2,
            'fchin': nfc2,
            'fchvispar': nfc2,
            'fchvisper': nfc2,
            'fchvisq': nfc2,
            'fchviskt': nfc2,
            'fchinert': nfc2,
            'fhm': nfc2s,

            # Drift velocities
            'vaecrb':  nfc2s,
            'vadia': nfc2s,
            'wadia': nfc2s,
            'veecrb': nfc2,
            'vedia': nfc2,
            'floe_noc': nfc2,
            'floi_noc': nfc2}



        in_b2fstate = {
            'zamin':s,
            'zamax':s,
            'zn':s,
            'am':s,
            'time': 1}
        in_b2fstate = update(in_b2f_common, in_b2fstate)





        ## Dimensions found in modules/b2us_plasma.F
        ## Read the subroutine createB2Source, etc instead because
        ## the writing one does not have the correct dim order.

        in_b2fplasmf = {
            # Plasma state

            # Fluxes
            'fne': nfc2,
            'fni': nfc2,
            'fmo': nfc2s,

            # Parallel current

            # Advanced fluxes
            'fchanml': nfc2,
            'fht': nfc2,
            'fhj': nfc2,
            'fhm': nfc2s,
            'fhp': nfc2s,

            # Drift velocities

            # Residuals
            'resco': ncvs,
            'reshe': ncv,
            'reshi': ncv,
            'reshn': ncv,
            'resmo': ncvs,
            'resmt': ncv,
            'respo': ncv,
            'reskt': ncv,
            'reszt': ncv,

            # Sources
            'sna': ncv2s,
            'smo': ncv4s,
            'smq': ncv4s,
            'shi': ncv4,
            'she': ncv4,
            'shn': ncv4,
            'skt': ncv4,
            'skt_prod': ncv,
            'skt_diss': ncv,
            'szt': ncv4,

            # Transport
            'calf': nfc2,
            'cdna': nfc2s,
            'cdpa': nfc2s,
            'ceqp': ncv,
            'chce': nfc2,
            'chci': nfc2,
            'chve': nfc2,
            'chvemx': nfc,
            'chvi': nfcs,
            'chvimx': nfc,
            'cdkt': nfc2,
            'cdzt': nfc2,
            'csig': nfc2,
            'cvla': nfc2s,
            'cvsa': nfc2s,
            'cthe': ncvs,
            'cthi': ncvs,
            'csigin': nfc2ss,
            'cvsa_cl': nfc2s,
            'fllime': nfc,
            'fllimi': nfc,
            'fllim0fna': nfc2s,
            'fllim0fhi': nfc2s,
            'fllimvisc': nfc,
            'f_luc_ke': nfc,
            'f_luc_ki': nfc,
            'f_luc_et': nfc,
            'f_luc_sg': nfc,
            'f_luc_al': nfc,
            'fllim_ke': nfc,
            'fllim_ki': nfc,
            'fllim_al': nfc,
            'fllim_al_c': ncv,

            'sig0': ncv,
            'hce0': ncv,
            'alf0': ncv,
            'hci0': ncv,
            'hcib': ncvs,
            'dpa0': ncvs,
            'dna0': ncvs,
            'vsa0': ncvs,
            'vla0': ncv2s,
            'dkt0': ncv,
            'dzt0': ncv,
            'dna_exb': ncv,
            'hce_exb': ncv,
            'hci_exb': ncv}
        in_b2fplasmf = update(in_b2f_common, in_b2fplasmf)





        #As found in modules/b2us_map.F
        in_b2fgmtry = {'isymm': 1,
            'cvfcp': ncv2,
            'cvfc': ncmxfc, #Excludes some cells? ncv*4= 14880 not 14088
            'cvvxp': ncv2,
            'cvvx': ncmxvx,
            'cvft': ncv,
            'cvreg': ncv,
            'cvlbl': ncv,
            'cvbb': ncv4,
            'cveb': ncv3,
            'cvx': ncv,
            'cvy': ncv,
            'cvsz': ncv,
            'cvhz': ncv,
            'cvhx': ncv,
            'cvqgam': ncv2,
            'cvvol': ncv,
            'cvconn': ncv,

            'fccv': nfc2,
            'fcvx': nfc2,
            'fcreg': nfc,  #nfc is first x-face then y faces
            'fclbl': nfc, #Face labels, b2mod_prep.F line 1169
            'fcbb': nfc4,
            'fcs': nfc,
            'fchc': nfc2,
            'fcht': nfc,
            'fcqgam': nfc2,
            'fcqalf': nfc2,
            'fcqbet': nfc2,
            'fcpbs': nfc,

            'vxfcp': nvx2,
            'vxfc': nvmxfc,
            'vxcvp': nvx2,
            'vxcv': nvmxcv,
            'vxbb': nvx4,
            'vxx': nvx,
            'vxy': nvx,
            'vxffbz': nvx,
            'vxfpsi': nvx,

            'ftcvp': nft2,
            'ftcv': ncv,
            'ftfcp': nft2,
            'ftfc': nfc,
            'ftreg': nft,
            'ftlbl': nft,

            'fsfcp': nfs2,
            'fsfc': nfc,
            'fspsi': nfs,

            'intcellp': nfc2,
            'intcellr': nfc2,
            'imapcv': xy,
            'imapfcx': xy,
            'imapfcy': xy,
            'imapvx': xy,
            'icornvx': x}



        in_b2fpardf = {'zamin':None, 'zamax':None, 'zn':None, 'am':None,
            'cbir':None, 'cbnr': None,'cbrbrk':None, 'cbsna':None, 'cbsmo':None,
           'cbshi':None, 'cbshe':None, 'cbsch':None, 'cbrec':None, 'cbmsa':None,
           'cbmsb':None, 'cbsna':None, 'cbsmo':None, 'cbshi':None, 'cbshe':None,
           'cbsch':None, 'cbrec':None, 'cbmsa':None, 'cbmsb':None, 'cbsna':None,
           'cbsmo':None, 'cbshi':None, 'cbshe':None, 'cbsch':None, 'cbrec':None,
           'cbmsa':None, 'cbmsb':None, 'cbsna':None, 'cbsmo':None, 'cbshi':None,
           'cbshe':None, 'cbsch':None, 'cbrec':None, 'cbmsa':None, 'cbmsb':None,
           'cbsna':None, 'cbsmo':None, 'cbshi':None, 'cbshe':None, 'cbsch':None,
           'cbrec':None, 'cbmsa':None, 'cbmsb':None, 'cbsna':None, 'cbsmo':None,
           'cbshi':None, 'cbshe':None, 'cbsch':None, 'cbrec':None, 'cbmsa':None,
           'cbmsb':None, 'cfdf0':None, 'cfdna':None, 'cfdpa':None, 'cfvla':None,
           'cfvsa':None, 'cfhci':None, 'cfhce':None, 'cfsig':None, 'cfalf':None,
           'cflim':None}

        in_b2frates = {
            'ppout':None,
            'rtnt':None,
            'rtnn':None,
            'rtns':None,
            'rtzmin':rtns,
            'rtzmax':rtns,
            'rtzn':rtns,
            'rtt':rtnt,
            'rtn':rtnn,
            'rtlt':rtnt,
            'rtln':rtnn,
            'rtlsa':rt,
            'rtlra':rt,
            'rtlqa':rt,
            'rtlcx':rt,
            'rtlrd':rt,
            'rtlbr':rt,
            'rtlza':rt,
            'rtlz2':rt,
            'rtlpt':rt,
            'rtlpi':rt}


        in_fort44 = {
            'dab2':fncva, 
            'tab2':fncva,
            'dmb2':fncvm,
            'tmb2':fncvm,
            'dib2':fncvi,
            'tib2':fncvi,
            'eneutrad':fncva,
            'emolrad':fncvm,
            'eionrad':fncvi,
        }


        in_d= {'b2fplasmf': in_b2fplasmf, 'b2fstate': in_b2fstate,
                'b2fstati': in_b2fstate, 'b2fgmtry': in_b2fgmtry,
                'b2fpardf': in_b2fpardf, 'b2frates': in_b2frates,
                'fort.44': in_fort44}

        for k, d in in_d.items():
            try:
                d.update(to_update[k])
            except:
                pass

        #files = read_config()['Files']
        files = self.config['Files']
        for loc in files['location']:
            for prio in files['priority']:
                if (prio in self.avail and os.path.dirname(
                    os.path.normpath(self.avail[prio]))
                    .startswith(self.rundir_rel(loc))):
                    if prio in in_d and signal in in_d[prio]:
                        #if loc != '.':
                        #    self.log.warning(
                        #        "'{0}' signal read from '{1}'.".format(signal, loc))
                        if prio == 'b2fstati':
                            self.log.warning(
                                "'{0}' signal read from 'b2fstati'.".format(signal))

                        return (prio, in_d[prio][signal])
        self.log.error("'{0}' could not be found in the".format(var)+
                    " implemented description of files.")
        return None







    # -------------------- Private methods ------------------------------------
    def _get_basic_dimensions(self):
        """ Information found in modules/b2us_prep.F
        
        ncv:  Total number of cells.
        
        ncmxvx: Max. no. of entries in cvVx (vertex list of each cell)
                      nCmxVx = 4 * m%nCi + 2*m%nCg
        
        ncmxfc: Max. no. of entries in cvFc (face list of each cell)
                      nCmxFc = 4 * m%nCi + m%nCg
        
        nvmxcv: max. no. of entries in vxCv (cell list of each vertex)
                corner vertices have only 3;
                each X-point vertex has 8; duplicate X-point vertices for slabs
                    ! nVmxCv = 4 * m%nVx - 4*nncut + 2*nncut*4
                      nVmxCv = 4 * (nx+1) * (ny+1) + 2*nncut*4
        
        nvmxfc: max. no. of entries is vxFc (face list of each vertex)
                corner vertices have only 2;
                other vertices on boundaries have only 3;
                each X-point vertex has 8; duplicate X-point vertices for slabs
               !nVmxFc = 4 * m%nVx - 4*2*nncut - 2*((nx-1) + (ny-1)) + 2*nncut*4
                nVmxFc = 4 * (nx+1) * (ny+1) - 2*((nx-1) + (ny-1))
        
        ncmxnv: max no. of entries in matrix
                 ! nCmxNv = 9*m%nCi - 4*nncut + ! internal cells (corners only 8)
                 !          6*m%nCg - 8*nncut + ! guard cells (at corners only 5)
                 !          2*8*nncut*4         ! correction cells around X-points
                 ! nCmxNv = 9 * (nx+1) * (ny+1)
                   nCmxNv = 9 * (nx+2) * (ny+2)  ! there are 2 guard cells

        Notes
        -----
        As I understand it, these maxima are given so that the *p can have
        the structure of [starting element, number of elements] for each
        individual entry.
        """
        #The definitions seem to be available in src/modules/b2us_prep.F
        if 'b2fgmtry' in self.avail:
            gdir = self.avail['b2fgmtry']
            try:
                with open(gdir) as fg:
                    for _ in range(2):
                        fg.readline()
                    tmp = fg.readline().split()
                    self.nci = int(tmp[0])
                    self.ncg = int(tmp[1])
                    self.ncv = int(tmp[2])

                    self.nfc = int(tmp[3])
                    self.nvx = int(tmp[4])
                    self.nfs = int(tmp[5])
                    self.nft = int(tmp[6])

                    for _ in range(1):
                        fg.readline()
                    tmp = fg.readline().split()
                    self.ncmxvx = int(tmp[0])
                    self.ncmxfc = int(tmp[1])
                    self.nvmxcv = int(tmp[2])
                    self.nvmxfc = int(tmp[3])
                    self.ncmx   = int(tmp[4])

                    for _ in range(1):
                        fg.readline()
                    tmp = fg.readline().split()
                    self.classical = bool(tmp[0])
                
            except:
                self.log.warning("WG related dims could not be retrieved.")

        #Read B2 dims:
        ## nx, ny, nncut
        if 'b2fgmtry' in self.avail:
            gdir = self.avail['b2fgmtry']
            try:
                with open(gdir) as fg:
                    for _ in range(8):
                        fg.readline()
                    tmp = fg.readline().split()
                    ##ATTENTION:
                    ## Or maybe it is still in WG, but only for some quantities??
                    self.nx = int(tmp[0]) # + 2 #Not in WG apparently.
                    self.ny = int(tmp[1]) # + 2 #Not in WG apparently.
                    ## Or maybe it is still in WG, but only for some quantities??
                    try:
                        self.nncut = int(tmp[2])
                    except:
                        pass ## It might be a property.
            except:
                self.log.warning("nx, ny, nncut could not be retrieved.")

        ## ns
        for tmp in ['b2fstate', 'b2fstati', 'b2fparam', 'b2fpardf']:
            if tmp in self.avail:
                gdirns = self.avail[tmp]
                if tmp == 'b2fstate' or tmp == 'b2fstati':
                    offset = 2
                else:
                    offset = 0
                break
        try:
            with open(gdirns) as fg:
                for _ in range(2):
                    fg.readline()
                tmp = fg.readline().split()
                self.ns = int(tmp[offset])
        except:
            if 'b2fplasmf' in self.avail:
                try:
                    pdir = self.avail['b2fplasmf']
                    with open(pdir) as fp:
                        for line in fp:
                            elements = line.split()
                            if elements[-1].strip().lower() == 'na':
                                self.ns = int(
                                    int(elements[-2])/self.ncv)
                                break
                except:
                    self.log.warning("ns could not be retrieved.")


        #Read Rates dims: rtnt, rtnn, rtns
        try:
            fdir = self.avail['b2frates']
            with open(fdir,'r') as fd:

                # Go to 'label':
                while ( True ):
                    line = fd.readline()
                    if line.split()[-1] == 'label':
                        break
                # Read out rates table, which is 2 words after
                #the timestamp (containing ':'):
                try:
                    self.rates_table = fd.readline().split(':')[1].split()[2]
                except:
                    self.log.warning("Name of atomic rates table"+
                                     " could not be retrieved")
                    self.rates_table = None

                # Go to 'rtnt,rtnn,rtns':
                while ( True ):
                    line = fd.readline()
                    if line.split()[-1] == 'rtnt,rtnn,rtns':
                        break

                # Read 'rtnt,rtnn,rtns':
                line = [int(x) for x in fd.readline().split()]
                self.rtnt = line[0] + 1
                self.rtnn = line[1] + 1
                self.rtns = line[2]
        except:
            self.log.warning("rtnt, rtnn and rtns could not be retrieved")
            self.rtnt = None
            self.rtnn = None
            self.rtns = None






        #Read EIRENE dims: natm, nmol and nion, only if not b2standalone
        if not self.b2standalone:
            try:
                fdir = self.avail['fort.44']
                with open(fdir,'r') as fd:
                    line = fd.readline().split()
                    if (int(line[0]) != self.ncv): 
                        self.log.error("Faulty fort.44")
                        self.natm = None
                        self.nmol = None
                        self.nion = None
                        self.jvft44 = None
                        return
                    self.jvft44 = line[2]
                    line = [int(x) for x in fd.readline().split()]
                    self.natm = line[0]
                    self.nmol = line[1]
                    self.nion = line[2]
                    self.atm = []
                    self.mol = []
                    self.ion = []
                    for _ in range(self.natm):
                        self.atm.append(fd.readline().strip())
                    for _ in range(self.nmol):
                        self.mol.append(fd.readline().strip())
                    for _ in range(self.nion):
                        self.ion.append(fd.readline().strip())
            except:
                self.log.warning("natm, nmol and nion could not be retrieved.")
                self.natm = None
                self.nmol = None
                self.nion = None
                self.jvft44 = None
                self.log.warning("Assuming b2 standalone run.")
                self._b2standalone=True
        else:
            self.natm = None
            self.nmol = None
            self.nion = None
            self.jvft44 = None

        return






    # -------------------- Operations -----------------------------------------

    #ATTENTION: what does this really do?
    def intcell(fvar, fc):
        """
        fc is either 'x' or 'y'.
        """
        return cvar



    # -------------------- Transformations ------------------------------------
    ## ATTENTION: COMPLETE!
    @property
    def _dimensions(self):
        return {
            ## Commons
            self.ncv:'ncv',
            self.nci:'nci',
            self.ncg:'ncg',
            self.nfc:'nfc',
            self.nvx:'nvx',
            self.nft:'nft',
            self.nfs:'nfs',
            ## Maxima
            self.ncmxvx:'ncmxvx',
            self.ncmxfc:'ncmxfc',
            self.nvmxcv:'nvmxcv',
            self.nvmxfc:'nvmxfc',
            self.ncmx:'ncmx',
        }

    def as_cv(self, var, vtype=None):
        """ Take whatever quantity it is in anything but cell index
        and transform it to cell center and cell index (or at least 
        to cell index.
        Similar to as_xy for classical grids.
        Inference might be a problem if two dimensions are ever the same.
        """
        self.log.warning("Not yet implemented!")
        if isinstance(var, str):
            var = get(self, var)
        if vtype is None:
            try:
                vtype=self._dimensions[var.shape[0]]
            except:
                self.log.error("Current dimensions could not be inferred!")
        return var


    def as_fc(self, var, vtype=None):
        """
        """
        self.log.warning("Not yet implemented!")
        if isinstance(var, str):
            var = get(self, var)
        if vtype is None:
            try:
                vtype=self._dimensions[var.shape[0]]
            except:
                self.log.error("Current dimensions could not be inferred!")
        return var


    def as_vx(self, var, vtype=None):
        """
        """
        self.log.warning("Not yet implemented!")
        if isinstance(var, str):
            var = get(self, var)
        if vtype is None:
            try:
                vtype=self._dimensions[var.shape[0]]
            except:
                self.log.error("Current dimensions could not be inferred!")
        return var


    def as_ft(self, var, vytpe=None):
        """
        """
        self.log.warning("Not yet implemented!")
        if isinstance(var, str):
            var = get(self, var)
        if vtype is None:
            try:
                vtype=self._dimensions[var.shape[0]]
            except:
                self.log.error("Current dimensions could not be inferred!")
        return var


    def as_fs(self, var, vtype=None):
        """
        """
        self.log.warning("Not yet implemented!")
        if isinstance(var, str):
            var = get(self, var)
        if vtype is None:
            try:
                vtype=self._dimensions[var.shape[0]]
            except:
                self.log.error("Current dimensions could not be inferred!")
        return var
