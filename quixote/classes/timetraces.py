"""
https://stackoverflow.com/questions/9667818/python-how-to-merge-two-class

A new better way of simply merging elements?
class A(object):...
class B(object):...

C = type('C', (A,B), dict(keys = values, ...))
It merges, giving A priority over B.

"""


# Common modules
import numpy as np
from scipy.io import netcdf_file

# Special modules
from ..tools import get, solps_property
from ..tools import priority, create_log
from ..extensions.heatflux import HeatFlux


alog = create_log("TimeTraces")


def TimeTraces(mother, location):
    """ I can even make mother to be a dictionary with the correct stuff.
    Maybe a bit of an overkill at first.
    """
    data_type = get(mother, 'data_type')
    grid_type = get(mother, 'grid_type')


    if data_type == 'rundir':
        if location.startswith('target'):
            if grid_type == 'structured':
                return type('TargetDataTT',
                (TargetDataRundir, StructuredTargetData), {})(mother, location)
            else:
                return type('TargetDataTT',
                (TargetDataRundir, UnstructuredTargetData), {})(mother, location)

        elif location.endswith('mp'):
            if grid_type == 'structured':
                return type('MidplaneDataTT',
                (MidplaneData, StructuredBTTData, RundirTT), {})(mother, location)
            else:
                return type('MidplaneDataTT',
                (MidplaneData, UnstructuredBTTData, RundirTT), {})(mother, location)

        elif location.startswith('pfr'):
            return type('PFRDataTT', (PFRData, RundirTT), {})(mother, location)

        elif location.lower() == 'mcw':
            #return MCWDataRundir(mother, location)
            return type('MCWDataTT', (MCWData, RundirTT), {})(mother, location)

        elif location.lower() == 'tallies': 
            ## Make it so data.global.reg('core') and .reg(1) give the same result
            ## Or data.tallies.te_max['core']
            ## So data.global.reg('core').te
            return type('TalliesDataTT',
            (TalliesData, RundirTT), {})(mother, location)

        else:
            mother.log.error("SolpsData type not supported in factory method")
            raise Exception









# =============================================================================

class BaseTimeTraces(object):
    """Base class for quantites with time traces for SolpsData.

    There are two types of time traces in SOLPS: 1D and 2D.
    The 1D store a single quantity on a single position on every time step.
    The 2D store profiles of quantities on every time step.

    The nomenclature of 1D time traces is implemented as follows:
    Prefixes:
    fe- : Energy flux
    fc- : Current

    Middle:
    -t- : total
    -e- : electronic
    -i- : ionic
    -ei-: electronic + ionic
    -ie-: electronic + ionic

    Sufixes:
    -x : poloidal
    -y : radial

    Terminations:
    _sep : At the separatrix
    _int : Integral
    _max : Maximum

    For example: the integrated total energy flux on the outerboard divertor
    (fetxap) receives te name fetx_int (fe-t-x-_int)
    """



    def __init__(self, mother, location):
        """
        Parameters
        ----------
        mother : object
            SolpsData instance
        location : str
            Special location out of which the data are being retrieved.
            Must be one of {'target1','target2','target3','target4','imp','omp'}
        """

        alog.debug("> Entering BaseTimeTraces __init__ for {}".format(location))

        self.location = location.lower()
        self.log = mother.log
        self.avail = mother.avail
        self.config = mother.config
        self.data_type = mother.data_type
        self.grid_type = mother.grid_type
        self.maggeom = mother.magnetic_geometry

        alog.debug("< Leaving BaseTimeTraces __init__ for {}".format(location))


    #    =============== Properties =======================
    @solps_property
    def time(self):
        """(time) : distance from the separatrix [s]"""
        self._read_signal('_time', 'timesa')


    # -------------------- Profile quantities ---------------------------------
    ## ATTENTION: WG does not seem to generate ds files??
    @solps_property
    def ds(self):
        """(ny) : distance from the separatrix [m]"""
        self._read_signal('_ds', 'ds{}')


    # -------------------- Singleton quantities -------------------------------
    @solps_property
    def ne_sep(self):
        """(time) : separatrix electron density [m^-3]"""
        self._read_signal('_ne_sep', 'nesep{}')

    @solps_property
    def po_sep(self):
        """(time) : separatrix potential [V]"""
        self._read_signal('_po_sep', 'posep{}')

    @solps_property
    def te_sep(self):
        """(time) : separatrix electron temperature [eV]"""
        self._read_signal('_te_sep', 'tesep{}')

    @solps_property
    def pse_sep(self):
        """(time) : separatrix electron static pressure [Pa]"""
        self._pse_sep = 1.602176565e-19*self.te_sep*self.ne_sep

    @solps_property
    def ti_sep(self):
        """(time) : separatrix electron temperature [eV]"""
        self._read_signal('_ti_sep', 'tisep{}')







class StructuredBTTData(BaseTimeTraces):

    #    =============== Properties =======================
    # -------------------- Profile quantities ---------------------------------
    @solps_property
    def an(self):
        """(time,ny) : atomic density [m^-3]"""
        self._read_signal('_an', 'an3d{}')

    @solps_property
    def mn(self):
        """(time,ny) : molecular density [m^-3]"""
        self._read_signal('_mn', 'mn3d{}')

    @solps_property
    def po(self):
        """(time,ny) : potential [V]"""
        self._read_signal('_po', 'po3d{}')

    @solps_property
    def te(self):
        """(time,ny) : electron temperature [eV]"""
        self._read_signal('_te', 'te3d{}')

    @solps_property
    def ti(self):
        """(time,ny) : ion temperature [eV]"""
        self._read_signal('_ti', 'ti3d{}')

    @solps_property
    def ne(self):
        """(time,ny) : electron density [m^-3]"""
        self._read_signal('_ne', 'ne3d{}')


    @solps_property
    def pse(self):
        """(time,ny) : electron static pressure [Pa]"""
        self._pse = 1.602176565e-19*self.te*self.ne

    # -------------------- Singleton quantities -------------------------------






class UnstructuredBTTData(BaseTimeTraces):
    #    =============== Properties =======================
    # -------------------- Profile quantities ---------------------------------

    # -------------------- Singleton quantities -------------------------------

    @solps_property
    def kt_sep(self):
        """(time) : separatrix turbulent kinetic energy [eV]"""
        self._read_signal('_kt_sep', 'ktsep{}')










# =============================================================================
class TargetData(BaseTimeTraces):
    """ Target Data Time Traces.
    For DN cases, information is given in pairs.
    """
    #    =============== Properties =======================
    # -------------------- Profile quantities ---------------------------------

    # -------------------- Singleton quantities -------------------------------
    @solps_property
    def fch_int(self):
        """(time) : integrated poloidal current [A]"""
        self._read_signal('_fch_int', 'fchx{}p')

    ## ATTENTION: Set correct quantity
    @solps_property
    def isat_int(self):
        """(time) : integrated ion saturation current [A]"""
        self._isat_int = self.fch_int


    ## For feexap, 0 and 1 mean bottom and top.
    ## Also... multiply appropiately so that possitive is always 
    ## towards the target, and neg away from??
    @solps_property
    def fhe_int(self):
        """(time) : integrated poloidal electron energy flux [W]"""
        self._read_signal('_fhe_int', 'feex{}p')

    @solps_property
    def fhi_int(self):
        """(time) : integrated poloidal ion energy flux [W]"""
        self._read_signal('_fhi_int', 'feix{}p')

    @solps_property
    def fhie_int(self):
        """(time) : integrated poloidal electron + ion energy flux [W]"""
        self._fhie_int = self.fhe_int + self.fhi_int

    @property
    def fhei_int(self):
        """(time) : integrated poloidal electron + ion energy flux [W]"""
        return self.fhie_int


    @solps_property
    def fht_int(self):
        """(time) : integrated poloidal total energy flux [W]"""
        self._read_signal('_fht_int', 'fetx{}p')

    @solps_property
    def fhs_int(self):
        """(time) : integrated poloidal total energy flux minus e + i [W]"""
        self._fhs_int = self.fht_int - self.fhie_int


    @solps_property
    def fna_int(self):
        """(time) : integrated poloidal particle flux [1/s]"""
        self._read_signal('_fna_int', 'fnix{}p')


    @solps_property
    def ne_max(self):
        """(time) : maximum electron density [m^-3]"""
        self._read_signal('_ne_max', 'nemx{}p')

    @solps_property
    def po_max(self):
        """(time) : maximum potential [V]"""
        self._read_signal('_po_max', 'pomx{}p')

    @solps_property
    def te_max(self):
        """(time) : maximum electron temperature [eV]"""
        self._read_signal('_te_max', 'temx{}p')

    @solps_property
    def pse_max(self):
        """(time) : maximum electron static pressure [Pa]"""
        self._pse_max = 1.602176565e-19*self.te_max*self.ne_max

    @solps_property
    def ti_max(self):
        """(time) : maximum electron temperature [eV]"""
        self._read_signal('_ti_max', 'timx{}p')

    @solps_property
    def pw_max(self):
        """(time) : maximum total power flux density? [W m^-2]
        Note: In long_name doesn't mention density, but units say this.
        """
        self._read_signal('_pw_max', 'pwmx{}p')








class UnstructuredTargetData(TargetData, UnstructuredBTTData):
    """
    """



class StructuredTargetData(TargetData, StructuredBTTData):
    """ Target Data Time Traces.
    For DN cases, information is given in pairs.
    """


    #    =============== Properties =======================
    # -------------------- Profile quantities ---------------------------------

    @solps_property
    def isat(self):
        """ (time, ny): Ion saturation current, as experimentally defined [A]"""
        pass #to be done

    @solps_property
    def jsat(self):
        """ (time, ny): Ion saturation current density [A/m^2]"""
        self._jsat = self.isat*self.da

    @solps_property
    def fch(self):
        """ (time, ny): poloidal current [A].
        In netcdf A, but in MDS+ saved with 1/da
        """
        self._read_signal('_fch', 'fc3d{}')
        if self.data_type == 'mds':
            self._fc = self._fc*self.da

    @property
    def fchd(self):
        """ (time,ny): poloidal current density [A/m^2]"""
        return self.fch/self.da


    @solps_property
    def fhe(self):
        """(time,ny) : poloidal electron energy flux [W]
        In netcdf W, but in MDS+ saved with 1/da
        """
        self._read_signal('_fhe', 'fe3d{}')
        if self.data_type == 'mds':
            self._fhe = self._fhe*self.da

    @property
    def fhed(self):
        """(time,ny) : poloidal electron energy flux density [W/m^2]"""
        return self.fhe/self.da


    @solps_property
    def fhi(self):
        """(time,ny) : poloidal ion energy flux [W]
        In netcdf W, but in MDS+ saved with 1/da
        """
        self._read_signal('_fhi', 'fi3d{}')
        if self.data_type == 'mds':
            self._fhi = self._fhi*self.da

    @property
    def fhid(self):
        """(time,ny) : poloidal ion energy flux  density [W/m^2]"""
        return self.fhi/self.da

    @solps_property
    def fhei(self):
        """(time,ny) : poloidal electron + ion energy flux [W]
        In netcdf W, but in MDS+ saved with 1/da
        """
        self._fhei = self.fhe+self.fhi

    @property
    def fheid(self):
        """(time,ny) : poloidal electron + ion energy flux density [W/m^2]"""
        return self.fhei/self.da



    @solps_property
    def fht(self):
        """(time,ny) : poloidal total energy flux [W].
        In netcdf W, but in MDS+ saved with 1/da
        """
        self._read_signal('_fht', 'ft3d{}')
        if self.data_type == 'mds':
            self._fht = self._fht*self.da

    @property
    def fhtd(self):
        """(time,ny) : poloidal total energy flux density [W/m^2]"""
        return self.fht/self.da



    @solps_property
    def fhs(self):
        """(time,ny) : poloidal total energy flux minus e+i [W].
        In netcdf W, but in MDS+ saved with 1/da
        """
        self._fhs = self.fht-self.fhei

    @property
    def fhsd(self):
        """(time,ny) : poloidal total energy flux density minus e+i [W/m^2]"""
        return self.fhs/self.da


### ATTENTION: ADD ALSO tallies quantities corresponding to targets!
#    @solps_property
#    def fhm(self):
#        """(time,ny) : poloidal kinetic energy flux [W].
#        In netcdf W, but in MDS+ saved with 1/da
#        """
#        self._read_signal('_fhm', 'ft3d{}')
#        if self.data_type == 'mds':
#            self._fht = self._fht*self.da
#
#    @property
#    def fhmd(self):
#        """(time,ny) : poloidal kinetic energy flux density [W/m^2]"""
#        return self.fhm/self.da






    @solps_property
    def fne(self):
        """(time,ny) : poloidal electron flux [s^-1]
        In netcdf s^-1, but in MDS+ saved with 1/da
        """
        self._read_signal('_fne', 'fl3d{}')
        if self.data_type == 'mds':
            self._fne = self._fne*self.da

    @property
    def fned(self):
        """(time,ny) : poloidal electron flux density [m^-2 s^-1]"""
        return self.fne/self.da


    @solps_property
    def fnd(self):
        """(time,ny) : poloidal spec one flux [s^-1]
        In netcdf s^-1, but in MDS+ saved with 1/da
        """
        self._read_signal('_fnd', 'fn3d{}')
        if self.data_type == 'mds':
            self._fnd = self._fnd*self.da

    @property
    def fndd(self):
        """(time,ny) : poloidal spec one flux density[m^-2 s^-1]"""
        return self.fndd/self.da


    @solps_property
    def fni(self):
        """(time,ny) : poloidal ion flux [s^-1]
        In netcdf s^-1, but in MDS+ saved with 1/da
        """
        self._read_signal('_fni', 'fo3d{}')
        if self.data_type == 'mds':
            self._fni = self._fni*self.da

    @property
    def fnid(self):
        """(time,ny) : poloidal ion flux density [m^-2 s^-1]"""
        return self.fni/self.da



    @solps_property
    def tp(self):
        """(time,ny) : Target plate temperature [eV]"""
        self._read_signal('_tp', 'tp3d{}')








    # -------------------- Extensions -------------------------------
    def heatflux(self,x, **kwargs):
        """ x must be in mm"""
        try:
            return self._heatflux(x)
        except:
            self.log.warning("Heat flux is not yet to be trusted")
            hfc = self.config['HeatFlux']
            ##Update with kwargs if given, and save it as hfc. Maybe read
            ## and check if TimeTraces was given an hfc kwarg or something.
            yi = np.average(self.ft,0)*hfc['yscale']
            xi = self.ds*hfc['xscale']
            parameters = hfc['initial values']
            if parameters['a'] == 'maxyi':
                parameters['a'] = np.max(yi)
            self._heatflux= HeatFlux(xi, yi, method=hfc['method'], p=parameters)
            self._heatflux_params = self._heatflux.p
            self._lambdaq = self._heatflux.lambdaq
            self._s = self._heatflux.s
            self._bkg = self._heatflux.bkg
            return self._heatflux(x)

    @solps_property
    def lambdaq(self):
        #import ipdb; ipdb.set_trace()
        self.log.warning("Heat flux is not yet to be trusted")
        hfc = self.config['HeatFlux']
        yi = np.average(self.ft,0)*hfc['yscale']
        xi = self.ds*hfc['xscale']
        parameters = hfc['initial values']
        if parameters['a'] == 'maxyi':
            parameters['a'] = np.max(yi)
        self._heatflux= HeatFlux(xi, yi, method=hfc['method'], p=parameters)
        self._heatflux_params = self._heatflux.p
        self._lambdaq = self._heatflux.lambdaq
        self._s = self._heatflux.s
        self._bkg = self._heatflux.bkg

    @property
    def pdl(self):
        return self.lambdaq

    @property
    def power_decay_lenght(self):
        return self.lambdaq

    @solps_property
    def s(self):
        self.log.warning("Heat flux is not yet to be trusted")
        hfc = self.config['HeatFlux']
        yi = np.average(self.ft,0)*hfc['yscale']
        xi = self.ds*hfc['xscale']
        parameters = hfc['initial values']
        if parameters['a'] == 'maxyi':
            parameters['a'] = np.max(yi)
        self._heatflux= HeatFlux(xi, yi, method=hfc['method'], p=parameters)
        self._heatflux_params = self._heatflux.p
        self._lambdaq = self._heatflux.lambdaq
        self._s = self._heatflux.s
        self._bkg = self._heatflux.bkg

    #ATTENTION: Bad naming ( = parallel force due to pstot in mockup_balance)
    @property
    def psf(self):
        return self.s

    @property
    def power_spread_factor(self):
        return self.s

    @solps_property
    def bkg(self):
        self.log.warning("Heat flux is not yet to be trusted")
        hfc = self.config['HeatFlux']
        yi = np.average(self.ft,0)*hfc['yscale']
        xi = self.ds*hfc['xscale']
        parameters = hfc['initial values']
        if parameters['a'] == 'maxyi':
            parameters['a'] = np.max(yi)
        self._heatflux= HeatFlux(xi, yi, method=hfc['method'], p=parameters)
        self._heatflux_params = self._heatflux.p
        self._lambdaq = self._heatflux.lambdaq
        self._s = self._heatflux.s
        self._bkg = self._heatflux.bkg

    @solps_property
    def heatflux_params(self):
        self.log.warning("Heat flux is not yet to be trusted")
        hfc = self.config['HeatFlux']
        yi = np.average(self.ft,0)*hfc['yscale']
        xi = self.ds*hfc['xscale']
        parameters = hfc['initial values']
        if parameters['a'] == 'maxyi':
            parameters['a'] = np.max(yi)
        self._heatflux= HeatFlux(xi, yi, method=hfc['method'], p=parameters)
        self._heatflux_params = self._heatflux.p
        self._lambdaq = self._heatflux.lambdaq
        self._s = self._heatflux.s
        self._bkg = self._heatflux.bkg



    def plot_heatflux(self, **kwargs):
        """ For now, only  out in SN"""
        import matplotlib.pyplot as plt
        which = priority(['which', 'side'], kwargs, 'out')
        canvas = priority(['canvas','fig','figure','ax'], kwargs, None)
        units = priority(['units','u'], kwargs, 'mm')
        color = priority(['color', 'c'], kwargs, 'r')
        try:
            eich = self.heatflux
        except:
            pass
        if canvas is None:
            fig, canvas = plt.subplots(1)
            canvas.set_xlabel('dS ['+units+']')
            canvas.set_ylabel('q$_\perp$ [MW m$^{-2}$]')
            ltext = r'$\lambda_q$ = '+'{0:.2f}  mm\n\n'.format(self.lambdaq)
            stext = r'S = '+'{0:.2f}  mm'.format(self.psf)
            canvas.text(0.70, 0.80, ltext+stext,
                fontsize=15,
                horizontalalignment='left',
                verticalalignment='center',
                bbox={'facecolor':'white', 'pad':10},
                transform= canvas.transAxes)
        xx = np.linspace(self.ds[0], self.ds[-1], 1000)*1e3
        canvas.plot(self.ds*1e3, np.average(self.ft,0)*1e-6, 'o', color = color)
        canvas.plot(xx, eich(xx), color = color)
        #ax.legend(loc = 'upper right')
        plt.draw()
        return










# =============================================================================
class MidplaneData(object):
    """
    For DDN cases, information is given in pairs.
    """


    #    =============== Properties =======================
    # -------------------- Profile quantities ---------------------------------

    @solps_property
    def dn(self):
        """(time,ny) : diffusion coefficient [m^2/s]"""
        self._read_signal('_dn', 'dn3d{}')

    @solps_property
    def dp(self):
        """(time,ny) : pressure diffusion coefficient [m^2/s]"""
        self._read_signal('_dp', 'dp3d{}')

    @solps_property
    def ke(self):
        """(time,ny) : electron thermal diffusivty [m^2/s]"""
        self._read_signal('_ke', 'ke3d{}')

    @solps_property
    def ki(self):
        """(time,ny) : electron thermal diffusivty [m^2/s]"""
        self._read_signal('_ki', 'ki3d{}')

    @solps_property
    def vs(self):
        """(time,ny) : viscosity coefficient [m kg^-1 s^-1]"""
        self._read_signal('_vs', 'vs3d{}')

    @solps_property
    def vx(self):
        """(time,ny) : poloidal pinch velocity [m/s]"""
        self._read_signal('_vx', 'vx3d{}')

    @solps_property
    def vy(self):
        """(time,ny) : radial pinch velocity [m/s]"""
        self._read_signal('_vy', 'vy3d{}')


    # -------------------- Singleton quantities -------------------------------
    @solps_property
    def dn_sep(self):
        """(time) : separatrix diffusion coefficient [m^2/s]"""
        self._read_signal('_dn_sep', 'dnsep{}')

    @solps_property
    def dp_sep(self):
        """(time) : separatrix pressure diffusion coefficient [m^2/s]"""
        self._read_signal('_dp_sep', 'dpsep{}')

    @solps_property
    def ke_sep(self):
        """(time) : separatrix electron thermal diffusivty [m^2/s]"""
        self._read_signal('_ke_sep', 'kesep{}')

    @solps_property
    def ki_sep(self):
        """(time) : separatrix ion thermal diffusivty [m^2/s]"""
        self._read_signal('_ki_sep', 'kisep{}')

    @solps_property
    def vs_sep(self):
        """(time) : separatrix viscosity coefficient [m kg^-1 s^-1]"""
        self._read_signal('_vs_sep', 'vssep{}')

    @solps_property
    def vx_sep(self):
        """(time) : separatrix poloidal pinch velocity [m/s]"""
        self._read_signal('_vx_sep', 'vxsep{}')

    @solps_property
    def vy_sep(self):
        """(time) : separatrix radial pinch velocity [m/s]"""
        self._read_signal('_vy_sep', 'vysep{}')










class RundirTT(object):
    #    =============== Properties =======================
    # -------------------- Profile quantities ---------------------------------
    # -------------------- Singleton quantities -------------------------------

    #def __init__(self, mother, location):
    #    alog.debug("> Entering RundirTT __init__ for {}".format(location))
    #    #super().__init__(mother, location)
    #    alog.debug("< Leaving RundirTT __init__ for {}".format(location))



    #    =============== Methods ==========================
    def _read_signal(self,var_name, signal_name, attach_to=None, location=None):
        """ Retrieve data from the MDS+, given tree link and signal name.

        read_signal does not return any value. It automatically attach
        the signal as an attribute with a given name to the given object
        or self.

        Parameters
        ----------
        var_name : str
            Storage name of the variable for the retrieve signal.
        signal_name : str
            Name to retrieve signal.
            Only the stem of signal_name is given.
        attach_to : object, optional
            signal_name will be attached as attribute to object
            attach_to. Default to self.

        Returns
        -------
        None
        """
        if not attach_to:
            attach_to = self
        if location is None:
            location = self.location
        try:
            #Needed implementation of selection in DN geometries
            #where two values are sometimes given back.
            #Only the correct should be attached, according to context
            var = self._parser(signal_name, var_name=var_name, location=location)
            var = np.squeeze(var)
            setattr(attach_to, var_name, var)
        except:
            self.log.exception("error reading '{0}''".format(var_name))
            #setattr(attach_to, var_name, None)


    def _parser(self, signal_name, var_name=None, location=None):
        """
        signal_name only contains the stem. The complete signal name must be
        evaluated here with the correct mixture of (signal_name + self._psym)

        Previous _read_ncfile(self, var) of Ferdinand
        """
        in_b2time = [
            'fi3dtr', 'tesepa', 'fchyip', 'tisepa_std', 'tesepi',
            'tesepm', 'te3dl', 'tesepa_std', 'tesepi_std', 'fnisapp',
            'fe3dtr', 'kisepm', 'lh3di', 'an3dl', 'ki3di', 'an3di',
            'timxip_av', 'temxap', 'an3da', 'mn3dl', 'posepa', 'fnisap',
            'temxip', 'pomxip_av', 'timxap', 'posepi', 'vxsepm',
            'timxip', 'posepm', 'an3dr', 'vysepm', 'temxap_av', 'ne3di',
            'ke3di', 'feeyap', 'feisipp', 'nemxap', 'ke3da', 'feeyip',
            'nemxip', 'nesepa_std', 'fnisip', 'vx3da', 'feesapp',
            'feixip', 'ln3di','tisepm_av', 'feisip', 'po3dtr',
            'timxap_std', 'po3dtl', 'dnsepm', 'temxip_std', 'fe3dl',
            'posepa_av', 'fchxip', 'timxip_std', 'tpmxip', 'mn3dtr',
            'fetxip', 'tp3dtl', 'tp3dtr', 'po3dl', 'vx3di', 'nemxap_av',
            'fetxap', 'fniyap', 'tisepi_std', 'temxip_av', 'pomxap_av',
            'nesepa', 'temxap_std', 'po3di', 'nesepi', 'fnisipp',
            'nesepm', 'nesepm_av', 'tmhacore', 'feesap', 'fniyip',
            'tisepm_std', 'feisapp', 'feesip', 'ntstep', 'posepm_std',
            'tisepa_av', 'po3da', 'fchsapp', 'tmhasol', 'fl3dtr',
            'dn3di', 'fchsipp', 'kesepm', 'feiyip', 'te3dr', 'fo3dtl',
            'fo3dtr', 'pomxap_std', 'feiyap', 'tesepa_av', 'nemxap_std',
            'fchyap', 'ne3dtr', 'dn3da', 'vs3di', 'ki3da', 'vs3da',
            'nesepa_av', 'tpmxap', 'fe3dtl', 'fnixap', 'tmne',
            'posepi_av', 'fchsip', 'pomxap', 'nemxip_av', 'ft3dr',
            'fchsap', 'pomxip', 'posepm_av', 'fl3dr', 'dpsepm', 'po3dr',
            'fo3dl', 'posepi_std', 'fl3dl', 'ti3dl', 'fnixip', 'ft3dl',
            'ne3dtl', 'mn3dr', 'feesipp', 'vy3da', 'mn3da', 'dp3da',
            'mn3di', 'timesa', 'feisap', 'nesepi_std', 'dp3di', 'fn3dtr',
            'nesepm_std', 'pomxip_std', 'te3dtr', 'tesepm_std', 'fetyap',
            'ne3dr', 'tmhadiv', 'fetyip', 'fchxap', 'te3dtl', 'fn3dtl',
            'ti3da', 'ln3da', 'feixap', 'ti3di', 'tesepi_av', 'tisepm',
            'tisepi', 'ti3dr', 'pwmxip', 'tisepa', 'te3da', 'fo3dr',
            'tesepm_av', 'pwmxap', 'lh3da', 'te3di', 'tp3dl', 'fetsip',
            'fetsapp', 'fi3dl', 'fi3dr', 'ne3dl', 'tp3dr', 'fetsap',
            'fetsipp', 'ntim_batch', 'tisepi_av', 'ne3da', 'an3dtr',
            'nemxip_std', 'fn3dr', 'fc3dl', 'posepa_std', 'batchsa',
            'ti3dtr', 'ft3dtl', 'feexap', 'fe3dr', 'fl3dtl', 'tmte',
            'feexip', 'ti3dtl', 'ft3dtr', 'tmti', 'fn3dl', 'fc3dr',
            'an3dtl', 'nesepi_av', 'fc3dtl', 'fc3dtr', 'vy3di',
            'timxap_av', 'vssepm', 'tpsepa', 'nastep', 'fi3dtl',
            'mn3dtl', 'tpsepi',
            ##WG
            'ktsepi','ktsepm','ktsepa']

        in_dsfiles  = ['dsl','dsi','dsa','dsr','dstl','dstr']
        in_dafiles  = ['dsL','dsR','dsTL','dsTR']

        in_b2tallies = [
            'b2bremreg','b2divua','b2divue','b2exba','b2exbe',
            'b2fraa','b2joule','b2qie','b2radreg','b2sext_sch_reg',
            'b2sext_she_reg','b2sext_shi_reg','b2sext_smo_reg',
            'b2sext_sna_reg','b2sext_sne_reg','b2she','b2she0',
            'b2shi','b2shi0','b2stbc_sch_reg','b2stbc_she_reg',
            'b2stbc_shi_reg','b2stbc_smo_reg','b2stbc_sna_reg',
            'b2stbc_sne_reg','b2stbm_sch_reg','b2stbm_she_reg',
            'b2stbm_shi_reg','b2stbm_smo_reg','b2stbm_sna_reg',
            'b2stbm_sne_reg','b2stbr_sch_reg','b2stbr_she_reg',
            'b2stbr_shi_reg','b2stbr_smo_reg','b2stbr_sna_reg',
            'b2stbr_sne_reg','b2str','b2visa','b2wrong1',
            'b2wrong2','b2wrong3','fchxreg','fchyreg','fhexreg',
            'fheyreg','fhixreg','fhiyreg','fhjxreg','fhjyreg',
            'fhmxreg','fhmyreg','fhpxreg','fhpyreg','fhtxreg',
            'fhtyreg','fnaxreg','fnayreg','nareg','ne2reg',
            'nereg','nireg','poreg','qconvexreg','qconveyreg',
            'qconvixreg','qconviyreg','rcxhireg','rcxmoreg',
            'rcxnareg','resco_reg','reshe_reg','reshi_reg',
            'resmo_reg','resmt_reg','respo_reg','rqahereg',
            'rqbrmreg','rqradreg','rrahireg','rramoreg',
            'rranareg','rsahireg','rsamoreg','rsanareg',
            'species','tereg','times','tireg','volreg']

        #ATTENTION: Improve names to make them more general? pump[time,natm] e.g.
        in_particle_fluxes = [
            'Ar_puff','Ar_pump','Ar_pump_core','D2_pump',
            'D2_pump_core','D_puff','D_puff_core','D_pump',
            'D_pump_core','N2_pump','N2_pump_core','N_puff',
            'N_pump','N_pump_core','time']


        if location is None:
            location = self.location

        if (signal_name.find('3d') != -1) or (signal_name == 'ds{}'):
            if var_name == '_da':
                signal_name = signal_name.format(self._symbols('da', location))
                var_index = None
            else:
                signal_name = signal_name.format(
                    self._symbols('profile', location))
                var_index = None
        elif ((signal_name.find('sep{}') != -1)
            or (signal_name.find('x{}p') != -1)
            or (signal_name.find('y{}p') != -1)):
            signal_name = signal_name.format(
                self._symbols('singleton', location))
            var_index = self._var_index('singleton', location)
        else:
            var_index = None

        ncfile = None
        dsfile = None
        dafile = None
        if signal_name in in_b2time:
            ncfile = 'b2time.nc'
        elif signal_name in in_b2tallies:
            ncfile = 'b2tallies.nc'
        elif signal_name in in_particle_fluxes:
            ncfile = 'particle_fluxes.nc'
        elif signal_name in in_dsfiles:
            dsfile = signal_name
        elif signal_name in in_dafiles:
            dafile = signal_name
        else:
            self.log.error(
                "Invalid variable (Not found): '{0}'".format(signal_name))
            return None

        if ncfile and ncfile not in self.avail:
            self.log.error("Ncfile '{0}' not in self.avail list".format(ncfile))
        elif dsfile and dsfile not in self.avail:
            self.log.error(
                    "DS-type file '{0}' not in self.avail list".format(dsfile))
        elif dafile and dafile not in self.avail:
            self.log.error(
                    "DA-type file '{0}' not in self.avail list".format(dsfile))

        if ncfile:
            try:
                with netcdf_file(self.avail[ncfile],'r') as fnc:
                    tmp = np.array(fnc.variables[signal_name].data)

                    if 'scale' in fnc.variables[signal_name].__dict__:
                        tmp *= fnc.variables[signal_name].scale

                    if var_index is not None:
                        tmp = tmp[..., var_index]
                    return tmp

            except:
                self.log.exception(
                    "'{0}' could not be read from ncfile '{1}'".format(
                        signal_name, ncfile))
                return None

        elif dsfile:
            try:
                with open(self.avail[dsfile],'r') as fds:
                    ds = []
                    for line in fds:
                        ds.append(float(line))
                    return np.array(ds)
            except:
                self.log.error(
                    "'{0}' could not be read from DS-type file '{1}'".format(
                        signal_name, dsfile))
                return None
        elif dafile:
            try:
                with open(self.avail[dafile],'r') as fda:
                    da = []
                    for line in fda:
                        da.append(float(line))
                    return np.array(da)
            except:
                self.log.error(
                    "'{0}' could not be read from DA-type file '{1}'".format(
                        signal_name, dafile))
                return None



    def _symbols(self, var_type, location =None):
        """ Returns character of the solps nomenclature that identify  the
        corresponding position.

        It uses self.location to determine it.

        Parameters
        ----------
        var_type: str
           Type of variable. It can be one of {'profile', 'singleton'}

        Returns
        -------
        str of single character
        """
        _profiles_symbols = {
            'target1':'l',
            'imp':'i',
            'omp':'a',
            'target4':'r',
            'target2':'tl',
            'target3':'tr'}
        _singletons_symbols = {
            'omp':'m',
            'imp':'', #Not defined, apparently
            'target1':'i',
            'target2':'i',
            'target3':'a',
            'target4':'a',
            'sol':'i',
            'div':'a'}
        _da_symbols = {
            'target1':'L',
            'target4':'R',
            'target2':'TL',
            'target3':'TR'}

        if location is None:
            location = self.location

        if var_type == 'profile':
            return _profiles_symbols[location]
        elif var_type == 'da':
            return _da_symbols[location]
        elif var_type == 'singleton':
            return _singletons_symbols[location]
        else:
            self.log.error("Type '{0}' is not valid".format(var_type))
            return None


    #ATTENTION: Check correctness.
    def _var_index(self, var_type, location=None):
        """ Returns index of the solps nomenclature that identify  the
        corresponding position.

        It uses self.location and self.maggeom to determine it.

        Parameters
        ----------
        var_type: str
           Type of variable. It can be one of {'profile', 'singleton'}

        Returns
        -------
        int or None
        """
        if location is None:
            location = self.location
        _dn_indices = {
            'omp': 0,
            'imp': 1,
            'target1': 0,
            'target2': 1,
            'target3': 1,
            'target4': 0}
        if var_type == 'singleton':
            if self.maggeom in ['cdn', 'ddn', 'ddnu']:
                try:
                    return _dn_indices[location]
                except:
                    return None
        return None






class TargetDataRundir(TargetData, RundirTT):

    @solps_property
    def da(self):
        self._read_signal('_da', 'ds{}')





class PFRData(object):
    """ Only temporary
    """
    def __init__(self, mother, location):
        """
        Parameters
        ----------
        mother : object
            SolpsData instance
        location : str
            Special location out of which the data are being retrieved.
        """

        alog.debug("> Entering PFRData __init__ for {}".format(location))

        self.location = location.lower()
        self.log = mother.log
        self.avail = mother.avail
        self.config = mother.config
        self.data_type = mother.data_type
        self.grid_type = mother.grid_type
        self.maggeom = mother.magnetic_geometry

        alog.debug("< Leaving PFRData __init__ for {}".format(location))





#class MCWDataRundir(RundirTT):
class MCWData(object):
    """ 
    """
    def __init__(self, mother, location):
        """
        Parameters
        ----------
        mother : object
            SolpsData instance
        location : str
            Special location out of which the data are being retrieved.
        """

        alog.debug("> Entering MCWData __init__ for {}".format(location))

        self.location = location.lower()
        self.log = mother.log
        self.avail = mother.avail
        self.config = mother.config
        self.data_type = mother.data_type
        self.grid_type = mother.grid_type
        self.maggeom = mother.magnetic_geometry

        alog.debug("< Leaving MCWData __init__ for {}".format(location))

    # -------------------- Singleton quantities -------------------------------
    @solps_property
    def fnasol_int(self):
        """(time,2?) : integrated radial particle flux, sol portion [1/s].
        For DN configurations:
            [:,0]: outer sol mcw border.
            [:,1]: inner sol mcw border.
        """
        self._read_signal('_fnasol_int', 'fniy{}p', location='sol')

    @solps_property
    def fnadiv_int(self):
        """(time,2?) : integrated radial particle flux, div portion [1/s].
        For DN configurations:
            [:,0]: lower div mcw border.
            [:,1]: upper div mcw border.
        """
        self._read_signal('_fnadiv_int', 'fniy{}p', location='div')


    @solps_property
    def fhesol_int(self):
        """(time,2?) : integrated radial electron energy flux,
        sol portion [W].
        For DN configurations:
            [:,0]: outer sol mcw border.
            [:,1]: inner sol mcw border.
        """
        self._read_signal('_fhesol_int', 'feey{}p', location='sol')

    @solps_property
    def fhediv_int(self):
        """(time,2?) : integrated radial electron energy flux,
        div portion [W].
        For DN configurations:
            [:,0]: lower div mcw border.
            [:,1]: upper div mcw border.
        """
        self._read_signal('_fhediv_int', 'feey{}p', location='div')


    @solps_property
    def fhisol_int(self):
        """(time,2?) : integrated radial ion energy flux,
        sol portion [W].
        For DN configurations:
            [:,0]: outer sol mcw border.
            [:,1]: inner sol mcw border.
        """
        self._read_signal('_fhisol_int', 'feiy{}p', location='sol')

    @solps_property
    def fhidiv_int(self):
        """(time,2?) : integrated radial ion energy flux,
        div portion [W].
        For DN configurations:
            [:,0]: lower div mcw border.
            [:,1]: upper div mcw border.
        """
        self._read_signal('_fhidiv_int', 'feiy{}p', location='div')


    @solps_property
    def fheisol_int(self):
        """(time,2?) : integrated radial electron + ion energy flux,
        sol portion [W].
        For DN configurations:
            [:,0]: outer sol mcw border.
            [:,1]: inner sol mcw border.
        """
        self.fheisol_int = self.fhesol_int + self.fhisol_int

    @solps_property
    def fheidiv_int(self):
        """(time,2?) : integrated radial electron + ion energy flux,
        div portion [W].
        For DN configurations:
            [:,0]: lower div mcw border.
            [:,1]: upper div mcw border.
        """
        self.fheidiv_int = self.fhesol_int + self.fhisol_int


    @solps_property
    def fhtsol_int(self):
        """(time,2?) : integrated radial total energy flux,
        sol portion [W].
        For DN configurations:
            [:,0]: outer sol mcw border.
            [:,1]: inner sol mcw border.
        """
        self._read_signal('_fhtsol_int', 'fety{}p', location='sol')

    @solps_property
    def fhtdiv_int(self):
        """(time,2?) : integrated radial total energy flux,
        div portion [W].
        For DN configurations:
            [:,0]: lower div mcw border.
            [:,1]: upper div mcw border.
        """
        self._read_signal('_fhtdiv_int', 'fety{}p', location='div')


    @solps_property
    def fchsol_int(self):
        """(time,2?) : integrated radial current, sol portion [A].
        For DN configurations:
            [:,0]: outer sol mcw border.
            [:,1]: inner sol mcw border.
        """
        self._read_signal('_fchsol_int', 'fchy{}p', location='sol')

    @solps_property
    def fchdiv_int(self):
        """(time,2?) : integrated radial current, div portion [A].
        For DN configurations:
            [:,0]: lower div mcw border.
            [:,1]: upper div mcw border.
        """
        self._read_signal('_fchdiv_int', 'fchy{}p', location='div')


























## Make it so data.global.reg('core') and .reg(1) give the same result
## Or data.tallies.te_max['core']
## So data.global.reg('core').te

class TalliesData(object):
    def __init__(self, mother, location):
        """
        Parameters
        ----------
        mother : object
            SolpsData instance
        location : str
            Special location out of which the data are being retrieved.
        """

        alog.debug("> Entering TalliesData __init__ for {}".format(location))

        self.location = location.lower()
        self.log = mother.log
        self.avail = mother.avail
        self.config = mother.config
        self.data_type = mother.data_type
        self.grid_type = mother.grid_type
        self.maggeom = mother.magnetic_geometry

        alog.debug("< Leaving TalliesData __init__ for {}".format(location))
    #    =============== Properties =======================
    # -------------------- Profile quantities ---------------------------------

    # -------------------- Singleton quantities -------------------------------
    @solps_property
    def tmne(self):
        """(time) : total number of particles []

        Notes
        -----
        np.sum(self.ne*self.vol) or np.sum(self.ne*self.nccv)
        """
        self._read_signal('_tmne', 'tmne')

    @solps_property
    def tmte(self):
        """(time) : total electron thermal energy [eV]

        Notes
        -----
        np.sum(te*ne*vol) or e static pressure times volume.
        """
        self._read_signal('_tmte', 'tmte')


    @solps_property
    def tmti(self):
        """(time) : total ion thermal energy [eV]

        Notes
        -----
        np.sum(ti*ni*vol)
        """
        self._read_signal('_tmti', 'tmti')

    @solps_property
    def tmt(self):
        """(time): total ions+electrons thermal energy [eV]"""
        self._tmt = self.tmte + self.tmti


    @solps_property
    def tmhacore(self):
        """(time) : H-alpha emissivity, core [photons m^-2 sr^-1 ??]"""
        self._read_signal('_tmhacore', 'tmhacore')

    @solps_property
    def tmhasol(self):
        """(time) : H-alpha emissivity, sol [photons m^-2 sr^-1 ??]"""
        self._read_signal('_tmhasol', 'tmhasol')

    @solps_property
    def tmhadiv(self):
        """(time) : H-alpha emissivity, div [photons m^-2 sr^-1 ??]"""
        self._read_signal('_tmhadiv', 'tmhadiv')




    ## Only for Structured so far
#    @solps_property
#    def rdneureg(self):
#        """(time,vregs) : Total radiation from Eirene neutrals [W]
#        To be used with self.cvregions[region]
#        """
#        self._read_signal('_rdneureg', 'rdneureg')



    @solps_property
    def fnayreg(self):
        """(time,yregs,ns) : []
        To be used with self.yregions[region]
        """
        self._read_signal('_fnayreg', 'fnayreg')

    @solps_property
    def fnaxreg(self):
        """(time,xregs,ns) : []
        To be used with self.xregions[region]
        """
        self._read_signal('_fnaxreg', 'fnaxreg')



    @solps_property
    def fchyreg(self):
        """(time,yregs,ns) : Poloidal current [A]
        To be used with self.yregions[region]
        """
        self._read_signal('_fchyreg', 'fchyreg')

    @solps_property
    def fchxreg(self):
        """(time,xregs,ns) : Radial current [A]
        To be used with self.xregions[region]
        """
        self._read_signal('_fchxreg', 'fchxreg')



    @solps_property
    def fhtyreg(self):
        """(time,yregs) : Radial total energy [W]
        To be used with self.yregions[region]
        """
        self._read_signal('_fhtyreg', 'fhtyreg')

    @solps_property
    def fhtxreg(self):
        """(time,xregs) : Poloidal total energy [W]
        To be used with self.xregions[region]
        """
        self._read_signal('_fhtxreg', 'fhtxreg')



    @solps_property
    def fheyreg(self):
        """(time,yregs) : Radial electron thermal energy [W]
        To be used with self.yregions[region]
        """
        self._read_signal('_fheyreg', 'fheyreg')

    @solps_property
    def fhexreg(self):
        """(time,xregs) : Poloidal electron thermal energy [W]
        To be used with self.xregions[region]
        """
        self._read_signal('_fhexreg', 'fhexreg')



    @solps_property
    def fhiyreg(self):
        """(time,yregs) : Radial ion thermal energy [W]
        To be used with self.yregions[region]
        """
        self._read_signal('_fhiyreg', 'fhiyreg')

    @solps_property
    def fhixreg(self):
        """(time,xregs) : Poloidal ion thermal energy [W]
        To be used with self.xregions[region]
        """
        self._read_signal('_fhixreg', 'fhixreg')




    @solps_property
    def fheiyreg(self):
        """(time,yregs) : Radial e+i thermal energy [W]
        To be used with self.yregions[region]
        """
        self._fheiyreg = self.fheyreg + self.fhiyreg

    @solps_property
    def fheixreg(self):
        """(time,xregs) : Poloidal e+i thermal energy [W]
        To be used with self.xregions[region]
        """
        self._fheixreg = self.fhexreg + self.fhixreg





    @solps_property
    def fhsyreg(self):
        """(time,yregs) : Radial total energy, minus e+i [W]
        To be used with self.yregions[region]
        """
        self._fhsyreg = self.fhtyreg - self.fheyreg - self.fhiyreg

    @solps_property
    def fhsxreg(self):
        """(time,xregs) : Poloidal total energy, minus e+i [W]
        To be used with self.xregions[region]
        """
        self._fhsxreg = self.fhtxreg - self.fhexreg - self.fhixreg



    @solps_property
    def fhpyreg(self):
        """(time,yregs) : Radial ionisation energy [W]
        To be used with self.yregions[region]
        """
        self._read_signal('_fhpyreg', 'fhpyreg')

    @solps_property
    def fhpxreg(self):
        """(time,xregs) : Poloidal ionisation energy [W]
        To be used with self.xregions[region]
        """
        self._read_signal('_fhpxreg', 'fhpxreg')



    @solps_property
    def fhmyreg(self):
        """(time,yregs) : Radial parallel kineitc energy [W]
        To be used with self.yregions[region]
        """
        self._read_signal('_fhmyreg', 'fhmyreg')

    @solps_property
    def fhmxreg(self):
        """(time,xregs) : Poloidal parallel kinetic energy [W]
        To be used with self.xregions[region]
        """
        self._read_signal('_fhmxreg', 'fhmxreg')





    @solps_property
    def fhjyreg(self):
        """(time,yregs) : Radial electrostatic energy [W]
        To be used with self.yregions[region]
        """
        self._read_signal('_fhjyreg', 'fhjyreg')

    @solps_property
    def fhjxreg(self):
        """(time,xregs) : Poloidal electrostatic energy [W]
        To be used with self.xregions[region]
        """
        self._read_signal('_fhjxreg', 'fhjxreg')





    ## This is like fnt + 3/2, but why is it then 3/2+5/2??
    @solps_property
    def fheconvyreg(self):
        """(time,yregs) : Radial convected electron energy [W]
        To be used with self.yregions[region]
        """
        self._read_signal('_fheconvyreg', 'qconveyreg')

    @solps_property
    def fheconvxreg(self):
        """(time,xregs) : Poloidal convected electron energy [W]
        To be used with self.xregions[region]
        """
        self._read_signal('_fheconvxreg', 'qconvexreg')


    @solps_property
    def fhiconvyreg(self):
        """(time,yregs) : Radial convected ion energy [W]
        To be used with self.yregions[region]
        """
        self._read_signal('_fhiconvyreg', 'qconviyreg')

    @solps_property
    def fhiconvxreg(self):
        """(time,xregs) : Poloidal convected ion energy [W]
        To be used with self.xregions[region]
        """
        self._read_signal('_fhiconvxreg', 'qconvixreg')




    ## Temporary patch because wtf is going on.
    @solps_property
    def fhntyreg(self):
        """(time,yregs) : Radial total energy, minus e+i [W]
        To be used with self.yregions[region]
        """
        self._fhntyreg = self.fhtyreg - (
            self.fheyreg +
            self.fhiyreg +
            np.sum(self.fhpyreg, axis=1) +
            np.sum(self.fhmyreg, axis=1) +
            self.fhjyreg
            )

    ## Temporary patch because wtf is going on.
    @solps_property
    def fhntxreg(self):
        """(time,xregs) : Poloidal total energy, minus e+i [W]
        To be used with self.xregions[region]
        """
        self._fhntxreg = self.fhtxreg - (
            self.fhexreg +
            self.fhixreg +
            np.sum(self.fhpxreg, axis=1) +
            np.sum(self.fhmxreg, axis=1) +
            self.fhjxreg
            )









#### =================== GLOBAL TIME TRACES ===================================



#class GlobalTimeTraces(object):
#    """Base class for global quantites with time traces for MDS+ SolpsData.
#    """
#    def __init__(self, mother, location):
#        """
#        Parameters
#        ----------
#        mother : object
#            SolpsData instance
#        """
#        self.location = location
#        self.log = mother.log
#
#
#
#
#
#    def _read_signal(self,var_name, signal_name, attach_to=None):
#        """ Retrieve data from the MDS+, given tree link and signal name.
#
#        read_signal does not return any value. It automatically attach
#        the signal as an attribute with a given name to the given object
#        or self.
#
#        Parameters
#        ----------
#        var_name : str
#            Storage name of the variable for the retrieve signal.
#        signal_name : str
#            Name to retrieve signal.
#            Only the stem of signal_name is given.
#        attach_to : object, optional
#            signal_name will be attached as attribute to object
#            attach_to. Default to self.
#
#        Returns
#        -------
#        None
#        """
#        if not self.conn_open:
#            self.conn.openTree('solps', self.shot)
#            self.conn_open=True
#
#        if not attach_to:
#            attach_to = self
#
#        #Not symmetrical wrt rundirtt, as parser gives array there
#        signal_link = self._parser(signal_name)
#        if signal_link:
#            try:
#                #Introduce here DN logic for singleton variables, by deciding
#                #which of the tuple in tmp is given back
#                tmp = self.conn.get(signal_link).value.T
#                setattr(attach_to, var_name, tmp)
#            except:
#                self.log.warning(
#                    "error reading '{1}' for '{0}'".format(
#                    signal_name,signal_link))
#                setattr(attach_to, var_name, None)
#        else:
#            setattr(attach_to, var_name, None)
#
#        if self.conn_open:
#            self.conn.closeTree('solps', self.shot)
#            self.conn_open = False
#
#
#    def _parser(self, signal_name):
#        """
#        signal_name only contains the stem. The complete signal name must be
#        evaluated here with the correct mixture of (signal_name + self._psym)
#
#        In the case of MDS+ signals, the location and name are formatted
#        directly into the MDS roots.
#
#        completing the formating will be only use for the parser of Rundir.
#        """
#
#        _whl = self.location.upper()
#
#        #Needed because singleton quantities use TARG instead
#        if _whl[:-1] == 'TARGET':
#            _wh = 'TARG'+_whl[-1]
#        else:
#            _wh = _whl
#
#        #No need to complete name
#        signal_name = signal_name.format("")
#
#        #For profiles
#        tprof = '\TIMEDEP::TOP.{0}:{1}'
#
#        #For singletons
#        tint = '\TIMEDEP::TOP.{1}:{0}INT'
#        tsep = '\TIMEDEP::TOP.{1}:{0}SEP'
#        tmax = '\TIMEDEP::TOP.{1}:{0}MAX'
#
#
#        mdstree_common ={
#                #Profile quantities
#                'an3d':tprof.format(_whl, 'AN'),
#                'ds':tprof.format(_whl, 'DS'),
#                'mn3d':tprof.format(_whl, 'MN'),
#                'ne3d':tprof.format(_whl, 'NE'),
#                'po3d':tprof.format(_whl, 'PO'),
#                'te3d':tprof.format(_whl, 'TE'),
#                'ti3d':tprof.format(_whl, 'TI'),
#
#                #Singleton quantities
#                'tesep':tsep.format(_wh,'TE'),
#                'tisep':tsep.format(_wh,'TI'),
#                'nesep':tsep.format(_wh,'NE'),
#                'posep':tsep.format(_wh,'PO'),
#                }
#
#
#        mdstree_midplanes ={
#                #Profile quantities
#                'dn3d':tprof.format(_whl, 'DN'),
#                'dp3d':tprof.format(_whl, 'DP'),
#                'ke3d':tprof.format(_whl, 'KE'),
#                'ki3d':tprof.format(_whl, 'KI'),
#                'vs3d':tprof.format(_whl, 'VS'),
#                'vx3d':tprof.format(_whl, 'VX'),
#                'vy3d':tprof.format(_whl, 'VY'),
#
#                #Singleton quantities
#                'dnsep':tsep.format(_wh,'DN'),
#                'dpsep':tsep.format(_wh,'DP'),
#                'kesep':tsep.format(_wh,'KE'),
#                'kisep':tsep.format(_wh,'KI'),
#                'vssep':tsep.format(_wh,'VS'),
#                'vxsep':tsep.format(_wh,'VX'),
#                'vysep':tsep.format(_wh,'VY'),
#                 }
#
#
#        mdstree_targets ={
#                #Profile quantities
#                'da':tprof.format(_whl, 'DA'),
#                'fc3d':tprof.format(_whl, 'FC'),
#                'fe3d':tprof.format(_whl, 'FE'),
#                'fi3d':tprof.format(_whl, 'FI'),
#                'fl3d':tprof.format(_whl, 'FL'),
#                'fn3d':tprof.format(_whl, 'FN'),
#                'fo3d':tprof.format(_whl, 'FO'),
#                'ft3d':tprof.format(_whl, 'FT'),
#
#                #Singleton quantities
#                'temxp':tmax.format(_wh,'TE'),
#                'timxp':tmax.format(_wh,'TI'),
#                'nemxp':tmax.format(_wh,'NE'),
#                'pomxp':tmax.format(_wh,'PO'),
#
#                'fchxp':tint.format(_wh,'FCH'),
#                'feexp':tint.format(_wh,'FEE'),
#                'feixp':tint.format(_wh,'FEI'),
#                'fetxp':tint.format(_wh,'FET'),
#                'fnixp':tint.format(_wh,'FNI'),
#                }
#
#
#        if _whl[:-1] == 'TARGET':
#            try:
#                mdstree_common.update(mdstree_targets)
#                return mdstree_common[signal_name]
#            except:
#                self.log.warning(
#                    "'{0}' not found/implemented for '{1}' MDSTREE".format(
#                                                    signal_name, self.location))
#                return None
#
#
#        elif _whl[-2:] == 'MP':
#            try:
#                mdstree_common.update(mdstree_midplanes)
#                return mdstree_common[signal_name]
#            except:
#                self.log.warning(
#                    "'{0}' not found/implemented for '{1}' MDSTREE".format(
#                                                    signal_name, self.location))
#                return None
#


















