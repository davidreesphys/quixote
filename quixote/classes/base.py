# Common modules:
import re
import numpy as np
from collections import OrderedDict


# Quixote modules:
from .timetraces import TimeTraces
from ..tools import solps_property, read_config
from ..tools import priority, elements
from ..tools import get, update
from ..tools import create_log
from ..tools import whichclass
from ..tools import linearize_rates, evaluate_rates
from ..tools.chords import rzeval_hull

from ..tools import solps_property2 

from ..extensions.chords import Chords
from ..extensions.eqout import EquationsOutput
from ..extensions.wlld import WlldBase

alog = create_log('quixote')




class BaseData(object):
    def __init__(self, *args, **kwargs):
        alog.debug("> Entering BaseData __init__")
        self.config = priority(['config','cfg'], kwargs, None)
        if self.config is None or not isinstance(self.config, dict):
            self.config = read_config()

        if not self.name:
            self.name = 'unnamed'

        self.log = create_log(self.class_type, {'ident': self.name})


        b2standalone = priority(['b2','b2standalone'], kwargs, None)
        if b2standalone is not None:
            self._b2standalone = b2standalone

        self.avail = {}
        self._check_avail()

        self._get_basic_dimensions()

        self.mp = 1.672621777e-27
        self.qe = 1.602176565e-19
        self.ev = self.qe # Alias
        self.kb = 1.380649e-23 #J/K

        self._attach_reuse(priority('reuse', kwargs, {}))

        self.omp = TimeTraces(self, 'omp')
        self.imp = TimeTraces(self, 'imp')

        self.mcw = TimeTraces(self, 'mcw')

        self.tallies = TimeTraces(self, 'tallies')

        super().__init__() ## No arguments, because super points to object.

        alog.debug("< Leaving  BaseData __init__")


# INITIAL FUNCTION ===========================================================
    def _attach_reuse(self, reuse):
        """ Should only accept '_attrs',
        since one cannot set a property.
        """
        for k, v in reuse.items():
            if not hasattr(self, k):
                setattr(self, k, v)




# EXTENSIONS ==================================================================
    @solps_property
    def chords(self):
        self._chords = Chords(self)

    def get(self, *args, **kwargs):
        return get(self,*args, **kwargs)


    @solps_property
    def eqout(self):
        self._eqout = EquationsOutput(self)


    @solps_property
    def wlld(self):
        self._wlld = WlldBase(self)



    def rz(self, var, point):
        return rzeval_hull(self, var, point)

# IDENT PROPERTIES ============================================================
    @solps_property
    def solps_code(self):
        """str: Name of SOLPS code package version."""
        if self.solpsversion[0:2] == '01':
            self._solps_code = 'solps5.0'
        elif self.solpsversion[0:2] == '03':
            self._solps_code = 'solps-iter'
        else:
            self._solps_code = None

    @property
    def path(self):
        return self.directory


    ## ---------------- REVISION    warning! ------------------------
    # -------------------- Names and texts ----------------------------------

    ## ATTENTION: Decide between D or H, for example, using the mass.
    @solps_property
    def ib2(self):
        self._ib2 = {}

    @solps_property
    def iatm(self):
        self._read_eirene_species()

    @solps_property
    def imol(self):
        self._read_eirene_species()

    @solps_property
    def iion(self):
        self._read_eirene_species()


    def _read_eirene_species(self):
        if 'fort.44' in self.avail:
            file = 'fort.44'
        elif 'fort.46' in self.avail:
            file = 'fort.46'
        else:
            file = None
        if file:
            self._iatm = {}
            self._imol = {}
            self._iion = {}
            with open(self.avail[file], 'r') as fr:
                fr.readline()
                fr.readline()
                for i in range(self.natm):
                    atm = fr.readline().strip()
                    self._iatm[i] = atm.title()
                    self._iatm[atm.title()] = i
                    self._iatm[atm.lower()] = i
                for i in range(self.nmol):
                    mol = fr.readline().strip()
                    self._imol[i] = mol.title()
                    self._imol[mol.title()] = i
                    self._imol[mol.lower()] = i
                for i in range(self.nion):
                    ion = fr.readline().strip()
                    self._iion[i] = ion.title()
                    self._iion[ion.title()] = i
                    self._iion[ion.lower()] = i
        else:
            pass
            ## Do the logic of b2_species, etc.





    ## Heavily improve this section with more feedback.

    ## ATTENTION: species_names is not available for ClassicalDataUSN
    #ATTENTION: Maybe change to fluid_species
    def b2_species(self, ns):
        """ Returns index of species if given a name or name for a given
        index of the B2 fluid species.
        """
        if type(ns) != str:
            return self.species_names[ns]
        else:
            return self.species_names.index(ns)

    #ATTENTION: This only works for atoms, not molecules or test ions.
    #ATTENTION: And add other like mol_species
    def atom_species(self, ns):
        """ Returns index of species if given a name or name for a given
        index of EIRENE atomic species.
        """
        if type(ns) != str:
            return self.atom_names[ns]
        else:
            return self.atom_names.index(ns)


    #ATTENTION: It gives back H, not D.
    #ATTENTION: change name to eirene_names? neutral_names?
    ## ELEMENTS IS NOW A FUNCTION
    @solps_property
    def atom_names(self):
        """list(str) : B2 neutrals names."""
        self._atom_names = []
        zn = -1
        for ns in range(self.ns):
            if zn != self.zn[ns]:
                zn = self.zn[ns]
                self._atom_names.append(elements(zn).title())


    #ATTENTION: It gives back H, not D.
    @solps_property
    def b2_names(self):
        """list(str): B2 species names."""
        self._species_names = []
        for ns in range(self.ns):
            name = elements(self.zn[ns]).title()
            if   self.za[ns] == 1: name += '+'
            elif self.za[ns] == 2: name += '++'
            elif self.za[ns] >= 3: name += str(int(self.za[ns])) + '+'
            self._species_names.append(name)

#    #ATTENTION: Create function instead: It returns name if given an index and
#    # index if given a name
#    # Maybe insert the logic here completely with the one of species_names
#    #Add the logic of species_index and eirene_index too
#    # Maybe add alias of fluid_species
#    def b2_species(self, id):
#        if type(id) != str:
#            return self.species_names[id]
#        else:
#            return self.species_names.index(id)
#    def eirene_species(self, id):
#        if type(id) != str:
#            return self.eirene_names[id]
#        else:
#            return self.eirene_names.index(id)


    #ATTENTION: Not obvious. Poor naming
    #Should be called eirene_names and eirene_index?
    def atom_index(self,atom):
        """Returns the natm index for the specified atom."""
        if atom == 'D':
            atom = 'H'
        return self.atom_names.index(atom.title())

    #ATTENTION: Not obvious. Poor naming
    #Should be called b2_names and b2_index?
    ## ATTENTION 2023: Rename to b2_species, atm_species, mol_species, 
    ## iatm returns the indices or names, this returns the same, but if given lists.
    def species_index(self,species):
        """Returns a list of species indizes calculated from the parameter
        'species'. This 'species' variable can be a list of species names
        (like ['N6+,N7+]') or a range of species, like 'N+-N5+' or a single
        species string (like 'N++')."""

        def sti(string): # string_to_index
            if type(string) == int: return string
            s = string.title()
            if s == 'D':  s = 'H'
            if s == 'D+': s = 'H+'
            if s in self.species_names: return self.species_names.index(s)
            s = s.replace('2+','++')
            if s in self.species_names: return self.species_names.index(s)
            # Else:
            self.log.error("Invalid species '%s'",string)

        if species == 'all':
            return list(range(self.ns))
        elif type(species) == int:
            return [species]
        elif type(species) == str:
            if '-' in species:
                sr = species.split('-')
                return list(range(sti(sr[0]),sti(sr[1])+1))
            else:
                return [sti(species)]
        elif type(species) == list:
            rlist = []
            for item in species:
                rlist.append(sti(item))
            return rlist






##  GEOMETRY ==================================================================

    # -------------------- Dimensions -----------------------------------------

    # -------------------- Indices and locators -------------------------------

    # -------------------- Structure ------------------------------------------
    @property
    def dv(self):
        """ Alias for :py::attr:`~vol`"""
        return self.vol



    # -------------------- Grid and vessel geometry ---------------------------




## SNAPSHOT ===================================================================

    # -------------------- Plasma Characterization ----------------------------

    @solps_property
    def zn(self):
        """(ns): Nuclear charge [e-]."""
        self._read_signal('_zn', 'zn')

    @solps_property
    def am(self):
        """(ns): Atomic mass [u]"""
        self._read_signal('_am', 'am')
    
    @solps_property
    def am_kg(self):
        """(ns): Atomic mass [kg]"""
        amu = 1.660539040e-27
        self._am_kg = amu * self.am


    @solps_property
    def mi(self):
        """({GRID_DIMS}, ns) : Atomic mass [kg]"""
        self._mi = self.am_kg * np.ones_like(self.na)


    @solps_property2
    def po(self):
        """({GRID_DIMS}): Electric potential [V]."""



    # -------------------- Densities ------------------------------------------

    @solps_property
    def na(self):
        """ ({GRID_DIMS}, ns): Fluid species density [m^-3]"""
        self._read_signal('_na','na')

    @solps_property
    def naeir(self):
        """({GRID_DIMS}, ns): Species densities corrected for Eirene neutrals [m^-3].

        Notes
        -----
        If b2standalone case, then naeir = :py:attr:`~na`.
        """
        self._naeir = self.na.copy()

        if self.b2standalone is False:
            iatm = 0
            for i,z in enumerate(self.za):
                if z == 0:
                    self._naeir[...,i] = self.dab2[...,iatm]
                    iatm += 1

    @solps_property
    def ne(self):
        """ ({GRID_DIMS}): Electron density [m^-3]"""
        self._read_signal('_ne','ne')

    @solps_property
    def dab2(self):
        """ ({GRID_DIMS}, natm): Eirene atomic density [m^-3]"""
        self._read_signal('_dab2', 'dab2')

    @solps_property
    def dmb2(self):
        """ ({GRID_DIMS}, nmol): Eirene molecular density [m^-3]"""
        self._read_signal('_dmb2', 'dmb2')

    @solps_property
    def dib2(self):
        """ ({GRID_DIMS}, nion): Eirene test ion density [m^-3]"""
        self._read_signal('_dib2', 'dib2')


    @solps_property
    def zeff(self):
        """ ({GRID_DIMS}): Effective charge [e-]"""
        self._zeff = np.sum(self.na*self.za**2, self.ns_axis)/self.ne



    @solps_property
    def na_isonuclear(self):
        """({GRID_DIMS}): Corrected total density per element [m^-3].

        Notes
        -----
        Sum of all charge states of isonuclear sequence.
        It uses EIRENE densities.
        """
        self._na_isonuclear = self._sum_isonuclear(self.naeir)

    @solps_property
    def fractional_abundance(self):
        """({GRID_DIMS}, ns): Fractional ion abundances as they appear
        in the simulation.
        """
        natot = np.zeros_like(self.na)
        natm = 0
        zn = self.zn[0]
        for ns in range(self.ns):
            if zn != self.zn[ns]:
                zn = self.zn[ns]
                natm += 1
            natot[...,ns] = self.na_isonuclear[...,natm]
        self._fractional_abundance = self.naeir / natot


    #ATTENTION: Better name? Alias?
    #ATTENTION: Improve docstring
    ## ATTENTION: 2022, why is wg (3720, 2), isonuclear.
    @solps_property
    def cimp(self):
        """({GRID_DIMS}, ns): Impurity concentration.

        Notes
        -----
        It uses EIRENE densities
        """
        ## Should be naeir or something??
        self._cimp = self.na_isonuclear / np.expand_dims(
            np.sum(self.naeir, self.ns_axis), self.ns_axis)


    @solps_property
    def ni(self):
        """ ({GRID_DIMS}): Total ion density, sum :py:attr:`~na`
        (:py:attr:`~za` >0) [m^-3]"""
        self._ni = np.sum(self.na[...,self.za>0], self.ns_axis)

    





    # -------------------- Temperatures ---------------------------------------

    @solps_property
    def te(self):
        """({GRID_DIMS}): Electron temperature [eV]"""
        self._read_signal('_te', 'te')

    @solps_property
    def ti(self):
        """({GRID_DIMS}): Ion temperature [eV]"""
        self._read_signal('_ti', 'ti')

    @solps_property
    def tab2(self):
        """({GRID_DIMS}, natm): Eirene atomic temperature [eV]"""
        self._read_signal('_tab2', 'tab2')

    @solps_property
    def tmb2(self):
        """({GRID_DIMS}, nmol): Eirene molecular temperature [eV]"""
        self._read_signal('_tmb2', 'tmb2')

    @solps_property
    def tib2(self):
        """({GRID_DIMS}, nion) : Eirene test ion temperature [eV]"""
        self._read_signal('_tib2', 'tib2')




    
    
    # -------------------- Pressures ------------------------------------------

    @solps_property
    def pse(self):
        """({GRID_DIMS}): Electron static pressure [Pa]"""
        self._pse = self.qe*self.ne*self.te
    
    @solps_property
    def psi(self):
        """({GRID_DIMS}): Ion static pressure  [Pa]"""
        self._psi = self.qe*np.sum(self.na[...,self.za>0],axis=self.ns_axis)*self.ti
    
    @solps_property
    def pstot(self):
        """({GRID_DIMS}) : Total static pressure (electrons + ions) [Pa]"""
        self._pstot = self.pse + self.psi



    @solps_property
    def pdyna(self):
        """({GRID_DIMS}, ns) : Dynamic pressure (m*n*ua**2) [Pa]

        Notes
        -----
        EIRENE densities are used.
        """
        self._pdyna = self.mi*self.naeir*self.ua**2


    @solps_property
    def ptot(self):
        """({GRID_DIMS}) : Total plasma pressure (static + dynamic)
        of the sum of all charged species [Pa]"""
        self._ptot = self.pstot + np.sum(self.pdyna[...,self.za>0],axis=self.ns_axis)






    # -------------------- Velocities -----------------------------------------

    @solps_property2
    def ua(self):
        """({GRID_DIMS}, ns) : Parallel velocity [m/s]"""




    # -------------------- Fluxes and energies --------------------------------

    @solps_property
    def fchp(self):
        """({FACE_DIMS}, 2):
        [...,0]: Product of parallel electric current and sign
        of the poloidal magnetic field  component through the
        face between the (ix, iy) cell and its left neighbor.
        [...,1] = 0
        """
        self._read_signal('_fchp','fch_p')



    @solps_property2
    def kinrgy(self):
        """ ({GRID_DIMS}): Total kinetic energy [J]
        """

    @property
    def kinetic_energy(self):
        return self.kinrgy


    #ATTENTION: Error from including neutrals?
    #ATTENTION: Was the previous description better?
    ##(nx,ny) : Stored energy per cell volume as given by the kinetic
    ##          energies of ions and electrons [J/m^3].
    @solps_property
    def stored_energy(self):
        """({GRID_DIMS}) : Stored kinetic energy density of ions
        and electrons [J/m^3].

        Notes
        -----

         E_stored = 3/2*k_B*T*n
                  = 3/2*k_B*(Te*ne + Ti*sum(na))
                  = 3/2*p

         (See e.g. "Thomas Puetterich - Control and Diagnostic of High-Z
                                        Impurities in Fusion Plasmas", p.16ff,
               or  "Vladislav Kotov   - Talk Juelich 2016-09-22)

         Possible error? Ei is using pdyna and psi, thus also including neutrals?
        """
        Ee =  3./2.*self.pse               # = 3/2*ne*te
        Ei = (3./2.*self.psi +             # = 3/2*sum(na*ti)
            1./2.*np.sum(self.pdyna,-1))   # = 1/2*sum(mi*na*ua**2) #Compatible?
        self._stored_energy = Ee+Ei




    



    # -------------------- Coefficients ---------------------------------------
    @solps_property
    def dpa0(self):
        """ ({GRID_DIMS}, ns): Particle pressure-driven diffusivity [m^2/s]"""
        self._read_signal('_dpa0', 'dpa0')

    @solps_property
    def dna0(self):
        """ ({GRID_DIMS}, ns): Particle density-driven diffusivity [m^2/s]"""
        self._read_signal('_dna0', 'dna0')






    # -------------------- Rates, losses/sources and residuals ----------------
    @solps_property2
    def sna(self):
        """ ({GRID_DIMS}, 2, ns):  Components of the total
        particle source [1/s].
        """

    @solps_property2
    def particle_source(self):
        """ ({GRID_DIMS}, ns): Total particle source per species [1/s].
        Corresponding to sna(...,0,:)+na*sna(...,1,:)
        """
        self._particle_source = self.sna[...,0,:] + self.sna[...,1,:]*self.na

    @solps_property2
    def resco(self):
        """ ({GRID_DIMS}, ns): Residuals of continuity equation.
        """

    @solps_property2
    def she(self):
        """ ({GRID_DIMS}, 4):  Components of the electron
        heat sources [eV/s].
        """

    @solps_property2
    def eheat_source(self):
        """ ({GRID_DIMS}): Total electron heat sources [eV/s].
        Corresponding to:
        sna(...,0)
        +te*she(...,1)
        +ne*she(...,2)
        +te*ne*she(...,3)
        """
        self._eheat_source = (self.she[...,0]
            + self.she[...,1]*self.te
            + self.she[...,2]*self.ne
            + self.she[...,3]*self.te*self.ne)

    @solps_property2
    def reshe(self):
        """ ({GRID_DIMS}): Residuals of the electron heat equation.
        """






    @solps_property2
    def shi(self):
        """ ({GRID_DIMS}, 4):  Components of the ion
        heat sources [eV/s].
        """

    @solps_property2
    def iheat_source(self):
        """ ({GRID_DIMS}): Total ion heat sources [eV/s].
        Corresponding to:
        sna(...,0)
        +ti*shi(...,1)
        +ni*she(...,2)
        +te*ne*she(...,3)
        """
        self._iheat_source = (self.shi[...,0]
            + self.shi[...,1]*self.ti
            + self.shi[...,2]*self.ni
            + self.shi[...,3]*self.ti*self.ni)

    @solps_property2
    def reshi(self):
        """ ({GRID_DIMS}): Residuals of the ion heat equation.
        """







    # -------------------- Radiation sources/sinks ----------------------------
    @solps_property2
    def eneutrad(self):
        """ (nx,ny,natm): Power radiated due to neutrals (atoms) [W]

        Notes
        ------
        Only atoms at the moment.
        """
        self._read_signal('_eneutrad', 'eneutrad')
        self._eneutrad = (self._eneutrad.T*self.rlcl.T).T

    @property
    def eatmrad(self):
        """ (nx,ny,natm): Power radiated due to atoms [W] """
        return self.eneutrad

    @property
    def eradatm(self):
        return self.eatmrad


    @solps_property2
    def emolrad(self):
        """ (nx,ny,nmol): Power radiated due to molecules [W] """
        self._read_signal('_emolrad', 'emolrad')
        self._emolrad = (self._emolrad.T*self.rlcl.T).T

    @property
    def eradmol(self):
        return self.emolrad

    @solps_property2
    def eionrad(self):
        """ (nx,ny,nion): Power radiated due to test ions [W] """
        self._read_signal('_eionrad', 'eionrad')
        self._eionrad = (self._eionrad.T*self.rlcl.T).T

    @property
    def eradion(self):
        return self.eionrad


    @solps_property
    def prad(self):
        """ (nx, ny): Total radiated power [W] """
        self._prad = (
            + np.sum(self.rqrad, axis=self.ns_axis)
            + np.sum(self.rqbrm, axis=self.ns_axis))
        if not self.b2standalone:
            self._prad += ( 
            - np.sum(self.eatmrad, axis=self.ns_axis)
            - np.sum(self.emolrad, axis=self.ns_axis)
            - np.sum(self.eionrad, axis=self.ns_axis))

    @solps_property
    def prad_iso(self):
        """ (nx, ny): Total radiated power per isonuclear sequence [W] """
        self._prad_iso = self._sum_isonuclear(self.prada)

    @solps_property
    def prada(self):
        """ (nx, ny, natm): Total radiated power per atomic species [W] """
        self._prada = self.rqrad + self.rqbrm
        if not self.b2standalone:
            iatm = 0
            for i,z in enumerate(self.za):
                if z == 0:
                    self._prada[...,i] = - self.eatmrad[...,iatm]
                    iatm += 1

    @solps_property
    def prad_iso_mol(self):
        """ (nx, ny): Total radiated power by molecules and molecular ions [W] """
        self._prad_iso_mol = - np.sum(self.emolrad, axis=self.ns_axis) - np.sum(self.eionrad, axis=self.ns_axis)

    @solps_property
    def prad_mol(self):
        """ (nx, ny): Total radiated power by molecules [W] """
        self._prad_mol = -self.emolrad



    # -------------------- EIRENE triangular grid -----------------------------



    # -------------------- B2FRATES VARIABLES ---------------------------------
    @solps_property
    def rtzmin(self):
        self._read_signal('_rtzmin', 'rtzmin')
        self._rtzmin = self._rtzmin.astype(int)

    @solps_property
    def rtzmax(self):
        self._read_signal('_rtzmax', 'rtzmax')
        self._rtzmax = self._rtzmax.astype(int)

    @solps_property
    def rtzn(self):
        self._read_signal('_rtzn', 'rtzn')
        self._rtzn = self._rtzn.astype(int)

    @solps_property
    def rtt(self):
        self._read_signal('_rtt', 'rtt')

    @solps_property
    def rtn(self):
        self._read_signal('_rtn', 'rtn')

    @solps_property
    def rtlt(self):
        self._read_signal('_rtlt', 'rtlt')

    @solps_property
    def rtln(self):
        self._read_signal('_rtln', 'rtln')

    @solps_property
    def rtlsa(self):
        """ (nx,ny,ns): Ionisation rate coefficient """
        self._read_signal('_rtlsa', 'rtlsa')

    @solps_property
    def rtlra(self):
        """ (nx,ny,ns): Recombination rate coefficient """
        self._read_signal('_rtlra', 'rtlra')

    @solps_property
    def rtlqa(self):
        """ (nx,ny,ns): Heat loss rate coefficient """
        self._read_signal('_rtlqa', 'rtlqa')

    @solps_property
    def rtlcx(self):
        self._read_signal('_rtlcx', 'rtlcx')

    @solps_property
    def rtlrd(self):
        """ (nx,ny,ns): Line radiation rate coefficient """
        self._read_signal('_rtlrd', 'rtlrd')

    @solps_property
    def rtlbr(self):
        """ (nx,ny,ns): Bremsstrahlung radiation rate coefficient """
        self._read_signal('_rtlbr', 'rtlbr')

    @solps_property
    def rtlza(self):
        """ (nx,ny,ns): Effective charge state"""
        self._read_signal('_rtlza', 'rtlza')
        self._rtlza = self._rtlza.astype(int)

    @solps_property
    def rtlz2(self):
        """ (nx,ny,ns): Effective square state"""
        self._read_signal('_rtlz2', 'rtlz2')
        self._rtlz2 = self._rtlz2.astype(int)

    @solps_property
    def rtlpt(self):
        """ (nx,ny,ns): Cumulative ionisation potential """
        self._read_signal('_rtlpt', 'rtlpt')

    @solps_property
    def rtlpi(self):
        """ (nx,ny,ns): Effective ionisation potential """
        self._read_signal('_rtlpi', 'rtlpi')





    ## Recalculated coefficients ----------------------------------------
    def _set_recalculated_coefficients(self):
        tmp = linearize_rates(self)
        for rl in ['rlsa', 'rlra', 'rlqa', 'rlrd',
            'rlbr', 'rlza', 'rlz2', 'rlpt', 'rlpi']:
            try:
                #if rl == 'rlza' or rl == 'rlz2':
                #    tmp[rl] = tmp[rl].astype(int)
                setattr(self, '_{}'.format(rl), tmp[rl])
                
            except:
                self.log.exception("b2spel: _{} could not be set".format(rl))

        return


    @solps_property
    def rlsa(self):
        """ (nx,ny,ns): Ionisation rate coefficient """
        self._set_recalculated_coefficients()

    @solps_property
    def rlra(self):
        """ (nx,ny,ns): Recombination rate coefficient """
        self._set_recalculated_coefficients()

    @solps_property
    def rlqa(self):
        """ (nx,ny,ns): Heat loss rate coefficient """
        self._set_recalculated_coefficients()

    @solps_property
    def rlrd(self):
        """ (nx,ny,ns): Line radiation rate coefficient """
        self._set_recalculated_coefficients()

    @solps_property
    def rlbr(self):
        """ (nx,ny,ns): Bremsstrahlung radiation rate coefficient """
        self._set_recalculated_coefficients()

    @solps_property
    def rlza(self):
        """ (nx,ny,ns): Effective charge state"""
        self._set_recalculated_coefficients()

    @solps_property
    def rlz2(self):
        """ (nx,ny,ns): Effective square state"""
        self._set_recalculated_coefficients()

    @solps_property
    def rlpt(self):
        """ (nx,ny,ns): Cumulative ionisation potential """
        self._set_recalculated_coefficients()

    @solps_property
    def rlpi(self):
        """ (nx,ny,ns): Effective ionisation potential """
        self._set_recalculated_coefficients()



    #@solps_property
    #def rlcx(self):
    #    self._read_signal('_rtlcx', 'rtlcx')




    ## Recalculated rates
    def _set_recalculated_rates(self):
        #tmp = b2sqel(self)
        tmp = evaluate_rates(self)
        for rr in ['rsa', 'rra', 'rqa', 'rrd',
            'rbr', 'rza', 'rz2', 'rpt', 'rpi']:
            try:
                setattr(self, '_{}'.format(rr), tmp[rr])
            except:
                self.log.exception("b2sqel: _{} could not be set".format(rr))


    @solps_property
    def rsa(self):
        """ (nx,ny,ns): Ionisation rate. """
        self._set_recalculated_rates()

    @solps_property
    def rra(self):
        """ (nx,ny,ns): Recombination rate """
        self._set_recalculated_rates()


    @solps_property
    def rqa(self):
        """ (nx,ny,ns): Heat loss rate """
        self._set_recalculated_rates()

    @solps_property
    def rrd(self):
        """ (nx,ny,ns): Line radiation rate """
        self._set_recalculated_rates()

    @solps_property
    def rbr(self):
        """ (nx,ny,ns): Bremsstrahlung radiation rate """
        self._set_recalculated_rates()

    @solps_property
    def rza(self):
        """ (nx,ny,ns): Effective charge state"""
        self._set_recalculated_rates()

    @solps_property
    def rz2(self):
        """ (nx,ny,ns): Effective square state"""
        self._set_recalculated_rates()

    @solps_property
    def rpt(self):
        """ (nx,ny,ns): Cumulative ionisation potential """
        self._set_recalculated_rates()

    @solps_property
    def rpi(self):
        """ (nx,ny,ns): Effective ionisation potential """
        self._set_recalculated_rates()






##  B2 INPUT AND RUN.LOG ======================================================









##  FUNCTIONS =================================================================

    #ATTENTION: Does it work when var is fully inherited?
    @classmethod
    def help(cls, var):
        """
        Returns docstring of requested property
        """
        try:
            print(get(cls, var).__doc__)
        except:
            print("No docstring yet.")


    @solps_property
    def docstrings(self):
        """ Dictionary with the docstrings of all methods and properties."""
        _cls = self.__class__
        mro = _cls.mro()
        mro.reverse()
        del(mro[0]) #Delete 'object' docstrings.

        self._docstrings = {}
        for clss in mro:
            clssdict = {var: vobj.__doc__
                for var, vobj in clss.__dict__.items()}
            self._docstrings = update(self._docstrings, clssdict)


    def find(self, regex):
        """
        Match regex pattern(s) against all the docstring.

        TODO
        ----
        Needs to be recursive, i.e., to go deep into extensions, etc.
        """

        if isinstance(regex, str):
            regex = [regex]

        nomatch=True

        for var, doc in self.docstrings.items():
            if doc:
                found = True
                for reg in regex:
                    res = re.search(
                        reg, doc.replace(r'\n','').replace('   ',''),
                        re.IGNORECASE)
                    if res is None:
                        found = False

                if found:
                    nomatch=False
                    print('-'*20)
                    print(var)
                    print(doc)
                    print('-'*20)

        if nomatch:
            print("No matching docstring found.")

        return

    
    def whichclass(self, attr):
        return whichclass(self, attr)



    def read_signal(self, signal_name, signal_link, attach_to = None):
        self.log.error("'{}' does not define 'read_signal'".format(self.class_type))



    # -------------------- Operations -----------------------------------------

    # -------------------- Private methods ------------------------------------

    def _get_basic_dimensions(self):
        self.log.error(
            "'{}' does not define '_get_basic_dimensions'".format(
            self.class_type))


    def _read_signal(self, signal_name, var_name, attach_to = None):
        self.log.error(
            "'{}' does not define '_read_signal'".format(self.class_type))

    def _check_avail(self):
        self.log.error(
            "'{}' does not define '_check_avail'".format(self.class_type))


    ## ATTENTION: ADAPT TO UNSTRUCTURED TOO
    ## ATTENTION: Review logic. Make it useful for unstructured too.
    #def _sum_cs(self,var):
    def _sum_isonuclear(self, var, axis=-1):
        """Returns the sum of var over all charge states per species.
        (var [...,ns,...] ==> [...,natm,...]).
        Assume axis=-1, but if not, axis of ns must be specified."""

        species_index = np.where(np.array(var.shape)==self.ns)[0][-1]
        # WARNING: This might cause problems if one of the dimensions
        #          of the variable 'var' is of size self.ns!
        shape = list(var.shape)
        shape[species_index] = self.natm
        shape = tuple(shape)
        zn = list(OrderedDict.fromkeys(self.zn))
        sum = np.zeros(shape)
        for natm in range(self.natm):
            # How can I make this more general???
            if   species_index == 0:
                sum[      natm,...] = np.sum(var[      self.zn==zn[natm],...],
                                             species_index)
            elif species_index == 1:
                sum[    :,natm,...] = np.sum(var[    :,self.zn==zn[natm],...],
                                             species_index)
            elif species_index == 2:
                sum[  :,:,natm,...] = np.sum(var[  :,:,self.zn==zn[natm],...],
                                             species_index)
            elif species_index == 3:
                sum[:,:,:,natm,...] = np.sum(var[:,:,:,self.zn==zn[natm],...],
                                             species_index)
        return sum

    ## ATTENTION: ADAPT TO UNSTRUCTURED TOO
    ## ATTENTION: Review logic. Make it useful for unstructured too.
    #def _avg_cs(self,var):
    def _avg_isonuclear(self, var, axis=-1):
        """Returns the average of var over all charge states per species.
        (var [nx,ny,ns] ==> [nx,ny,natm])
       Assume axis=-1, but if not, axis of ns must be specified."""
        avg = np.zeros((self.nx,self.ny,self.natm))
        natm = 0
        zn = self.zn[0]
        for ns in range(self.ns):
            if zn != self.zn[ns]:
                avg[:,:,natm] = avg[:,:,natm]/(zn+1)
                zn = self.zn[ns]
                natm += 1
            avg[:,:,natm] += var[:,:,ns]
        avg[:,:,natm] = avg[:,:,natm]/(zn+1)
        return avg


    ## ATTENTION: Adapt to unstructured too, on top of reviewing and generalizing
    ## the logic
    #ATTENTION: review logic
    def _rescale_neutrals(self,var):
        """Rescales the neutrals according to the switch 
        'b2mndr_rescale_neutrals_sources' in b2mn.dat"""
        factor = 1/float(self.read_switch('b2mndr_rescale_neutrals_sources'))
        rescale = np.ones(var.shape)
        species_index = np.where(np.array(var.shape)==self.ns)[0][-1]
        # WARNING: This might cause problems if one of the dimensions
        #          of the variable 'var' is of size self.ns!

        # How can I make this more general???
        if   species_index == 0:
            rescale[      self.za==0,...] = factor
        elif species_index == 1:
            rescale[    :,self.za==0,...] = factor
        elif species_index == 2:
            rescale[  :,:,self.za==0,...] = factor
        elif species_index == 3:
            rescale[:,:,:,self.za==0,...] = factor
        return var * rescale

