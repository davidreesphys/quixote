## ATTENTION: Allow DARKMODE: black bg, white vessel as default.
"""
TODO:
    -- Implement tests (create figures/canvas and test properties)
    -- Radial profiles from fhitz_*.
    -- Poloidal profiles from fhitz_*.
"""


import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.animation as animation
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from matplotlib.backends.backend_pdf import PdfPages


from ..tools import priority, update
from ..tools import read_config
from ..tools import extend, get
from ..tools import create_log

from ..tools.plots import shifted_color_map
from ..tools.plots import get_datalim
from ..tools.plots import initialize_canvas 
from ..tools.plots import cumsum_quantiles
from ..tools.plots import contours


from ..tools import parsers



alog = create_log("PlotClass")



# ===========================================
class BasePlot(object):
    """
    Logic:
    - Receive certain user parameters.
    - Process the parameters to separate them into those related to
      canvas initialization and process, artists and saving setups.
    - Read defaults from configuration files.
    - Overwrite the defaults by the user parameters.
    - Process canvas:

      + If given a canvas: Process the canvas.

      + If not given a canvas: Initialize and process the canvas.

    - Add artists or prepare for animation.
    - Plot or save, according to parameters.
    """

    def __init__(self, mother, canvas=None, **kwargs):
        #Allow other defaults instead of os.getcwd only
        self.storage_path = priority('storage_path', kwargs, os.getcwd())
        self._mother = mother #Solpspy case.

        #For speed, call config once and store it:
        #self.config = read_config()['Plots']
        #Even faster: 
        try:
            self.config = mother.config['Plots']
        except:
            self.config = read_config()['Plots']

        #Process the given kwargs into various parameter groups
        self.params = self._process_arguments(**kwargs)

        # Associate self.canvas and self.fig with user canvas or new canvas.
        self._create_canvas(canvas)

        # Compose the plot.
        self.compose()

        # If some post treatment (show, save, animate, etc) is requested.
        if self.params['post']['show']:
            self.show(recompose=False)
        if self.params['post']['save']:
            self.save(recompose=False)



    #------------------------
    def compose(self):
        """ Helper function to create the figure. It is called by
        plot and save.
        In the future, for recompositions, try to modify as much as possible
        instead of redrawing.
        """
        self._mother.log.error(
            "Compose method not yet implemented for {}.".format(self.__class__))

    def plot(self, recompose=True):
        """Create the figure
        """
        if recompose:
            self.compose()
        plt.show()

#    def show(self):
#        #try:
#        #    self.fig.show()
#        #except:
#        #    self._mother.log.error(
#        #      "plt.show() used because a GUI backend is required for fig.show().")
#        #    plt.show()
#
#        plt.show()
#        return

    def save(self, name, recompose=True):
        """
        """
        self._mother.log.error(
            "Save method not yet implemented for {}.".format(self.__class__))

    def animate(self):
        """Use the other methods to create an animation, if possible.
        """
        self._mother.log.error(
            "Animate method not yet implemented for {}.".format(self.__class__))


    #------------------------

    def _create_canvas(self, canvas):
        """ Having it separated from __init__ allows for required adjustsments.
        """
        if canvas:
            self.canvas = canvas
            self.fig = canvas.get_figure()
        else:
            creation_kw = {}
            if self.params['canvas']['figsize'] is not None:
                creation_kw['figsize'] = self.params['canvas']['figsize']
            #if self.params['canvas']['darkmode']:
            #    with plt.style.context('dark_background'):
            #        self.fig, self.canvas =  plt.subplots(**creation_kw)
            #else:
            #    self.fig, self.canvas =  plt.subplots(**creation_kw)
            self.fig, self.canvas =  plt.subplots(**creation_kw)
        return


    def _process_canvas(self):
        """ In some plot classes, a different type of processing might
        be required.
        Any replacement of this method should modify both:
        self.fig and self.canvas
        """

        canvas = self.canvas
        try:
            fig = self.fig
        except:
            fig = canvas.get_figure()
        ckw = self.params['canvas']


        #Set titles
        if ckw['title']:
            canvas.title(ckw['title'])
        if ckw['figtitle']:
            fig.title(ckw['figtitle'])

        # Set labels
        if ckw['xlabel']:
            canvas.set_xlabel(ckw['xlabel'])
        if ckw['ylabel']:
            canvas.set_ylabel(ckw['ylabel'])


        if ckw['background_color']:
            canvas.set_facecolor(ckw['background_color'])
        #elif ckw['darkmode']:
        #    canvas.set_facecolor('black')
    
        # Set the major ticks
        if ckw['xticks'] is not None and ckw['xticks'] != 'default':
            try:
                canvas.axes.set_xticks(ckw['xticks'])
            except:
                canvas.axes.set_xticks(ckw['xticks'](self))
        elif ckw['xticks'] is None:
            canvas.axes.set_xticks([])

        if ckw['yticks'] is not None and ckw['yticks'] != 'default':
            try:
                canvas.axes.set_yticks(ckw['yticks'])
            except:
                canvas.axes.set_yticks(ckw['yticks'](self))
        elif ckw['yticks'] is None:
            canvas.axes.set_yticks([])



        # Set the minor ticks
        if ckw['xticks_m'] is not None and ckw['xticks_m'] != 'default':
            try:
                canvas.axes.set_xticks(ckw['xticks_m'], minor=True)
            except:
                canvas.axes.set_xticks(ckw['xticks_m'](self), minor=True)
        elif ckw['xticks_m'] is None:
            canvas.axes.set_xticks([], minor=True)

        if ckw['yticks_m'] is not None and ckw['yticks_m'] != 'default':
            try:
                canvas.axes.set_yticks(ckw['yticks_m'], minor=True)
            except:
                canvas.axes.set_yticks(ckw['yticks_m'](self), minor=True)
        elif ckw['yticks_m'] is None:
            canvas.axes.set_yticks([], minor=True)



        # Set the tick labels appropiately.
        if ckw['xticklabels'] is not None and ckw['xticklabels'] != 'default':
            canvas.axes.set_xticklabels(ckw['xticklabels'])
        elif ckw['xticklabels'] is None:
            canvas.axes.set_xticklabels([])

        if ckw['yticklabels'] is not None and ckw['yticklabels'] != 'default':
            canvas.axes.set_yticklabels(ckw['yticklabels'])
        elif ckw['yticklabels'] is None:
            canvas.axes.set_yticklabels([])

        
        ####
        # Set_aspect allows for easier management of [xy]lims but
        # modifies the canvas shape and the space it occupies.
        # When only xlim is set, the picture is quite weird, as it
        # In line 1888 of matplotlib/axes/_base.py are explained
        # the different meanings of equal, scaled, etc.
        # After canvas.axis('scaled') I can do xlim, ylim and the box
        # adapts more or less (still space), but I can "zoom" and the
        # box adapts nicely.
        # After canvas.set_aspect('equal') I can do that too.

        # ATTENTION: Review logic. It seems to not be good, but works for phys.
        #canvas.axis('equal') = axes.set_aspect('equal', adjustable='datalim')
        ####
        if ((ckw['xlim'] is not None or ckw['ylim'] is not None)
                and ckw['aspect'] and ckw['adapt_canvas']):
                canvas.axes.set_aspect(ckw['aspect'])
        elif ckw['aspect']:
            canvas.axis(ckw['aspect'])


        #Save data lims in case they are needed.
##ATTENTION: SET UP AGAIN
        #_datalims = get_datalim(self.canvas)

        
    
##ATTENTION: SET UP AGAIN
        # Set [xy] limits of the axis.
        if ckw['xlim'] is not None:
            canvas.set_xlim(ckw['xlim'])
#        elif ckw['use_datalim']:
#            try:
#                canvas.set_xlim(_datalims[0])
#            except:
#                self._mother.log.error(
#                    "In {}, impossing xlim from dataLim failed.".format(
#                        self.__class__.__name__))
#
        if ckw['ylim'] is not None:
            canvas.set_ylim(ckw['ylim'])
#        elif ckw['use_datalim']:
#            try:
#                canvas.set_ylim(_datalims[1])
#            except:
#                self._mother.log.error(
#                    "In {}, impossing ylim from dataLim failed.".format(
#                        self.__class__.__name__))
#
#        



        # Adjusts subplots (mostly, white spacing around axis)
        if   ckw['adjust'] and isinstance(ckw['adjust'], dict):
            fig.subplots_adjust(**ckw['adjust'])
        elif ckw['adjust'] and isinstance(ckw['adjust'], list):
            fig.subplots_adjust(*ckw['adjust'])
        elif (ckw['adjust'] and isinstance(ckw['adjust'], str) and
            ckw['adjust'].lower() == 'tight'):
            fig.tight_layout()
        elif ckw['adjust'] is True:
            fig.tight_layout()
        else:
            self._mother.log.warning(
                "In {}, 'adjust' parameter is not valid.".format(
                    self.__class__.__name__))

        self.canvas = canvas
        self.fig = fig
        return


    #------------------------
    def _get_defaults(self):
        """
        Get defaults for parameters according to plotting stack. E.g:
                            Vessel
                              v
                            Base Figure

        Maybe could accept a list of names and just update in order. E.g:
        self._get_defaults(['base figure', 'vessel'])

        """
        fconf = self.config['base figure']

        return fconf

    #------------------------
    def _process_arguments(self, **kwargs):
        """ Returns dictionary which keys are'canvas', 'plot', 'post';
        either given by user or config defaults.
        """
        params = priority(['params','parameters'], kwargs,
                {'canvas':{}, 'artists':{}, 'post':{}})

        canvas_params = priority(['canvas_params', 'cp'], kwargs, {})
        artists_params = priority(['artists_params', 'plot_params','ap'],
                kwargs, {})
        post_params = priority(['post_params', 'pp'], kwargs, {})

        use_defaults = priority(['use_def','use_defaults', 'defaults'],
                kwargs, True)


        params['canvas'] = update(params['canvas'], canvas_params)
        params['artists']= update(params['artists'],artists_params)
        params['post']   = update(params['post'],   post_params)

        if use_defaults:
            tmp = {}
            tmp['canvas']= self._canvas_parameters(**kwargs)
            tmp['artists']= self._artists_parameters(**kwargs)
            tmp['post']   = self._post_parameters(**kwargs)
            return update(tmp, params)

        else:
            return params


    #------------------------
    def _canvas_parameters(self, **kwargs):
        """ Separate the user given kwargs in different dicts for different
        purposes, as well as setting up the defaults.
        """
        defaults = self._get_defaults()
        tmp = {}

        # Figure
        tmp['figtitle'] = priority(['figtitle','fig_title', 'ftitle'],
             kwargs, defaults['figtitle'])
        tmp['figsize'] = priority(['figsize','fig_size','size_fig'],
             kwargs, defaults['figsize'])

        # Canvas
        tmp['title'] = priority(['title','canvas_title', 'ctitle'],
             kwargs, defaults['title'])
        tmp['aspect'] = priority(['aspect', 'aspect_ratio'], kwargs,
                defaults['aspect'])
        tmp['adapt_canvas'] = priority(['adapt_canvas', 'reshape_canvas'],
                kwargs, defaults['adapt_canvas'])
        tmp['adjust'] = priority(['adjust'], kwargs, defaults['adjust'])
        tmp['xlabel'] = priority('xlabel', kwargs, defaults['xlabel'])
        tmp['ylabel'] = priority('ylabel', kwargs, defaults['ylabel'])

        tmp['xticklabels'] = priority('xticklabels',
                kwargs, defaults['xticklabels'])
        tmp['yticklabels'] = priority('yticklabels',
                kwargs, defaults['xticklabels'])

        tmp['xticks'] = priority('xticks', kwargs, defaults['xticks'])
        tmp['xticks_m'] = priority('xticks_m', kwargs, defaults['xticks_m'])

        tmp['yticks'] = priority('yticks', kwargs, defaults['yticks'])
        tmp['yticks_m'] = priority('yticks_m', kwargs, defaults['yticks_m'])

        tmp['xlim'] = priority('xlim', kwargs, defaults['xlim'])
        tmp['ylim'] = priority('ylim', kwargs, defaults['ylim'])

        tmp['use_datalim'] = priority(['use_datalim','datalim'], kwargs,
                defaults['use_datalim'])

        tmp['process_canvas'] = priority(['process_canvas'],
                kwargs, defaults['process_canvas'])

        tmp['background_color']  = priority(['bc','bgc', 'background_color'],
                kwargs, defaults['background_color'])
        #tmp['darkmode'] = priority(['darkmode', 'dark'], kwargs, defaults['darkmode'])
        return tmp


    #------------------------
    def _artists_parameters(self, **kwargs):
        """ Separate the user given kwargs in different dicts for different
        purposes, as well as setting up the defaults.
        """
        self._mother.log.error(
            "Artists params not yet implemented for {}.".format(self.__class__))


    #------------------------
    def _post_parameters(self, **kwargs):
        """ Separate the user given kwargs in different dicts for different
        purposes, as well as setting up the defaults.
        """
        defaults = self._get_defaults()
        tmp = {}
        tmp['show'] = priority(['stand_alone', 'plot','show', 'print'],
                kwargs, defaults['show'])

        tmp['save'] = priority(['save'], kwargs, defaults['save'])
        return tmp



    #------------------------
    def _generate_artist(self,**kwargs):
        """
        """

# ===========================================























## ATTENTION: Or equivalent. If no self.vessel, then plot 4 lines in min, max
## of the grid plus some margin.
# ===========================================
class VesselPlot(BasePlot):
    """ Plots self.vessel

    Parameters
    ----------
    canvas: matplotlib axes. Optional.
        Object in which the vessel will be plotted.

    figsize: tuple, optional.
        If no canvas have been given, then this function will create
        figure of figsize and attach an axes.
        Default: (8,6)
        Alias: fig_size , size_fig.

    xlim: tuple or list, optional.
        Impose specific x limits on the figure. Both xlim and ylim
        must be compatible in the sense that either the aspect ratio
        is set to equal and thus either one of the limits or the figure
        size must adapt, as one of them must be not independent.
        Default: None

    ylim: tuple or list, optional.
        Impose specific y limits on the figure. Both xlim and ylim
        must be compatible in the sense that either the aspect ratio
        is set to equal and thus either one of the limits or the figure
        size must adapt, as one of them must be not independent.
        Default: None

    xticklabels: list, optional.
        Desired value to place the x tick labels.
        If 'default', then no further actions are taken.
        If None, then the ticks are removed.
        Default: 'default'

    yticklabels: list, optional.
        Desired value to place the y tick labels.
        If 'default', then no further actions are taken.
        If None, then the ticks are removed.
        Default: 'default'

    color: str, list(str) or dict(isurf:float), optional.
        If str, it must be a valid color argument of matplotlib, e.g. 'k'.
        Then that color is assigned to all plotted surfaces.
        If list(str), it must be of lenght equal to number of surfaces.
        It specifies the color of every surface.
        If dict, then keys must be the index of the surfaces.
        Default: 'k'
        Alias: colors, color_surfaces, colors_surfaces

    linewidth: float, list(float) or dict(isurf:float), optional.
        If float, then that line width is assigned to all plotted surfaces.
        If list(float), it must be of lenght equal to number of surfaces.
        It specifies the line width of every surface.
        If dict, then keys must be the index of the surfaces.
        Default: 1
        Alias: lw

    linestyle: float, list(float) or dict, optional.
        If float, then that line style is assigned to all plotted surfaces.
        If list(float), it must be of lenght equal to number of surfaces.
        It specifies the line width of every surface.
        If dict, then keys must be the index of the surfaces.
        Default: '-'
        Alias: ls

    stand_alone: bool, optional.
        If True, then use plt.show()
        Alias: plot, show, print.




    Returns
    -------

    fig: matplotlib figure
        If canvas was given, then it returns the fig of that canvas.
        Else, it returns the new fig created for the new canvas.

    canvas: matplotlib axes
        If canvas was given, the same canvas.
        Else, it returns the new canvas.




    Notes
    -----
    Add the option of line2D to pass it so it can be use elsewhere
    object.append(new lines in the for loop) then can be add with
    axis.add_line(line)


    For the future, separate clearly the process and kwargs related to the
    creation and process of the canvas from the creation and process
    of the vessel.
    """

    def __init__(self,mother,**kwargs):
        super().__init__(mother, **kwargs)


#    #------------------------
#    def compose(self):
#        ## Reserve for self.plot or self.animate
#        ##Create and/or process to get the final canvas and figure.
#
#        vessel = self._generate_artist(self.params['artists'])
#
#        self.fig, self.canvas = self._initialize_canvas(
#                canvas=self.canvas, kw=self.params['canvas'])
#
#        for surf in vessel:
#            self.canvas.add_line(surf)
#
#
#        ### This apparently work
#        ##self.canvas.relim()
#        ##self.canvas.autoscale_view(True,True,True)
#
#        if self._adapt_canvas:
#            #Apply limits
#            self.canvas.set_aspect('equal', anchor='C')
#            self.canvas.set_xlim(1.0,2.0)
#            self.canvas.set_ylim([-1.2, -0.5])
#        else:
#            self.canvas.axis('equal')
#            self.canvas.set_xlim([1.0,  2.0])
#            #self.canvas.set_ylim([-1.2, -0.5])
#
#
#        return


    def compose(self):
        """ At this point, self.canvas and self.fig should exist
        already. They both are unprocessed yet.
        """

        vessel = self._generate_artist(self.params['artists'])
        for surf in vessel:
            self.canvas.add_line(surf)

        if self.params['canvas']['process_canvas']:
            self._process_canvas()
        return




    #------------------------
    def _get_defaults(self):
        """ Plot stack:
        Vessel
          v
        Base Figure
        """
        fconf = self.config['base figure']
        vconf = self.config['vessel']
        return update(fconf, vconf)



    ##------------------------

    ### ATTENTION: 
    ### Most likely unnecessary now since  adapt_canvas is set at baseplot level.
    #def _canvas_parameters(self, **kwargs):
    #    defaults = self._get_defaults()
    #    tmp = super()._canvas_parameters(**kwargs)

    #    tmp['adapt_canvas'] = priority(['adapt_canvas', 'reshape_canvas'],
    #            kwargs, defaults['adapt_canvas'])

    #    return tmp



    def _artists_parameters(self, **kwargs):
        """ Separate the user given kwargs in different dicts for different
        purposes, as well as setting up the defaults.

        For color, lw and ls:
        If it is a list, must be complete
        Elif a dict, use plain default color/ls/lw and update.
        Elif a strg/num, create a dict with that for each surface.
        """
        defaults = self._get_defaults() ##ATTENTION: Unnecessary call???

        tmp = {}
        color = priority(['vessel_color','surface_color','color','colors',
            'color_surfaces', 'colors_surfaces'], kwargs, defaults['color'])
        lw = priority(['vessel_lw', 'vessel_linewidth','linewidth','lw'],
                kwargs, defaults['lw'])
        ls = priority(['vessel_ls', 'vessel_linestyle','linestyle','ls'],
                kwargs, defaults['ls'])

        #darkmode = priority(['darkmode', 'dark'], kwargs, defaults['darkmode']) #ATTENTION??

        #Get number of vessel surfaces.
        nsurf = self._mother.vessel.shape[1]

        ## Color
        ##ATTENTION??
        #if darkmode and color ==  defaults['color']:
        #    tmp['color'] = 'white'

        if isinstance(color, list):
            tmp['color'] = {i: color[i] for i in range(nsurf)}
        elif isinstance(color, dict):
            vessel_color = {i: defaults['color'] for i in range(nsurf)}
            tmp['color'] = update(vessel_color, color)
        elif isinstance(color,str):
            tmp['color'] = {i: color for i in range(nsurf)}
        else:
            self._mother.log.error(
              "In VesselPlot: 'color' set to an incompatible value.")

        ## Line width
        if   isinstance(lw, list):
            tmp['lw'] = {i: lw[i] for i in range(nsurf)}
        elif isinstance(lw, dict):
            vessel_lw = {i: defaults['lw'] for i in range(nsurf)}
            tmp['lw'] = update(vessel_lw, lw)
        elif isinstance(lw,int) or isinstance(lw,float):
            tmp['lw'] = {i: lw for i in range(nsurf)}
        else:
            self._mother.log.error(
              "In VesselPlot: 'lw' set to an incompatible value.")

        ## Line style
        if   isinstance(ls, list):
            tmp['ls'] = {i: ls[i] for i in range(nsurf)}
        elif isinstance(ls, dict):
            vessel_ls = {i: defaults['ls'] for i in range(nsurf)}
            tmp['ls'] = update(vessel_ls, ls)
        elif isinstance(ls,str):
            tmp['ls'] = {i: ls for i in range(nsurf)}
        else:
            self._mother.log.error(
              "In VesselPlot: 'ls' set to an incompatible value.")

        return tmp




    #------------------------
    def _generate_artist(self, akw):
        """
        """
        nsurf = self._mother.vessel.shape[1]
        tmp = []
        for i in range(nsurf):
            R = np.array([self._mother.vessel[0,i],self._mother.vessel[2,i]])
            z = np.array([self._mother.vessel[1,i],self._mother.vessel[3,i]])
            #tmp.append(mpl.lines.Line2D(R,z, color=akw['color'][i],
            #    linewidth=akw['lw'][i], ls=akw['ls'][i]))
            tmp.append(mpl.lines.Line2D(R,z, color=akw['color'][i],
                lw=akw['lw'][i], ls=akw['ls'][i]))
        return tmp

# ===========================================








































##ATTENTION: New idea 2022 below
## For triangs, I would have to see if there is a way to labl all the faces and do the same trick.

# ===========================================
class GridDataPlot(BasePlot):
    """ Class for everything related to plot the grid and grid data.
    2021: The computational script done as a function using heat map seems to work better than this one.
    Copy whatever possible to improve the design of this one.
    Put ticks on each cell and restrict to xlim=[-0.5, nx],
    y=[-0.5, ny]. Maybe change behaviour? When xlim, ylim are given
    then assume that those are the indices, thus make the -.5 calcultion
    automatically.
    Also, start in 0,0, like the numpy array.


    New idea! 2022: Area of highlight by getting all the cells to be highlighted and then
    checking all the face numbers, and then going np.unique, and plotting lines on the rest.
    basically, that is how you know if a sufrfaces is interior or exterior to the group as a whole.
    """
    #def __init__(self,mother,var, **kwargs):
    def __init__(self,mother,var =None, dims=None, **kwargs):
        if var is None:
            self.var = 'grid'
        elif isinstance(var, str) and var != 'grid':
            #xdims = priority(['extra_dims','xdims','dims','dim'], kwargs, [])
            #dims = extend(...,xdims)
            dim =  extend(...,dims)
            self.var = get(mother, var)[tuple(dim)]
        else:
            self.var = var
        super().__init__(mother, **kwargs)






    #------------------------
    def _get_defaults(self):
        """ Plot stack:
        Phys or Comp
          v
        Gdata
          v
        Vessel
          v
        Base Figure
        """
        fconf = self.config['base figure']
        vconf = self.config['vessel']
        gconf = self.config['gdata']
        econf = self.config['gdata phys']
        return update(update(update(fconf, vconf), gconf),econf)


    #------------------------




    #--------------------------------------------------------------------------
    #def _vessel_parameters(self, ckw):
    #    """ To be set from either vessel_kw dict or vessel_*
    #    or from the standard kwargs and defaults as much as possible.
    #    E.g:
    #    vessel_color should be given back as vessel_kw['color'] to match
    #    what vesselplot is expecting.
    #    """

    #------------------------
    def _canvas_parameters(self, **kwargs):
        defaults = self._get_defaults()
        tmp = super()._canvas_parameters(**kwargs)

        vessel_color = priority(['vessel_color','vcolor', 'vc'],
                kwargs, defaults, None)
        vessel_lw = priority(['vessel_lw','vessel_linewidth', 'vlw'],
                kwargs, defaults, None)
        vessel_ls = priority(['vessel_ls','vessel_linestyle', 'vls'],
                kwargs, defaults, None)

        if vessel_color:
            tmp['vessel_color'] = vessel_color
        if vessel_lw:
            tmp['vessel_lw'] = vessel_lw
        if vessel_ls:
            tmp['vessel_ls'] = vessel_ls

        return tmp


    #------------------------
    def _artists_parameters(self, **kwargs):
        """
        """
        defaults = self._get_defaults()
#        nx = self._mother.nx
#        ny = self._mother.ny
#        nci = self._mother.nci

        tmp = {}


        tmp['mask'] = priority('mask', kwargs, defaults['mask'])
        tmp['mask_color'] = priority(['mask_color', 'cmask', 'color_mask'],
                kwargs, defaults['mask_color'])

        tmp['norm'] = priority('norm', kwargs, defaults['norm'])
        tmp['cmap'] = priority(['cmap', 'color_map'], kwargs, defaults['cmap'])
        tmp['cmap_center'] = priority(['cmap_center', 'center'],
                kwargs, defaults['cmap_center'])
        tmp['cmap_shift'] = priority(['cmap_shift', 'shift'],
                kwargs, defaults['cmap_shift'])
        tmp['clim'] = priority(['clim', 'clims', 'color_limits', 'limits_color'],
                kwargs, defaults['clim'])
        tmp['cbar'] = priority(['cbar', 'color_bar', 'colorbar'],
                kwargs, defaults['cbar'])
        tmp['clabel'] = priority(['clabel','colorbar_label','color_bar_label'],
                kwargs, defaults['clabel'])
        tmp['extend_cbar'] = priority(['extend_cbar'],
                kwargs, defaults['extend_cbar'])

        tmp['grid'] = self._mother.grid

        tmp['use_grid'] = priority(['use_grid','grid_use','plot_grid'],
                kwargs, defaults['use_grid'])

        # Is lw in conflict with vessel_lw?? No because Vessel only
        #reaches to vessel level config, thus is never replaced.
        #but then vessel lw is never changed in GridDataPlot.

        ## ATTENTION: Why not just rename stuff correctly?
        #vessel_lw, grid_lw, etc
        tmp['lw'] = priority(['grid_lw', 'lw'], kwargs, defaults['lw'])

        # Aren't both the same??
        if isinstance(self.var, str) and self.var == 'grid':
            tmp['edgecolor'] = priority(
                    ['gridcolor', 'grid_color', 'gc',
                     'edgecolor', 'edge_color', 'ec'],
                kwargs, defaults['fallback_ec'])
        elif tmp['use_grid']:
            tmp['edgecolor'] = priority(
                    ['gridcolor', 'grid_color', 'gc',
                     'edgecolor', 'edge_color', 'ec'],
                kwargs, defaults['fallback_ec'])
        else:
            tmp['edgecolor'] = priority(
                    ['gridcolor', 'grid_color', 'gc',
                     'edgecolor', 'edge_color', 'ec'],
                kwargs, defaults['edgecolor'])
            tmp['lw'] = 0.0 # If use_grid false, then 0.0 to avoid weird stuff.

        #When var = 'grid' options:
        tmp['facecolor'] = priority(['facecolor','fc'], kwargs,
                defaults['facecolor'])
        tmp['face_alpha'] = priority(
            ['face_alpha', 'facealpha', 'f_alpha', 'fa'], kwargs,
            defaults['face_alpha'])

        ## Unnecessary for now
        #tmp['grid_face_color'] = mpl.colors.colorConverter.to_rgba(
        #        tmp['facecolor'], alpha=['face_alpha'])


        ##ATTENTION: Add also posibility of separatrix line 
        ## and contour of the grid (not for each cell, just the ouside,
        ## the core and pfr
        ## Maybe create many plotting method so that people can for example only plot
        ## the contour in an already created axes, etc.


        tmp['quantiles'] = priority(['quantiles','quantile'], kwargs, None)
        tmp['quantiles_color'] = priority(
            ['quantiles_colors', 'quantile_color', 'qc', 'qcs'], kwargs, 'r')
        tmp['quantiles_ls'] = priority(
            ['quantiles_lss', 'quantile_ls', 'qls', 'qlss'], kwargs, '-')
        tmp['quantiles_lw'] = priority(
            ['quantiles_lws', 'quantile_lw', 'qlw', 'qlws'], kwargs, 1.0)



        tmp['levels'] = priority(['levels','level', 'lvs', 'lv'], kwargs, [])
        tmp['ofmax'] = priority(['ofmax', 'of_max', 'maxlvl', 'maxlvls'], kwargs, [])
        tmp['ofmin'] = priority(['ofmin', 'of_min', 'minlvl', 'minlvls'], kwargs, [])

        tmp['contours_color'] = priority(['contours_colors', 'ccs'], kwargs, 'r')
        tmp['contours_ls'] = priority(['contours_lss', 'contour_ls', 'clss'], kwargs, '-')
        tmp['contours_lw'] = priority(['contours_lws', 'contour_lw', 'clws'], kwargs, 1.0)


        return tmp







        ### ADAPT ix AND iy FROM self.region()
        ##If mask only delimits the poloidal region, expand to grid
        #if (mask.ndim == 1) and (mask.shape == nx).all():
        #    bad = (np.array([[mask[i]]*self._mother.ny
        #           for i in range(self._mother.nx)]) == False)
        #elif (mask.ndim == 2) and 
        #(mask.shape == ( self._mother.nx,self._mother.ny)):
        #    bad = (mask == False)




    def _generate_artist(self, var, akw):
        """ Returns a patch collection with the correct colors and setup.
        It is suitable for animation, since each frame it can be passed
        var[i,:,:]. It would mean that limits get reapplied too.

        Parameters
        ----------
        var: numpy array (nx,ny).
            Must be a grid data array. If the desire quantity has different
            dimensions or different number of dimensions, that will be treated
            somewhere else.

        akw: dict.
            Contains the artists parameters.


        Returns
        ------
        p: PatchCollection
            Ready to be attached to the canvas. It has the corresponding
            shape, values, mask, cmap type, clims and value scale.
        """

        if isinstance(var, str) and  var == 'grid':
            grid_type = self._mother.grid_type
            if grid_type == 'structured':
                nx = self._mother.nx
                ny = self._mother.ny
            elif grid_type == 'unstructured':
                nci = self._mother.nci
        elif len(var.shape) == 2:
            nx = self._mother.nx
            ny = self._mother.ny
            grid_type = 'structured'
        elif len(var.shape) == 1:
            nci = self._mother.nci
            grid_type = 'unstructured'
        else:
            self._mother.log.exception(
            "Number of dimensions must be either 2, (nx, ny), or 1, ncv/nci")
            raise Exception


        #Initialize patch collection with lists of polygons and values.
        gres = []
        vgres = []
        if grid_type=='structured':
            for i in range(nx):
                for k in range(ny):
                    if isinstance(var, str) and var == 'grid':
                        if akw['mask'] is not None:
                            if akw['mask'][i,k]:
                                alpha = akw['face_alpha']
                            else:
                                alpha = 0.0
                        else:
                            alpha = akw['face_alpha']
                        gres.append(Polygon(akw['grid'][i,k],
                            color=akw['facecolor'],
                            alpha = alpha))
                    else:
                        gres.append(Polygon(akw['grid'][i,k]))
                        vgres.append(var[i,k])

        elif grid_type =='unstructured':
            for i in range(nci):
                if isinstance(var, str) and var == 'grid':
                    if akw['mask'] is not None:
                        if akw['mask'][i]:
                            alpha = akw['face_alpha']
                        else:
                            alpha = 0.0
                    else:
                        alpha = akw['face_alpha']
                    gres.append(Polygon(akw['grid'][i],
                        color=akw['facecolor'],
                        alpha = alpha))
                else:
                    gres.append(Polygon(akw['grid'][i]))
                    vgres.append(var[i])



        if isinstance(var, str) and var == 'grid':
            p = PatchCollection(gres, match_original=True)
        else:
            vgres = np.array(vgres)
            p = PatchCollection(gres)

        # Convert str cmap into real cmap.
        #if type(akw['cmap']) == str:
        ## Now allow list of colors and create map out of the list
        if isinstance(akw['cmap'], str):
            try:
                try:
                    akw['cmap'] = plt.cm.__dict__[akw['cmap']].copy()
                except:
                    akw['cmap'] = plt.cm.hot.copy()
            except:
                try:
                    akw['cmap'] = plt.cm.__dict__[akw['cmap']]
                except:
                    akw['cmap'] = plt.cm.hot
        elif isinstance(akw['cmap'], list):
            try:
                akw['cmap'] = colors.LinearSegmentedColormap.from_list('mycmap', akw['cmap'])
            except:
                try:
                    akw['cmap'] = plt.cm.hot
                except:
                    akw['cmap'] = plt.cm.hot.copy()



        #Apply mask, if given. Limits recalculated for masked array.
        if akw['mask'] is not None and not isinstance(var, str):
            if grid_type=='structured':
                bad = (akw['mask'] == False).reshape(nx*ny) # == not(mask)
            elif grid_type=='unstructured':
                bad = ~akw['mask'][:nci]
            akw['cmap'].set_bad(color=akw['mask_color'])
            vgres = np.ma.masked_where(bad,vgres)
            p.set_clim((np.min(vgres),np.max(vgres))) #under/over needed?


        #Applye value limits, if given. They must be compatible with clim.
        if akw['clim'] is not None and not isinstance(var, str):
            p.set_clim(akw['clim'])
            try:
                aux_cmap = plt.cm.get_cmap(akw['cmap']).copy()
            except:
                aux_cmap = plt.cm.get_cmap(akw['cmap'])
            akw['cmap'].set_under(aux_cmap(0.0))
            akw['cmap'].set_over(aux_cmap(1.0))




        #ATTENTION: Does not work with clims yet
        #Limits can probably be added with start and stop key arguments
        # of shifted_color_map.

        #ATTENTION: Probably won't work with log scale

        #cmap_center gives the value of center of the color map, e.g., 0.0)
        # Important for divergent cmaps specially.
        if akw['cmap_center'] is not None and not isinstance(var, str):
            mx = np.max(vgres) if akw['clim'] is None else akw['clim'][1]
            mn = np.min(vgres) if akw['clim'] is None else akw['clim'][0]
            a = mx-mn
            b = mn
            center_fraction = (akw['cmap_center']-b)/a
            akw['cmap'] = shifted_color_map(akw['cmap'], name=akw['cmap'].name,
                    midpoint=center_fraction)
            #cmap = shifted_color_map(akw['cmap'], name=akw['cmap'].name,
            #        midpoint=center_fraction, start=mn, stop=mx)



        #Linear is the default, but if log, clim has to be set again inside
        # yscale('symlog') is symmetrical logarithm
        # Could be changed to apply here the limits directly also for linear
        if akw['norm'] == 'log' and not isinstance(var, str):
          if akw['clim'] is not None:
            p.set_norm(colors.LogNorm(
                vmin=akw['clim'][0], vmax=akw['clim'][1]))
          else:
            p.set_norm(colors.LogNorm())

        ## Should clim be given in absolute values? Even allow for 4 values?
        ## ATTENTION 2024: It requires linthresh!!
        elif akw['norm'] == 'symlog' and not isinstance(var, str):
          if akw['clim'] is not None:
            p.set_norm(colors.SymLogNorm(linthresh=akw['clim'][0],
                vmin=-akw['clim'][1], vmax=akw['clim'][1]))
          else:
            p.set_norm(colors.SymLogNorm())



        #When var == 'grid', always display grid.
        if isinstance(var, str) and var == 'grid':
            p.set(linewidth=akw['lw'])
        #Display grid? If so, use lw.
        else:
            if akw['use_grid']:
                p.set(array=vgres, cmap =akw['cmap'], linewidth=akw['lw'])
            else:
                p.set(array=vgres, cmap =akw['cmap'], linewidth=0.75)

        # Equivalent to "set_gridcolor"
        p.set_edgecolor(akw['edgecolor'])

        return p



    def compose(self):
        p = self._generate_artist(self.var, self.params['artists'])

        # tmp here is needed because canvas and fig are not returned.
        tmp = VesselPlot(self._mother, canvas=self.canvas,
                    **self.params['canvas'])

        self.canvas = tmp.canvas
        self.fig = tmp.fig

        self.canvas.add_collection(p)

        akw = self.params['artists']

        # Note 2021:
        # There is another way to work with colorbars:
        # from mpl_toolkits.axes_grid1 import make_axes_locatable
        # divider = make_axes_locatable(canvas)
        # cax = divider.append_axes("right", size="5%", pad=0.05)
        # plt.colorbar(im, cax=cax)
        if akw['cbar'] and not isinstance(self.var, str):
            if akw['clim'] is not None and akw['clabel']:
                self.fig.colorbar(p,ax=self.canvas,extend='both',
                        label=akw['clabel'])
            elif akw['clim'] is not None:
                self.fig.colorbar(p, ax=self.canvas, extend='both')
            elif akw['clabel']:
                self.fig.colorbar(p, ax=self.canvas, label=akw['clabel'])
            else:
                self.fig.colorbar(p, ax=self.canvas)

        if akw['quantiles'] is not None:
            cumsum_quantiles(self.canvas, self._mother, self.var,
                quantiles=akw['quantiles'], colors=akw['quantiles_color'],
                lss=akw['quantiles_ls'], lws=akw['quantiles_lw'])

        if akw['levels'] != [] or akw['ofmax'] != [] or akw['ofmin'] != []: 
            contours(self.canvas, self._mother, self.var,
                levels=akw['levels'], ofmax=akw['ofmax'], ofmin=akw['ofmin'],
                colors=akw['contours_color'],
                lss=akw['contours_ls'], lws=akw['contours_lw'])
        return


#    ##ATTENTION: 2022. this needs to do the whole process again.
#    ## Self compose should do everything in init, including
#    ## recreating exhausted canvases and figures.
#    def __call__(self, canvas=None):
#        """ This is not working.
#        """
#        if canvas:
#            self.canvas = canvas
#            self.fig = canvas.get_figure()
#        self.compose()
#        #Do the params['post'] too??s
#




    def __call__(self, canvas=None):
        """ This is not working.
        """
        self._create_canvas(canvas)
        self.compose()
        #Do the params['post'] too??s












# ===========================================

#def HighlightPlot(mother, mask, *args, **kwargs):
#    hc = priority(['hc', 'highlighcolor', 'highlight_color',
#        'hlc'], kwargs, mother.config['Plots']['highlight']
#    if mother.grid_type=='unstructured':
#        var = np.ones(mother.nci)
#    elif mother.grid_type=='unstructured':
#        var = np.ones((mother.nx, mother.ny))
#    return GridDataPlot(mother, var, *args, mask=mask, **kwargs)



















##ATTENTION: Improve?? Maybe not everything in generate artists is necessary??
##ATTENTION: NOT ADAPTED FOR UNSTRUCTURED/STRUCTURED??
# ===========================================
class TriangDataPlot(GridDataPlot):
    """ Plots of the triangular grid of EIRENE
    """
    def _generate_artist(self, var, akw):
        """ Returns a patch collection with the correct colors and setup.
        It is suitable for animation, since each frame it can be passed
        var[i,:,:]. It would mean that limits get reapplied too.

        Parameters
        ----------
        var: numpy array (nx,ny).
            Must be a grid data array. If the desire quantity has different
            dimensions or different number of dimensions, that will be treated
            somewhere else.

        akw: dict.
            Contains the artists parameters.


        Returns
        ------
        p: PatchCollection
            Ready to be attached to the canvas. It has the corresponding
            shape, values, mask, cmap type, clims and value scale.
        """
        akw['grid'] = self._mother.triang

        #ntr = self._mother.ntriang
        #But why?? Apparently triangs except for SOL cells.
        ## ATTENTION: Only ncvtot??? What about structured???
        ntr = self._mother.ncvtot


        #Initialize patch collection with lists of polygons and values.
        gres = []
        vgres = []
        for i in range(ntr):
            if isinstance(var, str) and var == 'grid':
                gres.append(Polygon(akw['grid'][i],
                    color=akw['facecolor'],
                    alpha = akw['face_alpha']))

            else:
                gres.append(Polygon(akw['grid'][i]))
                vgres.append(var[i])

        if isinstance(var, str) and var == 'grid':
            p = PatchCollection(gres, match_original=True)
        else:
            vgres = np.array(vgres)
            p = PatchCollection(gres)




        # Convert str cmap into real cmap.
        if type(akw['cmap']) == str:
            try:
                try:
                    akw['cmap'] = plt.cm.__dict__[akw['cmap']].copy()
                except:
                    akw['cmap'] = plt.cm.hot.copy()
            except:
                try:
                    akw['cmap'] = plt.cm.__dict__[akw['cmap']]
                except:
                    akw['cmap'] = plt.cm.hot

        #Apply mask, if given. Limits recalculated for masked array.
        if akw['mask'] is not None and not isinstance(var, str):
            bad = (akw['mask'] == False).reshape(ntr) # == not(mask)
            akw['cmap'].set_bad(color=akw['mask_color'])
            vgres = np.ma.masked_where(bad,vgres)
            p.set_clim((np.min(vgres),np.max(vgres))) #under/over needed?


        #Applye value limits, if given. They must be compatible with clim.
        if akw['clim'] is not None and not isinstance(var, str):
            p.set_clim(akw['clim'])
            try:
                aux_cmap = plt.cm.get_cmap(akw['cmap'])
            except:
                aux_cmap = plt.cm.get_cmap(akw['cmap'])
            akw['cmap'].set_under(aux_cmap(0.0))
            akw['cmap'].set_over(aux_cmap(1.0))




        #ATTENTION: Does not work with clims yet
        #Limits can probably be added with start and stop key arguments
        # of shifted_color_map.

        #ATTENTION: Probably won't work with log scale

        #cmap_center gives the value of center of the color map, e.g., 0.0)
        # Important for divergent cmaps specially.
        if akw['cmap_center'] is not None and not isinstance(var, str):
            mx = np.max(vgres) if akw['clim'] is None else akw['clim'][1]
            mn = np.min(vgres) if akw['clim'] is None else akw['clim'][0]
            a = mx-mn
            b = mn
            center_fraction = (akw['cmap_center']-b)/a
            akw['cmap'] = shifted_color_map(akw['cmap'], name=akw['cmap'].name,
                    midpoint=center_fraction)
            #cmap = shifted_color_map(akw['cmap'], name=akw['cmap'].name,
            #        midpoint=center_fraction, start=mn, stop=mx)



        #Linear is the default, but if log, clim has to be set again inside
        # yscale('symlog') is symmetrical logarithm
        # Could be changed to apply here the limits directly also for linear
        if akw['norm'] == 'log' and not isinstance(var, str):
          if akw['clim'] is not None:
            p.set_norm(colors.LogNorm(
                vmin=akw['clim'][0], vmax=akw['clim'][1]))
          else:
            p.set_norm(colors.LogNorm())

        elif akw['norm'] == 'symlog' and not isinstance(var, str):
          if akw['clim'] is not None:
            p.set_norm(colors.SymLogNorm(
                vmin=akw['clim'][0], vmax=akw['clim'][1]))
          else:
            p.set_norm(colors.SymLogNorm())


        #Display grid? If so, use lw.
        ## ATTENTION: Logic has changed for use_grid
        #if akw['use_grid']:
        #    p.set(array=vgres, cmap =akw['cmap'], linewidth=akw['lw'])
        #else:
        #    p.set(array=vgres, cmap =akw['cmap'], linewidth=0.75)

        if isinstance(var, str) and var == 'grid':
            p.set(linewidth=akw['lw'])
        else:
            #Why logic different than in GridDataPlot?? 
            p.set(array=vgres, cmap =akw['cmap'], linewidth=akw['lw'])


        # Equivalent to "set_gridcolor"
        #ATTENTION 2021: Logic has changed in get parameters when use_grid
        p.set_edgecolor(akw['edgecolor'])

        return p









##ATTENTION: I had to modify it as of Nov 2023 because it did not work at all without .T
## and now it plot weirds. WHY?
def CarrePlot(path='.', canvas=None, structure_name='structure.dat',
        rzpsi_name='rzpsi.dat', levels=20, plot_psi='contour', legend=True):

    struct_path = os.path.join(path, structure_name)
    rzpsi_path = os.path.join(path, rzpsi_name)


    if canvas is None:
        fig, canvas = plt.subplots(figsize=[5.5,8])
        canvas.axes.set_aspect(('equal'))
    
    rzpsi = parsers.rzpsi(rzpsi_path)

    ##ATTENTION: Depending on which rzpsi file,
    ## it seems that one need either psi or psi.T
    if plot_psi == 'contour':
        canvas.contour(rzpsi['r'], rzpsi['z'], rzpsi['psi'], levels=levels)
        #canvas.contour(rzpsi['r'], rzpsi['z'], rzpsi['psi'].T, levels=levels)
        #canvas.contour(rzpsi['z'], rzpsi['r'], rzpsi['psi'], levels=levels)
    elif plot_psi == 'contourf':
        canvas.contourf(rzpsi['r'], rzpsi['z'], rzpsi['psi'], levels=levels)
        #canvas.contourf(rzpsi['r'], rzpsi['z'], rzpsi['psi'].T, levels=levels)
    
    structs = parsers.structure(struct_path)
    for i, st in enumerate(structs):
        canvas.plot(st[:,0], st[:,1], label=i+1)
    
    if legend:
        canvas.legend()
    plt.tight_layout()
    return canvas










###ATTENTION REplaced by errorplot in tools plots???
#### ATTENTION: CORRECT ALL OF THIS.
#### Wait, first or last dimension must be 3???
#def Error2DLine(x, obj, **kwargs):
#    """
#    First dimension must be 3.
#    Wait first or last????
#    """
#    canvas = priority(['canvas', 'ax'], kwargs, None)
#    fargs = priority('args', kwargs, [])
#    fkwargs = priority('kwargs', kwargs, [])
#    use_nan = priority('use_nan', kwargs, [])
#    #ptype = priority(['plot','ptype'], kwargs, 'errorbar')
#    ptype = priority(['plot','ptype'], kwargs, 'area')
#
#    pretty=  {}
#    prettyline=  {}
#    prettyerror=  {}
#    prettyarea={}
#    color = priority(['color','colour','c'], kwargs, None)
#    if color is not None:
#        pretty['color'] = color
#    prettyerror['ecolor'] = priority(['ecolor','ecolour','ec'], kwargs, None)
#    #prettyline['lw'] = priority(['lw','linewidth'], kwargs, 1.0)
#    prettyline['lw'] = priority(['lw','linewidth'],
#            kwargs, plt.rcParams['lines.linewidth'])
#    prettyerror['elinewidth'] = priority(['elw','elinewidth'], kwargs, None)
#    prettyline['ls'] = priority(['ls','linestyle'], kwargs, '-')
#    prettyerror['capsize'] = priority('capsize', kwargs, 0.0)
#    prettyerror['capthick'] = priority('capthick', kwargs, 0.0)
#
#    prettyline['marker'] = priority(['marker','mk'], kwargs, '.')
#    ## Default to current devault of lines, not to these numbers, no??
#    #prettyline['ms'] = priority(['ms','markersize'], kwargs, 0.0)
#
#    pretty['alpha'] = priority(['alpha'], kwargs, 1.0)
#    #prettyarea['alpha'] = priority(['alpha_area'], kwargs, 1.0)
#    prettyarea['alpha'] = priority(['alpha_area'], kwargs, 0.3)
#
#
#    if canvas is None:
#        fig, canvas = initialize_canvas(**kwargs)
#
#    if obj.ndim != 2:
#        alog.exception(
#            "Obj ndim must be 2, and either one must be of size 3.")
#    if obj.shape[0] == 3:
#        y = obj.T
#    else:
#        y = obj
#
#    ## is this correct??? Also not correct for larger arrays
#    #if obj.ndim == 2 and (y[0,0]<y[0,1] or y[0,0]>y[0,2]):
#    #    y = y.T ## For the case of 3 by 3.
#
#
#
#    if ptype=='fill' or ptype=='area':
#        artist = []
#        artist.append(canvas.fill_between(x, y[:,1], y[:,2],
#            **update(pretty, prettyarea)))
#        artist.append(canvas.plot(x, y[:,0], **update(pretty, prettyline)))
#
#    else:
#        ##Does NOT work with NaN correctly
#        ##ATTENTION: allow asym bars???
#        prettify= update(pretty, update(prettyline, prettyerror))
#        artist =  canvas.errorbar(x, y[:,0],
#        ##ATTENTION: allow asym bars???
#            yerr=(np.abs(y[:,1:].T-y[:,0])), **prettify)
#
#    return canvas, artist


