## Common modules
import os
import numpy as np
import warnings

from collections import namedtuple


## Quixote modules
from ..tools import module_path, solps_property, priority
from ..tools import update, purge, read_config, get
from ..tools import create_log
from ..tools import solps_property2
from ..tools import vessel_square_structured, vessel_square_unstructured

from ..tools import parsers

alog = create_log('quixote')




class RundirData(object):
    data_type = 'rundir'

    def __init__(self, ident=None, *args, **kwargs):
        alog.debug("> Entering Rundir __init__")
        if (ident is None) or (ident =='.'):
            self.rundir = os.getcwd()
        else:
            if os.path.isdir(ident) is True:
                self.rundir = ident
            else:
                alog.exception("'{0}' is not a directory".format(ident))
                raise Exception
        name = priority('name', kwargs, None)
        if name is None:
            self.name = os.path.basename(os.path.normpath(self.rundir))
        else:
            self.name = name
        self.ident = self.name # Alias to ensure compatibility with older code

        try:
            with open(os.path.join(self.rundir,'shotnumber.history')) as f:
                self.shot = int(f.readlines()[-1])
        except:
            self.shot = None

        ## Use try, except, in case that there is no other super?
        super().__init__(*args, **kwargs)
        alog.debug("< Leaving  Rundir __init__")

##  EXTENSIONS ================================================================

##  IDENT PROPERTIES ==========================================================


    @solps_property
    def b2standalone(self):
        #if self.read_switch('b2mndr_eirene') is not None:
        #    self._b2standalone = not bool(int(self.read_switch('b2mndr_eirene')))
        #else:
        #    self._b2standalone = True #Default
        try:
            self._b2standalone = not bool(int(
            self.read_switch('b2mndr_eirene')))
        except:
            self.log.error(
                "Defaulting to b2standalone due to lack of 'b2mn.dat'")
            self._b2standalone = True #Default



    @solps_property
    def solpsversion(self):
        """str: SOLPS numerical version."""
        if 'b2fplasmf' in self.avail:
            fd = self.avail['b2fplasmf']
        elif 'b2fstate' in self.avail:
            fd = self.avail['b2fstate']
        elif 'b2fstati' in self.avail:
            fd = self.avail['b2fstati']
        elif 'b2fgmtry' in self.avail:
            fd = self.avail['b2fgmtry']
        with open(fd,'r') as b2p:
            self._solpsversion = b2p.readline()

    @solps_property
    def branch(self):
        """str: SOLPS numerical version."""
        if 'b2fplasmf' in self.avail:
            fd = self.avail['b2fplasmf']
        elif 'b2fstate' in self.avail:
            fd = self.avail['b2fstate']
        elif 'b2fstati' in self.avail:
            fd = self.avail['b2fstati']
        elif 'b2fgmtry' in self.avail:
            fd = self.avail['b2fgmtry']
        with open(fd,'r') as b2p:
            title = b2p.readline()[7:17].split('.')
            self._branch = '{}.{}.{}'.format(
                *[str(int(piece)) for piece in title])



    @property
    def directory(self):
        """str: Full path of self.rundir."""
        return os.path.abspath(self.rundir)

    @solps_property
    def basename(self):
        """str: Bottom directory of self.rundir"""
        self._basename = self.rundir.rstrip('/').split('/')[-1]

    @solps_property
    def baserun(self):
        """str: Directory of the baserun folder"""
        dire = self.directory.rstrip('/')
        while not os.path.isdir(dire + '/baserun'):
            dire = dire.rsplit('/',1)[0]
            if dire == '':
                return None
        self._baserun = dire + '/baserun'


    def rundir_rel(self, rel='.'):
        """ Returns the absolute path for a given relative path from rundir.
        """
        return os.path.abspath(os.path.join(self.directory, rel))



##  GEOMETRY ==================================================================




    # -------------------- Structure ------------------------------------------
    ### ATTENTION: What if I make vessel a namespace for all the stuff related
    ### to PFCs. Or maybe call them pfc. But add everything to it.
    ### Even things like assigning quantities to wall elements, like colour
    ### pumped flux, etc.
    ### Their corresponding SURFMOD, etc
    @solps_property
    def vessel(self):
        """(4,NLIMI) : Array containing the data for additional surfaces
                       defined in input.dat block 3b or in the mesh.extra
                       file."""
        #import ipdb; ipdb.set_trace()
        if 'input.dat' in self.avail:
            try:
                self._read_structure_input_dat()
                return
            except:
                pass
        if 'mesh.extra' in self.avail:
            try:
                self._vessel = self.vessel_mesh
                return
            except:
                pass
        if self.grid_type == 'structured':
            self._vessel = vessel_square_structured(self)
        elif self.grid_type == 'unstructured':
            self._vessel = vessel_square_unstructured(self)

    @solps_property
    def vessel_colors(self):
        """A dictionary containing the color indizes ILCOL as defined in the
        input.dat file for each additional surface. (Format {nsurface : ILCOL})"""
        if 'input.dat' in self.avail:
            self._read_structure_input_dat()
        else:
            self._vessel_colors = None #Maybe just a list of 'k'?

    @solps_property
    def vessel_surfmods(self):
        """A dictionary containing the SURFMODE defined in input.dat
        for each additional surface. (Format {nsurface : SURFMOD})"""
        if 'input.dat' in self.avail:
            self._read_structure_input_dat()
        else:
            self._vessel_surfmods = None 

    @solps_property
    def vessel_labels(self):
        """A dictionary containing the LABELS defined in input.dat
        for each additional surface. (Format {nsurface : LABEL})"""
        if 'input.dat' in self.avail:
            self._read_structure_input_dat()
        else:
            self._vessel_labels = None 

    @solps_property
    def mesh_extra(self):
        self._read_mesh_extra()


    #ATTENTION: Add input.dat as source of strata
    @solps_property
    def nstrata(self):
        """ Number of strata present in b2.neutrals.parameters.
        """
        try:
            with open(self.avail['b2.neutrals.parameters'], 'r') as b2f:
                    for line in b2f:
                        dummy = line.strip('\n,').replace("="," ").split()
                        if dummy[0].lower() == 'nstrai':
                            self._nstrata = int(dummy[1])
                            break
        except:
            self._nstrata = None


#    ## ATTENTION: Does not work properly in WG yet.
#    ## In newer versions of the code, lines can be broken
#    ## ATTENTION 2022: Substitute by f90nml.
#    @solps_property
#    def strata(self):
#        """ Provides basic information about the strata from
#        b2.neutral.parameters: Type, position, start and end.
#        """
#        import pdb; pdb.set_trace()
#        if 'b2.neutrals.parameters' not in self.avail:
#            self.log.exception("b2.neutrals.parameters required for strata")
#            return
#        with open(self.avail['b2.neutrals.parameters'], 'r') as b2f:
#                names = []
#                rcpos = []
#                rcstart = []
#                rcend = []
#                for line in b2f:
#                    dummy = line.strip().split('=')
#                    if dummy[0].lower().strip() == 'crcstra':
#                        dummy = dummy[1].replace(","," ")
#                        dummy = dummy.replace("'","").split()
#                        for strata in dummy:
#                            names.append(strata)
#                    elif dummy[0].lower().strip() == 'rcpos':
#                        dummy = dummy[1].replace(","," ").split()
#                        for strata in dummy:
#                            rcpos.append(int(strata))
#                    elif dummy[0].lower().strip() == 'rcstart':
#                        dummy = dummy[1].replace(","," ").split()
#                        for strata in dummy:
#                            rcstart.append(int(strata))
#                    elif dummy[0].lower().strip() == 'rcend':
#                        dummy = dummy[1].replace(","," ").split()
#                        for strata in dummy:
#                            rcend.append(int(strata))
#        Stratum = namedtuple('Stratum', ['crcstra','rcpos','rcstart','rcend'])
#        self._strata = [Stratum(*s) for s in zip(names,rcpos,rcstart,rcend)]


    ## ATTENTION: This can be improved significantly by trying to add
    ## as many elements as possible to stratum, such as userfluxparm
    @solps_property
    def strata(self):
        """ Provides basic information about the strata from
        b2.neutral.parameters: Type, position, start and end.
        Note: Use _read_b2file_param instead???
        But at the moment _read_b2file_param not yet fully ready.
        """

        if 'b2.neutrals.parameters' not in self.avail:
            self.log.exception("b2.neutrals.parameters required for strata")
            return
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            #tmp = parser_f90namelist(
            tmp = parsers.f90namelist(
                self.avail['b2.neutrals.parameters'])['neutrals']
        if self.grid_type=='structured':
            elements = ['crcstra','rcpos','rcstart','rcend', 'recyc']
        else:
            elements = ['crcstra','rcstart','rcend', 'recyc']

        Stratum = namedtuple('Stratum', elements)
        contents = [tmp[ele] for ele in elements]
        dummy = []
        for ct in zip(*contents):
            dummy.append(Stratum(*ct))
                
        self._strata = dummy
        #self._strata = [Stratum(*s) for s in zip(names,rcpos,rcstart,rcend)]



## SNAPSHOT ===================================================================

    # -------------------- Dimensions -----------------------------------------

    # -------------------- Indices and locators -------------------------------

    # -------------------- Plasma Characterization ----------------------------

    @solps_property
    def zamin(self):
        """(ns): Minimum charge of the bundle/ion/atom [e-]."""
        self._read_signal('_zamin', 'zamin')
        self._zamin = self._zamin.astype(int)

    @solps_property
    def zamax(self):
        """(ns): Maximum charge of the bundle/ion/atom [e-]."""
        self._read_signal('_zamax', 'zamax')
        self._zamax = self._zamax.astype(int)

    @solps_property
    def za(self):
        """(ns): Average charge of the bundle/ion/atom [e-]."""
        self._za  = (self.zamin+self.zamax)/2.0



    # -------------------- Densities ------------------------------------------
    
    
    # -------------------- Temperatures ---------------------------------------
    

    # -------------------- Pressures ------------------------------------------

    
    # -------------------- Velocities -----------------------------------------
    @solps_property
    def uadia(self):
        """ ({GRID_DIMS}, 2): Total effective drift velocity [m/s]
        """
        self._read_signal('_uadia','uadia')
    
    
    # -------------------- Fluxes and energies --------------------------------
    @solps_property2
    def fna(self):
        """({GRID_DIMS}, 2, ns): Particle flux through cell faces [s^-1].

        fna[...,0]: poloidal flux through left face.
        Alias of :py:attr:`~fnax`.

        fna[...,1]: radial flux through bottom face.
        Alias of :py:attr:`~fnay`.
        """

    @solps_property
    def fna32(self):
        """({GRID_DIMS}, 2, ns): Convective particle flux through cell faces [s^-1].

        fna32[...,0]: poloidal flux through left face.
        Alias of :py:attr:`~fnax32`.

        fna32[...,1]: radial flux through bottom face.
        Alias of :py:attr:`~fnay32`.
        """
        self._read_signal('_fna32','fna_32')

    @solps_property
    def fna52(self):
        """({GRID_DIMS}, 2, ns): Conductive particle flux through cell faces [s^-1].

        fna52[...,0]: poloidal flux through left face.
        Alias of :py:attr:`~fnax52`.

        fna52[...,1]: radial flux through bottom face.
        Alias of :py:attr:`~fnay52`.
        """
        self._read_signal('_fna52','fna_52')


    @solps_property
    def fnax(self):
        """({GRID_DIMS}, ns): Poloidal particle flux of species is through the x-face
        between the cell (ix, iy) and its left neighbour (ix-1,iy) [s^-1].

        Notes
        -----
        fnax[0] = np.zeros(ny, ns) to complete the dimensions.
        """
        self._fnax = self.fna[...,0,:]

    @solps_property
    def fnax32(self):
        """({GRID_DIMS}, ns): Convective poloidal particle flux of species is
        through the x-face between the cell (ix, iy) and its left
        neighbour (ix-1, iy) [s^-1].

        Notes
        -----
        fnax[0] = np.zeros(ny, ns) to complete the dimensions.
        """
        self._fnax32 = self.fna32[...,0,:]

    @solps_property
    def fnax52(self):
        """({GRID_DIMS}, ns): Conductive poloidal particle flux of species is
        through the x-face between the cell (ix, iy) and its left
        neighbour (ix-1, iy) [s^-1].

        Notes
        -----
        fnax[0] = np.zeros(ny, ns) to complete the dimensions.
        """
        self._fnax52 = self.fna52[...,0,:]


    @solps_property
    def fnay(self):
        """({GRID_DIMS}, ns): Radial particle flux of species is through the y-face
        between the cell (ix, iy) and its bottom neighbour (ix, iy-1) [s^-1].

        Notes
        -----
        fnay[:,0,:] = np.zeros(nx, ns) to complete the dimensions.
        """
        self._fnay = self.fna[...,1,:]

    @solps_property
    def fnay32(self):
        """({GRID_DIMS}, ns): Convective radial particle flux of species is
        through the y-face between the cell (ix, iy) and its bottom
        neighbour (ix, iy-1) [s^-1].

        Notes
        -----
        fnay[:,0,:] = np.zeros(nx, ns) to complete the dimensions.
        """
        self._fnay32 = self.fna32[...,1,:]

    @solps_property
    def fnay52(self):
        """({GRID_DIMS}, ns): Conductive radial particle flux of species is
        through the y-face between the cell (ix, iy) and its bottom
        neighbour (ix, iy-1) [s^-1].

        Notes
        -----
        fnay[:,0,:] = np.zeros(nx, ns) to complete the dimensions.
        """
        self._fnay52 = self.fna52[...,1,:]



    @solps_property2
    def fne(self):
        """({GRID_DIMS}, 2): Electron particle flux through
        cell faces [s^-1].

        fne[...,0]: poloidal flux through left face.
        Alias of :py:attr:`~fnex`.

        fne[...,1]: radial flux through bottom face.
        Alias of :py:attr:`~fney`.
        """

    @solps_property
    def fnex(self):
        """({GRID_DIMS}, ns): Poloidal electron particle flux through
        the y-face between the cell (ix, iy) and its left neighbour
        (ix-1, iy) [s^-1].

        Notes
        -----
        fnex[0,:,:] = np.zeros(ny, ns) to complete the dimensions.
        """
        self._fnex = self.fne[...,0]

    @solps_property
    def fney(self):
        """({GRID_DIMS}, ns): Radial electron particle flux through
        the y-face between the cell (ix, iy) and its bottom neighbour
        (ix, iy-1) [s^-1].

        Notes
        -----
        fney[:,0,:] = np.zeros(nx, ns) to complete the dimensions.
        """
        self._fney = self.fne[...,1]




    ##ATTENTION: Different result from fch, why? What terms are nto taken into account?
    @solps_property2
    def fnj(self):
        """({GRID_DIMS}, 2): Current through cell faces [A].

        fne[...,0]: poloidal current through left face.
        Alias of :py:attr:`~fnjx`.

        fne[...,1]: radial current through bottom face.
        Alias of :py:attr:`~fnjy`.
        """
        self._fnj = np.sum(self.za*self.qe*self.fna, axis=-1) -self.qe*self.fne

    @solps_property
    def fnjx(self):
        """({GRID_DIMS}, ns): Poloidal current through
        the y-face between the cell (ix, iy) and its left neighbour
        (ix-1, iy) [s^-1].

        Notes
        -----
        fnjx[0,:,:] = np.zeros(ny, ns) to complete the dimensions.
        """
        self._fnjx = self.fnj[...,0]

    @solps_property
    def fnjy(self):
        """({GRID_DIMS}, ns): Radial current through
        the y-face between the cell (ix, iy) and its bottom neighbour
        (ix, iy-1) [s^-1].

        Notes
        -----
        fnjy[:,0,:] = np.zeros(nx, ns) to complete the dimensions.
        """
        self._fnjy = self.fnj[...,1]








    @solps_property
    def fmo(self):
        self._read_signal('_fmo','fmo')

    @solps_property
    def fmox(self):
        self._fmox = self.fmo[...,0,:]

    @solps_property
    def fmoy(self):
        self._fmoy = self.fmo[...,1,:]


    @solps_property
    def fhe(self):
        self._read_signal('_fhe','fhe')

    @solps_property
    def fhex(self):
        self._fhex = self.fhe[...,0]

    @solps_property
    def fhey(self):
        self._fhey = self.fhe[...,1]


    @solps_property
    def fhi(self):
        self._read_signal('_fhi','fhi')

    @solps_property
    def fhix(self):
        self._fhix = self.fhi[...,0]

    @solps_property
    def fhiy(self):
        self._fhiy = self.fhi[...,1]



    @solps_property
    def fch(self):
        self._read_signal('_fch','fch')

    @solps_property
    def fchx(self):
        self._fchx = self.fch[...,0]

    @solps_property
    def fchy(self):
        self._fchy = self.fch[...,1]



    @solps_property
    def fch32(self):
        """ ({FACE_DIMS}, 2): Convective current [A]
        """
        self._read_signal('_fch32', 'fch_32')

    @solps_property
    def fch52(self):
        """ ({FACE_DIMS}, 2): Conductive current [A]
        """
        self._read_signal('_fch52', 'fch_52')



    #ATTENTION: Only for structured right now.
    #ATTENTION: Only for <3.0.5, I believe.
    @solps_property
    def fhj(self):
        """ ({GRID_DIMS}, 2): Electrostatic energy flux.
        """
        self._read_signal('_fhj', 'fhj')
        if self._fhj is None:
        #if int(self.solpsversion[-3:])<=5:
            fhj = np.zeros((self.nx,self.ny,2))
            for ny in range(self.ny):
                for nx in range(self.nx):
                    if self.leftix[nx,ny] >= 0:
                        fhj[nx,ny,0] = (-0.5 * self.fch[nx,ny,0] *
                            (self.po[nx,ny] +
                             self.po[self.leftix[nx,ny],
                                     self.leftiy[nx,ny]]))
                    else:
                        fhj[nx,ny,0] = 0

                    if self.bottomiy[nx,ny] >= 0:
                        fhj[nx,ny,1] = (-0.5 * self.fch[nx,ny,1] *
                            (self.po[nx,ny] +
                             self.po[self.bottomix[nx,ny],
                                     self.bottomiy[nx,ny]]))
                    else:
                        fhj[nx,ny,1] = 0
            self._fhj = fhj


    @solps_property
    def fhjx(self):
        self._fhjx = self.fhj[...,0]

    @solps_property
    def fhjy(self):
        self._fhjy = self.fhj[...,1]




    #ATTENTION: Only for structured right now.
    #ATTENTION: Only for <3.0.5, I believe.
    @solps_property
    def fhm(self):
        """ ({GRID_DIMS}, 2): Parallel kinetic energy flow.
        """
        self._read_signal('_fhm', 'fhm')
        if self._fhm is None:
        #if int(self.solpsversion[-3:])<=5:
            fhm = np.zeros((self.nx,self.ny,2,self.ns))
            for ns in range(self.ns):
                for ny in range(self.ny):
                    for nx in range(self.nx):
                        if self.leftix[nx,ny] >= 0:
                            fhm[nx,ny,0,ns] = (0.5*self.fna[nx,ny,0,ns]*
                                (self.kinrgy[int(self.leftix[nx,ny]),
                                             int(self.leftiy[nx,ny]),ns]
                                 + self.kinrgy[nx,ny,ns]))
                        else:
                            fhm[nx,ny,0,ns] = 0
                        if self.bottomiy[nx,ny] >= 0:
                            fhm[nx,ny,1,ns] = (0.5*self.fna[nx,ny,1,ns]*
                                (self.kinrgy[int(self.bottomix[nx,ny]),
                                             int(self.bottomiy[nx,ny]),ns]
                                 + self.kinrgy[nx,ny,ns]))
                        else:
                            fhm[nx,ny,1,ns] = 0
            self._fhm = fhm

    @solps_property
    def fhmx(self):
        self._fhmx = self.fhm[...,0,:]

    @solps_property
    def fhmy(self):
        self._fhmy = self.fhm[...,1,:]




    #ATTENTION: Only for structured right now.
    #ATTENTION: Only for <3.0.5, I believe.
    @solps_property
    def fhp(self):
        """ ({GRID_DIMS}, 2): Ionisation energy flow.
        """
        #if int(self.solpsversion[-3:])<=5:
        self._read_signal('_fhp', 'fhp')
        if self._fhp is None:
            fhp = np.zeros((self.nx,self.ny,2,self.ns))
            for ns in range(self.ns):
                for ny in range(self.ny):
                    for nx in range(self.nx):
                        if self.leftix[nx,ny] >= 0:
                            fhp[nx,ny,0,ns] = (0.5*self.fna[nx,ny,0,ns]*
                                (self.rpt[self.leftix[nx,ny],
                                             self.leftiy[nx,ny],ns]
                                 + self.rpt[nx,ny,ns]))*self.qe
                        else:
                            fhp[nx,ny,0,ns] = 0
                        if self.bottomiy[nx,ny] >= 0:
                            fhp[nx,ny,1,ns] = (0.5*self.fna[nx,ny,1,ns]*
                                (self.rpt[self.bottomix[nx,ny],
                                             self.bottomiy[nx,ny],ns]
                                 + self.rpt[nx,ny,ns]))*self.qe
                        else:
                            fhp[nx,ny,1,ns] = 0
            self._fhp = fhp

    @solps_property
    def fhpx(self):
        self._fhpx = self.fhp[...,0,:]

    @solps_property
    def fhpy(self):
        self._fhpy = self.fhp[...,1,:]




    #ATTENTION: Only for structured right now.
    #ATTENTION: Only for <3.0.5, I believe.
    @solps_property
    def fht(self):
        """ ({GRID_DIMS}, 2): Total energy flow.

        Notes
        -----
        Not exactly same as FT
        """
        self._read_signal('_fht', 'fht')
        if self._fht is None:
        #if self.solps_code == 'solps-iter' and int(self.solpsversion[-3:])<=5:
            if self.solps_code == 'solps-iter':
                self._fht = (self.fhe + self.fhi + self.fhj +
                        np.sum(self.fhp,-1) + np.sum(self.fhm,-1) + self.fnt)
            elif self.solps_code == 'solps5.0':
                self._fht = (self.fhe + self.fhi + self.fhj +
                        np.sum(self.fhp,-1) + np.sum(self.fhm,-1))


    @solps_property
    def fhtx(self):
        self._fhtx = self.fht[...,0]

    @solps_property
    def fhty(self):
        self._fhty = self.fht[...,1]



    # -------------------- Coefficients ---------------------------------------

    # -------------------- Rates, losses/sources and residuals ----------------


    # -------------------- EIRENE triangular grid -----------------------------

    @property
    def ntriang(self):
        return self.triang.shape[0]

    @solps_property
    def triang(self):
        """ Triangles from the EIRENE grid.
        (ntriang,3,2)
        """
        self._read_triang_mesh()

    @solps_property
    def crx_triang(self):
        self._read_triang_mesh()

    @solps_property
    def cry_triang(self):
        self._read_triang_mesh()

    @solps_property
    def ncvtot(self):
        """ Real number of cells in which triang grid quantities are defined??
        """
        with open(self.avail['fort.46'], 'r') as fd:
            self._ncvtot = int(fd.readline().split()[0])


    ## ATTENTION: Probably only correct for WG.
    ## For ST, >0 probably relates to nx, ny
    @solps_property
    def triang_reg(self):
        """ Region of the triangles.
            -1: void region
            >0: Corresponding ici cell.

        Max self.triang_red.max() = self.nci .
        """
        self._triang_reg = np.full(self.ntriang,np.nan, dtype=int)

        with open(self.avail['fort.35'],'r') as fort35:
            fort35.readline()
            for itriang in range(self.ntriang):
                self._triang_reg[itriang] = int(fort35.readline().split()[10])


    def _read_triang_mesh(self):
        """ Triangular grid of EIRENE.
        For WG, the shape of fort.33 seems to have changed.
        """

        with open(self.avail['fort.33'],'r') as fort33:
            napex = int(fort33.readline())
            R = []
            z = []
            elements = fort33.readline().split()

            # WG format
            if len(elements) == 3:
                R.append(np.float64(elements[1]))
                z.append(np.float64(elements[2]))

                for i in range(1,napex):
                    elements = fort33.readline().split()
                    R.append(np.float64(elements[1]))
                    z.append(np.float64(elements[2]))
                R = np.array(R)
                z = np.array(z)

            # ST format
            elif len(elements) == 4:
                for ele in elements:
                    R.append(np.float64(ele))

                ceiling = int(np.ceil(napex/4))
                for i in range(1, ceiling):
                    elements = fort33.readline().split()
                    for ele in elements:
                        R.append(np.float64(ele))
                for i in range(ceiling):
                    elements = fort33.readline().split()
                    for ele in elements:
                        z.append(np.float64(ele))

            else:
                self.log.exception("Unrecognized fort.33 format")


        with open(self.avail['fort.34'],'r') as fort34:
            ntriang = int(fort34.readline())
            i_apex1 = []
            i_apex2 = []
            i_apex3 = []
            for line in fort34:
                elements = line.split()
                i_apex1.append(int(elements[1]))
                i_apex2.append(int(elements[2]))
                i_apex3.append(int(elements[3]))


        self._crx_triang = np.full((ntriang,3), np.nan)
        self._cry_triang = np.full((ntriang,3), np.nan)
        for i in range(ntriang):
            self._crx_triang[i,0] = R[i_apex1[i]-1]
            self._crx_triang[i,1] = R[i_apex2[i]-1]
            self._crx_triang[i,2] = R[i_apex3[i]-1]

            self._cry_triang[i,0] = z[i_apex1[i]-1]
            self._cry_triang[i,1] = z[i_apex2[i]-1]
            self._cry_triang[i,2] = z[i_apex3[i]-1]

        self._crx_triang *= 0.01 #In meters
        self._cry_triang *= 0.01 #In meters

        self._triang = np.zeros((ntriang,3,2))
        self._triang[...,0] = self._crx_triang
        self._triang[...,1] = self._cry_triang

        return



    @solps_property
    def pdena(self):
        """(ncvtot,natm) : Atoms particle density [m^-3]"""
        self._read_fort46('_pdena', 'pdena')
        self._pdena *= 1e6 # Change from cgs to SI.

    @solps_property
    def pdenm(self):
        """(ncvtot,nmol) : Molecules particle density [m^-3]"""
        self._read_fort46('_pdenm', 'pdenm')
        self._pdenm *= 1e6 # Change from cgs to SI.

    @solps_property
    def pdeni(self):
        """(ncvtot,nion) : Test ions particle density [m^-3]"""
        self._read_fort46('_pdeni', 'pdeni')
        self._pdeni *= 1e6 # Change from cgs to SI.


    @solps_property
    def edena(self):
        """(ncvtot,natm) : Energy density carried by atoms [eV m^-3]"""
        self._read_fort46('_edena', 'edena')
        self._edena *= 1e6 # Change from cm to m.

    @solps_property
    def edenm(self):
        """(ncvtot,nmol) : Energy density carried by molecules [eV m^-3]"""
        self._read_fort46('_edenm', 'edenm')
        self._edenm *= 1e6 # Change from cm to m.

    @solps_property
    def edeni(self):
        """(ncvtot,nion) : Energy density carried by test ions [eV m^-3]"""
        self._read_fort46('_edeni', 'edeni')
        self._edeni *= 1e6 # Change from cm to m.



    @solps_property
    def vdena(self):
        """(ncvtot,3,natm) : momentum carried by atoms [kg m^-3 s^-1]

        Notes
        -----
        vdena [...,0] X-directed.
        vdena [...,1] Y-directed.
        vdena [...,2] Z-directed.
        """
        self._vdena = np.zeros((self.ncvtot, 3, self.natm))
        self._vdena[:,0,:] = self.vxdena
        self._vdena[:,1,:] = self.vydena
        self._vdena[:,2,:] = self.vzdena

    @solps_property
    def vxdena(self):
        """(ncvtot,nmol) : X-directed momentum carried by atoms [kg m^-3 s^-1]"""
        self._read_fort46('_vxdena', 'vxdena')
        self._vxdena *= 1e-2*1e4 # cgs to SI.
    @solps_property
    def vydena(self):
        """(ncvtot,natm) : Y-directed momentum carried by atoms [kg m^-3 s^-1]"""
        self._read_fort46('_vydena', 'vydena')
        self._vydena *= 1e-2*1e4 # cgs to SI.
    @solps_property
    def vzdena(self):
        """(ncvtot,natm) : Z-directed momentum carried by atoms [kg m^-3 s^-1]"""
        self._read_fort46('_vzdena', 'vzdena')
        self._vzdena *= 1e-2*1e4 # cgs to SI.



    @solps_property
    def vdenm(self):
        """(ncvtot,3,nmol) : momentum carried by molecules [kg m^-3 s^-1]

        Notes
        -----
        vdenm [...,0] X-directed.
        vdenm [...,1] Y-directed.
        vdenm [...,2] Z-directed.
        """
        self._vdenm = np.zeros((self.ncvtot, 3, self.nmol))
        self._vdenm[:,0,:] = self.vxdenm
        self._vdenm[:,1,:] = self.vydenm
        self._vdenm[:,2,:] = self.vzdenm

    @solps_property
    def vxdenm(self):
        """(ncvtot,nmol) : X-directed momentum carried by molecules [kg m^-3 s^-1]"""
        self._read_fort46('_vxdenm', 'vxdenm')
        self._vxdenm *= 1e-2*1e4 # cgs to SI.
    @solps_property
    def vydenm(self):
        """(ncvtot,nmol) : Y-directed momentum carried by molecules [kg m^-3 s^-1]"""
        self._read_fort46('_vydenm', 'vydenm')
        self._vydenm *= 1e-2*1e4 # cgs to SI.
    @solps_property
    def vzdenm(self):
        """(ncvtot,nmol) : Z-directed momentum carried by molecules [kg m^-3 s^-1]"""
        self._read_fort46('_vzdenm', 'vzdenm')
        self._vzdenm *= 1e-2*1e4 # cgs to SI.





    def _read_fort46(self, signal_name, var_name, attach_to=None):
        """
        """
        if not attach_to:
            attach_to = self
        try:
            var = parsers.fort46(self.avail['fort.46'], var_name)
            setattr(attach_to, signal_name, var)
        except:
            self.log.exception("error reading '{0}''".format(var_name))




##  B2 INPUT AND RUN.LOG ======================================================

    ##ATTENTION: TO BE DONE
    @solps_property
    def resalld(self):
        """ Same as the quantities extracted with resall_D script.
        """


    @solps_property
    def recyc(self):
        self._recyc = self._read_b2file_param(
            'b2.neutrals.parameters','recyc',(self.ns, self.nstrata))


    @solps_property
    def userfluxparm(self):
        self._userfluxparm = self._read_b2file_param(
            'b2.neutrals.parameters','userfluxparm',(self.nstrata, 2))



##  FUNCTIONS =================================================================

    ## ATTENTION 2023: Use f90nml instead???
    ## ATTENTION: Still things to do.
    def _read_b2file_param(self, file, param, dims, dtype=float):
       """ 
        This cannot read chemical sputtering, bceause I need to create a case
        in which if no new variable is found in the next line, one needs to
        assume that it is still the same variable.

        Todo:
            - More than one parameter per line.
            - Parameters defined in more than one line.
       """
       #import pdb; pdb.set_trace()
       try:
           var = np.zeros(dims, dtype=dtype) #Assuming that default is zero.
           with open(self.avail[file], 'r') as b2f:
               for line in b2f:
                   dummy = line.strip('\n,').split("=")
                   name = dummy[0].split("(")
                   if name[0].strip().lower() == param:
                       found_param = True #For params defined in more than a line
                       if dims == 0:
                           return dtype(dummy[1])
                       try:
                           inds = [int(i.replace(")", "").strip())-1
                               for i in name[1].split(',')]
                           ele = [i for i in dummy[1].split(',')]

                           if len(inds) == 1:
                               ele[inds[0]] = ele
                           elif len(inds) == 2:
                               if len(ele) >1:
                                   for l,element in enumerate(ele):
                                       var[l, inds[1]] = element
                               else:
                                   var[inds[0],inds[1]] = dummy[1]
                           else:
                               self.log.error(
                                   "Arrays with more than two dimensions"+
                                   " cannot be read yet with this routine.")
                       except:
                           pass
               return var 
       except:
           self.log.error(
              "'{0}' could not be read in/from '{1}'".format(param, file))
           return None        


    def find_file(self, file):
        """
        Find file with the corresponding priority given in the config file.
        """
        #fileloc = read_config()['Files']['location']
        fileloc = self.config['Files']['location']

        for ifd in fileloc:
            if ifd.startswith('.'): #If relative path
                root = self.rundir_rel(ifd) 
            else:
                root = os.path.abspath(ifd)
            fd = os.path.join(root, file)
            if (os.path.isfile(fd) and
                #os.stat(fd) != 0): 
                os.stat(fd).st_size > 0): 
                return os.path.abspath(fd)
        self.log.info(
                "'{0}' could not be found in any of the given locations."
                .format(file))
        return None


    def read_signal(self, signal_name, var_name, attach_to = None):
        """
        Similar API to the one of mds: Name then link

        MdsData         read_signal('_zn', 'blablabla:ZN')
        RundirData      read_signal('_zn', 'zn')

        Does it add something or should I simply rename parser to read_signal?
        """
        if not attach_to:
            attach_to = self
        try:
            var = self.parser(var_name)
            setattr(attach_to, signal_name, var)
        except:
            self.log.warning("error reading '{0}''".format(var_name))
            setattr(attach_to, signal_name, None)




    def read_switch(self, name, b2file = 'b2mn.dat', default=None):
        """
          Used to read switchers of b2-stylefiles (b2*.dat)
          Maybe in the future, add dictionary with location according to name
          similar to self.where for the outputs.

        Parameters
        ----------
        name: (str)
            Name of the switch
        b2file: (str)
            Name of the b2-style file in which the switch is declared

        Outputs
        -------
        (str)
            Value of the switch
        """
        try:
            with open(self.avail[b2file], 'r') as f:
                stack = []
                for line in f:
                    dummy = line.replace("'","").split()
                    try:
                        if dummy[0].lower() == name.lower():
                            stack.append(dummy[1])
                    except:
                        pass
                try:
                    return stack[-1]
                except:
                    return default
        except:
            self.log.error(
                "'{}' could not be read from '{}'".format(name, b2file))
            return default




    ## Make it a returner, and use solps_property2 to make it easier
    ## solps_property will do setattr(self, 'var', self.read_signal('var')
    ## This would also help if I could allow read_signal to accept
    ## a where and a dimension.
    def _read_signal(self, signal_name, var_name, attach_to = None):
        """
        Similar API to the one of mds: Name then link

        MdsData         read_signal('_zn', 'blablabla:ZN')
        RundirData      read_signal('_zn', 'zn')

        Does it add something or should I simply rename parser to read_signal?
        """
        if not attach_to:
            attach_to = self
        try:
            fname, dims = self.where(var_name)
            #import ipdb; ipdb.set_trace()
            var = self.parser(var_name)
            if fname in ['fort.44'] and self.grid_type == 'structured' :
                var = self._extend(var)
            setattr(attach_to, signal_name, var)
        except Exception:
            self.log.exception("Error reading '{0}'".format(var_name))
            setattr(attach_to, signal_name, None)



    def _check_avail(self):
        """
        Check which files are avail for the rundir.
        Include also a check for size, correction, etc.

        TODO:
        Check of size should not only check for non zero,
        but also for correct size.
        """
        #names = read_config()['Files']['names']
        names = self.config['Files']['names']
        tmp = {}
        for file in [
        #B2 dat files
            'b2mn.dat',
            'b2ag.dat',
            'b2ar.dat',
            'b2ah.dat',

        #B2 input/parameters files
            'b2.neutrals.parameters',
            'b2.boundary.parameters',
            'b2.transport.parameters',
            'b2.transport.inputfile',
            'b2.user.parameters',

        #B2F files
            'b2fplasmf',
            'b2fplasma',
            'b2fstate',
            'b2fstati',
            'b2fgmtry', 
            'b2fpardf',
            'b2frates',
            'b2fparam',

        #Other B2 files
            'mesh.extra',

        #Eirene files
            'input.dat',
            'fort.33',
            'fort.34',
            'fort.35',
            'fort.44',
            'fort.46',

        #log files
            'run.log',
            'eir.log',

        #ncfiles and friends
            'b2time.nc',
            'b2tallies.nc',
            'balance.nc',
            'particle_fluxes.nc',

        #ds/da files
            'dsL',
            'dsl',
            'dsR',
            'dsr',
            'dsTLP',
            'dsTL',
            'dstl',
            'dsTRP',
            'dsTR',
            'dstr',
            'dsi',
            'dsa',
            'dsLP',
            'dsp',
            'dsRP',

            ]:

            if file in names:
                fd = names[file]
                if isinstance(fd, str):
                    file = fd
                elif isinstance(fd, list):
                    for ifd in fd:
                        if self.find_file(ifd) is not None:
                            file = ifd
                            break
            tmp[file] = self.find_file(file)
            self.avail = purge(tmp)



    def parser(self, var):
        """
        Parses and decide which file to parse.
        It simply returns the array with the value or None
        For now, it only parses known variables and files
        Maybe it would be beter to have different parsers separately
        and let read_signal to decide
        """
        try:
            fname, dims = self.where(var)
            fd = self.avail[fname]
            nentries = np.prod(dims)
            factor = self._factors(var)
        except:
            self.log.warning("'{0}' could not be parsed.".format(var))
            return None

        b2f_like_files = ['b2fplasmf','b2fstate','b2fstati','b2fgmtry',
                'b2fpardf', 'b2frates', 'b2fparam']
        fort_like_files = ['fort.44']
        time_trace_like_files = []

        data = []

        #B2f files
        if fname in b2f_like_files:
            return parsers.b2(fd, var, dims)*factor

        #Eirene output for SOLPS-ITER
        elif fname in fort_like_files:
            return parsers.fort44(fd, var)*factor
        else:
            self.log.exception(
                "In parser: file type, b2-like or fort.44-like, not assigned.")
            raise Exception
















    def _factors(self, var):
        """
        Temperatures are stored in J and not in eV
        """
        temps = {'te','ti','tab2','tmb2','tib2'}
        if var.lower() in temps:
           return 6.2415093432601784e+18
        else:
            return 1.0


    def _read_mesh_extra(self, target=None):
        """
        Reads mesh.extra to extract the vessel structure inside
        """
        try:
            with open(self.avail['mesh.extra'], 'r') as f:
                lines = f.readlines()
                nsurf = len(lines)
                vessel = np.zeros((nsurf,4))
                for i,line in enumerate(lines):
                    line.split()
                    vessel[i,:] = np.array([float(line.split()[k])
                                            for k in range(4)])
                self._vessel_mesh = vessel.T
        except:
            self._vessel_mesh = None


    def _read_structure_input_dat(self):
        """
        Parses the input.dat file and produces the following variables:

        self._vessel (4,NLIMI) : Array containing the data (positions)
                                 for the additional surfaces defined in 
                                 input.dat block 3b.

                                 NLIMI is the total number of surfaces as
                                 defined in input.dat.

                                 x1 = self._vessel[0,:]
                                 y1 = self._vessel[1,:]
                                 x2 = self._vessel[2,:]
                                 y2 = self._vessel[3,:]

        self._vessel_colors    : A dictionary containing the color index
                                 ILCOL as defined in the input.dat file for
                                 each surface. (Format {nsurface : ILCOL})
        """

        # Read input.dat into buffer:
        with open(self.avail['input.dat'],'r') as file:
            lines = file.readlines()

        # Parse input.dat:
        i      = 0
        nlines = len(lines)

        # Go to Block 3B:
        while i < nlines and not lines[i].lower().startswith("*** 3b"):
            i += 1
        i += 1

        # Parse Block 3B:
        data  = [[],[],[],[]] # Format: x1,y1,x2,y2
        ILCOL = []            # color integer ILCOL as defined in input.dat
        SURFMOD = []
        LABEL = []
        while i+3 < nlines and not lines[i].startswith("***"):
            if lines[i].startswith("*"):
                LABEL.append(lines[i].strip())
                data[0].append(float(lines[i+3][0 :12])/100.0) # (cm --> m)
                data[1].append(float(lines[i+3][12:24])/100.0)
                data[2].append(float(lines[i+3][36:48])/100.0)
                data[3].append(float(lines[i+3][48:60])/100.0)
                ILCOL.append(int(lines[i+2].split()[5]))
                if lines[i+4].startswith("SURFMOD"):
                    SURFMOD.append(lines[i+4].strip())
                else:
                    SURFMOD.append('default')
                i += 4
            else:
                i += 1

        if data == []:
            self.log.warning("Could not read any vessel data from 'input.dat'!")
            raise Exception

        self._vessel = np.array(data)
        self._vessel_colors = {nsurf:ilcol for nsurf,ilcol in enumerate(ILCOL)}
        self._vessel_surfmods = {nsurf:mod for nsurf,mod in enumerate(SURFMOD)}
        self._vessel_labels = {nsurf:label for nsurf,label in enumerate(LABEL)}





    # -------------------- Operations -----------------------------------------
 
    #ATTENTION: ???
    def open_file(self,file): 
        """Opens .png, .pdf, .(e)ps or text files in the run directory."""  
        open_file(file=file,dir=self.rundir)  

 

