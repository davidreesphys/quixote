"""
It seems that balance.nc finally works!
"""

import os
import numpy as np
import scipy.io as io
from IPython import embed


class Balance(object):
    def __init__(self, path):
        if isinstance(path, str):
            file = io.netcdf_file(path)
        self.__dict__.update(file.dimensions)
        self.nx = self.nx_plus2
        self.ny = self.ny_plus2
        for k, v in file.variables.items():
            if len(v.data) == 1:
                self.__dict__.update({k: v.data[0]})
            else:
                self.__dict__.update({k: v.data.T})

        self._get_common()
        embed()

    def _get_common(self):
        self.r = self.crx.copy()
        self.z = self.cry.copy()
        self.cr = np.mean(self.r, 2)
        self.cz = np.mean(self.z, 2)
        return


    def particle(self, include, exclude):
        pass






if __name__ == '__main__':
    path = '/data1/ip3/solps-runs/308/mastu/ipar_mastu_cd_45470_drifts/puff=4.00e21_pump=0.001_drifts/balance.nc'
    tmp = Balance(path)

