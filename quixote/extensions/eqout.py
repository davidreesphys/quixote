import os
import re
import numpy as np
import glob
import itertools
import warnings

# Quixote modules
from ..tools import solps_property
from ..tools import create_log
from ..tools import extend, get

alog = create_log('EqOut')

def EquationsOutput(mother):
    if mother.grid_type == 'structured':
        if mother.branch == '3.0.8': 
            return type('EquationsOutput',
                (EquationsOutputST, EqOut5pt), {})(mother)
        elif mother.branch == '3.1.0': 
            return type('EquationsOutput',
                (EquationsOutputST, EqOut9pt), {})(mother)

    elif mother.grid_type == 'unstructured':
        return type('EquationsOutput', (EquationsOutputUS, ), {})(mother)

    else:
        alog.exception(
        "Grid Data Type must be either structured or unstructured")




class BaseEquationsOutput(object):
    """ This class serves to read and work with the extra output files when
    using 'b2wdat_iout'     '4'.

    .. note::
        For obvious reasons, the simulation must have been run
        with ``'b2wdat_iout'`` ``'4'``.

        Check for files in the output/ directory
        in your run directory.


    If ``b2tfnb_drift_style`` = 1, ``uadia`` and ``uaecrb``
    are given in cell face (default). Else in cell center.

    .. warning::
        The following quantities were tricky to understand and implement,
        and some equations can still not be fully recovered out of its
        component.

        **Use with caution!**

    Parameters
    ----------
    mother: SolpsData like object.
        Rundir path and variables stored in `mother` will be used to
        find and parse the output files from equations.


    """
    def __init__(self, mother):
        self.mother = mother
        self.log = mother.log
        self.ns = mother.ns
        self.qe = mother.qe
        self.za = mother.za
        self.b2standalone = mother.b2standalone
        self.gtype= mother.grid_type
        for tmp in ['output', 'b2mn.exe.dir/output']:
            try:
                tmpdir = os.path.join(mother.rundir, tmp)
                if (os.path.isdir(tmpdir) and
                len(glob.glob(os.path.join(tmpdir, '*')))>1):
                    self.outdir = tmpdir
                    self.log.debug(
                    "Output directory set to '{0}'".format(self.outdir))
                    break
            except:
                pass
        else:
            self.log.exception("No 'output' directory is found in rundir.")


        self.wout = mother.read_switch('b2wdat_iout', 'b2mn.dat')
        if self.wout is None:
            self.wout = '0 (Default)' #Current default

        if self.wout != '1' and self.wout != '4':
            self.log.info("'b2wdat_iout' in b2mn.dat should be either "+
                    "'1' or '4' but is {0}.".format(self.wout))



    ### --------------------- EQUATIONS COMPOSITION ---------------------------
    ### ATTENTION: COMPLETE!

    @property
    def continuity(self):
        """ Deep dictionary for continuity equation"""
    #elif mother.branch == '3.1.0': 
    #elif mother.grid_type == 'unstructured':
        if self.mother.branch == '3.0.8': 
            resolution = {
                'resco':{},


                'sna':{
                    'sna_eir':{},
                    'sna_ion':{},
                    'sna_rec':{},
                    'sna_cx':{},
                    'sna_phys':{}
                    },


                'dnadt':{},


                'fnax':{ ## Effective particle flux, same as eqout fnax
                    'fnax_ua':{}, ##  Due to Parallel velocity
                    'fnax_ecrb':{ ## Due to ExB  drift
                        'vbecrbx':{}, ## ExB velocity
                    },
                    'fnax_gradna':{ ## Due to density gradient (Patankar scheme)
                        'cdnax':{}, ##  D,n,a (Patankar scheme?)
                    },
                    'fnax_gradp':{ ## Due to density gradient (Patankar scheme)
                        'cdnax':{}, ##  D,p,a (Patankar scheme?)
                    },
                    'fnax_psch':{},  ## Due to Pfirsch-Schluter current, opposite sign
                    'fnax_dpccor':{}, ## Due to DPC correction velocity
                },


                
                'fnax_real':{ ## Should be the real
                    'fnax_ua':{}, 
                    'fnax_ecrb':{ 
                        'vbecrbx':{},
                    },
                    'fnax_wdia':{
                        'wbdiax':{},
                    },
                    'fnax_gradna':{
                        'cdnax':{},
                    },
                    'fnax_gradp':{
                        'cdnax':{},
                    },
                    'fnax_dpccor':{},
                },


                'fnbx':{ ## Same as b2fstat*/b2fplasmf fnax
                    'fnax_ua':{}, ##  Due to Parallel velocity
                    'fnax_ecrb':{ ## Due to ExB  drift
                        'vbecrbx':{}, ## ExB velocity
                    },
                    'fnax_dia':{ ## Due to effective diamagnetic drift
                        'vbdiax':{}, ## Effective diamagnetic velocity
                    },
                    'fnax_gradna':{ ## Due to density gradient (Patankar scheme)
                        'cdnax':{}, ##  D,n,a (Patankar scheme?)
                    },
                    'fnax_gradp':{ ## Due to density gradient (Patankar scheme)
                        'cdnax':{}, ##  D,p,a (Patankar scheme?)
                    },
                    'fnax_dpccor':{}, ## Due to DPC correction velocity
                },




            }

        return resolution


    @property
    def potential(self):
        """ Deep dictionary for the potential equation. """
        if self.mother.branch == '3.0.8': 
            resolution = {
                'respo':{},


                'fchx':{ ## Same as b2fstat*/b2fplasmf fchx, not equout
                    'fchx_par':{},
                    'fchx_anomalous':{},
                    'fchx_dia':{},
                    'fchx_inert':{},
                    'fchx_vispar':{},
                    'fchx_visper':{},
                    'fchx_visq':{},
                    #'fchx_in':{}, #Should also be present, but file is not in output
                    },

            }

        return resolution





    @property
    def electron_heat(self):
        """ Deep dictionary for the electron heat equation. """
    #elif mother.branch == '3.1.0': 
    #elif mother.grid_type == 'unstructured':
        if self.mother.branch == '3.0.8': 
            resolution = {
                'reshe':{
                    'she':{},
                    'shei':{},
                    'fhe':{},
                },


                'she':{
                    'she_eir':{},
                    'she_rad':{},
                    'she_fr':{},
                    'she_div_ue':{}, ## Counted twice? Here and in fhe?
                    'she_div_ecrb':{}, ## Counted twice? Here and in fhe?
                    'she_phys':{},
                    },


                'dnetedt':{},

                'nedtedt':{},

                'tednedt':{},

                'fhex':{
                    'fhex_32':{
                        'fnex_he':{
                            'fnbx_he':{},
                            'fchx_par':{},
                        },
                        'te':{},
                    },
                    'fhex_cond':{
                        'chcex':{},
                    },
                    'fhex_fchp':{}, ## Alternatively is _aet
                    'fhex_psch':{},
                },

                'fhex_b2f':{
                    'fhex_32':{
                        'fnex':{},
                        'te':{},
                    },
                    'fhex_cond':{
                        'chcex':{},
                    },
                    'fhex_fchp':{}, ## Alternatively is _aet
                },


                'fhey':{
                    'fhey_32':{
                        'fney_he':{
                            'fnby_he':{},
                        },
                        'te':{},
                    },
                    'fhey_cond':{
                        'chcey':{},
                    },
                    'fhey_aet':{},
                    'fhey_psch':{},
                    'fhey_fch_stoch':{},
                },



            }

        return resolution










    @property
    def ion_heat(self):
        """ Deep dictionary for the ion heat equation. """
    #elif mother.branch == '3.1.0': 
    #elif mother.grid_type == 'unstructured':
        if self.mother.branch == '3.0.8': 
            resolution = {
                'reshi':{},


                'shi':{
                    'shi_rad':{},
                    'shi_ion':{},
                    'shi_fr':{},
                    'shi_du':{},
                    'shi_dd':{},
                    'shi_phys':{},
                    },


                'dnitidt':{},

                'nidtidt':{},

                'tidnidt':{},

                'fhix':{
                    'fhix_32':{
                        'fnax_he':{},
                        'ti':{},
                    },
                    'fhix_cond':{
                        'chcix':{},
                    },
                    'fhix_psch':{},
                },

                'fhix_b2f':{
                    'fhix_32':{},
                    'fhex_cond':{},
                    'fhex_fchp':{}, ## Alternatively is _aet
                },
            }

        return resolution








    ### ------------------------- FUNCTIONS -----------------------------------


    def _read_gdata_signal(self, var, dims, enforce=None, standalone_file=False):
        """ Signals are usually split in various files, depending on ions,
        neutrals, etc.
        It uses glob and thus shell and its wildcard expansion, but it is
        the easier way around.
        It reads the maximum number in the file to determine dimension.

        Enforce is a regex line that must be found the filename.
        This is required to accomodate the interesting naming convention
        of allowing up to 4 digits to stay together in some files.
        """
        factor = self._factors(var)
        tmp = sorted(glob.glob(os.path.join(self.outdir, var+'.dat')))
        if enforce:
            files = []
            for file in tmp:
                if len(re.findall(enforce, os.path.basename(file))) == 1:
                    files.append(file)
                elif len(re.findall(enforce, os.path.basename(file))) > 1:
                    self.log.error(
                    "Undetermined file validity: '{}' enforcing '{}'"
                    .format(file, enforce))
        else:
            files = tmp

        try:
            if standalone_file:
                fns = []
            else:
                fns = [re.findall(r'\d\d\d\D', fl)[-1][:-1] for fl in files]
        except:
            fns = []

        if len(files) == 0:
            raise Exception("File(s) containing '{}' not found.".format(var))
        elif fns == []:
            return np.array(self._read_gdata_file(files[0]), dtype=float)*factor
        else:
            fns = [int(re.findall(r'\d\d\d\D', fl)[-1][:-1]) for fl in files]
            tmp = np.full(dims, np.nan, dtype=float)
            for si, fl in zip(fns, files):
                try:
                    tmp[...,si] = np.array(
                    self._read_gdata_file(fl), dtype=float)*factor
                except:
                    self.log.error(
                    "File '{0}' does not fit in is='{1}'.".format(fl, si))

            return tmp





    ## ATTENTION: Not even temperatures are called like this anymore
    def _factors(self, var):
        """
        Temperatures are stored in J and not in eV
        """
        temps = {'te','ti','tab2','tmb2','tib2'}
        if var.lower() in temps:
           return 6.2415093432601784e+18
        else:
            return 1.0




## fna
## in 308 is fnax, fnay
## in 310 is fnax fnaxy, fnay fnayx
## in 320 is fna_r and fna_th for each nfc
## Surface vector quantities could be a new or a special function
## read_gdata signal takes quantity type or something "ncv" "nfc" "nfc vector"
## Assume for now only vector nfc?? At least from IO, not calculated.
## There seems to be one quantity that is not vectorial on nfc b2tfnb_dpbm, wtf that means. I think it might be some median gradient.
## So check if variable size begins with ncv or nfc or any other quantity and treat them separately.

    ### ------------------------ GEOMETRY QUANTITIES --------------------------

    @solps_property
    def vol(self):
        """ ({GRID DIMS}): Cell volume, sqrt(g) = hx * hy1 * hz [m^-3].
        """
        self._vol = self._read_gdata_signal(
            'vol', extend(self.cvdims), standalone_file=True)

    @solps_property
    def hx(self):
        """ ({GRID DIMS}): Poloidal scale factor [m].
        """
        self._hx = self._read_gdata_signal(
            'hx', extend(self.cvdims), standalone_file=True)

    @solps_property
    def hy(self):
        """ ({GRID DIMS}): [m].
        Notes
        -----
        hy1 is the correct quantity to use for reconstructions.
        """
        self._hy = self._read_gdata_signal(
            'hy', extend(self.cvdims), standalone_file=True)

    @solps_property
    def hy1(self):
        """ ({GRID DIMS}): Corrected hy, actual radial distance between
        neighbouring flux surfaces [m].
        """
        self._hy1 = self._read_gdata_signal(
            'hy1', extend(self.cvdims), standalone_file=True)


    @solps_property
    def gsx(self):
        """ ({GRID DIMS}): Area of the face between cell and its left neighbour [m^2].
        """
        self._gsx = self._read_gdata_signal(
            'gsx', extend(self.cvdims), standalone_file=True)

    @property
    def sx(self):
        """ ({GRID DIMS}): Alias of gsx [m^2].
        """
        return  self.gsx

    @solps_property
    def sxl(self):
        """ Sx but mapped into left face.

        Notes
        -----
        <vol>/<hx> is not the same as <vol/hx>
        """
        volavg = self.map(self.vol, 'left')
        hxavg = self.map(self.hx, 'left')
        sxl = volavg/hxavg
        sxl[np.isnan(sxl)] = 0.0
        self._sxl = sxl


    @solps_property
    def gsy(self):
        """ ({GRID DIMS}): Area of the face between cell and its bottom neighbour [m^2].
        """
        self._gsy = self._read_gdata_signal(
            'gsy', extend(self.cvdims), standalone_file=True)

    @property
    def sy(self):
        """ ({GRID DIMS}): Alias of gsy [m^2].
        """
        return  self.gsy


    @solps_property
    def syb(self):
        """ Sy but mapped into bottom face.

        Notes
        -----
        <vol>/<hy> is not the same as <vol/hy>
        """
        volavg = self.map(self.vol, 'bottom')
        hyavg = self.map(self.hy, 'bottom')
        syb = volavg/hyavg
        syb[np.isnan(syb)] = 0.0
        self._syb = syb

    @solps_property
    def syb1(self):
        """ Sy but mapped into bottom face.

        Notes
        -----
        <vol>/<hy> is not the same as <vol/hy>
        THIS IS THE CORRECT ONE to reconstruct e.g. V ExB flow.
        """
        volavg = self.map(self.vol, 'bottom')
        hy1avg = self.map(self.hy1, 'bottom')
        syb1 = volavg/hy1avg
        syb1[np.isnan(syb1)] = 0.0
        self._syb1 = syb1


    @solps_property
    def leftix(self):
        """ ({GRID DIMS}): X index of left neighbour.
        """
        self._leftix = self._read_gdata_signal(
            'leftix', extend(self.cvdims), standalone_file=True).astype(int)
        self._leftix += 1

    @solps_property
    def leftiy(self):
        """ ({GRID DIMS}): Y index of left neighbour.
        """
        self._leftiy = self._read_gdata_signal(
            'leftiy', extend(self.cvdims), standalone_file=True).astype(int)
        self._leftiy += 1

    @solps_property
    def bottomix(self):
        """ ({GRID DIMS}): X index of bottom neighbour.
        """
        self._bottomix = self._read_gdata_signal(
            'bottomix', extend(self.cvdims), standalone_file=True).astype(int)
        self._bottomix += 1

    @solps_property
    def bottomiy(self):
        """ ({GRID DIMS}): Y index of bottom neighbour.
        """
        self._bottomiy = self._read_gdata_signal(
            'bottomiy', extend(self.cvdims), standalone_file=True).astype(int)
        self._bottomiy += 1

    @solps_property
    def rightix(self):
        """ ({GRID DIMS}): X index of right neighbour.
        """
        self._rightix = self._read_gdata_signal(
            'rightix', extend(self.cvdims), standalone_file=True).astype(int)
        self._rightix += 1

    @solps_property
    def rightiy(self):
        """ ({GRID DIMS}): Y index of right neighbour.
        """
        self._rightiy = self._read_gdata_signal(
            'rightiy', extend(self.cvdims), standalone_file=True).astype(int)
        self._rightiy += 1

    @solps_property
    def topix(self):
        """ ({GRID DIMS}): X index of top neighbour.
        """
        self._topix = self._read_gdata_signal(
            'topix', extend(self.cvdims), standalone_file=True).astype(int)
        self._topix += 1

    @solps_property
    def topiy(self):
        """ ({GRID DIMS}): Y index of top neighbour.
        """
        self._topiy = self._read_gdata_signal(
            'topiy', extend(self.cvdims), standalone_file=True).astype(int)
        self._topiy += 1






    ### ------------------------ CONTINUITY EQUATION --------------------------
    ### BASIC ANALYSIS --------------------------------------------------------

    @solps_property
    def resco(self):
        """ ({GRID DIMS}, ns): Residuals of the continuity equation [1/s].
        """
        self._resco = self._read_gdata_signal(
            'b2npc*_resco*', extend(self.cvdims, self.ns))


    @solps_property
    def sna(self):
        """ ({GRID DIMS}, ns): Total particle source [1/s].

        From equations/b2npc9.F:
        b2npc9_sna = snb(:,:,0)+nb*snb(:,:,1)

        These pieces are apparently also saved.
        """
        self._sna = self._read_gdata_signal(
            'b2npc*_sna*', extend(self.cvdims, self.ns),
            enforce="sna"+r"\d\d\d\D")


    @solps_property
    def na(self):
        """ ({GRID DIMS}, ns): Ion density [m^-3].
        """
        self._na = self._read_gdata_signal(
            'b2npc*_na*', extend(self.cvdims, self.ns),
            enforce="na"+r"\d\d\d\D")


    @solps_property
    def na0(self):
        """ ({GRID DIMS}, ns): Initial Ion density [1/s].
        """
        self._na0 = self._read_gdata_signal(
            'b2npc*_na0_*', extend(self.cvdims, self.ns),
            enforce="na0_"+r"\d\d\d\D")


    @solps_property
    def ne(self):
        """ ({GRID DIMS}): Electron density [m^-3].
        """
        self._ne = self._read_gdata_signal(
            'ne', extend(self.cvdims), standalone_file=True)



    ### SOURCES ---------------------------------------------------------------
    @solps_property
    def sna_eir(self):
        """ ({GRID DIMS}, ns): Particle source due to EIRENE neutrals.

        Notes
        -----
        In the future, maybe replace nans of neutrals by - isonuclear sum?
        """
        self._sna_eir = self._read_gdata_signal(
            'b2stbr_sna_eir*', extend(self.cvdims, self.ns))

    @solps_property
    def sna_bc(self):
        """ ({GRID DIMS}, ns): Particle source in the guard cells
        due to boundary conditions.
        """
        self._sna_bc = self._read_gdata_signal(
            'b2stbc_phys_sna*', extend(self.cvdims, self.ns))


    @solps_property
    def sna_ion(self):
        """ ({GRID DIMS}, ns): Particle source due to ionization,
        using fluid neutrals.
        """
        if not self.b2standalone:
            self.log.warning("sna_ion uses fluid neutrals")
        self._sna_ion = self._read_gdata_signal(
            'b2stel_sna_ion*', extend(self.cvdims, self.ns))

    @solps_property
    def sna_rec(self):
        """ ({GRID DIMS}, ns): Particle source due to recombination,
        using fluid neutrals.
        """
        if not self.b2standalone:
            self.log.warning("sna_rec uses fluid neutrals")
        self._sna_rec = self._read_gdata_signal(
            'b2stel_sna_rec*', extend(self.cvdims, self.ns))

    @solps_property
    def sna_cx(self):
        """ ({GRID DIMS}, ns): Particle source due to charge exchange,
        using fluid neutrals.
        """
        if not self.b2standalone:
            self.log.warning("sna_cx uses fluid neutrals")
        self._sna_cx = self._read_gdata_signal(
            'b2stcx_sna*', extend(self.cvdims, self.ns))



    @solps_property
    def sna_input(self):
        """ ({GRID DIMS}, ns): Input Total particle source [1/s].

        Notes
        -----
        Corresponding b2npc*_iout switch required.
        """
        self._sna_input = self._read_gdata_signal(
            'b2npc*_sna_input*', extend(self.cvdims, self.ns))


    @solps_property
    def sna0(self):
        """ ({GRID DIMS}, ns): First component of sna.

        Notes
        -----
        Corresponding b2npc*_iout switch required.
        """
        self._sna0 = self._read_gdata_signal(
            'b2npc*_sna0*', extend(self.cvdims, self.ns),
            enforce="sna0"+r"\d\d\d\D")

    @solps_property
    def sna1(self):
        """ ({GRID DIMS}, ns):  Second component of sna.

        Notes
        -----
        Corresponding b2npc*_iout switch required.
        """
        self._sna1 = self._read_gdata_signal(
            'b2npc*_sna1*', extend(self.cvdims, self.ns),
            enforce="sna1"+r"\d\d\d\D")





    ### ------------------------ MOMENTUM EQUATION ----------------------------
    ### BASIC ANALYSIS --------------------------------------------------------

    @solps_property
    def ua(self):
        """ ({GRID DIMS}, ns): Parallel velocity [m/s]

        Notes
        -----
        In cell center?
        """

        self._ua = self._read_gdata_signal(
            'b2npmo_ua*', extend(self.cvdims, self.ns),
            enforce="ua"+r"\d\d\d\D")


    @solps_property
    def ue(self):
        """ ({GRID DIMS}): Electron parallel velocity [m/s]
        """

        self._ue = self._read_gdata_signal(
            'b2npmo_ue', extend(self.cvdims), standalone_file=True)


    @solps_property
    def smb(self):
        """ ({GRID DIMS}, ns): Total momentum source [N m]
        """

        self._ua = self._read_gdata_signal(
            'b2npmo_smb*', extend(self.cvdims, self.ns))






    ### ---------------ELECTRON HEAT EQUATION ---------------------------------
    ### BASIC ANALYSIS --------------------------------------------------------

    @solps_property
    def te(self):
        """ ({GRID DIMS}): Electron temperature [J].
        """
        self._te = self._read_gdata_signal(
            'b2nph*_te', extend(self.cvdims), standalone_file=True)


    @solps_property
    def reshe(self):
        """ ({GRID DIMS}): Residuals of the electron heat equation [W].
        """
        self._reshe = self._read_gdata_signal(
            'b2nph*_reshe', extend(self.cvdims), standalone_file=True)


    @solps_property
    def she(self):
        """ ({GRID DIMS}): Electron heat external sources,
        i.e, no i-e exchange [W].

        Notes
        -----
        she = she(:,:,0)
            + te*she(:,:,1)
            + ne*she(:,:,2)
            + te*ne*she(:,:,3)

        """
        self._she = self._read_gdata_signal(
            'b2nph*_she', extend(self.cvdims), standalone_file=True)



    @solps_property
    def shei(self):
        """ ({GRID DIMS}): Heat transfer from electron to ions [W].

        Notes
        -----
        According to equations/b2pnh9.F:
        reshe = reshe - shei
        reshi = reshi + shei

        """
        self._shei = self._read_gdata_signal(
            'b2nph*_shei', extend(self.cvdims), standalone_file=True)
    


    @solps_property
    def shet(self):
        """ ({GRID DIMS}): Total electron heat equation source [W].

        Notes
        -----
        According to equations/b2pnh9.F:
        reshe = reshe - shei = she -shei -dfx -dfy
        reshi = reshi + shei = shi +shei -dfx -dfy

        With this implementation,
        fhe[target] - fhe[xpoint] = shet -divy fhey -divx fhex

        It is much closer than using she alone, or + shei, so I assume that
        this is fine :):

        """
        self._shet = self.she - self.shei



    ### COMPONENTS OF SOURCES
    @solps_property
    def she_eir(self):
        """ ({GRID DIMS}):  Electron heat source due to Eirene neutrals [W]

        Notes
        -----

        """
        self._she_eir = self._read_gdata_signal(
            'b2stbr_she_eir', extend(self.cvdims), standalone_file=True)

    @solps_property
    def she_rad(self):
        """ ({GRID DIMS}): Electron heat source due to fluid radiation [W].

        Notes
        -----
        C.103 ion+rec+brm+line

        """
        self._she_rad = self._read_gdata_signal(
            'b2stel_she_rad', extend(self.cvdims), standalone_file=True)

    @solps_property
    def she_fei(self):
        """ ({GRID DIMS}): Electron heat source due to friction with ions
        and the thermal force, or jE [W].

        Notes
        -----

        """
        self._she_fei = self._read_gdata_signal(
            'b2sihs__shefr', extend(self.cvdims), standalone_file=True)

    @property
    def she_fr(self):
        return self.she_fei



    @solps_property
    def she_div_ue(self):
        """ ({GRID DIMS}): Source due to divergence of ue? But then double counted
        with -div??[W].

        Notes
        -----

        """
        self._she_div_ue = self._read_gdata_signal(
            'b2sihs__shedu', extend(self.cvdims), standalone_file=True)

    @solps_property
    def she_div_ecrb(self):
        """ ({GRID DIMS}): Source due to divergence of ExB? But then double counted
        with -div??[W].

        Notes
        -----

        """
        self._she_div_ecrb = self._read_gdata_signal(
            'b2sihs__shedd', extend(self.cvdims), standalone_file=True)


    @solps_property
    def she_phys(self):
        """ ({GRID DIMS}):  Electron heat source due to boundary conditions [W]

        Notes
        -----

        """
        self._she_phys = self._read_gdata_signal(
            'b2stbc_phys_she', extend(self.cvdims), standalone_file=True)









    ### ---------------ION HEAT EQUATION --------------------------------------
    ### BASIC ANALYSIS

    @solps_property
    def ti(self):
        """ ({GRID DIMS}): Ion temperature [J].
        """
        self._ti = self._read_gdata_signal(
            'b2nph*_ti', extend(self.cvdims), standalone_file=True)


    @solps_property
    def reshi(self):
        """ ({GRID DIMS}): Residuals of the ion heat equation [W].
        """
        self._reshi = self._read_gdata_signal(
            'b2nph*_reshi', extend(self.cvdims), standalone_file=True)


    @solps_property
    def shi(self):
        """ ({GRID DIMS}): Total ion heat source [W].

        Notes
        -----
        she = shi(:,:,0)
            + ti*shi(:,:,1)
            + ni*shi(:,:,2)
            + ti*ni*shi(:,:,3)

        """
        self._shi = self._read_gdata_signal(
            'b2nph*_shi', extend(self.cvdims), standalone_file=True)





























####===========================================================================

class EquationsOutputST(BaseEquationsOutput):
    def __init__(self, mother):
        super().__init__(mother)

        self.nx = mother.nx
        self.ny = mother.ny
        self.cvdims = (self.nx, self.ny)
        self.fcdims = (self.nx, self.ny)
        self.vxdims = (self.nx, self.ny)

    def _read_gdata_file(self, varfile):
        """ Reading function for files which contain normal grid data.
        """
        tmp = []
        with open(os.path.join(self.outdir, varfile)) as fr:
            fr.readline()
            for line in fr:
                dummy = [float(var) for var in line.strip().split()[1:]]
                if dummy != []:
                    tmp.append(dummy)
        tmp.reverse()
        return np.array(tmp).T






    ## CONTINUITY =============================================================
    @solps_property
    def dnadt(self):
        """ ({GRID DIMS}, ns): Particle source in the guard cells
        due to boundary conditions.
        """
        self._dnadt = self._read_gdata_signal(
            'b2npc*_dnadt*', extend(self.cvdims, self.ns))


















####===========================================================================
####===========================================================================
####===========================================================================

class EqOut5pt(object):
    ## --------------- FUNCIONS ----------------------------------------------
    def flux_balance_terms(self, f, mask, xsign=1.0):
        """
            Poloidal fluxes can be multiplied by xsign so that they are positive
            if pointing towards the target.
            However, then the dx forms have to be converted back, to give the
            correct result that dxF = Ft-Fu = sources
        """
        if isinstance(xsign, str):
            xsign= self.mother.xsigns[xsign]
        ffn = f.copy()
        ffn[:,:,0] *= xsign
        dfna = self.div_flux(ffn)
        sxdfx = self.sum(dfna[:,:,0], mask, dimension='x')
        sxdfy = self.sum(dfna[:,:,1], mask, dimension='x')
        sydfx = self.sum(dfna[:,:,0], mask, dimension='y')
        sydfy = self.sum(dfna[:,:,1], mask, dimension='y')
        return {
            'f':ffn, 
            'df':dfna,
            'sxdfx':xsign*sxdfx, ## I think?? This should give the correct Ft-Fu = sources
            'sxdfy':sxdfy,
            'sydfx':xsign*sydfx, ## I think??
            'sydfy':sydfy,
        }
    
    def source_balance_terms(self, s, mask):
        src = s.copy()
        sxsrc = self.sum(src, mask, dimension='x')
        sysrc = self.sum(src, mask, dimension='y')
        return {
            'src':src, 
            'sxsrc':sxsrc,
            'sysrc':sysrc,
            'sx':sxsrc,
            'sy':sysrc,
        }






    ### ------------------------ GEOMETRY QUANTITIES --------------------------
    @solps_property
    def bb(self):
        """ ({GRID DIMS}): Total magnetic field [T].
        """
        self._bb = self._read_gdata_signal(
            'bb', extend(self.cvdims), standalone_file=True)

    @solps_property
    def bbx(self):
        """ ({GRID DIMS}): Poloidal magnetic field [T].
        """
        self._bb = self._read_gdata_signal(
            'bbx', extend(self.cvdims), standalone_file=True)

    @solps_property
    def bbz(self):
        """ ({GRID DIMS}): Toroidal magnetic field [T].
        """
        self._bb = self._read_gdata_signal(
            'bbz', extend(self.cvdims), standalone_file=True)


    @solps_property
    def dxy(self):
        """ ({GRID DIMS}): Dxy, Bz/B^2 - Bz/<B>^2_core [1/T].
        
        Notes
        -----
        From b2aux/b2xbzb.F
        """
        bbsum = 0.0
        volsum = 0.0
        #for core, do sums Maybe just multiply by coremasks
        if volsum > 0.0:
            bzb = bbsum/volsum
            self._dxy = self.bbz/self.bb**2 - self.bz/bzb
        else:
            self._dxy = 0.0




                    
    ## CONTINUITY =============================================================
    ### FLUXES ----------------------------------------------------------------
    @solps_property
    def fna(self):
        """ ({FACE DIMS}, 2, ns): Particle flux [1/s]

        Notes
        -----
        Different from both b2fstat* fch and _recon_fna
        For that, use self.fnb
        """
        self._fna = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._fna[...,0,:] = self._read_gdata_signal(
            'b2npc*_fnax*', extend(self.fcdims, self.ns))
        self._fna[...,1,:] = self._read_gdata_signal(
            'b2npc*_fnay*', extend(self.fcdims, self.ns))

    @property
    def fnax(self):
        return self.fna[...,0,:]

    @property
    def fnay(self):
        return self.fna[...,1,:]



    @solps_property
    def fnb(self):
        """ ({FACE DIMS}, 2, ns): Particle flux output to
        b2fstat*/b2fplasmf [1/s].
        """
        self._fnb = np.full(extend(self.fcdims, 2, self.ns), np.nan)

        self._fnb[...,0,:] = self._read_gdata_signal(
            'b2tfnb_fnbx*', extend(self.fcdims, self.ns))
        self._fnb[...,1,:] = self._read_gdata_signal(
            'b2tfnb_fnby*', extend(self.fcdims, self.ns))

    @property
    def fnbx(self):
        return self.fnb[...,0,:]

    @property
    def fnby(self):
        return self.fnb[...,1,:]




    @solps_property
    def fna_real(self):
        """ ({FACE DIMS}, 2, ns): Real particle flux
        """
        self._fna_real = self.fna - self.fna_psch + self.fna_wdia

    @property
    def fnax_real(self):
        return self.fna_real[...,0,:]

    @property
    def fnay_real(self):
        return self.fna_real[...,1,:]




    @solps_property
    def fnb_he(self):
        """ ({FACE DIMS}, 2, ns): Particle flux used for energy equations [1/s].
        """
        self._fnb_he = np.full(extend(self.fcdims, 2, self.ns), np.nan)

        self._fnb_he[...,0,:] = self._read_gdata_signal(
            'b2tfnb_fnb_hex*', extend(self.fcdims, self.ns))
        self._fnb_he[...,1,:] = self._read_gdata_signal(
            'b2tfnb_fnb_hey*', extend(self.fcdims, self.ns))

    @property
    def fnbx_he(self):
        return self.fnb_he[...,0,:]

    @property
    def fnby_he(self):
        return self.fnb_he[...,1,:]





    @solps_property
    def fna_eir(self):
        """ ({FACE DIMS}, 2, ns): Particle flux passed to Eirene [1/s].

        Notes
        -----
        It requires additional switch 'b2tfrn_iout' '1'
        """

        self._fna_eir = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._fna_eir[...,0,:] = self._read_gdata_signal(
            'b2tfrn_fna_eirx*', extend(self.fcdims, self.ns))
        self._fna_eir[...,1,:] = self._read_gdata_signal(
            'b2tfrn_fna_eiry*', extend(self.fcdims, self.ns))

    @property
    def fnax_eir(self):
        return self.fna_eir[...,0,:]

    @property
    def fnay_eir(self):
        return self.fna_eir[...,1,:]







    @solps_property
    def fne_eir(self):
        """ ({FACE DIMS}, 2): Electron particle flux passed to Eirene [1/s].

        Notes
        -----
        It requires additional switch 'b2tfrn_iout' '1'

        Somewhat close to fnax_eir - fchx_par/qe
        """

        self._fne_eir = np.full(extend(self.fcdims, 2), np.nan)
        self._fne_eir[...,0] = self._read_gdata_signal(
            'b2tfrn_fne_eirx', extend(self.fcdims), standalone_file=True)
        self._fne_eir[...,1] = self._read_gdata_signal(
            'b2tfrn_fne_eiry', extend(self.fcdims), standalone_file=True)

    @property
    def fnex_eir(self):
        return self.fne_eir[...,0]

    @property
    def fney_eir(self):
        return self.fne_eir[...,1]











    ### COMPONENTS OF FLUXES ----------------------------------------------------

    @solps_property
    def bxuanax(self):
        self._bxuanax = self._read_gdata_signal(
            'b2tfnb_bxuanax*', extend(self.fcdims, self.ns))

    @property
    def fnax_ua(self):
        return self.bxuanax



    @solps_property
    def vaecrbna(self):
        """ ({FACE DIMS}, 2, ns): Particle flux due to the ExB drift [s^-1]
        """
        self._vaecrbna = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._vaecrbna[...,0,:] = self._read_gdata_signal(
            'b2tfnb_vaecrbnax*', extend(self.fcdims, self.ns))
        self._vaecrbna[...,1,:] = self._read_gdata_signal(
            'b2tfnb_vaecrbnay*', extend(self.fcdims, self.ns))

    @property
    def vaecrbnax(self):
        return self.vaecrbna[...,0,:]

    @property
    def vaecrbnay(self):
        return self.vaecrbna[...,1,:]


    @property
    def fna_ecrb(self):
        return self.vaecrbna

    @property
    def fnax_ecrb(self):
        return self.vaecrbnax

    @property
    def fnay_ecrb(self):
        return self.vaecrbnay


    @solps_property
    def fne_ecrb(self):
        """ Is this the correct definition?
        The flow is ambipolar, and also vecrb is the same
        """
        self._fne_ecrb = np.nansum(self.fnax_ecrb*self.za, axis=-1)

    @property
    def fnex_ecrb(self):
        return self.fne_ecrb[...,0]

    @property
    def fney_ecrb(self):
        return self.fne_ecrb[...,1]




    @solps_property
    def vadiana(self):
        """ ({FACE DIMS}, 2, ns): Particle flux due to the effective
        diamagnetic drift [s^-1]
        """
        self._vadiana = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._vadiana[...,0,:] = self._read_gdata_signal(
            'b2tfnb_vadianax*', extend(self.fcdims, self.ns))
        self._vadiana[...,1,:] = self._read_gdata_signal(
            'b2tfnb_vadianay*', extend(self.fcdims, self.ns))

    @property
    def vadianax(self):
        return self.vadiana[...,0,:]

    @property
    def vadianay(self):
        return self.vadiana[...,1,:]


    @property
    def fna_dia(self):
        return self.vadiana

    @property
    def fnax_dia(self):
        return self.vadianax

    @property
    def fnay_dia(self):
        return self.vadianay


    @solps_property
    def wadiana(self):
        """ ({FACE DIMS}, 2, ns): Particle flux due to the real 
        diamagnetic drift [s^-1]
        """
        self._wadiana = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        naavg =  self.map(self.na, 'left')
        tmp = (self.sxl.T*naavg.T*self.wbdiax.T).T
        tmp[np.isnan(tmp)] = 0.0
        self._wadiana[...,0,:] = tmp
        naavg =  self.map(self.na, 'bottom')
        tmp = (self.sxl.T*naavg.T*self.wbdiay.T).T
        tmp[np.isnan(tmp)] = 0.0
        self._wadiana[...,1,:] = tmp 

    @property
    def wadianax(self):
        return self.wadiana[...,0,:]

    @property
    def wadianay(self):
        return self.wadiana[...,1,:]


    @property
    def fna_wdia(self):
        return self.wadiana

    @property
    def fnax_wdia(self):
        return self.wadianax

    @property
    def fnay_wdia(self):
        return self.wadianay



    @solps_property
    def dpat_mdf_gradna(self):
        """ ({FACE DIMS}, 2, ns): Particle flux due to the denstiy gradients [s^-1]
        """
        self._dpat_mdf_gradna = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._dpat_mdf_gradna[...,0,:] = self._read_gdata_signal(
            'b2tfnb_dPat_mdf_gradnax*', extend(self.fcdims, self.ns))
        self._dpat_mdf_gradna[...,1,:] = self._read_gdata_signal(
            'b2tfnb_dPat_mdf_gradnay*', extend(self.fcdims, self.ns))

    @property
    def dpat_mdf_gradnax(self):
        return self.dpat_mdf_gradna[...,0,:]

    @property
    def dpat_mdf_gradnay(self):
        return self.dpat_mdf_gradna[...,1,:]

    @property
    def fna_gradna(self):
        return self.dpat_mdf_gradna

    @property
    def fnax_gradna(self):
        return self.dpat_mdf_gradnax

    @property
    def fnay_gradna(self):
        return self.dpat_mdf_gradnay



    @solps_property
    def dgradpb(self):
        """ ({FACE DIMS}, 2, ns): Particle flux due to the pressure gradients [s^-1]
        """
        self._dgradpb = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._dgradpb[...,0,:] = self._read_gdata_signal(
            'b2tfnb_dgradpbx*', extend(self.fcdims, self.ns))
        self._dgradpb[...,1,:] = self._read_gdata_signal(
            'b2tfnb_dgradpby*', extend(self.fcdims, self.ns))

    @property
    def dgradpbx(self):
        return self.dgradpb[...,0,:]

    @property
    def dgradpby(self):
        return self.dgradpb[...,1,:]

    @property
    def fna_gradp(self):
        return self.dgradpb

    @property
    def fnax_gradp(self):
        return self.dgradpbx

    @property
    def fnay_gradp(self):
        return self.dgradpby



    @solps_property
    def fnbpsch(self):
        """ ({FACE DIMS}, 2, ns): Particle flux due to Pfirst-Schluter [s^-1]

        Notes
        -----
        Same sign as in equation balance, opposite than raw output, solps p. 440
        """
        self._fnbpsch = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._fnbpsch[...,0,:] = -self._read_gdata_signal(
            'b2tfnb_fnbPSchx*', extend(self.fcdims, self.ns))
        self._fnbpsch[...,1,:] = -self._read_gdata_signal(
            'b2tfnb_fnbPSchy*', extend(self.fcdims, self.ns))

    @property
    def fnbpschx(self):
        return self.fnbpsch[...,0,:]

    @property
    def fnbpschy(self):
        return self.fnbpsch[...,1,:]


    @property
    def fna_psch(self):
        return self.fnbpsch

    @property
    def fnax_psch(self):
        return self.fnbpschx

    @property
    def fnay_psch(self):
        return self.fnbpschy




    @solps_property
    def dpccornax(self):
        """ ({FACE DIMS}, ns): Particle flux due to DPC correction [s^-1]
        """
        self._dpccornax = self._read_gdata_signal(
            'b2tfnb_dpccornax*', extend(self.fcdims, self.ns))

    @property
    def fnax_dpccor(self):
        return self.dpccornax




    @solps_property
    def cvlbnay(self):
        """ ({FACE DIMS}, ns): Particle flux due to anomalous velocity [s^-1]
        """
        self._cvlbnay = self._read_gdata_signal(
            'b2tfnb_cvlbnay*', extend(self.fcdims, self.ns))

    @property
    def fnay_cvl(self):
        return self.cvlbnay

    @property
    def fnay_anml(self):
        return self.cvlbnay

    @property
    def fnay_anomalous(self):
        return self.cvlbnay




    @solps_property
    def fnax_fch_extra(self):
        tmp = np.zeros(extend(self.fcdims, self.ns))
        for v in ['fchax_anomalous', 'fchax_inert',
            'fchax_vispar', 'fchax_visper', 'fchax_visq']:
            tmp = np.nansum(np.vstack((tmp[np.newaxis], get(self,v)[np.newaxis])) ,axis=0)
        tmp /= (self.mother.qe*self.mother.za)
        tmp[np.isinf(tmp)] = 0.0
        tmp[np.isnan(tmp)] = 0.0
        self._fnax_fch_extra = tmp


    @solps_property
    def fnay_fch_extra(self):
        tmp = np.zeros(extend(self.fcdims, self.ns))
        for v in ['fchay_anomalous', 'fchay_inert',
            'fchay_vispar', 'fchay_visper', 'fchay_visq']:
            tmp = np.nansum(np.vstack((tmp[np.newaxis], get(self,v)[np.newaxis])) ,axis=0)
        tmp /= (self.mother.qe*self.mother.za)
        tmp[np.isinf(tmp)] = 0.0
        tmp[np.isnan(tmp)] = 0.0
        self._fnay_fch_extra = tmp


    @solps_property
    def fna_fch_extra(self):
        self._fna_fch_extra = np.zeros(extend(self.fcdims,2, self.ns))
        self._fna_fch_extra[...,0,:] = self.fnax_fch_extra
        self._fna_fch_extra[...,1,:] = self.fnay_fch_extra




    # Velocities --------------------------------------------------------------
    @solps_property
    def vbecrb(self):
        """ ({FACE DIMS}, 2, ns): ExB velocity [m/s]
        """
        self._vbecrb = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._vbecrb[...,0,:] = self._read_gdata_signal(
            'b2tfnb_vbecrbx*', extend(self.fcdims, self.ns))
        self._vbecrb[...,1,:] = self._read_gdata_signal(
            'b2tfnb_vbecrby*', extend(self.fcdims, self.ns))


    @property
    def vbecrbx(self):
        return self.vbecrb[...,0,:]

    @property
    def vbecrby(self):
        return self.vbecrb[...,1,:]


    @property
    def vaecrb(self):
        return self.vbecrb

    @property
    def vaecrbx(self):
        return self.vbecrbx

    @property
    def vaecrby(self):
        return self.vbecrby


    @property
    def vecrb(self):
        """ Logical name, since V is the same for all species. """
        return self.vbecrb

    @property
    def vecrbx(self):
        """ Logical name, since V is the same for all species. """
        return self.vbecrbx

    @property
    def vecrby(self):
        """ Logical name, since V is the same for all species. """
        return self.vbecrby


    @property
    def veecrb(self):
        """ At least in b2fstat*/b2fplasmf, vbecrb and veecrb are identical
        as they should be, according to the theory.
        """
        return self.vbecrb[...,1]

    @property
    def veecrbx(self):
        return self.vbecrb[...,0,1]

    @property
    def veecrby(self):
        return self.vbecrb[...,1,1]




    @solps_property
    def vbdia(self):
        """ ({FACE DIMS}, 2, ns): Effective diamagnetic velocity [m/s]
        """
        self._vbdia = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._vbdia[...,0,:] = self._read_gdata_signal(
            'b2tfnb_vbdiax*', extend(self.fcdims, self.ns))
        self._vbdia[...,1,:] = self._read_gdata_signal(
            'b2tfnb_vbdiay*', extend(self.fcdims, self.ns))


    @property
    def vbdiax(self):
        return self.vbdia[...,0,:]

    @property
    def vbdiay(self):
        return self.vbdia[...,1,:]



    @solps_property
    def wbdia(self):
        """ ({FACE DIMS}, 2, ns): Real diamagnetic velocity [m/s]
        """
        self._wbdia = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._wbdia[...,0,:] = self._read_gdata_signal(
            'b2tfnb_wbdiax*', extend(self.fcdims, self.ns))
        self._wbdia[...,1,:] = self._read_gdata_signal(
            'b2tfnb_wbdiay*', extend(self.fcdims, self.ns))


    @property
    def wbdiax(self):
        return self.wbdia[...,0,:]

    @property
    def wbdiay(self):
        return self.wbdia[...,1,:]





    @solps_property
    def wadia(self):
        """ ({FACE DIMS}, 2, ns): Ion diamagnetic velocity to pass to Eirene [m/s]
        
        Notes
        -----
        Same as self.wbdia
        """
        self._wadia = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._wadia[...,0,:] = self._read_gdata_signal(
            'b2tfrn_wadiax*', extend(self.fcdims, self.ns))
        self._wadia[...,1,:] = self._read_gdata_signal(
            'b2tfrn_wadiay*', extend(self.fcdims, self.ns))

    @property
    def wadiax(self):
        return self.wadia[...,0,:]

    @property
    def wadiay(self):
        return self.wadia[...,1,:]





    @solps_property
    def wedia(self):
        """ ({FACE DIMS}, 2, ns): Electron diamagnetic velocity to pass to Eirene [m/s]
        """
        self._wedia = np.full(extend(self.fcdims, 2), np.nan)
        self._wedia[...,0] = self._read_gdata_signal(
            'b2tfrn_wediax', extend(self.fcdims), standalone_file=True)
        self._wedia[...,1] = self._read_gdata_signal(
            'b2tfrn_wediay', extend(self.fcdims), standalone_file=True)

    @property
    def wediax(self):
        return self.wedia[...,0,:]

    @property
    def wediay(self):
        return self.wedia[...,1,:]








    @solps_property
    def uap(self):
        """ ({GRID DIMS}, ns): Parallel velocity, previous time step? [m/s]
        """
        self._uap = self._read_gdata_signal(
            'b2npmo_uap*', extend(self.fcdims, self.ns),
            enforce="uap"+r"\d\d\d\D")



    # Reconstructions for sanity checks ---------------------------------------


    @property
    def _recon_fnax(self):
        """ Same as eqout fnax """
        tmp = np.zeros(extend(self.fcdims, self.ns))
        for v in self.continuity['fnax'].keys():
            tmp = np.nansum(np.vstack((tmp[np.newaxis], get(self,v)[np.newaxis])) ,axis=0)
        return tmp

    @property
    def _recon_fnbx(self):
        """ Same as b2fstat*/b2fplasmf fnax to rtol=0.1 and mostly to rtol=0.01 """
        tmp = np.zeros(extend(self.fcdims, self.ns))
        for v in self.continuity['fnbx'].keys():
            tmp = np.nansum(np.vstack((tmp[np.newaxis], get(self,v)[np.newaxis])) ,axis=0)
        return tmp



    @property
    def _recon_b2tfnb_vaecrbnax(self):
        """ Visually perfect reconstruction
        Just for testing purposes.
        """
        naavg =  self.map(self.na, 'left')
        recon = (self.sxl.T*naavg.T*self.vbecrbx.T).T
        recon[np.isnan(recon)] = 0.0
        return recon

    @property
    def _recon_b2tfnb_vaecrbnay1(self):
        """ Close enough approximation (within a few %)
        """
        naavg =  self.map(self.na, 'bottom')
        recon = (self.syb1.T*naavg.T*self.vbecrby.T).T
        recon[np.isnan(recon)] = 0.0
        return recon



    @property
    def _recon_b2tfnb_vadianax(self):
        """ Close enough approximation (within a few %)
        Just for testing purposes.
        """
        naavg =  self.map(self.na, 'left')
        recon = (self.sxl.T*naavg.T*self.vbdiax.T).T
        recon[np.isnan(recon)] = 0.0
        return recon

    @property
    def _recon_b2tfnb_vadianay1(self):
        """ Close enough approximation (within a few %)
        """
        naavg =  self.map(self.na, 'bottom')
        recon = (self.syb1.T*naavg.T*self.vbdiay.T).T
        recon[np.isnan(recon)] = 0.0
        return recon








    ## POTENTIAL EQUATION =====================================================
    @solps_property
    def po(self):
        """ ({GRID DIMS}): Electric potential [V].

        Notes
        -----
        Different from b2fstat* po.
        """
        self._po = self._read_gdata_signal(
            'b2npp*_po', extend(self.cvdims), standalone_file=True)

    @solps_property
    def fch(self):
        """ ({FACE DIMS}): Total current [A]

        Notes
        -----
        Different from both b2fstat* fch and _recon_fch
        """
        self._fch = np.full(extend(self.fcdims, 2), np.nan)
        self._fch[...,0] = self._read_gdata_signal(
            'b2npp*_fchx', extend(self.fcdims), standalone_file=True)
        self._fch[...,1] = self._read_gdata_signal(
            'b2npp*_fchy', extend(self.fcdims), standalone_file=True)

    @property
    def fchx(self):
        return self.fch[...,0]

    @property
    def fchy(self):
        return self.fch[...,1]


    ### COMPONENTS OF CURRENTS --------------------------------------------------
    @solps_property
    def fch_px(self):
        self._fch_px = self._read_gdata_signal(
            'b2tfch__fch_px', extend(self.fcdims), standalone_file=True)

    @property
    def fchx_par(self):
        return self.fch_px




    @solps_property
    def fchdia(self):
        """ ({FACE DIMS}): Current due to the diamagnetic drift [A]
        """
        self._fchdia = np.full(extend(self.fcdims, 2), np.nan)
        self._fchdia[...,0] = self._read_gdata_signal(
            'b2tfch__fchdiax', extend(self.fcdims), standalone_file=True)
        self._fchdia[...,1] = self._read_gdata_signal(
            'b2tfch__fchdiay', extend(self.fcdims), standalone_file=True)

    @property
    def fchdiax(self):
        return self.fchdia[...,0]

    @property
    def fchdiay(self):
        return self.fchdia[...,1]


    @property
    def fch_dia(self):
        return self.fchdia

    @property
    def fchx_dia(self):
        return self.fchdiax

    @property
    def fchy_dia(self):
        return self.fchdiay




    @solps_property
    def fchinert(self):
        """ ({FACE DIMS}): Current due to ion inertia [A]
        """
        self._fchinert = np.full(extend(self.fcdims, 2), np.nan)
        self._fchinert[...,0] = self._read_gdata_signal(
            'b2tfch__fchinertx', extend(self.fcdims), standalone_file=True)
        self._fchinert[...,1] = self._read_gdata_signal(
            'b2tfch__fchinerty', extend(self.fcdims), standalone_file=True)

    @property
    def fchinertx(self):
        return self.fchinert[...,0]

    @property
    def fchinerty(self):
        return self.fchinert[...,1]


    @property
    def fch_inert(self):
        return self.fchinert

    @property
    def fchx_inert(self):
        return self.fchinertx

    @property
    def fchy_inert(self):
        return self.fchinerty






    @solps_property
    def fchvispar(self):
        """ ({FACE DIMS}): Current due to the parallel viscosity [A]
        """
        self._fchvispar = np.full(extend(self.fcdims, 2), np.nan)
        self._fchvispar[...,0] = self._read_gdata_signal(
            'b2tfch__fchvisparx', extend(self.fcdims), standalone_file=True)
        self._fchvispar[...,1] = self._read_gdata_signal(
            'b2tfch__fchvispary', extend(self.fcdims), standalone_file=True)

    @property
    def fchvisparx(self):
        return self.fchvispar[...,0]

    @property
    def fchvispary(self):
        return self.fchvispar[...,1]


    @property
    def fch_vispar(self):
        return self.fchvispar

    @property
    def fchx_vispar(self):
        return self.fchvisparx

    @property
    def fchy_vispar(self):
        return self.fchvispary






    @solps_property
    def fchvisper(self):
        """ ({FACE DIMS}): Current due to the perpendicular viscosity [A]
        """
        self._fchvisper = np.full(extend(self.fcdims, 2), np.nan)
        self._fchvisper[...,0] = self._read_gdata_signal(
            'b2tfch__fchvisperx', extend(self.fcdims), standalone_file=True)
        self._fchvisper[...,1] = self._read_gdata_signal(
            'b2tfch__fchvispery', extend(self.fcdims), standalone_file=True)

    @property
    def fchvisperx(self):
        return self.fchvisper[...,0]

    @property
    def fchvispery(self):
        return self.fchvisper[...,1]


    @property
    def fch_visper(self):
        return self.fchvisper

    @property
    def fchx_visper(self):
        return self.fchvisperx

    @property
    def fchy_visper(self):
        return self.fchvispery






    @solps_property
    def fchvisq(self):
        """ ({FACE DIMS}): Current due to the viscosity due to heat (?) [A]
        """
        self._fchvisq = np.full(extend(self.fcdims, 2), np.nan)
        self._fchvisq[...,0] = self._read_gdata_signal(
            'b2tfch__fchvisqx', extend(self.fcdims), standalone_file=True)
        self._fchvisq[...,1] = self._read_gdata_signal(
            'b2tfch__fchvisqy', extend(self.fcdims), standalone_file=True)

    @property
    def fchvisqx(self):
        return self.fchvisq[...,0]

    @property
    def fchvisqy(self):
        return self.fchvisq[...,1]


    @property
    def fch_visq(self):
        return self.fchvisq

    @property
    def fchx_visq(self):
        return self.fchvisqx

    @property
    def fchy_visq(self):
        return self.fchvisqy






    @solps_property
    def fchanml(self):
        """ ({FACE DIMS}): Current due to the viscosity due to heat (?) [A]
        """
        self._fchanml = np.full(extend(self.fcdims, 2), np.nan)
        self._fchanml[...,0] = self._read_gdata_signal(
            'b2tfch__fchanmlx', extend(self.fcdims), standalone_file=True)
        self._fchanml[...,1] = self._read_gdata_signal(
            'b2tfch__fchanmly', extend(self.fcdims), standalone_file=True)

    @property
    def fchanmlx(self):
        return self.fchanml[...,0]

    @property
    def fchanmly(self):
        return self.fchanml[...,1]


    @property
    def fch_anomalous(self):
        return self.fchanml

    @property
    def fchx_anomalous(self):
        return self.fchanmlx

    @property
    def fchy_anomalous(self):
        return self.fchanmly





    @solps_property
    def fchinert_a(self):
        """ ({FACE DIMS}, ns): Current due to ion inertia per species [A]
        """
        self._fchinert_a = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._fchinert_a[...,0,:] = self._read_gdata_signal(
            'b2tfch__fchinert_ax*', extend(self.fcdims, self.ns))
        self._fchinert_a[...,1,:] = self._read_gdata_signal(
            'b2tfch__fchinert_ay*', extend(self.fcdims, self.ns))

    @property
    def fchinert_ax(self):
        return self.fchinert_a[...,0,:]

    @property
    def fchinert_ay(self):
        return self.fchinert_a[...,1,:]


    @property
    def fcha_inert(self):
        return self.fchinert_a

    @property
    def fchax_inert(self):
        return self.fchinert_ax

    @property
    def fchay_inert(self):
        return self.fchinert_ay





    @solps_property
    def fchvispar_a(self):
        """ ({FACE DIMS}, ns): Current due to parallel viscosity per species [A]
        """
        self._fchvispar_a = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._fchvispar_a[...,0,:] = self._read_gdata_signal(
            'b2tfch__fchvispar_ax*', extend(self.fcdims, self.ns))
        self._fchvispar_a[...,1,:] = self._read_gdata_signal(
            'b2tfch__fchvispar_ay*', extend(self.fcdims, self.ns))

    @property
    def fchvispar_ax(self):
        return self.fchvispar_a[...,0,:]

    @property
    def fchvispar_ay(self):
        return self.fchvispar_a[...,1,:]


    @property
    def fcha_vispar(self):
        return self.fchvispar_a

    @property
    def fchax_vispar(self):
        return self.fchvispar_ax

    @property
    def fchay_vispar(self):
        return self.fchvispar_ay





    @solps_property
    def fchvisper_a(self):
        """ ({FACE DIMS}, ns): Current due to perpendicular viscosity per species [A]
        """
        self._fchvisper_a = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._fchvisper_a[...,0,:] = self._read_gdata_signal(
            'b2tfch__fchvisper_ax*', extend(self.fcdims, self.ns))
        self._fchvisper_a[...,1,:] = self._read_gdata_signal(
            'b2tfch__fchvisper_ay*', extend(self.fcdims, self.ns))

    @property
    def fchvisper_ax(self):
        return self.fchvisper_a[...,0,:]

    @property
    def fchvisper_ay(self):
        return self.fchvisper_a[...,1,:]


    @property
    def fcha_visper(self):
        return self.fchvisper_a

    @property
    def fchax_visper(self):
        return self.fchvisper_ax

    @property
    def fchay_visper(self):
        return self.fchvisper_ay





    @solps_property
    def fchvisq_a(self):
        """ ({FACE DIMS}, ns): Current due to viscosity due to q (?) per species [A]
        """
        self._fchvisq_a = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._fchvisq_a[...,0,:] = self._read_gdata_signal(
            'b2tfch__fchvisq_ax*', extend(self.fcdims, self.ns))
        self._fchvisq_a[...,1,:] = self._read_gdata_signal(
            'b2tfch__fchvisq_ay*', extend(self.fcdims, self.ns))

    @property
    def fchvisq_ax(self):
        return self.fchvisq_a[...,0,:]

    @property
    def fchvisq_ay(self):
        return self.fchvisq_a[...,1,:]


    @property
    def fcha_visq(self):
        return self.fchvisq_a

    @property
    def fchax_visq(self):
        return self.fchvisq_ax

    @property
    def fchay_visq(self):
        return self.fchvisq_ay





    @solps_property
    def fchanml_a(self):
        """ ({FACE DIMS}, ns): Anomalous urrent per species [A]
        """
        self._fchanml_a = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._fchanml_a[...,0,:] = self._read_gdata_signal(
            'b2tfch__fchanml_ax*', extend(self.fcdims, self.ns))
        self._fchanml_a[...,1,:] = self._read_gdata_signal(
            'b2tfch__fchanml_ay*', extend(self.fcdims, self.ns))

    @property
    def fchanml_ax(self):
        return self.fchanml_a[...,0,:]

    @property
    def fchanml_ay(self):
        return self.fchanml_a[...,1,:]


    @property
    def fcha_anomalous(self):
        return self.fchanml_a

    @property
    def fchax_anomalous(self):
        return self.fchanml_ax

    @property
    def fchay_anomalous(self):
        return self.fchanml_ay


    # Reconstructions for sanity checks ---------------------------------------


    @property
    def _recon_fchx(self):
        """ Same as eqout fchx """
        tmp = np.zeros(extend(self.fcdims))
        for v in self.potential['fchx'].keys():
            tmp = np.nansum(np.vstack((tmp[np.newaxis], get(self,v)[np.newaxis])) ,axis=0)
        return tmp




























    ## MOMENTUM ===============================================================

    @solps_property
    def fmo(self):
        """ ({GRID DIMS}, ns): Momentum flux [N m]
        """
        self._fmo = np.full(extend(self.fcdims, 2, self.ns), np.nan)
        self._fmo[...,0,:] = self._read_gdata_signal(
            'b2npmo_fmox*', extend(self.fcdims, self.ns))
        self._fmo[...,1,:] = self._read_gdata_signal(
            'b2npmo_fmoy*', extend(self.fcdims, self.ns))

    @property
    def fmox(self):
        return self.fmo[...,0,:]

    @property
    def fmoy(self):
        return self.fmo[...,1,:]














    ## ELECTRON HEAT EQUATION =================================================
    @solps_property
    def fhe(self):
        """ ({GRID DIMS}): Electron heat flux [W].
        """
        self._fhe = np.full(extend(self.fcdims, 2), np.nan)
        self._fhe[...,0] = self._read_gdata_signal(
            'b2nph*_fhex', extend(self.fcdims), standalone_file=True)
        self._fhe[...,1] = self._read_gdata_signal(
            'b2nph*_fhey', extend(self.fcdims), standalone_file=True)

    @property
    def fhex(self):
        return self.fhe[...,0]

    @property
    def fhey(self):
        return self.fhe[...,1]



    @solps_property
    def fhe_eir(self):
        """ ({GRID DIMS}): Electron heat flux passed to Eirene [W].

        Notes
        -----
        It requires additional switch 'b2tfrn_iout' '1'
        """
        self._fhe_eir = np.full(extend(self.fcdims, 2), np.nan)
        self._fhe_eir[...,0] = self._read_gdata_signal(
            'b2tfrn_fhe_eirx', extend(self.fcdims), standalone_file=True)
        self._fhe_eir[...,1] = self._read_gdata_signal(
            'b2tfrn_fhe_eiry', extend(self.fcdims), standalone_file=True)

    @property
    def fhex_eir(self):
        return self.fhe_eir[...,0]

    @property
    def fhey_eir(self):
        return self.fhe_eir[...,1]







    ### COMPONENTS OF SOURCES ---------------------------------------------------




    ### COMPONENTS OF FLUXES ----------------------------------------------------
    @solps_property
    def fhe_32(self):
        """ ({GRID DIMS}): Convective electron heat flux [W].
        """
        self._fhe_32 = np.full(extend(self.fcdims, 2), np.nan)
        self._fhe_32[...,0] = self._read_gdata_signal(
            'b2tfhe__qe_32x', extend(self.fcdims), standalone_file=True)
        self._fhe_32[...,1] = self._read_gdata_signal(
            'b2tfhe__qe_32y', extend(self.fcdims), standalone_file=True)

    @property
    def fhex_32(self):
        return self.fhe_32[...,0]

    @property
    def fhey_32(self):
        return self.fhe_32[...,1]


    @property
    def fhex_conv(self):
        return self.fhe_32[...,0]

    @property
    def fhey_conv(self):
        return self.fhe_32[...,1]





    @solps_property
    def fhe_cond(self):
        """ ({GRID DIMS}): Conductive electron heat flux [W].
        """
        self._fhe_cond = np.full(extend(self.fcdims, 2), np.nan)
        self._fhe_cond[...,0] = self._read_gdata_signal(
            'b2tfhe__qe_ke_gTx', extend(self.fcdims), standalone_file=True)
        self._fhe_cond[...,1] = self._read_gdata_signal(
            'b2tfhe__qe_ke_gTy', extend(self.fcdims), standalone_file=True)

    @property
    def fhex_cond(self):
        return self.fhe_cond[...,0]

    @property
    def fhey_cond(self):
        return self.fhe_cond[...,1]






    ## ATTENTION: File not found??
    ## ATTENTION: Apparently only fchp or aet, and fchp is the most modern one. I think manual is wrong
    ## and what is contained in aet is fchp
    #@solps_property
    #def fhex_fchp(self):
    #    """ ({GRID DIMS}): Reduction of the electron heat flux due to parallel current [W].
    #    """
    #    self._fhex_fchp = self._read_gdata_signal(
    #        'b2tfhe__qe_fchpTex', extend(self.fcdims), standalone_file=True)





    @solps_property
    def fhe_aet(self):
        """ ({GRID DIMS}): Electric field induced electron heat flux [W].
        """
        self._fhe_aet = np.full(extend(self.fcdims, 2), np.nan)
        self._fhe_aet[...,0] = self._read_gdata_signal(
            'b2tfhe__qe_alphaTehx', extend(self.fcdims), standalone_file=True)
        self._fhe_aet[...,1] = self._read_gdata_signal(
            'b2tfhe__qe_alphaTehy', extend(self.fcdims), standalone_file=True)

    @property
    def fhex_aet(self):
        return self.fhe_aet[...,0]

    @property
    def fhey_aet(self):
        return self.fhe_aet[...,1]


    @property
    def fhe_fchp(self):
        return self.fhe_aet

    @property
    def fhex_fchp(self):
        return self.fhe_aet[...,0]

    @property
    def fhey_fchp(self):
        return self.fhe_aet[...,1]





    @solps_property
    def fhe_psch(self):
        """ ({GRID DIMS}): Pfirsch-Schluter electron heat flux [W].
        """
        self._fhe_psch = np.full(extend(self.fcdims, 2), np.nan)
        self._fhe_psch[...,0] = -self._read_gdata_signal(
            'b2tfhe__fhePSchx', extend(self.fcdims), standalone_file=True)
        self._fhe_psch[...,1] = -self._read_gdata_signal(
            'b2tfhe__fhePSchy', extend(self.fcdims), standalone_file=True)

    @property
    def fhex_psch(self):
        return self.fhe_psch[...,0]

    @property
    def fhey_psch(self):
        return self.fhe_psch[...,1]





    @solps_property
    def fhey_fch_stoch(self):
        self._fhey_fch_stoch = self._read_gdata_signal(
            'b2tfhe__fchstochy', extend(self.fcdims), standalone_file=True)








    @solps_property
    def fne_he(self):
        self._fne_he = np.full(extend(self.fcdims, 2), np.nan)
        tmp = np.nansum(self.za*self.fnb_he, axis=-1)
        self._fne_he[...,0] = tmp[...,0] - self.fchx_par/self.qe
        self._fne_he[...,1] = tmp[...,1]

    @property
    def fnex_he(self):
        return self.fne_he[...,0]

    @property
    def fney_he(self):
        return self.fne_he[...,1]







    # Reconstructions for sanity checks ---------------------------------------
    @property
    def _recon_reshe(self):
        """ From equations/b2nph9.F: If something something, then reshe=reshe-shei
        What is the correct term for electron heat equation balance?
        """
        df = self.div_flux(self.fhe)
        return self.she - df[...,0] - df[...,1] - self.shei



    ##ATTENTION: Not yet 
    @property
    def _recon_she(self):
        """ Same as equout she to rtol=0.01 for most """
        tmp = np.zeros(extend(self.fcdims))
        for v in self.electron_heat['she'].keys():
            tmp = np.nansum(np.vstack((tmp[np.newaxis], get(self,v)[np.newaxis])) ,axis=0)
        return tmp



    @property
    def _recon_fhex(self):
        """ Same as equout fhex. """
        tmp = np.zeros(extend(self.fcdims))
        for v in self.electron_heat['fhex'].keys():
            tmp = np.nansum(np.vstack((tmp[np.newaxis], get(self,v)[np.newaxis])) ,axis=0)
        return tmp

    @property
    def _recon_fhey(self):
        """ Same as equout fhey. """
        tmp = np.zeros(extend(self.fcdims))
        for v in self.electron_heat['fhey'].keys():
            tmp = np.nansum(np.vstack((tmp[np.newaxis], get(self,v)[np.newaxis])) ,axis=0)
        return tmp



    @property
    def _recon_fhex_b2f(self):
        """ Same as b2fstat*/b2fplasmf fhex within rtol=0.01 for most entries.
        There might still be some terms wrong, because it does not correspond
        to the definition.
        """
        tmp = np.zeros(extend(self.fcdims))
        for v in self.electron_heat['fhex_b2f'].keys():
            tmp = np.nansum(np.vstack((tmp[np.newaxis], get(self,v)[np.newaxis])) ,axis=0)
        return tmp



    @property
    def _recon_fnex_he_alt(self):
        """ Very close to fnex_he but not exactly the same everywhere.
        """
        tel = self.map(self.te, 'left')
        return (2/3)*self.fhex_32/tel




    @property
    def _recon_fhex_32(self):
        """ Very close to fhex_32 but not exactly the same everywhere.
        """
        tel = self.map(self.te, 'left')
        return 1.5*self.fnex_he*tel










    ## ION HEAT ===============================================================
    @solps_property
    def fhi(self):
        """ ({GRID DIMS}): Ion
        """
        self._fhi = np.full(extend(self.fcdims, 2), np.nan)
        self._fhi[...,0] = self._read_gdata_signal(
            'b2nph*_fhix*', extend(self.fcdims), standalone_file=True)
        self._fhi[...,1] = self._read_gdata_signal(
            'b2nph*_fhiy*', extend(self.fcdims), standalone_file=True)

    @property
    def fhix(self):
        return self.fhi[...,0]

    @property
    def fhiy(self):
        return self.fhi[...,1]




    @solps_property
    def fhi_eir(self):
        """ ({GRID DIMS}): Ion heat flux passed to Eirene [W].

        Notes
        -----
        It requires additional switch 'b2tfrn_iout' '1'
        """
        self._fhi_eir = np.full(extend(self.fcdims, 2), np.nan)
        self._fhi_eir[...,0] = self._read_gdata_signal(
            'b2tfrn_fhi_eirx', extend(self.fcdims), standalone_file=True)
        self._fhi_eir[...,1] = self._read_gdata_signal(
            'b2tfrn_fhi_eiry', extend(self.fcdims), standalone_file=True)

    @property
    def fhix_eir(self):
        return self.fhi_eir[...,0]

    @property
    def fhiy_eir(self):
        return self.fhi_eir[...,1]








    ### COMPONENTS OF FLUXES ----------------------------------------------------

    # Reconstructions for sanity checks ---------------------------------------

















    ## FUNCTIONS ==============================================================
    def div_flux(self, ffn):
        """ Divergence of INTEGRATED fluxes (NOT flux densities):
        Radial divergence of radial flux and Poloidal divergence of poloidal
        flux, given as ffn = ffn[nx,ny,2,ns].

        Flux is assumed to be integrated over the surface of the cell.
        As I understand it:

        [divx(sqrt(g)/hx Flux), divy(sqrt(g)/hy Flux) ]

        Copied from solvers/b2ursc.F

        Notes
        -----
        Is the minus sign correct? Is Moulton's div the same?
        It is "contribution to residuals from ffn[xy]", which according to the
        initial equation is resb = rhs - ffn. Thus -div is calculated.


        Parameters
        ----------
        ffn: (nx, ny, 2, ...)
            Flux which divergence is to be calculated.
            ffn[:,:,0] should be the poloidal flux.
            ffn[:,:,1] should be the radial flux.

        Returns
        -------
        (nx, ny, 2, ...)
            [:,:,0] poloidal divergence of the poloidal flux.
            [:,:,1] radial  divergence of the radial flux.
        """

        #div = np.zeros((list(itertools.chain.from_iterable(
        #    [(self.nx,self.ny,2),ffn.shape[3:]]))))
        div = np.zeros(extend((self.nx, self.ny, 2) , ffn.shape[3:])) #Same??
        for ix in range(self.nx):
            for iy in range(self.ny):
                rix = int(self.rightix[ix,iy])
                riy = int(self.rightiy[ix,iy])
                lix = int(self.leftix[ix,iy])

                tix = int(self.topix[ix,iy])
                tiy = int(self.topiy[ix,iy])
                biy = int(self.bottomiy[ix,iy])

        # -divx(Fx) # I think it is x -1.0
                if lix == -1:
                    div[ix,iy,0] = -ffn[rix,riy,0]

                elif rix == self.nx:
                    div[ix,iy,0] = ffn[ix,iy,0]
                else:
                    div[ix,iy,0] = ffn[ix,iy,0] -ffn[rix,riy,0]

        # -divy(Fy) # I think it is x -1.0
                if biy == -1:
                    div[ix,iy,1] = -ffn[tix,tiy,1]

                elif tiy == self.ny:
                    div[ix,iy,1] = ffn[ix,iy,1]
                else:
                    div[ix,iy,1] = ffn[ix,iy,1] -ffn[tix,tiy,1]

        return -div


    def map(self, var, to, now='center', method='average'):
        """ Map center quantities to faces and viceversa.

        Parameters
        ----------
        var: (nx,ny,...)
            Quantity to be mapped.

        to: str
            Can be 'left', 'bottom', 'center' or 'faces'.
            If 'left' or 'bottom' then assume quantity is 'center'.
            If 'center' then now is required.
            If 'faces', then output is (...,2) with 0 being 'left'
            and (...,1) being 'bottom'.

        now: str, optional.
            What is the current position of the quantity.
            Only required if to is 'center'.

        method: str, optional.
            What type of mapping is desired.
            'interpolation' means linear interpolation using h[x|y].
            'volume' means "volumen interpolation" using vol.
            'average' or 'avg' means (Cell+Neighbour)/2.0

        Returns
        -------
        (nx,ny,...)
            Mapped quantity to the corresponding place.

        Notes
        -----
        The division of averages is not the same as the average of the
        division.
        I.e.:
            < vol/hx > =! <vol>/<hx>

        """
        tmp = np.zeros_like(var)
        dix = getattr(self, to+'ix')
        diy = getattr(self, to+'iy')
        for ix in range(self.nx):
            for iy in range(self.ny):
                if   to.lower() == 'left'   and dix[ix,iy]<0:
                    continue
                elif   to.lower() == 'bottom' and diy[ix,iy]<0:
                    continue

                #Review the indexing of these. matlab is 1, python 0, mds -1
                elif to.lower() == 'right'  and dix[ix,iy]>self.nx:
                    continue
                elif to.lower() == 'top'    and diy[ix,iy]>self.ny:
                    continue

                varc = var[ix,iy]
                varn = var[dix[ix,iy],diy[ix,iy]]

                if method.lower() == 'volume':
                    dvc = self.vol[ix,iy]
                    dvn = self.vol[dix[ix,iy],diy[ix,iy]]
                    tmp[ix,iy] = (varc*dvn + varn*dvc)/(dvn+dvc)
                elif 'average' or 'avg':
                    tmp[ix,iy] = (varc + varn)/2.0

        return tmp




    ## Replace sum with this? While this is true for certain cases
    ## all the function do is summing already integrated quantities.
    #def integrate(self, var, gridmask, dimension='x'):
    #    return sum(self, var, gridmask, dimension)

    #ATTENTION: Improve docstring.
    ## ATTENTION 2024: Review logic
    def sum(self,var,gridmask, dimension='x'):
        """
        Selection method for the region should be pretty similar to that
        of the average function.
        Change creation of mask as to allow creation of sums with
        more than one dimensions (nx,ns), (ny,ns), etc.
        Add an automatic system to detect in which side of the DDNU
        or if in USN, etc, to get the reverse way of increasing X

        Parameters
        ----------
        var: (nx, ny, ...)
            Quantity to be summed up.

        gridmask: (nx, ny)
            True for indices that are added to the sum.

        dimension: {'x', 'y'}, optional
            Sum in the 'x' poloidal, or 'y' radial dimension.

        Returns
        -------
        (ny, ...) or (nx, ...)
            Sum of quantity along the dimension axis.
        """
        nx = var.shape[0]
        ny = var.shape[1]
        if gridmask is None:
            gridmask = np.ones((nx, ny), dtype=np.bool)
        gmask = gridmask.astype(int)
        if dimension == 'x':
            tmp = np.full(extend(ny,var.shape[2:]), np.nan)
            msk = np.any(gridmask, axis=0)
            for iy in range(ny):
                if msk[iy] == False:
                    continue
                tmp[iy] = np.nansum((gmask[:,iy].T*var[:,iy].T).T, axis=0)
            return tmp

        elif dimension == 'y':
            tmp = np.full(extend(nx,var.shape[2:]), np.nan)
            msk = np.any(gridmask, axis=1)
            for ix in range(nx):
                if msk[ix] == False:
                    continue
                tmp[ix] = np.nansum((gmask[ix].T*var[ix].T).T, axis=0)
            return tmp

        else:
            self.log.error("Dimension must be either 'x' or 'y'")
            return


    def delta(self, svar):
        """
        Calculates delta of svar as var - var(i/j-1)
        Similar to gradient but without lenght scaling.
        """
        if isinstance(svar, str):
            var = getattr(self, svar)
        else:
            var = svar.copy()
        btix = self.bottomix
        btiy = self.bottomiy
        ltix = self.leftix
        ltiy = self.leftiy
        deltas = np.zeros(list(itertools.chain.from_iterable(
                          [var.shape,[2]])))
        tmp = np.zeros_like(var)
        for ix in range(var.shape[0]):
            for iy in range(1,var.shape[1]):
                bix = btix[ix,iy]
                biy = btiy[ix,iy]
                tmp[ix,iy] = var[ix,iy] - var[bix,biy]
        deltas[...,1] = tmp
        tmp = np.zeros_like(var)
        for ix in range(var.shape[0]):
            for iy in range(1,var.shape[1]):
                lix = ltix[ix,iy]
                liy = ltiy[ix,iy]
                tmp[ix,iy] = var[ix,iy] - var[lix,liy]
        deltas[...,0] = tmp
        return deltas




    def residuals(self, sfn, ffn):
        """
        Python translation from b2ursc.F

        *.documentation
        *
        *  1. purpose
        *
        *     B2URSC computes the residual of a scalar conservation equation.
        *
        *
        *  3. description (see also routine b2cdca)
        *
        *     This routine computes the residual of a scalar conservation
        *     equation, integrated over cells of the b2 mesh. The equation has
        *     the form
        *       div(flux) = rhs ,
        *     and the residual is defined as
        *       resfn = rhs-div(flux) .
        *     The flux is specified by ffn, and the right hand side has the
        *     linearised representation
        *       rhs(,) = sfn(,,0)+sfn(,,1)*fun(,) .
        *     This linearisation may include a contribution from an implicit
        *     discretisation in time.
        *
        *     The input argument ffn specifies fluxes integrated over cell
        *     faces, and sfn specifies source coefficients integrated over
        *     cell volumes. Therefore, the expression for the integrated
        *     residual on any cell does not involve any geometric factors.


        In this case, all species are calculated with the same call.
        sfn here is the recomposed source and not the liniarized pieces.

        IMPORTANT NOTE:
        --------------
        divy(fnay) is different from debugger and thus might be wrong in
        here or just because of big numbers / small numbers arithmetics.
        """
        # Sources
        resfn = sfn.copy()

        #Divergence of fluxes
        div = self.div_flux(ffn)
        resfn += div[...,0,:] + div[...,1,:]

        return resfn















####===========================================================================

class EqOut9pt(object):

    ## CONTINUITY =============================================================
    @solps_property
    def fna(self):
        """ ({GRID DIMS}, 2, 2, ns):
        """
        self._fna = np.full(extend(self.fcdims, 2, 2, self.ns), np.nan)

        self._fna[...,0,0,:] = self._read_gdata_signal(
            'b2npc*_fnax0*', extend(self.fcdims, self.ns))
        self._fna[...,0,1,:] = self._read_gdata_signal(
            'b2npc*_fnaxy*', extend(self.fcdims, self.ns))
        self._fna[...,1,0,:] = self._read_gdata_signal(
            'b2npc*_fnayx*', extend(self.fcdims, self.ns))
        self._fna[...,1,1,:] = self._read_gdata_signal(
            'b2npc*_fnay0*', extend(self.fcdims, self.ns))


    @solps_property
    def fnb(self):
        """ ({GRID DIMS}, 2, ns):

        Notes
        -----
        Are they summed over both radial and poloidal surfaces??
        """
        self._fnb = np.full(extend(self.fcdims, 2, self.ns), np.nan)

        self._fnb[...,0,:] = self._read_gdata_signal(
            'b2tfnb_fnbx*', extend(self.fcdims, self.ns))
        self._fnb[...,1,:] = self._read_gdata_signal(
            'b2tfnb_fnby*', extend(self.fcdims, self.ns))





    ## ELECTRON HEAT ==========================================================
    ## ATTENTION: Does not work
    @solps_property
    def fhe(self):
        """ ({GRID DIMS}, 2, 2):
        """
        self._fhe = np.full(extend(self.fcdims, 2, 2), np.nan)

        self._fhe[...,0,0] = self._read_gdata_signal(
            'b2nph*_fhex', extend(self.fcdims),
            enforce="fhex"+r"\.")
        self._fhe[...,0,1] = self._read_gdata_signal(
            'b2nph*_fhexy*', extend(self.fcdims))
        self._fhe[...,1,0] = self._read_gdata_signal(
            'b2nph*_fheyx*', extend(self.fcdims))
        self._fhe[...,1,1] = self._read_gdata_signal(
            'b2nph*_fhey', extend(self.fcdims),
            enforce="fhey"+r"\.")





    ## ION HEAT ===============================================================
    @solps_property
    def fhi(self):
        """ ({GRID DIMS}, 2, 2):
        """
        self._fhi = np.full(extend(self.fcdims, 2, 2), np.nan)

        self._fhi[...,0,0] = self._read_gdata_signal(
            'b2nph*_fhix', extend(self.fcdims),
            enforce="fhix"+r"\.")
        self._fhi[...,0,1] = self._read_gdata_signal(
            'b2nph*_fhixy*', extend(self.fcdims))
        self._fhi[...,1,0] = self._read_gdata_signal(
            'b2nph*_fhiyx*', extend(self.fcdims))
        self._fhi[...,1,1] = self._read_gdata_signal(
            'b2nph*_fhiy', extend(self.fcdims),
            enforce="fhiy"+r"\.")


















####===========================================================================

class EquationsOutputUS(BaseEquationsOutput):
    def __init__(self, mother):
        super().__init__(mother)

        self.ncv = mother.ncv
        self.nfc = mother.nfc
        self.nvx = mother.nvx
        self.cvdims = self.ncv
        self.fcdims = self.nfc
        self.vxdims = self.nvx

    def _read_gdata_file(self, varfile):
        """ Reading function for files which contain normal grid data.
        """

        tmp = []
        with open(os.path.join(self.outdir, varfile)) as fr:
            for line in fr:
                tmp.append(line.strip().split()[-1])
        return tmp


    ## CONTINUITY =============================================================
    @solps_property
    def fna(self):
        """ ({FACE DIMS}, 2, ns):
        """
        self._fna = np.full(extend(self.fcdims, 2, self.ns), np.nan)

        self._fna[...,0,:] = self._read_gdata_signal(
            'b2npc*_fna_th*', extend(self.fcdims, self.ns))
        self._fna[...,1,:] = self._read_gdata_signal(
            'b2npc*_fna_r*', extend(self.fcdims, self.ns))

    @solps_property
    def fnb(self):
        """ ({FACE DIMS}, 2, ns): 

        Notes
        -----
        Are they summed over both radial and poloidal surfaces??
        """
        self._fnb = np.full(extend(self.fcdims, 2, self.ns), np.nan)

        self._fnb[...,0,:] = self._read_gdata_signal(
            'b2tfnb_fnb_th*', extend(self.fcdims, self.ns))
        self._fnb[...,1,:] = self._read_gdata_signal(
            'b2tfnb_fnb_r*', extend(self.fcdims, self.ns))







    ## ELECTRON HEAT ==========================================================
    @solps_property
    def fhe(self):
        """ ({FACE DIMS}, 2):
        """
        self._fhe = np.full(extend(self.fcdims, 2), np.nan)

        self._fhe[...,0] = self._read_gdata_signal(
            'b2nph*_fhe_th*', extend(self.fcdims))
        self._fhe[...,1] = self._read_gdata_signal(
            'b2nph*_fhe_r*', extend(self.fcdims))







    ## ION HEAT ===============================================================
    @solps_property
    def fhi(self):
        """ ({FACE DIMS}, 2):
        """
        self._fhi = np.full(extend(self.fcdims, 2), np.nan)

        self._fhi[...,0] = self._read_gdata_signal(
            'b2nph*_fhi_th*', extend(self.fcdims))
        self._fhi[...,1] = self._read_gdata_signal(
            'b2nph*_fhi_r*', extend(self.fcdims))



