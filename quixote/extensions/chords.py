# Common modules
import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.patches import Polygon,Circle
from matplotlib.collections import PatchCollection
from collections import OrderedDict
import glob

# Quixote modules
from ..tools import solps_property, priority, read_config
from ..tools import get, extend

from ..tools import point_in_cell
from ..tools import distance_cylindrical_coordinates
from ..tools import parse_los_file

from ..tools import Intersection, LineSegment

# =============================================================================

class Chords(object):
    def __init__(self, mother, **kwargs):
        """
        For WG, which requires b2.user.params anyway,
        take rzomp and rzimp as OMP-1 and IMP-1 automatically??
        """
        self.mother = mother
        self.grid_type = self.mother.grid_type
        self.log = self.mother.log
        self.config = mother.config

        # Combine default files with user given files.
        self.avail = self._find_default_los_coord_files()
        user_los = priority(['losfile','coords'], kwargs, OrderedDict())
        if isinstance(user_los,str):
            user_los = [user_los]
        if isinstance(user_los, list):
            user_los.reverse()
            for fl in user_los:
                if os.path.isfile(fl) and os.stat(fl).st_size > 0:
                    path = os.path.abspath(fl)
                    name = os.path.basename(path)
                    self.avail[name] = path
        elif isinstance(user_los, dict):
            for name, path in user_los.items():
                if os.path.isfile(path) and os.stat(path).st_size > 0:
                    self.avail[name] = os.path.abspath(path)

        self.chords = OrderedDict()
        self._read_coords_files()



    ## ---------    EXTERNAL FUNCTIONS    ------------------------------------
    def add_chord(self, losname, start, end):
        if losname in self.chords:
            self.log.info("Duplicated LOS '{0}'. Chord updated.".format(losname))
        if self.grid_type=='structured':
            self.chords[losname] = ChordStructured(
                self.mother, losname, start, end)
        elif self.grid_type=='unstructured':
            self.chords[losname] = ChordUnstructured(
                self.mother, losname, start, end)



    ## ATTENTION: Review logic on this one.
    ## Why redo? But also, is it such a huge problem?
    def add_los_file(self, coords):

        if isinstance(coords, str):
            path = os.path.abspath(coords)
            name = os.path.basename(path)
            coords = [{}]
            coords[0][name] = path

        elif isinstance(coords, list):
            for element in coords:
                if isinstance(element, str):
                    path = os.path.abspath(element)
                    name = os.path.basename(path)
                    element = {}
                    element[name] = path

        for los_file in coords:
            for name, path in los_file.items():
                if os.path.isfile(path) and os.stat(path).st_size > 0:
                    self.avail[name] = os.path.abspath(path)

        ## Why not simply add them instead of having to redo the thing?
        ## in the previosu step, after adding to avail
        ## just parse_los_file(self.avail[name])
        ## and for los in ... add_chord
        self.chords = OrderedDict()
        self._read_coords_files()
    
        return


    ## -----------  DUNDER FUNCTIONS -----------------------------------------

    def __getitem__(self,key): # Allows acessing elements via Chords()[key]
        return self.chords[key]

    def __iter__(self):
        return iter(self.chords.items())

    def __repr__(self):        # Default print output
        return ( 'Chords '
                 + str(self.chords.keys()).replace('[','{').replace(']','}') )

    #Imitation of dict iterators
    def items(self):
        return self.chords.items()

    def keys(self):
        return self.chords.keys()

    def values(self):
        return self.chords.values()




    ## ---------    INTERNAL FUNCTIONS    ------------------------------------
    def _find_default_los_coord_files(self):
        """ Find files with the default pattern in the default locations
        given by the configuration file.
        '.' should be interpreted as rundir path.
        """
        patterns = self.config['Chords']['files']
        patterns.reverse()
        files = OrderedDict()
        for pat in patterns:
            try:
                #patterns given for default assumed relative to case
                pat = os.path.join(self.mother.directory,pat)
                tmp = glob.glob(pat)
                for fl in tmp:
                    if os.path.isfile(fl) and os.stat(fl).st_size > 0:
                        path = os.path.abspath(fl)
                        name = os.path.basename(path)
                        files[name] = path
            except:
                self.log.error("Pattern '{0}' caused an error".format(pat))
        return files



    def _read_coords_files(self):
        for name, path in self.avail.items():
            loses = parse_los_file(path)
            for losname, v in loses.items():
                self.add_chord(losname, v[0], v[1])








## ATTENTION:
## SUBSTITUE CHORDS BY CLIFFORD ALGEBRA ELEMENTS??
## With  conformal geometric algebra, it seems that intersections can be
## easily generalized and calculated.
## Using for example the python package clifford.


# =============================================================================

class Chord(object):
    """
    An object containing the information for one line of sight (LOS), and
    methods for plotting and for the calculation of line integrated and
    line averaged values.
    """

    def __init__(self, mother, name, start, end):
        """
        Parameters:
        -----------
        mother : SolpsData object
            The solpspy run which is to be considered.

        name : str
            The name of the line of sight

        start : (r,z,phi) np.array or list
            LOS start point

        end : (r,z,phi) np.array or list
            LOS end point

        los : (2,3) numpy.ndarray
            The line of sight, specified as an numpy.ndarray with 
            [0]: starting point (r,z,rho).
            [1]: ending   point (r,z,rho).


        Notes
        -----
        In structured, guard cells are still considered.
        Use mother.rlcl to avoid that.
        icell for line_segments in guarding cells should be -999999
        """

        self.mother  = mother
        self.log   = self.mother.log
        self.config = mother.config
        self.grid_type = mother.grid_type

        self.name  = name            # Name of the line of sight
        self.start = np.array(start) # LOS start point: (r,z,phi) vector
        self.end   = np.array(end)   # LOS end point:   (r,z,phi) vector
        self.los   = np.vstack([self.start,self.end])




    @solps_property
    def indices(self):
        """ List of indices of cells which the LOS intersects.
        The list is not ordered, and not unique. It provides every single
        index as encountered by the LOS.
        """
        tmp = []
        for ls in self.line_segments:
            if np.all(np.array(ls.icell)>=0):
                tmp.append(ls.icell)
        if self.grid_type =='unstructured':
            self._indices = np.array(tmp)
        elif self.grid_type=='structured':
            self._indices = np.array(tmp)




    ## ATTENTION: This is much better than the other representation, no?
    @solps_property
    def bins(self):
        """ (2*nls): Bins corresponding to line segments.
        Line segment i corresponds to bins[2i],bins[2i+1]

        Notes
        -----
        Use bins and binned together.

        Do not parse out bins of existing line_segments.
        Either eliminate line_segments of guarding cells,
        or keep them but set y values to nan when needed.
        Otherwise, no longer true that ibin and ils are
        simply related.
        Simply parse out in min, max, etc.
        """
        x = [0]
        for ls in self.line_segments:
            x.append(x[-1])
            x.append(x[-1] + ls.length)
        self._bins = np.array(x[1:])



    ## ATTENTION: This is much better than the other representation, no?
    def binned(self, var, **kwargs):
        """ (2*nls, extra dims): Data for easier representation of bins.
        Otherwise use evaluate (which will now be only centers).

        Note
        ----
        Use bins and binned together.
        """
        missing = priority(['missing','replace'],
            kwargs, self.config['Chords']['missing'])

        if isinstance(var, str):
            var = get(self.mother, var)
        if self.grid_type=='unstructured':
            extra_dims = var.shape[1:]
        elif self.grid_type=='structured':
            extra_dims = var.shape[2:]

        y = [np.full(extra_dims, missing)]
        for ls in self.line_segments:
            if np.any(np.array(ls.icell) < 0): 
                val = np.full(extra_dims, missing)
            else:
                if self.grid_type =='unstructured':
                    val = var[ls.icell]
                elif self.grid_type == 'structured':
                    val = var[ls.icell[0], ls.icell[1]]
            y.append(val)
            y.append(val)
        return np.array(y[1:])



    ## Much more clear, no???
    @solps_property
    def lsc(self):
        """ (nls): Position of the line segment center, corresponding
        with the center of the bins.
        """
        self._lsc = np.zeros(len(self.line_segments))
        for i in range(self._lsc.shape[0]):
            self._lsc[i] = (self.bins[2*i]+self.bins[2*i+1])/2.0

    @property
    def ds(self):
        """ (nls): Alias of lsc. """
        return self.lsc


    def evaluate(self, var, **kwargs):
        missing = priority(['missing','replace'],
            kwargs, self.config['Chords']['missing'])
        if isinstance(var, str):
            var = get(self.mother, var)
        if self.grid_type=='unstructured':
            extra_dims = var.shape[1:]
        elif self.grid_type=='structured':
            extra_dims = var.shape[2:]

        dims = extend(self.lsc.shape[0], extra_dims)
        y = np.full(dims, missing)
        for i, ls in enumerate(self.line_segments):
            if np.any(np.array(ls.icell) < 0): 
                y[i] = np.full(extra_dims, missing)
            else:
                if self.grid_type =='unstructured':
                    y[i] = var[ls.icell]
                elif self.grid_type == 'structured':
                    y[i] = var[ls.icell[0], ls.icell[1]]
        return y



    def maximum(self, var):
        return np.nanmax(self.evaluate(var), axis=0)

    def minimum(self, var):
        return np.nanmin(self.evaluate(var), axis=0)


    def average(self, var):
        """Calculate the line average of the given value for the LOS.
        """
        if self.cdlength == 0:
            return 0
        else:
            return self.integrate(var) / self.cdlength

    def integrate(self, var=None):
        """
        To add to test:
        integrate()
        tmp = np.ones(GRID, 2, 9)
        integrate(tmp) ## Should give (2,9) with same result as ()
        and then some quantities, but the first ones are good
        for calibration.
        """
        if var is None:
            if self.grid_type=='unstructured':
                var = np.ones(self.mother.nci)
            elif self.grid_type=='structured':
                var = np.ones((self.mother.nx, self.mother.ny))
        elif isinstance(var, str):
            var = get(self.mother, var)

        if self.grid_type=='unstructured':
            extra_dims = var.shape[1:]
        elif self.grid_type=='structured':
            extra_dims = var.shape[2:]
        integral = np.zeros(extra_dims)

        for ls in self.line_segments:
            if np.any(np.array(ls.icell) < 0): 
                continue
            if self.grid_type=='unstructured':
                integral += ls.length*var[ls.icell]
            elif self.grid_type=='structured':
                integral += ls.length*var[ls.icell[0], ls.icell[1]]

        return integral




    @solps_property
    def line_segments(self):
        """A list of LineSegment objects."""
        self._line_segments = self._get_line_segments()

    @property
    def ls(self):
        return self.line_segments


    ## ATTENTION:
    # Shouldn't this be distance_toroidal_coordinates?
    @solps_property
    def length(self):
        """Total length of the line of sight (including
        parts outside the computational domain)."""
        self._length = distance_cylindrical_coordinates(self.start, self.end)

    @solps_property
    def cdlength(self):
        """Length of the line of sight within the computational domain."""
        self._cdlength = self.integrate()










    ##ATTENTION: 
    ## Getting rid off duplicates is good, but ordering??
    ## That possibly eliminates a lot of nuance, no?
    def _get_line_segments(self):
        """Returns a list of line segments which are calculated
        from the intersections of the LOS with the grid.
        """

        self.intersections = self._get_intersections() ##Stru correct
        self.intersections = self._sort_unique_intersections()

        if len(self.intersections) == 2:
            self.log.info("Line of sight '{}' has no".format(self.name)+
                "intersection with the computational domain.")
            return []
        if self.intersections[0].l < 0:
            self.log.info("Line of sight '{}': ".format(self.name) +
                "startpoint inside of the grid!")
        if self.intersections[-1].l > 1:
            self.log.info("Line of sight '{}': ".format(self.name) +
                "endpoint inside of the grid!")

        line_segments = []
        for i in range(len(self.intersections)-1):
            
            start = np.array([self.intersections[i].r,
                              self.intersections[i].z,
                              self.intersections[i].p])

            end   = np.array([self.intersections[i+1].r,
                              self.intersections[i+1].z,
                              self.intersections[i+1].p])

            lcenter = np.average([self.intersections[i].l,
                                  self.intersections[i+1].l])

            center = self._get_point_along_line(lcenter)[:2] # (r,z)

            if self.grid_type == 'unstructured':
                for ici in (self.intersections[i].cells
                    +self.intersections[i+1].cells):
                    if ici < 0:
                        continue
                    if point_in_cell(self.mother.grid[ici],center):
                        nci = ici
                        break
                else:
                    nci = -999999
                line_segments.append(LineSegment(start, end, nci))

            elif self.grid_type == 'structured':
                for x,y in (self.intersections[i].cells
                    +self.intersections[i+1].cells):
                    if x < 0 or y < 0:
                        continue
                    ##Does this remove guarding cells??
                    elif not self.mother.rlcl[x,y]:
                        continue
                    if point_in_cell(self.mother.grid[x,y], center):
                        ix = x
                        iy = y
                        break
                else:
                    ix = iy = -999999
                line_segments.append(LineSegment(start, end, [ix,iy]))

            ## Debugging:
            self.log.debug("From intersections")
            self.log.debug("r: {}".format(self.intersections[i].r))
            self.log.debug("z: {}".format(self.intersections[i].z))
            self.log.debug("p: {}".format(self.intersections[i].p))
            self.log.debug("l: {}".format(self.intersections[i].l))
            self.log.debug("cells: {}".format(self.intersections[i].cells))

            self.log.debug("and")
            self.log.debug("r: {}".format(self.intersections[i+1].r))
            self.log.debug("z: {}".format(self.intersections[i+1].z))
            self.log.debug("p: {}".format(self.intersections[i+1].p))
            self.log.debug("l: {}".format(self.intersections[i+1].l))
            self.log.debug("cells: {}".format(self.intersections[i+1].cells))

            self.log.debug("Line segment")
            self.log.debug("start: {}".format(line_segments[-1].start))
            self.log.debug("end: {}".format(line_segments[-1].end))
            self.log.debug("icell: {}".format(line_segments[-1].icell))
            self.log.debug("")

        ## IPAR: How to check results myself
        # Check result:
        for ls in line_segments[1:-1]:
            if np.any(np.array(ls.icell)<0):
                self.log.info("Line of sight '{}'".format(self.name)+
                    "not fully covered by the computational domain.")
                break

        return line_segments
        




    def _sort_unique_intersections(self):
        if self.grid_type == 'unstructured':
            missing = -999999
        elif self.grid_type =='structured':
            missing = [-999999, -999999]

        # Add start point:
        self.intersections.append(
            Intersection(
            self.start[0], self.start[1], self.start[2], 0.0, [missing]))
        intersdict = {}
        for inter in self.intersections:
            if inter.l not in intersdict:
                intersdict[inter.l] = inter
            else:
                intersdict[inter.l].combine(inter)
        # --> unique

        intersections = []
        for l in sorted(intersdict.keys()):
            # TODO: What behavior do I want? Is this good?
            #if l > 1 and [-1] in intersdict[l].cells:
            if l > 1 and [-999999] in intersdict[l].cells:
                continue
            if l > 1.1 or l < -0.1:
                continue
            intersections.append(intersdict[l])
        # --> sorted & filtered

        # Add end point only if no other intersection was found:
        if len(intersections) == 1:
            intersections.append(
                Intersection(
                self.end[0], self.end[1], self.end[2], 1.0, [missing]))

        return intersections










    ## ATTENTION: Maybe replace ici by icell, which can be ici or [ix,iy]
    def _find_cellface_intersections(self, cellface, icell):
        """Returns the intersection points of self.los with a
        grid cell-face specified by its starting and endpoint
        as an numpy.ndarray of shape (2,2)
        
        Notes
        -----
        Ferdinand's magic. I haven't touched except to adapt it.


        The calculation is based on the following system of equations:
        [ra + G * (rb-ra)]*cos(T) = rc*cos(pc) + L [rd * cos(pd) - rc * cos(pc)]
        [ra + G * (rb-ra)]*sin(T) = rc*sin(pc) + L [rd * sin(pd) - rc * sin(pc)]
         za + G * (zb-za)         = zc         + L [ zd - zc ]

        With:
         - ra,rb,za,zb: The start / end positions of the cell face (r,z)
         - rc,rd,zc,zd,pd,pd: The start / end positions of the LOS (r,z,rho)
         - T: The toroidal angle Theta
         - G: The position along the cell face (G=0 at the starting point
            and G=1 at the end point)
         - L: The position along the LOS       (L=0 at the starting point
            and L=1 at the end point)
        """
        # Can contain two intersections, since a toriodal LOS
        # can pass through a cell-face twice!
        intersections = []


        # The points defining the cell face:
        ra = cellface[0,0] # R
        za = cellface[0,1] # Z
        rb = cellface[1,0] # R
        zb = cellface[1,1] # Z

        # The points defining the LOS:
        rc = self.los[0,0] # R
        zc = self.los[0,1] # Z
        pc = self.los[0,2] # Phi
        rd = self.los[1,0] # R
        zd = self.los[1,1] # Z
        pd = self.los[1,2] # Phi

        # L can be calculated via the quadratic formula after having solved
        # the above system of equations (in which Theta can be eliminated by
        # solving for cos(T) and sin(T) and using cos^2 + sin^2 = 1):
        #
        # L1/2 = [ -b +/- sqrt(b^2 - 4ac) ] / 2a

        # For this we calculate a, b and c (using a few auxiliary parameters):
        cc  = np.cos(pc*np.pi/180.0)
        sc  = np.sin(pc*np.pi/180.0)
        cd  = np.cos(pd*np.pi/180.0)
        sd  = np.sin(pd*np.pi/180.0)
        cdc = (rd*cd-rc*cc)
        sdc = (rd*sd-rc*sc)

        # ==================================================================
        # Handle special cases (to avoid later zero divisions, etc.):
        if rc == rd and zc == zd and pc == pd:
            # Invalid LOS
            # self.log.warning("Line of sight '" + self.name + "': "
            #                  + "Invalid LOS Parameters!")
            return []
        if za == zb == zc == zd:
            # LOS coincides with (horizontal) cell face
            # => Find intersections with rings at r == ra and r == rb:
            for r0 in [ra,rb]:
                a = cdc**2 + sdc**2
                b = 2*rc*(sc*sdc + cc*cdc)
                c = rc**2 - r0**2
                bac = b**2 - 4*a*c
                if bac <= 0:
                    # No intersection exists:
                    continue
                L1 = (-b + np.sqrt(bac)) / (2*a)
                L2 = (-b - np.sqrt(bac)) / (2*a)
                for L in [L1,L2]:
                    cT = (rc*cc + L*cdc) / r0
                    T  = np.arccos(cT) * 180.0/np.pi # --> in degrees
                    # Rounding errors can lead to cT being slightly
                    # bigger than one, which leads to T = nan:
                    if np.isnan(T):
                        if   cT >  0.99 and cT <  1.01:
                            T = 0.0
                        elif cT < -0.99 and cT > -1.01:
                            T = 180.0
                        else:
                            self.log.warning("Line of sight '"+self.name+"': "
                                             +"Theta = nan, could not determine"
                                             +" arccos(" + str(cT) + ")")
                    intersections.append(Intersection(r0, za, T, L, [icell]))
                    # print("Special Case A:",L,nx,ny)
            return intersections
        elif za == zb and zc == zd:
            # No intersection exists:
            # print("Special Case B")
            return []
        elif za == zb:
            # Search for intersection of LOS with a plane at z == za == zb:
            L    = (za-zc)/(zd-zc)
            Pint = self._get_point_along_line(L)
            # print("Special Case C:",L,nx,ny)
            return [Intersection(Pint[0], Pint[1], Pint[2], L, [icell])]
        if ( pc == pd and
             # Cell face and LOS-r-z-projection parallel:
             (rb-ra) != 0.0 and (zb-za) != 0.0 ## ipar: Check non zero div.
             and (rd-rc)/(rb-ra) == (zd-zc)/(zb-za) and
             # Cell face and LOS-r-z-projection coinciding:
             (rd-rc) != 0.0 and (zd-zc) != 0.0 ## ipar: Check non zero div.
             and (ra-rc)/(rd-rc) == (za-zc)/(zd-zc) ):
            La = (ra-rc)/(rd-rc)
            Lb = (rb-rc)/(rd-rc)
            intersections.append(Intersection( ra, za, pc, La, [icell]))
            intersections.append(Intersection( rb, zb, pc, Lb, [icell]))
            # print("Special Case C:",Lz,Lb,nx,ny)
            return intersections
        # ==================================================================
        # Back to the standard case ...

        zdc = (rb-ra)*(zd-zc)/(zb-za) # ATTENTION: ZERO DIVISION?!
        zca = (rb-ra)*(zc-za)/(zb-za) # ATTENTION: ZERO DIVISION?!

        a = cdc**2 + sdc**2 - zdc**2
        b = 2*(rc*cc*cdc + rc*sc*sdc - zdc*(ra+zca))
        c = rc**2 - (ra + zca)**2

        # Check if a solution exists (bac := b^2 - 4ac):
        bac = b**2 - 4*a*c
        if bac <= 0:
            # No intersection exists:
            return []

        L1 = (-b + np.sqrt(bac)) / (2*a)
        L2 = (-b - np.sqrt(bac)) / (2*a)

        for L in [L1,L2]:
            G = (zc - za + L*(zd-zc)) / (zb-za)
            if G < 0 or G > 1:
                # Intersection outside of grid cell:
                continue

            cT = (rc*cc + L*cdc) / (ra + G*(rb-ra))
            with np.errstate(invalid='ignore'): #Suppress RuntimeWarning
                T  = np.arccos(cT) * 180.0/np.pi # --> in degrees

            # Rounding errors can lead to cT being slightly
            # bigger than one, which leads to T = nan:
            if np.isnan(T):
                if   cT >  0.99 and cT <  1.01:
                    T = 0.0
                elif cT < -0.99 and cT > -1.01:
                    T = 180.0
                else:
                    self.log.warning("Line of sight '" + self.name + "': "
                                     + "Theta = nan, could not determine "
                                     + "arccos(" + str(cT) + ")")

            rint = ra + G*(rb-ra)
            zint = za + G*(zb-za)
            pint = T

            intersections.append(Intersection(rint, zint, pint, L, [icell]))
        return intersections





    def _get_point_along_line(self, l, line=None):
        """Returns the (r,z,phi) coordinates of the point on the line 'line'
        at the position l along the line.

        Parameters
        ----------
        l : float
            Position along the line, l==0 at the starting point
            of the line and l==1 at the endpoint of the line.

        line : numpy.ndarray of shape (2,3) (default: self.los)
            An 2x3 array defining the start and endpoint in
            (r,z,phi) coordinates of the line to be considered.
            If line==None self.los is used.
        """

        if line is None:
            line = self.los

        # The points defining the line:
        rc = line[0,0] # R
        zc = line[0,1] # Z
        pc = line[0,2] # Phi
        rd = line[1,0] # R
        zd = line[1,1] # Z
        pd = line[1,2] # Phi

        cc  = np.cos(pc*np.pi/180.0)
        sc  = np.sin(pc*np.pi/180.0)
        cd  = np.cos(pd*np.pi/180.0)
        sd  = np.sin(pd*np.pi/180.0)

        # Cartesian coordinates:
        x = rc*cc + l*(rd*cd-rc*cc)
        y = rc*sc + l*(rd*sd-rc*sc)
        z = zc    + l*(zd-zc)

        # Zylindrical coordinates:
        r = np.sqrt(x**2+y**2)
        z = z
        p = np.arctan2(x,y) * 180.0/np.pi # --> in degrees

        return np.array([r,z,p])



    ## ATTENTION: For structured, probably something like only real cells.
    def _trim_var(self, var):
        if self.grid_type=='unstructured':
            try:
                return var[:self.mother.nci]
            except:
                self.log.error("Var '{}' could not be trimmed.".format(var))
                return None
        elif self.grid_type=='structured':
            try:
                val = var.copy()
                val[~self.mother.rlcl] = np.nan
                return val
            except:
                self.log.error("Var '{}' could not be trimmed.".format(var))
                return None
































##ATTENTION: This is fine for now, but I will need a better plotting routine.
## One that uses my classes and that allows for multiple lines, etc.
## Maybe the multiple lines could be done for the chords one.
## But also improve things like line average and integral having fewer decimanls,
## str and print handling, etc.


    def plot(self,var=None,**kwargs):
        """Plots the value of var along the chord (line of sight),
        if var=None only plots the chord in the 2d computational domain.

        Arguments:
          - var :    The value which will be plotted (np.array[nx,ny(,ns)])
          - title :  Title of the value plot

          - var      :  The value which will be plotted (np.array[nx,ny(,ns)]).
                        Can also be a list of values which will all be plotted
                        in one plot (so they should be of the same type, eg.
                        densities for different ionization stages).
          - legend   :  Legend for the plot of multiple vars (must be a list)
          - title    :  Title of the value plot
          - ylabel   :  y-label of the value plot
          - factor   :  y-factor as indicated on the y-label (e.g. kW => 1e3)
          - save_as  :  Filename under which the plot will be saved
          - showplot :  Toggles if the plot should be shown
          - nolos    :  If nolos == True only the value plot will be shown
          - notext   :  Suppress the integral/average text in the value plot
          - log      :  Logarithmic scale

        Example:
        Chords(run)['RIN-2'].plot(var      = run.ne,
                                  title    = "Electron Density",
                                  ylabel   = "$n_e$ [$10^{19}\mathrm{m}^{-3}$]",
                                  factor   = 1e19,
                                  save_as  = 'RIN-2_ne.pdf',
                                  showplot = True )
        """

        # Possible options and defaults:
        title       = kwargs.pop('title'    , '')
        ylabel      = kwargs.pop('ylabel'   , '')
        legend      = kwargs.pop('legend'   , None)
        factor      = kwargs.pop('factor'   , 1)
        save_as     = kwargs.pop('save_as'  , None)
        showplot    = kwargs.pop('showplot' , True)
        printval    = kwargs.pop('printval' , False)
        plotgrid    = kwargs.pop('plotgrid' , True)
        nolos       = kwargs.pop('nolos'    , False)
        notext      = kwargs.pop('notext'   , False)
        log         = kwargs.pop('log'      , False)
        cmap        = kwargs.pop('cmap'     , None)
        plot_vessel = kwargs.pop('plot_vessel'    , True)
        for kw in kwargs:
            self.log.error("Invalid option: %s",kw)
            return

        # Preparation:
        if var is None: var = []
        if type(var) != list: var = [var]
        if type(legend) != list: legend = len(var) * ['']
        while len(legend) < len(var):
            legend.append('')
        for v in var:
            if self._trim_var(v) is None:
                return
        if save_as is not None and not save_as.endswith('.pdf.'): 
            save_as += '.pdf'
        if type(cmap) == str:
            try:
                cmap = plt.cm.get_cmap(cmap)
            except:
                self.log.error("Invalid color map: %s",cmap)
                return

        # 3 Basic options:
        #  - Plot only the line of sight in the grid
        #  - Plot only the values of 'var' along the los
        #  - Plot both next to each other
        if var is []:  plotlos = True  ; plotval = False
        elif nolos:    plotlos = False ; plotval = True
        else:          plotlos = True  ; plotval = True

        # Create figure:
        if plotlos and plotval:
            fig, (c_los, c_val) = plt.subplots(1, 2, figsize=(12,6),
                                      gridspec_kw = {'width_ratios':[1, 2]})
        elif plotlos:
            fig = plt.figure(figsize=(8,11))
            c_los = fig.add_subplot(1,1,1)
        elif plotval:
            fig = plt.figure(figsize=(11,8))
            c_val = fig.add_subplot(1,1,1)

        # ======================
        # Create the line of sight plot:
        if plotlos:

            # Plot vessel:
            if plot_vessel:
                for i in range(self.mother.vessel.shape[1]):
                    R = np.array([self.mother.vessel[0,i],self.mother.vessel[2,i]])
                    z = np.array([self.mother.vessel[1,i],self.mother.vessel[3,i]])
                    c_los.plot(R,z,'k',linewidth = 1)

            ## Plot grid:
            if plotgrid:
                for ici in range(self.mother.nci):
                        c_los.add_patch(Polygon(self.mother.grid[ici], True,
                                                linewidth=0.5, edgecolor='gray',
                                                fill=False))
            # Plot line of sight (LOS):
            for ls in self.line_segments:
                # Highlight cells:
                if plotgrid and ls.ici != -1:
                    c_los.add_patch(Polygon(self.mother.grid[ls.ici],
                                            alpha=0.5))
                # Draw the line of sight (each segment):
                c_los.add_patch(Polygon(ls.seg[:,:2],color='k'))
                # Highlight the intersection points:
                c_los.add_patch(Circle(ls.seg[0,:2],0.00003,color='k'))
                c_los.add_patch(Circle(ls.seg[1,:2],0.00003,color='k'))
            ## Highlight the starting point:
            #c_los.add_patch(Circle(self.start[:2],0.0035,color='k'))
            ##c_los.add_patch(Circle(self.line_segments[0].start[:2],
            ##                       0.0035,color='k'))

            # Plot options:
            c_los.axis('equal')
            xmin=self.mother.grid[...,0].min()
            xmax=self.mother.grid[...,0].max()
            ymin=self.mother.grid[...,1].min()
            ymax=self.mother.grid[...,1].max()
            xmin=min(xmin,self.start[0])
            xmax=max(xmax,self.start[0])
            ymin=min(ymin,self.start[1])
            ymax=max(ymax,self.start[1])
            dy = (ymax - ymin)*0.05
            dx = (xmax - xmin)*0.05
            c_los.axis([xmin-dx, xmax+dx, ymin-dy, ymax+dy])
            c_los.set_title(self.name)
            c_los.set_xlabel('R [m]')
            c_los.set_ylabel('z [m]')

            if not plotval:
                # Show the plot:
                fig.tight_layout()
                self.mother.plot.pdfpages.save()
                if showplot: plt.show()
                if save_as is not None:
                    plt.savefig(save_as)
                plt.close(fig)
                return

        # ======================
        # Create the value plot:
        xmin = np.inf
        xmax = -np.inf
        for vi,v in enumerate(var):
            x = [0]
            y = [0]
            i = 0
            if printval:
                print('')
                print(55*'=')
                print('Line of sight: ',self.name)
                desc = legend[vi]
                if desc == '': desc = title
                if desc != '':
                    print('Values for the',desc,'plot:')
                desc = ylabel.replace('\mathrm','').replace('$','')
                desc = desc.replace('{','').replace('}','').replace('\\','')
                print('{:<25}'.format('Distance along LOS [m]'),desc)
                print('')
            for ls in self.line_segments:
                if ls.ici == -1: val = 0
                else: val = v[ls.ici]
                val = val / factor
                x.append(x[i])
                y.append(val)
                x.append(x[i] + ls.length)
                y.append(val)
                if printval:
                    print('{:<25}'.format(np.average([x[-1],x[-2]])),val)
                i += 2
            x.append(x[-1])
            y.append(0)
            if cmap is not None:
                color = cmap(float(vi)/(len(var)-1))
                c_val.plot(x,y,label=legend[vi],color=color)
            else:
                c_val.plot(x,y,label=legend[vi])

            # Set the plot range: (hide zero-values at the edges)
            i = 0
            while i < len(y)-1:
                if y[i] != 0: break
                i += 1
            xmin = min(xmin,x[i])
            i = len(y)-1
            while i > 0:
                if y[i] != 0: break
                i -= 1
            xmax = max(xmax,x[i])
        dx = (xmax - xmin)*0.05
        c_val.set_xlim([xmin-dx, xmax+dx])
        if legend[0] != '': c_val.legend(loc=0)

        # Plot options:
        c_val.set_xlabel("Distance along the line of sight ('"
                      + self.name + "') [m]")
        c_val.set_ylabel(ylabel)
        c_val.set_title(title)
        if log: c_val.set_yscale('log')

        if len(var) == 1 and not notext:
            text  = 'Line integral: ' + str(self.integrate(var[0])) + '\n'
            text += 'Line average: ' + str(self.average(var[0]))
            c_val.text( 0.05, 0.9, text, transform=c_val.transAxes )

        # Show the plot:
        fig.tight_layout(w_pad=3.2)
        #self.mother.plot.pdfpages.save()
        #if showplot: plt.show()
        #if save_as is not None:
        #    fig.savefig(save_as)
        plt.show()
        plt.close(fig)










class ChordStructured(Chord):
    def __init__(self, mother, name, start, end):
        super().__init__(mother, name, start, end)
        #self.real = self.mother.masks.rlcl

    @solps_property
    def isep(self):
        """ return the index of the line segment(s) which intersect(s)
        with the separatrix. List of indices, including []
        """
        tmp = []
        for i, ls in enumerate(self.line_segments):
            if ls.icell[1] == self.mother.isep:
                tmp.append(i)
        self._isep = np.array(tmp)

    ## ATTENTION: Multiply by real cells now or in other place?
    ## I think this is good, so that only real cells have intersections
    ## calculated for them.
    def _get_intersections(self):
        """Returns all intersections of self.los with the grid.

        Notes
        -----
        Contains duplicate intersections, since the same intersection
        can occur in different grid cells (if they share a cell face)!
        """
        intersections = []
        for ix in range(self.mother.nx):
            for iy in range(self.mother.ny):
                #if self.real[ix,iy]:
                if True:
                    intersections.extend(
                    self._find_grid_cell_intersections([ix,iy]))
        return intersections



    ##ATTENTION: It could be possible to use this for both BUT
    ## Check that both answers give the same result.
    ## I.e. create tests for this before any merge.
    def _find_grid_cell_intersections(self, icell):
        """Returns all intersections of self.los with the grid cell icell.

        Notes
        -----
        Can contain more than two intersections, since a toriodal LOS
        can pass through a grid cell twice!
        """
        intersections = []
        if self.grid_type == 'unstructured':
            gcell = self.mother.grid[icell]
        elif self.grid_type == 'structured':
            gcell = self.mother.grid[icell[0], icell[1]]
        for iv in range(gcell.shape[0]-1):
            cf = np.vstack([gcell[iv], gcell[iv+1]])
            intersections.extend(self._find_cellface_intersections(cf,icell))
        return intersections








class ChordUnstructured(Chord):
    def __init__(self, mother, name, start, end):
        super().__init__(mother, name, start, end)


    @solps_property
    def isep(self):
        """ return the index of the line segment(s) which intersect(s)
        with the separatrix. List of indices, including []
        """
        tmp = []
        for i, ls in enumerate(self.line_segments):
            if self.mother.is_isep(ls.icell):
                tmp.append(i)
        self._isep = np.array(tmp)





    def _get_intersections(self):
        """Returns all intersections of self.los with the grid.

        Notes
        -----
        Contains duplicate intersections, since the same intersection
        can occur in different grid cells (if they share a cell face)!
        """
        intersections = []
        for ici in range(self.mother.nci):
            intersections.extend(self._find_grid_cell_intersections(ici))
        return intersections



    ## ATTENTION: could this be merged by making ix,iy again xy
    ## and substituting vx vy for the technique of using grid?
    ## grid also uses vx vy, I think
    def _find_grid_cell_intersections(self, ici):
        """Returns all intersections of self.los with the grid cell ici.

        Notes
        -----
        Can contain more than two intersections, since a toriodal LOS
        can pass through a grid cell twice!
        tmp2 = tmp2[tmp2>-1] # Flattened array. If required, find new criteria.
        """
        intersections = []
        cf = np.zeros((2,2))
        tmp = self.mother.icvfc[ici]
        #Filter out guard and dead cells
        tmp = tmp[tmp>-1]
        tmp2 = self.mother.fcvx[tmp] 
        vx = self.mother.vxx[tmp2]
        vy = self.mother.vxy[tmp2]
        for x,y in zip(vx,vy):
            cf[:,0] = x
            cf[:,1] = y
            intersections.extend(self._find_cellface_intersections(cf,ici))
        return intersections
