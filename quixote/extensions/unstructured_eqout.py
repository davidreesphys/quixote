import os
import re
import numpy as np
import pdb
import glob
import matplotlib as mpl
import matplotlib.pyplot as plt
import itertools

# Quixote modules
from ..tools import solps_property
from ..tools import priority
#from ..b25_src.utility.intcor import intcor

class EquationsOutput(object):
    """ This class serves to read and work with the extra output files when
    using 'b2wdat_iout'     '4'.

    .. note::
        For obvious reasons, the simulation must have been run
        with ``'b2wdat_iout'`` ``'4'``.

        Check for files in the output/ directory
        in your run directory.


    If ``b2tfnb_drift_style`` = 1, ``uadia`` and ``uaecrb``
    are given in cell face (default). Else in cell center.

    .. warning::
        The following quantities were tricky to understand and implement,
        and some equations can still not be fully recovered out of its
        component.

        **Use with caution!**

    Parameters
    ----------
    mother: SolpsData like object.
        Rundir path and variables stored in `mother` will be used to
        find and parse the output files from equations.


    """
    def __init__(self, mother):
        self.mother = mother
        self.log = mother.log
        self.ns = mother.ns
        self.qe = mother.qe

        for tmp in ['output', 'b2mn.exe.dir/output']:
            try:
                tmpdir = os.path.join(mother.rundir, tmp)
                if os.path.isdir(tmpdir) and os.path.isfile(
                   os.path.join(tmpdir,'info.dat')):
                    self.outdir = tmpdir
                    self.log.info("output directory set to '{0}'".format(self.outdir))
                    break
            except:
                pass
        else:
            self.log.exception("No 'output' directory is found in rundir.")


        self.iout = mother.read_switch('b2wdat_iout', 'b2mn.dat')
        if self.iout is None:
            self.iout = '0 (Default)' #Current default

        # Not necessarily, they could also output specific equations
        if self.iout != '1' and self.iout != '4':
            self.log.error("'b2wdat_iout' in b2mn.dat should be either "+
                    "'1' or '4' but is {0}.".format(self.iout))



    ### --------------------- EQUATIONS COMPOSITION ---------------------------
    ### ATTENTION: COMPLETE!
    @property
    def continuity(self):
        """ Deep dictionary for continuity equation"""
        resolution = {
            'resco':{},
            'sna':{
                'sna_eir':{},
                'sna_ion':{},
                'sna_rec':{},
                'sna_cx':{},
                'sna_phys':{}
                },
            'dnadt':{},
            'fnax':{
                'bxuanax':{},
                'vaecrbnax':{},
                'dpccornax':{},
                'vadianax':{
                    'vbdiax':{},
                    'wbdiax':{}
                    },
                },
            }

        return resolution




    ### ------------------------- FUNCTIONS -----------------------------------


    def read_gdata_signal(self, var, dims):
        """ Signals are usually split in various files, depending on ions,
        neutrals, etc.
        It uses glob and thus shell and its wildcard expansion, but it is
        the easier way around.
        It reads the maximum number in the file to determine dimension.
        """
        factor = self._factors(var)
        files = glob.glob(os.path.join(self.outdir, var+'.dat'))
        if len(files) == 0:
            raise Exception("File(s) containing '{}' not found.".format(var))
        elif len(files) == 1:
            return self.parse_gdata_file(files[0])*factor
        else:
            regex = re.compile(r'\d+')
            fns = [int(regex.findall(fl)[-1]) for fl in files]
            tmp = np.full(dims, np.nan, dtype=float)
            for si, fl in zip(fns, files):
                try:
                    tmp[...,si] = np.array(
                        self.parse_gdata_file(fl), dtype=float)*factor
                except:
                    self.log.error(
                        "File '{0}' does not fit in is='{1}'.".format(fl, si))
            return tmp


#    def _read_gdata_file(self, varfile):
#        """ Reading function for files which contain normal grid data.
#        """
#
#        tmp = []
#        with open(os.path.join(self.outdir, varfile)) as fr:
#            fr.readline()
#            for line in fr:
#                tmp.append(line.strip().split()[-1])
#        return tmp



    def _factors(self, var):
        """
        Temperatures are stored in J and not in eV
        """
        temps = {'te','ti','tab2','tmb2','tib2'}
        if var.lower() in temps:
           return 6.2415093432601784e+18
        else:
            return 1.0





class EqOutST(EquationsOutput):
    def __init__(self, mother):
        super().__init__(mother)

        self.nx = mother.nx+2
        self.ny = mother.ny+2

    def parse_gdata_file(self,varfile):
        """ Reading function for files which contain normal grid data.
        """

        #import ipdb; ipdb.set_trace()
        tmp = np.zeros((self.nx, self.ny))
        with open(os.path.join(self.outdir, varfile)) as fr:
                assert (self.nx == len(fr.readline().split()))
                #for iy in reversed(xrange(self.ny)):
                #    tmp[:,iy] = [float(val) for val in fr.readline().split()[1:]]

                iy= self.ny -1
                while iy >= 0:
                    line  = fr.readline()
                    if line =='' or line=='\n':
                        continue

                    pieces = line.split()[1:]
                    for ix, val in enumerate(pieces):
                            tmp[ix,iy] = float(val)
                    iy -= 1
        return tmp
    ### ------------------------ CONTINUITY EQUATION --------------------------
    ### BASIC ANALYSIS

    @solps_property
    def na(self):
        """ (nx, ny, ns): Particle density [m^-3].
        """
        self._na = self.read_gdata_signal('b2npc*_na*_st',
            (self.nx, self.ny, self.ns))

    @solps_property
    def resco(self):
        """ (nx, ny, ns): Residuals of the continuity equation [m^-3 s^-1].
        """
        self._resco = self.read_gdata_signal('b2npc*_resco*_st',
            (self.nx, self.ny, self.ns))






class EqOutUS(EquationsOutput):
    def __init__(self, mother):
        super().__init__(mother)

        self.ncv = mother.ncv
        self.nfc = mother.nfc



    def divergence(self, flow):
        """ Copy of utility/div.F
        """
        dflo=0.0
        for ifc in range(self.nfc): 
            pass



    ### ------------------------ CONTINUITY EQUATION --------------------------
    ### BASIC ANALYSIS


    @solps_property
    def na(self):
        """ (ncv, ns): Particle density [m^-3].
        """
        self._na = self.read_gdata_signal('b2npc*_na*', (self.ncv, self.ns))


    @solps_property
    def resco(self):
        """ (ncv, ns): Residuals of the continuity equation [m^-3 s^-1].
        """
        self._resco = self.read_gdata_signal('b2npc*_resco*', (self.ncv, self.ns))


    @solps_property
    def sna(self):
        """ (ncv, ns): Total particle source [1/s].

        From equations/b2npc9.F:
        b2npc9_sna = snb(:,:,0)+nb*snb(:,:,1)

        These pieces are apparently also saved.
        """
        self._sna = self.read_gdata_signal('b2npc*_sna*', (self.ncv, self.ns))

    ### Sources

    ##ATTENTION: Doesn-t seem correct.
    @solps_property
    def sna_ion(self):
        """ Particle source due to ionization.
        """
        self._sna_ion = self.read_gdata_signal('b2stel_sna_ion*', (self.ncv, self.ns))

    ##ATTENTION: Doesn-t seem correct.
    @solps_property
    def sna_rec(self):
        """ Particle source due to recombination.
        """
        self._sna_rec = self.read_gdata_signal('b2stel_sna_rec*', (self.ncv, self.ns))

    ##ATTENTION: Doesn-t seem correct.
    @solps_property
    def sna_eir(self):
        """ Particle source due to EIRENE neutrals.
        """
        self._sna_eir = self.read_gdata_signal('b2stbr_sna_eir*', (self.ncv, self.ns))

    ##ATTENTION: Doesn-t seem correct.
    @solps_property
    def sna_phys(self):
        """ Particle source in the guard cells due to boundary conditions.
        """
        self._sna_phys = self.read_gdata_signal('b2stbc_phys_sna*', (self.ncv, self.ns))





