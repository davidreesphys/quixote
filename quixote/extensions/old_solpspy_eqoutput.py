import os
import numpy as np
import pdb
import glob
import matplotlib as mpl
import matplotlib.pyplot as plt
import itertools

from solpspy.tools.tools import solps_property
from solpspy.tools.tools import priority
from solpspy.b25_src.utility.intcor import intcor

class EquationsOutput(object):
    """ This class serves to read and work with the extra output files when
    using 'b2wdat_iout'     '4'.

    .. note::
        For obvious reasons, the simulation must have been run
        with ``'b2wdat_iout'`` ``'4'``.

        Check for files in the output/ directory
        in your run directory.


    If ``b2tfnb_drift_style`` = 1, ``uadia`` and ``uaecrb``
    are given in cell face (default). Else in cell center.

    .. warning::
        The following quantities were tricky to understand and implement,
        and some equations can still not be fully recovered out of its
        component.

        **Use with caution!**

    Parameters
    ----------
    mother: SolpsData like object.
        Rundir path and variables stored in `mother` will be used to
        find and parse the output files from equations.


    """
    def __init__(self, mother):
        self.mother = mother
        self.log = mother.log
        self.nx = mother.nx
        self.ny = mother.ny
        self.ns = mother.ns
        self.za = mother.za
        self.qe = mother.qe

        try:
            self.outdir = os.path.join(mother.rundir, 'output')
            assert os.path.isdir(self.outdir)
        except:
            self.log.exception("No 'output' directory is found in rundir.")


        self.iout = mother.read_switch('b2wdat_iout', 'b2mn.dat')
        if self.iout is None:
            self.iout = '0 (Default)' #Current default

        if self.iout != '1' and self.iout != '4':
            self.log.error("'b2wdat_iout' in b2mn.dat should be either "+
                    "'1' or '4' but is {0}.".format(self.iout))


    ### ------------------------- FUNCTIONS -----------------------------------

#    def div_flux(self,ffn):
#        """ Divergence of fluxes:
#        Radial divergence of radial flux and
#        Poloidal divergence of poloidal flux,
#        given as ffn = ffn[nx,ny,2,ns].
#
#        Flux is assumed to be integrated over the surface of the cell.
#        As I understand it:
#
#        [divx(sqrt(g)/hx Flux), divy(sqrt(g)/hy Flux) ]
#
#        Copied from solvers/b2ursc.F
#
#        Notes
#        -----
#        Is the minus sign correct? Is Moulton's div the same?
#        It is "contribution to residuals from ffn[xy]", which according to the
#        initial equation is resb = rhs - ffn. Thus -div is calculated.
#
#        """
#        div = np.zeros((self.nx,self.ny,2,self.ns))
#        for ix in xrange(self.nx):
#            for iy in xrange(self.ny):
#                rix = int(self.rightix[ix,iy])
#                riy = int(self.rightiy[ix,iy])
#                lix = int(self.leftix[ix,iy])
#                liy = int(self.leftiy[ix,iy])
#
#                tix = int(self.topix[ix,iy])
#                tiy = int(self.topiy[ix,iy])
#                bix = int(self.bottomix[ix,iy])
#                biy = int(self.bottomiy[ix,iy])
#
#        # -divx(Fx) # I think it is x -1.0
#                if lix == -1:
#                    div[ix,iy,0] = -ffn[rix,riy,0]
#
#                elif rix == self.nx:
#                    div[ix,iy,0] = ffn[ix,iy,0]
#                else:
#                    div[ix,iy,0] = ffn[ix,iy,0] -ffn[rix,riy,0]
#
#        # -divy(Fy) # I think it is x -1.0
#                if biy == -1:
#                    div[ix,iy,1] = -ffn[tix,tiy,1]
#
#                elif tiy == self.ny:
#                    div[ix,iy,1] = ffn[ix,iy,1]
#                else:
#                    div[ix,iy,1] = ffn[ix,iy,1] -ffn[tix,tiy,1]
#
#        return -div




    def div_flux(self,ffn):
        """ Divergence of fluxes:
        Radial divergence of radial flux and
        Poloidal divergence of poloidal flux,
        given as ffn = ffn[nx,ny,2,ns].

        Flux is assumed to be integrated over the surface of the cell.
        As I understand it:

        [divx(sqrt(g)/hx Flux), divy(sqrt(g)/hy Flux) ]

        Copied from solvers/b2ursc.F

        Notes
        -----
        Is the minus sign correct? Is Moulton's div the same?
        It is "contribution to residuals from ffn[xy]", which according to the
        initial equation is resb = rhs - ffn. Thus -div is calculated.


        Parameters
        ----------
        ffn: (nx, ny, 2, ...)
            Flux which divergence is to be calculated.
            ffn[:,:,0] should be the poloidal flux.
            ffn[:,:,1] should be the radial flux.

        Returns
        -------
        (nx, ny, 2, ...)
            [:,:,0] poloidal divergence of the poloidal flux.
            [:,:,1] radial  divergence of the radial flux.
        """

        #div = np.zeros((self.nx,self.ny,2,self.ns))
        div = np.zeros((list(itertools.chain.from_iterable(
            [(self.nx,self.ny,2),ffn.shape[3:]]))))
        for ix in range(self.nx):
            for iy in range(self.ny):
                rix = int(self.rightix[ix,iy])
                riy = int(self.rightiy[ix,iy])
                lix = int(self.leftix[ix,iy])
                liy = int(self.leftiy[ix,iy])

                tix = int(self.topix[ix,iy])
                tiy = int(self.topiy[ix,iy])
                bix = int(self.bottomix[ix,iy])
                biy = int(self.bottomiy[ix,iy])

        # -divx(Fx) # I think it is x -1.0
                if lix == -1:
                    div[ix,iy,0] = -ffn[rix,riy,0]

                elif rix == self.nx:
                    div[ix,iy,0] = ffn[ix,iy,0]
                else:
                    div[ix,iy,0] = ffn[ix,iy,0] -ffn[rix,riy,0]

        # -divy(Fy) # I think it is x -1.0
                if biy == -1:
                    div[ix,iy,1] = -ffn[tix,tiy,1]

                elif tiy == self.ny:
                    div[ix,iy,1] = ffn[ix,iy,1]
                else:
                    div[ix,iy,1] = ffn[ix,iy,1] -ffn[tix,tiy,1]

        return -div


    def map(self, var, to, now=None, method='average'):
        """ Map center quantities to faces and viceversa.

        Parameters
        ----------
        var: (nx,ny,...)
            Quantity to be mapped.

        to: str
            Can be 'left', 'bottom', 'center' or 'faces'.
            If 'left' or 'bottom' then assume quantity is 'center'.
            If 'center' then now is required.
            If 'faces', then output is (...,2) with 0 being 'left'
            and (...,1) being 'bottom'.

        now: str, optional.
            What is the current position of the quantity.
            Only required if to is 'center'.

        method: str, optional.
            What type of mapping is desired.
            'interpolation' means linear interpolation using h[x|y].
            'volume' means "volumen interpolation" using vol.
            'average' or 'avg' means (Cell+Neighbour)/2.0

        Returns
        -------
        (nx,ny,...)
            Mapped quantity to the corresponding place.

        Notes
        -----
        The division of averages is not the same as the average of the
        division.
        I.e.:
            < vol/hx > =! <vol>/<hx>

        """
        tmp = np.zeros_like(var)
        dix = getattr(self, to+'ix')
        diy = getattr(self, to+'iy')
        for ix in range(self.nx):
            for iy in range(self.ny):
                if   to.lower() == 'left'   and dix[ix,iy]<0:
                    continue
                elif   to.lower() == 'bottom' and diy[ix,iy]<0:
                    continue

                #Review the indexing of these. matlab is 1, python 0, mds -1
                elif to.lower() == 'right'  and dix[ix,iy]>self.nx:
                    continue
                elif to.lower() == 'top'    and diy[ix,iy]>self.ny:
                    continue

                varc = var[ix,iy]
                varn = var[dix[ix,iy],diy[ix,iy]]

                if method.lower() == 'volume':
                    dvc = self.vol[ix,iy]
                    dvn = self.vol[dix[ix,iy],diy[ix,iy]]
                    tmp[ix,iy] = (varc*dvn + varn*dvc)/(dvn+dvc)
                elif 'average' or 'avg':
                    tmp[ix,iy] = (varc + varn)/2.0

        return tmp




    #ATTENTION: Change name to poloidal_integration or something like it??
    #ATTENTION: Improve docstring.
    def sum(self,var,gridmask, dimension='x'):
        """
        Selection method for the region should be pretty similar to that
        of the average function.
        Change creation of mask as to allow creation of sums with
        more than one dimensions (nx,ns), (ny,ns), etc.
        Add an automatic system to detect in which side of the DDNU
        or if in USN, etc, to get the reverse way of increasing X

        Parameters
        ----------
        var: (nx, ny, ...)
            Quantity to be summed up.

        gridmask: (nx, ny)
            True for indices that are added to the sum.

        dimension: {'x', 'y'}, optional
            Sum in the 'x' (poloidal, or 'y' radial dimension.

        Returns
        -------
        (ny, ...) or (nx, ...)
            Sum of quantity along the dimension axis.
        """
        nx = var.shape[0]
        ny = var.shape[1]
        if gridmask is None:
            gridmask = np.ones((nx, ny), dtype=np.bool)
        gmask = gridmask.astype(np.int)
        if dimension == 'x':
            tmp = np.full(ny, np.nan)
            msk = np.any(gridmask,0)
            for iy in range(ny):
                if msk[iy] == False:
                    continue
                tmp[iy] = (np.sum(gmask[:,iy]*var[:,iy]))
            return tmp

        elif dimension == 'y':
            tmp = np.full(nx, np.nan)
            msk = np.any(gridmask,1)
            for ix in range(nx):
                if msk[ix] == False:
                    continue
                tmp[ix] = (np.sum(gmask[ix]*var[ix]))
            return tmp

        else:
            self.log.error("Dimension must be either 'x' or 'y'")
            return


    def delta(self, svar):
        """
        Calculates delta of svar as var - var(i/j-1)
        Similar to gradient but without lenght scaling.
        """
        if isinstance(svar, str):
            var = getattr(self, svar)
        else:
            var = svar.copy()
        btix = self.bottomix
        btiy = self.bottomiy
        ltix = self.leftix
        ltiy = self.leftiy
        deltas = np.zeros(list(itertools.chain.from_iterable(
                          [var.shape,[2]])))
        tmp = np.zeros_like(var)
        for ix in range(var.shape[0]):
            for iy in range(1,var.shape[1]):
                bix = btix[ix,iy]
                biy = btiy[ix,iy]
                tmp[ix,iy] = var[ix,iy] - var[bix,biy]
        deltas[...,1] = tmp
        tmp = np.zeros_like(var)
        for ix in range(var.shape[0]):
            for iy in range(1,var.shape[1]):
                lix = ltix[ix,iy]
                liy = ltiy[ix,iy]
                tmp[ix,iy] = var[ix,iy] - var[lix,liy]
        deltas[...,0] = tmp
        return deltas




    def residuals(self, sfn, ffn):
        """
        Python translation from b2ursc.F

        *.documentation
        *
        *  1. purpose
        *
        *     B2URSC computes the residual of a scalar conservation equation.
        *
        *
        *  3. description (see also routine b2cdca)
        *
        *     This routine computes the residual of a scalar conservation
        *     equation, integrated over cells of the b2 mesh. The equation has
        *     the form
        *       div(flux) = rhs ,
        *     and the residual is defined as
        *       resfn = rhs-div(flux) .
        *     The flux is specified by ffn, and the right hand side has the
        *     linearised representation
        *       rhs(,) = sfn(,,0)+sfn(,,1)*fun(,) .
        *     This linearisation may include a contribution from an implicit
        *     discretisation in time.
        *
        *     The input argument ffn specifies fluxes integrated over cell
        *     faces, and sfn specifies source coefficients integrated over
        *     cell volumes. Therefore, the expression for the integrated
        *     residual on any cell does not involve any geometric factors.


        In this case, all species are calculated with the same call.
        sfn here is the recomposed source and not the liniarized pieces.

        IMPORTANT NOTE:
        --------------
        divy(fnay) is different from debugger and thus might be wrong in
        here or just because of big numbers / small numbers arithmetics.
        """
        # Sources
        resfn = sfn.copy()

        #Divergence of fluxes
        div = self.div_flux(ffn)
        resfn += div[...,0,:] + div[...,1,:]

        return resfn








    def _read_gdata_signal(self, var, correct_dims=None):
        """ Signals are usually split in various files, depending on ions,
        neutrals, etc.
        It uses glob and thus shell and its wildcard expansion, but it is
        the easier way around.
        It reads the maximum number in the file to determine dimension.
        """
        if correct_dims is None:
            factor = self._factors(var)
            files = glob.glob(os.path.join(self.outdir, var+'.dat'))
            if len(files) == 0:
                raise Exception("File(s) containing '{}' not found.".format(var))
            elif len(files) == 1:
                return self._read_gdata_file(files[0])*factor
            else:
                ns = [int(fl[-7:-4]) for fl in files]
                tmp = np.full((self.nx, self.ny, max(ns)+1), np.nan)
                for si, fl in zip(ns, files):
                    tmp[...,si] = self._read_gdata_file(fl)*factor
                return tmp
        else:
            factor = self._factors(var)
            files = glob.glob(os.path.join(self.outdir, var+'.dat'))
            if len(files) == 0:
                raise Exception("File(s) containing '{}' not found.".format(var))
            else:
                tmp = np.zeros(correct_dims)
                ns = [int(fl[-7:-4]) for fl in files]
                for si, fl in zip(ns, files):
                    tmp[...,si] = self._read_gdata_file(fl)*factor
                return tmp


    def _read_gdata_file(self,varfile):
        """ Reading function for files which contain normal grid data.
        """

        tmp = np.zeros((self.nx, self.ny))
        with open(os.path.join(self.outdir, varfile)) as fr:
                assert (self.nx == len(fr.readline().split()))
                #for iy in reversed(xrange(self.ny)):
                #    tmp[:,iy] = [float(val) for val in fr.readline().split()[1:]]

                iy= self.ny -1
                while iy >= 0:
                    line  = fr.readline()
                    if line =='' or line=='\n':
                        continue

                    pieces = line.split()[1:]
                    for ix, val in enumerate(pieces):
                            tmp[ix,iy] = float(val)
                    iy -= 1
        return tmp




    def _factors(self, var):
        """
        Temperatures are stored in J and not in eV
        """
        temps = {'te','ti','tab2','tmb2','tib2'}
        if var.lower() in temps:
           return 6.2415093432601784e+18
        else:
            return 1.0





    ### ------------------------ GEOMETRY VARIALBES  --------------------------
    @solps_property
    def vol(self):
        """ (nx,ny): Cell volumen [m^3]
        """
        self._vol = self._read_gdata_signal('vol')

    @solps_property
    def hx(self):
        """ (nx,ny): Poloidal scale factor [m]
        """
        self._hx = self._read_gdata_signal('hx')

    @solps_property
    def hy(self):
        """ (nx,ny): Radial scale factor [m]
        """
        self._hy = self._read_gdata_signal('hy')

    @solps_property
    def hy1(self):
        """ (nx,ny): Corrected radial scale factor, hy*cosA [m].
        cosA is the cosine of the complementary angle between the x-direction
        and the y-direction on the (iy,ix) cell.
        """
        self._hy1 = self._read_gdata_signal('hy1')


    @solps_property
    def hz(self):
        """ (nx,ny): Toroidal scale factor [m]
        """
        self._hz = self._read_gdata_signal('hz')

    @solps_property
    def pbsx(self):
        """ (nx,ny): .. math:: pbsx = \\frac{\\sqrt{g}}{h_x} b_x
        """
        self._pbsx = self._read_gdata_signal('pbsx')


    @solps_property
    def leftix(self):
        self._leftix = self._read_gdata_signal('leftix').astype(np.int)
        self._leftix += 1

    @solps_property
    def leftiy(self):
        self._leftiy = self._read_gdata_signal('leftiy').astype(np.int)
        self._leftiy += 1

    @solps_property
    def rightix(self):
        self._rightix = self._read_gdata_signal('rightix').astype(np.int)
        self._rightix += 1

    @solps_property
    def rightiy(self):
        self._rightiy = self._read_gdata_signal('rightiy').astype(np.int)
        self._rightiy += 1

    @solps_property
    def bottomix(self):
        self._bottomix = self._read_gdata_signal('bottomix').astype(np.int)
        self._bottomix += 1

    @solps_property
    def bottomiy(self):
        self._bottomiy = self._read_gdata_signal('bottomiy').astype(np.int)
        self._bottomiy += 1

    @solps_property
    def topix(self):
        self._topix = self._read_gdata_signal('topix').astype(np.int)
        self._topix += 1

    @solps_property
    def topiy(self):
        self._topiy = self._read_gdata_signal('topiy').astype(np.int)
        self._topiy += 1



    @solps_property
    def crx(self):
        self._crx = np.zeros((self.nx,self.ny,4))
        for i in range(4):
            self._crx[...,i] = self._read_gdata_file(
                os.path.join(self.outdir,'crx{}.dat'.format(i)))

    @property
    def r(self):
        return self.crx


    @solps_property
    def cry(self):
        self._cry = np.zeros((self.nx,self.ny,4))
        for i in range(4):
            self._cry[...,i] = self._read_gdata_file(
                os.path.join(self.outdir,'cry{}.dat'.format(i)))

    @property
    def z(self):
        return self.cry


    @solps_property
    def cr(self):
        self._cr = np.mean(self.r, 2)

    @solps_property
    def cz(self):
        self._cz = np.mean(self.z, 2)


    @solps_property
    def cr_x(self):
        self._cr_x = (self.r[1:,:,0]+self.r[1:,:,2])/2.0

    @solps_property
    def cr_y(self):
        self._cr_y = (self.r[:,1:,0]+self.r[:,1:,1])/2.0

    @solps_property
    def cz_x(self):
        self._cz_x = (self.z[1:,:,0]+self.z[1:,:,2])/2.0

    @solps_property
    def cz_y(self):
        self._cz_y = (self.z[:,1:,0]+self.z[:,1:,1])/2.0


    ## ATTENTION: We need self.sep
    @property
    def sep(self):
        try:
            return self.mother.sep
        except:
            return 19

    @solps_property
    def ds(self):
        """(nx,ny) : Radial distance from the separatrix [m]"""
        self._ds = np.zeros((self.nx,self.ny))
        proxy = np.zeros((self.nx,self.ny))
        for i in range(self.nx):
            proxy[i,0] = 0
            for k in range(1,self.ny):
                proxy[i,k] = (proxy[i,k-1] +
                              np.sqrt((self.cr[i,k] - self.cr[i,k-1])**2 +
                                      (self.cz[i,k] - self.cz[i,k-1])**2))
            ds_offset = (proxy[i,self.sep-1] + proxy[i,self.sep])/2
            self._ds[i,:] = proxy[i,:] - ds_offset



    @solps_property
    def bb(self):
        """ (nx,ny): Total magnetic field [T]
        """
        self._bb = self._read_gdata_signal('bb')

    @solps_property
    def bbx(self):
        """ (nx,ny): Poloidal magnetic field [T]
        """
        self._bbx = self._read_gdata_signal('bbx')

    @solps_property
    def bbz(self):
        """ (nx,ny): Toroidal magnetic field [T]
        """
        self._bbz = self._read_gdata_signal('bbz')





    @solps_property
    def sxl(self):
        """ Sx but mapped into left face.
        <vol>/<hx> is not the same as <vol/hx>
        """
        volavg = self.map(self.vol, 'left')
        hxavg = self.map(self.hx, 'left')
        sxl = volavg/hxavg
        sxl[np.isnan(sxl)] = 0.0
        self._sxl = sxl

    @solps_property
    def syb(self):
        """ Sy but mapped into bottom face.
        <vol>/<hy> is not the same as <vol/hy>
        """
        volavg = self.map(self.vol, 'bottom')
        hyavg = self.map(self.hy, 'bottom')
        #self._syb = volavg/hyavg
        syb = volavg/hyavg
        syb[np.isnan(syb)] = 0.0
        self._syb = syb

    @solps_property
    def syb1(self):
        """ Sy but mapped into bottom face.
        <vol>/<hy> is not the same as <vol/hy>
        THIS IS THE CORRECT ONE to reconstruct e.g. V ExB flow.
        """
        volavg = self.map(self.vol, 'bottom')
        hy1avg = self.map(self.hy1, 'bottom')
        #self._syb1 = volavg/hy1avg
        syb1 = volavg/hy1avg
        syb1[np.isnan(syb1)] = 0.0
        self._syb1 = syb1





    ### ------------------------ CONTINUITY EQUATION --------------------------
    ### BASIC ANALYSIS

    @solps_property
    def b2npc_resco(self):
        """ (nx, ny, ns): Residuals of the continuity equation [m^-3 s^-1].
        """
        self._b2npc_resco = self._read_gdata_signal('b2npc*_resco*')

    @solps_property
    def b2srdt_snadt(self):
        """ (nx, ny, ns): Partial time derivative of the fluid density [1/s]
        From sources/b2srdt.F:
        b2srdt_snadt = snadt(:,:,0,is)+snadt(:,:,1,is)*na(:,:,is)

        LaTeX: - \sqrt{g} \frac{\partial n_{a}}{\partial t}
        """
        self._b2srdt_snadt = self._read_gdata_signal('b2srdt_snadt*')


    @solps_property
    def b2npc_sna(self):
        """ (nx, ny, ns): Total particle source [1/s].

        From equations/b2npc9.F:
        b2npc9_sna = snb(:,:,0)+nb*snb(:,:,1)

        These pieces are apparently also saved.
        """
        self._b2npc_sna = self._read_gdata_signal('b2npc*_sna*')

    @solps_property
    def b2npc_fnax(self):
        """ (nx, ny, ns): Effective Poloidal particle flux [1/s].
        For ions, it is the effective flow, not the physical one,
        but it should give the same divergence as a physical one.

        For ions, it can be recreated as:

        recon =

               \+ :py:attr:`~b2tfnb_bxuanax`

               \+ :py:attr:`~b2tfnb_vaecrbnax`

               \+ :py:attr:`~b2tfnb_dpccornax`

               \+ :py:attr:`~b2tfnb_dPat_mdf_gradnax`

               \- :py:attr:`~b2tfnb_fnbPSchx`
        """
        self._b2npc_fnax = self._read_gdata_signal('b2npc*_fnax*')

    @solps_property
    def b2npc_fnay(self):
        """ (nx,ny,ns): Effective Radial particle flux [1/s].
        For ions, it is the effective flow, not the physical one,
        but it should give the same divergence as a physical one.

        For ions, it can be recreated as:
        """
        self._b2npc_fnay = self._read_gdata_signal('b2npc*_fnay*')



    @solps_property
    def b2npc_fna(self):
        self._b2npc_fna = np.zeros((self.nx,self.ny,2,self.ns))
        self._b2npc_fna[...,0,:] = self.b2npc_fnax
        self._b2npc_fna[...,1,:] = self.b2npc_fnay


#    @solps_property
#    def cont_rhs(self):
#        self._cont_rhs = self.b2srdt_snadt + self.b2npc_sna
#
#    @solps_property
#    def continuity_equation(self):
#        """ (nx,ny,ns): Sum of all the terms of the cont. equ. = 0 [1/s*m^-3]
#        Should equal resco, but it doesn't.
#        """
#        self._continuity_equation = self.residuals(
#                self.b2npc_sna, self.b2npc_fna)
#
#

    @solps_property
    def b2tfnb_fnbx(self):
        """ (nx,ny,ns): Real(?) Poloidal particle flux [m^2 s^-1].
        Almost equal to b2fstate fnax.
        """
        self._b2tfnb_fnbx = self._read_gdata_signal('b2tfnb_fnbx*')

    @solps_property
    def b2tfnb_fnby(self):
        """ (nx,ny,ns): Real(?) Radial particle flux [m^2 s^-1].
        Almost equal to b2fstate fnay.
        """
        self._b2tfnb_fnby = self._read_gdata_signal('b2tfnb_fnby*')



    @solps_property
    def b2tfnb_fnb(self):
        self._b2tfnb_fnb = np.zeros((self.nx,self.ny,2,self.ns))
        self._b2tfnb_fnb[...,0,:] = self.b2tfnb_fnbx
        self._b2tfnb_fnb[...,1,:] = self.b2tfnb_fnby



    ### FINE ANALYSIS
    @solps_property
    def b2npc_na(self):
        """ (nx, ny, ns): Fluid density [m^-3]
        """
        self._b2npc_na = self._read_gdata_signal('b2npc*_na*')

    @solps_property
    def ne(self):
        """ (nx, ny): Electron density [m^-3]
        """
        self._ne = self._read_gdata_signal('ne')


    @solps_property
    def b2tfnb_kbnrgy(self):
        """ Kinetic energy.
        """
        self._b2tfnb_kbnrgy = self._read_gdata_signal('b2tfnb_kbnrgy')


    ### Sources
    @solps_property
    def b2stel_sna_ion(self):
        """ Ion source due to ionization.
        """
        self._b2stel_sna_ion = self._read_gdata_signal('b2stel_sna_ion*')

    @solps_property
    def b2stel_sna_rec(self):
        """ Ion source due to recombination.
        """
        self._b2stel_sna_rec = self._read_gdata_signal('b2stel_sna_rec*')

    @solps_property
    def b2stbr_sna_eir(self):
        """ Ion source due to EIRENE neutrals.
        """
        self._b2stbr_sna_eir = self._read_gdata_signal('b2stbr_sna_eir*')

    @solps_property
    def b2stbc_phys_sna(self):
        """ Ion source in the guard cells due to boundary conditions.
        """
        self._b2stbc_phys_sna = self._read_gdata_signal('b2stbc_phys_sna*')


    ### Fluxes
    @solps_property
    def b2tfnb_bxuanax(self):
        """ Parallel flow.
        """
        self._b2tfnb_bxuanax = self._read_gdata_signal('b2tfnb_bxuanax*')


    @solps_property
    def b2tfnb_vaecrbnax(self):
        """ Poloidal ExB flow.
        """
        self._b2tfnb_vaecrbnax = self._read_gdata_signal('b2tfnb_vaecrbnax*',
                correct_dims= (self.nx,self.ny,self.ns))

    @solps_property
    def b2tfnb_vaecrbnay(self):
        """ Radial ExB flow.
        """
        self._b2tfnb_vaecrbnay = self._read_gdata_signal('b2tfnb_vaecrbnay*',
                correct_dims= (self.nx,self.ny,self.ns))

    @solps_property
    def b2tfnb_vaecrbna(self):
        """ ExB flow.
        """
        self._b2tfnb_vaecrbna = np.zeros((self.nx,self.ny,2,self.ns))
        self._b2tfnb_vaecrbna[...,0,:] = self.b2tfnb_vaecrbnax
        self._b2tfnb_vaecrbna[...,1,:] = self.b2tfnb_vaecrbnay

    @property
    def _recon_b2tfnb_vaecrbnax(self):
        """ Visually perfect reconstruction
        Just for testing purposes.
        """
        volavg = self.map(self.vol, 'left')
        hxavg =  self.map(self.hx, 'left')
        sxavg =  volavg/hxavg
        naavg =  self.map(self.b2npc_na, 'left')
        vecrb =  self.b2tfnb_vbecrbx.copy()
        recon = (sxavg.T*naavg.T*vecrb.T).T
        recon[np.isnan(recon)] = 0.0
        return recon

    @property
    def _recon_b2tfnb_vaecrbnay1(self):
        """ Visually perfect reconstruction
        Just for testing purposes.
        THIS IS THE CORRECT ONE to reconstruct e.g. V ExB flow.
        HY1 > HY
        """
        naavg =  self.map(self.b2npc_na, 'bottom')
        vecrb =  self.b2tfnb_vbecrby.copy()
        recon = (self.syb1.T*naavg.T*vecrb.T).T
        recon[np.isnan(recon)] = 0.0
        return recon



    @solps_property
    def b2tfnb_dpccornax(self):
        """ Flow due to numerical correction of the poloidal
        velocity due to dpc.
        """
        self._b2tfnb_dpccornax = self._read_gdata_signal('b2tfnb_dpccornax*')


    @solps_property
    def b2tfnb_cvlbnay(self):
        """ Anomalous pinch flow.
        """
        self._b2tfnb_cvlbnay = self._read_gdata_signal('b2tfnb_cvlbnay*')

    @solps_property
    def b2tfnb_dPat_mdf_gradnax(self):
        """ Corrected poloidal anomalous diffusion after Pantakar's scheme.
        .. note::
            This is the flow used in the balance equation.
        """
        self._b2tfnb_dPat_mdf_gradnax = self._read_gdata_signal(
                'b2tfnb_dPat_mdf_gradnax*',
                correct_dims= (self.nx,self.ny,self.ns))

    @solps_property
    def b2tfnb_dPat_mdf_gradnay(self):
        """ Corrected radial anomalous diffusion after Pantakar's scheme.
        .. note::
            This is the flow used in the balance equation.
        """
        self._b2tfnb_dPat_mdf_gradnay = self._read_gdata_signal(
                'b2tfnb_dPat_mdf_gradnay*',
                correct_dims= (self.nx,self.ny,self.ns))

    @solps_property
    def b2tfnb_dPat_mdf_gradna(self):
        self._b2tfnb_dPat_mdf_gradna = np.zeros((self.nx,self.ny,2,self.ns))
        self._b2tfnb_dPat_mdf_gradna[...,0,:] = self.b2tfnb_dPat_mdf_gradnax
        self._b2tfnb_dPat_mdf_gradna[...,1,:] = self.b2tfnb_dPat_mdf_gradnay



    @solps_property
    def b2tfnb_vadianax(self):
        """ Effective gradB poloidal flow,
        even if it is not named like that in pdf??
        Cell face if drift_style=1 (default).
        """
        self._b2tfnb_vadianax = self._read_gdata_signal('b2tfnb_vadianax*',
                correct_dims= (self.nx,self.ny,self.ns))

    @solps_property
    def b2tfnb_vadianay(self):
        """ Effective gradB radial flow,
        even if it is not named like that in pdf??
        """
        self._b2tfnb_vadianay = self._read_gdata_signal('b2tfnb_vadianay*',
                correct_dims= (self.nx,self.ny,self.ns))

    @solps_property
    def b2tfnb_vadiana(self):
        self._b2tfnb_vadiana = np.zeros((self.nx,self.ny,2,self.ns))
        self._b2tfnb_vadiana[...,0,:] = self.b2tfnb_vadianax
        self._b2tfnb_vadiana[...,1,:] = self.b2tfnb_vadianay



    @solps_property
    def b2tfnb_wbdianax(self):
        """ Real poloidal diamagnetic flux.

        .. note::
            Manual reconstruction of diamagnetic flux using real
            :math:`V_{dia}`, instead of effective :math:`\\tilde{V}_{dia}`.
        """
        naavg =  self.map(self.b2npc_na, 'left')
        wbdia =  self.b2tfnb_wbdiax.copy()
        recon = (self.sxl.T*naavg.T*wbdia.T).T
        recon[np.isnan(recon)] = 0.0
        self._b2tfnb_wbdianax = recon

    @solps_property
    def b2tfnb_wbdianay(self):
        """ Real poloidal diamagnetic flux.

        .. note::
            Manual reconstruction of diamagnetic flux using real
            :math:`V_{dia}`, instead of effective :math:`\\tilde{V}_{dia}`.
        """
        naavg =  self.map(self.b2npc_na, 'bottom')
        wbdia =  self.b2tfnb_wbdiay.copy()
        recon = (self.syb1.T*naavg.T*wbdia.T).T
        recon[np.isnan(recon)] = 0.0
        self._b2tfnb_wbdianay = recon

    @solps_property
    def b2tfnb_wbdiana(self):
        self._b2tfnb_wbdiana = np.zeros((self.nx,self.ny,2,self.ns))
        self._b2tfnb_wbdiana[...,0,:] = self.b2tfnb_wbdianax
        self._b2tfnb_wbdiana[...,1,:] = self.b2tfnb_wbdianay




    @solps_property
    def vedia(self):
        from solpspy.b25_src.transport.b2tfed import b2tfed
        self._veecrb, self._vedia = b2tfed(self.mother)

    @solps_property
    def veecrb(self):
        from solpspy.b25_src.transport.b2tfed import b2tfed
        self._veecrb, self._vedia = b2tfed(self.mother)


    @property
    def veecrbx(self):
        return self.wedia[...,0]

    @property
    def veecrby(self):
        return self.wedia[...,1]

    @property
    def vediax(self):
        return self.wedia[...,0]

    @property
    def vediay(self):
        return self.wedia[...,1]



    @solps_property
    def wedia(self):
        from solpspy.b25_src.transport.b2tfed import calculate_wedia
        self._wedia = calculate_wedia(self.mother)

    @property
    def wediax(self):
        return self.wedia[...,0]

    @property
    def wediay(self):
        return self.wedia[...,1]


    @solps_property
    def veecrbnex(self):
        """ Reconstruction of ecrb flux for electrons
        """
        neavg =  self.map(self.ne, 'left')
        veecrb =  self.veecrbx.copy()
        recon = (self.sxl.T*neavg.T*veecrb.T).T
        recon[np.isnan(recon)] = 0.0
        self._veecrbnex = recon

    @solps_property
    def veecrbney(self):
        """ Reconstruction of ecrb flux for electrons
        """
        neavg =  self.map(self.ne, 'bottom')
        veecrb =  self.veecrby.copy()
        recon = (self.syb1.T*neavg.T*veecrb.T).T
        recon[np.isnan(recon)] = 0.0
        self._veecrbney = recon

    @solps_property
    def veecrbne(self):
        self._veecrbne = np.zeros((self.nx,self.ny,2,self.ns))
        self._veecrbne[...,0,:] = self.veecrbnex
        self._veecrbne[...,1,:] = self.veecrbney



    @solps_property
    def vedianex(self):
        """ Reconstruction of diamagnetic flux using real Vdia
        """
        neavg =  self.map(self.ne, 'left')
        vedia =  self.vediax.copy()
        recon = (self.sxl.T*neavg.T*vedia.T).T
        recon[np.isnan(recon)] = 0.0
        self._vedianex = recon

    @solps_property
    def vedianey(self):
        """ Reconstruction of diamagnetic flux using real Vdia
        """
        neavg =  self.map(self.ne, 'bottom')
        vedia =  self.vediay.copy()
        recon = (self.syb1.T*neavg.T*vedia.T).T
        recon[np.isnan(recon)] = 0.0
        self._vedianey = recon

    @solps_property
    def wediane(self):
        self._vediane = np.zeros((self.nx,self.ny,2,self.ns))
        self._vediane[...,0,:] = self.vedianex
        self._vediane[...,1,:] = self.vedianey



    @solps_property
    def wedianex(self):
        """ Reconstruction of diamagnetic flux using real Vdia
        """
        neavg =  self.map(self.ne, 'left')
        wedia =  self.wediax.copy()
        recon = (self.sxl.T*neavg.T*wedia.T).T
        recon[np.isnan(recon)] = 0.0
        self._wedianex = recon

    @solps_property
    def wedianey(self):
        """ Reconstruction of diamagnetic flux using real Vdia
        """
        neavg =  self.map(self.ne, 'bottom')
        wedia =  self.wediay.copy()
        recon = (self.syb1.T*neavg.T*wedia.T).T
        recon[np.isnan(recon)] = 0.0
        self._wedianey = recon

    @solps_property
    def wediane(self):
        self._wediane = np.zeros((self.nx,self.ny,2,self.ns))
        self._wediane[...,0,:] = self.wedianex
        self._wediane[...,1,:] = self.wedianey








    @solps_property
    def b2tfnb_dgradpbx(self):
        """ Corrected poloidal anomalous flux due to pressure gradient.
        """
        self._b2tfnb_dgradpbx = self._read_gdata_signal('b2tfnb_dgradpbx*',
                correct_dims= (self.nx,self.ny,self.ns))

    @solps_property
    def b2tfnb_dgradpby(self):
        """ Corrected radial anomalous flux due to pressure gradient.
        """
        self._b2tfnb_dgradpby = self._read_gdata_signal('b2tfnb_dgradpby*',
                correct_dims= (self.nx,self.ny,self.ns))


    @solps_property
    def b2tfnb_fnbPSchx(self):
        """ Pfirst-Schlueter poloidal particle flux.
        """
        self._b2tfnb_fnbPSchx = self._read_gdata_signal('b2tfnb_fnbPSchx*')

    @solps_property
    def b2tfnb_fnbPSchy(self):
        """ Pfirst-Schlueter radial particle flux.
        """
        self._b2tfnb_fnbPSchy = self._read_gdata_signal('b2tfnb_fnbPSchy*')

    @solps_property
    def b2tfnb_fnbPSch(self):
        self._b2tfnb_fnbPSch = np.zeros((self.nx,self.ny,2, self.ns))
        self._b2tfnb_fnbPSch[...,0] = self.b2tfnb_fnbPSchx
        self._b2tfnb_fnbPSch[...,1] = self.b2tfnb_fnbPSchy



    @solps_property
    def b2tfnb_fnb_hex(self):
        """ (nx,ny): Poloidal particle flux modified for heat eq [s^-1].
        """
        self._b2tfnb_fnb_hex = self._read_gdata_signal('b2tfnb_fnb_hex*')

    @solps_property
    def b2tfnb_fnb_hey(self):
        """ (nx,ny): Radial particle flux modified for heat eq [s^-1].
        """
        self._b2tfnb_fnb_hey = self._read_gdata_signal('b2tfnb_fnb_hey*')

    @solps_property
    def b2tfnb_fnb_he(self):
        self._b2tfnb_fnb_he = np.zeros((self.nx,self.ny,2, self.ns))
        self._b2tfnb_fnb_he[...,0] = self.b2tfnb_fnb_hex
        self._b2tfnb_fnb_he[...,1] = self.b2tfnb_fnb_hey


    ### Transport coefficients
    @solps_property
    def b2trno_cdnax(self):
        """ Anomalous transport cofficients before Pantakar correction
        at poloidal cell faces.
        ???
        """
        self._b2trno_cdnax = self._read_gdata_signal('b2trno_cdnax*')

    @solps_property
    def b2trno_cdnay(self):
        """ Anomalous transport cofficients before Pantakar correction
        at radial cell faces.
        ??? Does not really look like  the other one.
        """
        self._b2trno_cdnay = self._read_gdata_signal('b2trno_cdnay*')



    @solps_property
    def b2tqna_dna(self):
        """ Anomalous transport cofficients before Pantakar correction
        at cell center.
        """
        self._b2tqna_dna = self._read_gdata_signal('b2tqna_dna*')




    ### Velocities
    @solps_property
    def b2tfnb_vbecrbx(self):
        """ ExB poloidal velocity.
        """
        self._b2tfnb_vbecrbx = self._read_gdata_signal('b2tfnb_vbecrbx*',
                correct_dims= (self.nx,self.ny,self.ns))

    @solps_property
    def b2tfnb_vbecrby(self):
        """ ExB radial velocity.
        """
        self._b2tfnb_vbecrby = self._read_gdata_signal('b2tfnb_vbecrby*',
                correct_dims= (self.nx,self.ny,self.ns))


    @solps_property
    def b2tfnb_vbdiax(self):
        """ gradB poloidal velocity.
        """
        self._b2tfnb_vbdiax = self._read_gdata_signal('b2tfnb_vbdiax*')

    @solps_property
    def b2tfnb_vbdiay(self):
        """ gradB radial velocity.
        """
        self._b2tfnb_vbdiay = self._read_gdata_signal('b2tfnb_vbdiay*')


    @solps_property
    def b2tfnb_wbdiax(self):
        """ Diamagnetic poloidal velocity.
        """
        self._b2tfnb_wbdiax = self._read_gdata_signal('b2tfnb_wbdiax*')

    @solps_property
    def b2tfnb_wbdiay(self):
        """ Diamagnetic radial velocity.
        """
        self._b2tfnb_wbdiay = self._read_gdata_signal('b2tfnb_wbdiay*')


    @solps_property
    def b2tfnb_vla0y(self):
        """ Anomalous pinch velocity.
        """
        self._b2tqna_vla0y = self._read_gdata_signal('b2tqna_vla0y*')

























































    ### ------------------------ MOMENTUM   EQUATION --------------------------
    ### BASIC ANALYSIS

    @solps_property
    def b2npmo_smb(self):
        """ (nx,ny,ns): Total momentum source [].
        """
        self._b2npmo_smb = self._read_gdata_signal('b2npmo_smb*')

    @solps_property
    def b2npmo_fmox(self):
        """ (nx,ny,ns): Total poloidal momentum flux [].
        """
        self._b2npmo_fmox = self._read_gdata_signal('b2npmo_fmox*')

    @solps_property
    def b2npmo_fmoy(self):
        """ (nx,ny,ns): Total radial momentum flux [].
        """
        self._b2npmo_fmoy = self._read_gdata_signal('b2npmo_fmoy*')

    @solps_property
    def b2npmo_fmo(self):
        """ (nx,ny,2,ns): Total momentum flux [].
        """
        self._b2npmo_fmo = np.zeros((self.nx,self.ny,2,self.ns))
        self._b2npmo_fmo[:,:,0] = self.b2npmo_fmox
        self._b2npmo_fmo[:,:,1] = self.b2npmo_fmoy

    @solps_property
    def b2npmo_resmo(self):
        """ (nx,ny,ns): Residuals of the momentum equation [].
        """
        self._b2npmo_resmo = self._read_gdata_signal('b2npmo*_resmo*')


    @solps_property
    def b2srdt_smodt(self):
        """ (nx,ny,ns): Partial time derivative of the flux []
        """
        self._b2srdt_smodt = self._read_gdata_signal('b2srdt_smodt*')




    ### FINE ANALYSIS
    ### Sources
    @solps_property
    def b2npmo_smocf(self):
        """ Centrifugal force.
        """
        self._b2npmo_smocf = self._read_gdata_signal('b2npmo_smocf*')

    @solps_property
    def b2npmo_smogpi(self):
        """ Ion pressure force ??
        """
        self._b2npmo_smogpi = self._read_gdata_signal('b2npmo_smogpi*')

    @solps_property
    def b2npmo_smogpo(self):
        """ Electrostatic force ??
        """
        self._b2npmo_smogpo = self._read_gdata_signal('b2npmo_smogpo*')




    ### Fluxes
    @solps_property
    def b2nmo_fmox(self):
        """ Total poloidal momentum flux.
        """
        self._b2nmo_fmox = self._read_gdata_signal('b2nmo_fmox*')

    @solps_property
    def b2nmo_fmoy(self):
        """ Total radial momentum flux.
        """
        self._b2nmo_fmoy = self._read_gdata_signal('b2nmo_fmoy*')


    ### Velocities
    @solps_property
    def b2npmo_ua(self):
        """ Parallel velocity.
        """
        self._b2npmo_ua = self._read_gdata_signal('b2npmo_ua*')















    ### -------------- ELECTRON HEAT EQUATION ---------------------------------

    @solps_property
    def b2nph_te(self):
        """ Electron temperature [J]
        """
        self._b2nph_te = self._read_gdata_signal('b2nph*_te')

    @solps_property
    def b2srdt_shedt(self):
        """ (nx,ny): Source of electron heat due to time evolution [W].
        From sources/b2srdt.F (255):
        This term = shedt(:,:,0)+shedt(:,:,1)*te+
                    shedt(:,:,2)*ne + shedt(:,:,3)*te*ne
        Negative sign.
        """
        self._b2srdt_shedt = self._read_gdata_signal('b2srdt_shedt')

    @solps_property
    def b2nph_fhex(self):
        """ (nx,ny): Effective electron poloidal heat flux [W].
        """
        self._b2nph_fhex = self._read_gdata_signal('b2nph*_fhex')

    @solps_property
    def b2nph_fhey(self):
        """ (nx,ny): Effective electron poloidal heat flux [W].
        """
        self._b2nph_fhey = self._read_gdata_signal('b2nph*_fhey')

    @solps_property
    def b2nph_fhe(self):
        self._b2nph_fhe = np.zeros((self.nx,self.ny,2))
        self._b2nph_fhe[...,0] = self.b2nph_fhex
        self._b2nph_fhe[...,1] = self.b2nph_fhey


    @solps_property
    def b2nph_she(self):
        """ (nx,ny): Total source of electron heat [W].
        Effectively, dt(pe) + grad(Gamma heat e) = This term.
        """
        self._b2nph_she = self._read_gdata_signal('b2nph*_she')

#    @solps_property
#    def b2npht_she(self):
#        """ (nx,ny): Total source of electron heat [W].
#        Effectively, dt(pe) + grad(Gamma heat e) = This term.
#        Alternative form?? Compare to other version.
#        """
#        self._b2npht_she = self._read_gdata_signal('b2npht_she')





    @solps_property
    def b2tfhe__fhePSchx(self):
        """ (nx,ny): Pfirsch-Schlueter electron poloidal heat flux [W].
        """
        self._b2tfhe__fhePSchx = self._read_gdata_signal('b2tfhe__fhePSchx')

    @solps_property
    def b2tfhe__fhePSchy(self):
        """ (nx,ny): Pfirsch-Schlueter electron radial heat flux [W].
        """
        self._b2tfhe__fhePSchy = self._read_gdata_signal('b2tfhe__fhePSchy')

    @solps_property
    def b2tfhe__fhePSch(self):
        self._b2tfhe__fhePSch = np.zeros((self.nx,self.ny,2))
        self._b2tfhe__fhePSch[...,0] = self.b2tfhe__fhePSchx
        self._b2tfhe__fhePSch[...,1] = self.b2tfhe__fhePSchy




    @solps_property
    def b2tfhe__qe_ke_gTx(self):
        """ (nx,ny): gradT (conductive) electron poloidal heat flux [W].
        Negative sign.
        """
        self._b2tfhe__qe_ke_gTx = self._read_gdata_signal('b2tfhe__qe_ke_gTx')

    @solps_property
    def b2tfhe__qe_ke_gTy(self):
        """ (nx,ny): gradT (conductive) electron radial heat flux [W].
        Negative sign.
        """
        self._b2tfhe__qe_ke_gTy = self._read_gdata_signal('b2tfhe__qe_ke_gTy')


    @solps_property
    def b2tfhe__qe_ke_gT(self):
        self._b2tfhe__qe_ke_gT = np.zeros((self.nx,self.ny,2))
        self._b2tfhe__qe_ke_gT[...,0] = self.b2tfhe__qe_ke_gTx
        self._b2tfhe__qe_ke_gT[...,1] = self.b2tfhe__qe_ke_gTy


    @solps_property
    def b2tfhe__qe_32x(self):
        """ (nx,ny): Conductive poloidal electron  heat flux [W].
        Negative sign.
        """
        self._b2tfhe__qe_32x = self._read_gdata_signal('b2tfhe__qe_32x')

    @solps_property
    def b2tfhe__qe_32y(self):
        """ (nx,ny): Conductive radial electron  heat flux [W].
        Negative sign.
        """
        self._b2tfhe__qe_32y = self._read_gdata_signal('b2tfhe__qe_32y')


    @solps_property
    def b2tfhe__qe_32(self):
        self._b2tfhe__qe_32 = np.zeros((self.nx,self.ny,2))
        self._b2tfhe__qe_32[...,0] = self.b2tfhe__qe_32x
        self._b2tfhe__qe_32[...,1] = self.b2tfhe__qe_32y



#    @solps_property
#    def b2tfhe__qe_fchpTex(self):
#        """ (nx,ny): Electron heat flux related to poloidal projection of
#        the parallel current [W].
#        """
#        self._b2tfhe__qe_fchpTex = self._read_gdata_signal('b2tfhe__qe_fchpTex')


    @solps_property
    def b2tfhe__qe_alphaTehx(self):
        """ (nx,ny): Poloidal electron heat flux due to electric field or
        poloidal projecto of parallel current [W].
        Negative sign.
        """
        self._b2tfhe__qe_alphaTehx = self._read_gdata_signal(
                'b2tfhe__qe_alphaTehx')

    @solps_property
    def b2tfhe__qe_alphaTehy(self):
        """ (nx,ny): Radial electron heat flux due to electric field [W].
        Negative sign.
        """
        self._b2tfhe__qe_alphaTehy = self._read_gdata_signal(
                'b2tfhe__qe_alphaTehy')


    @solps_property
    def b2tfhe__qe_alphaTeh(self):
        self._b2tfhe__qe_alphaTeh = np.zeros((self.nx,self.ny,2))
        self._b2tfhe__qe_alphaTeh[...,0] = self.b2tfhe__qe_alphaTehx
        self._b2tfhe__qe_alphaTeh[...,1] = self.b2tfhe__qe_alphaTehy





    @solps_property
    def b2tfhe__qediax(self):
        """ (nx,ny): Diamagnetic poloidal energy term (5/2) [W].
        Negative sign.
        """
        self._b2tfhe__qediax = self._read_gdata_signal('b2tfhe__qediax')

    @solps_property
    def b2tfhe__qediay(self):
        """ (nx,ny): Diamagnetic radial energy term (5/2) [W].
        Negative sign.
        """
        self._b2tfhe__qediay = self._read_gdata_signal('b2tfhe__qediay')


    @solps_property
    def b2tfhe__qedia(self):
        self._b2tfhe__qedia = np.zeros((self.nx,self.ny,2))
        self._b2tfhe__qedia[...,0] = self.b2tfhe__qediax
        self._b2tfhe__qedia[...,1] = self.b2tfhe__qediay






#    @solps_property
#    def qeirx(self):
#        """ Reconstruction based on C.101 (* vol/h) [W].
#        Could be same as total power (Eirene works with total energy).
#        """
#        tmp = 5.0/2.0*self.feirx*self.b2nph_te + self.b2tfhe_qe_alphaTehx #str?
#        return tmp + self.b2tfhe__qe_ke_gTx
#
#
#
#    @solps_property
#    def feirx(self):
#











    ### -------------- ION      HEAT EQUATION ---------------------------------

    @solps_property
    def b2nph_ti(self):
        """ Ion temperature [J]
        """
        self._b2nph_ti = self._read_gdata_signal('b2nph*_ti')

    @solps_property
    def b2srdt_shidt(self):
        """ (nx,ny): Source of ion heat due to time evolution [W].
        From sources/b2srdt.F (255):
        This term = shidt(:,:,0)+shidt(:,:,1)*ti+
                    shidt(:,:,2)*ni + shidt(:,:,3)*ti*ni
        """
        self._b2srdt_shidt = self._read_gdata_signal('b2srdt_shidt')

    @solps_property
    def b2nph_fhix(self):
        """ (nx,ny): Effective ion poloidal heat flux [W].
        """
        self._b2nph_fhix = self._read_gdata_signal('b2nph*_fhix')

    @solps_property
    def b2nph_fhiy(self):
        """ (nx,ny): Effective ion poloidal heat flux [W].
        """
        self._b2nph_fhiy = self._read_gdata_signal('b2nph*_fhiy')

    @solps_property
    def b2nph_fhi(self):
        self._b2nph_fhi = np.zeros((self.nx,self.ny,2))
        self._b2nph_fhi[...,0] = self.b2nph_fhix
        self._b2nph_fhi[...,1] = self.b2nph_fhiy


    @solps_property
    def b2nph_shi(self):
        """ (nx,ny): Total source of ion heat [W].
        Effectively, dt(pi) + grad(Gamma heat i) = This term.
        """
        self._b2nph_shi = self._read_gdata_signal('b2nph*_shi')





    @solps_property
    def b2tfhi__qi_ki_gTx(self):
        """ (nx,ny): gradT (conductive) ion poloidal heat flux [W].
        Negative sign.
        """
        self._b2tfhi__qi_ki_gTx = self._read_gdata_signal('b2tfhi__qi_ki_gTx')

    @solps_property
    def b2tfhi__qi_ki_gTy(self):
        """ (nx,ny): gradT (conductive) ion radial heat flux [W].
        Negative sign.
        """
        self._b2tfhi__qi_ki_gTy = self._read_gdata_signal('b2tfhi__qi_ki_gTy')


    @solps_property
    def b2tfhi__qi_ki_gT(self):
        self._b2tfhi__qi_ki_gT = np.zeros((self.nx,self.ny,2))
        self._b2tfhi__qi_ki_gT[...,0] = self.b2tfhi__qi_ki_gTx
        self._b2tfhi__qi_ki_gT[...,1] = self.b2tfhi__qi_ki_gTy



    @solps_property
    def b2tfhi__qidiax(self):
        """ (nx,ny): Diamagnetic poloidal energy term (5/2) [W].
        Negative sign.
        """
        self._b2tfhi__qidiax = self._read_gdata_signal('b2tfhi__qidiax')

    @solps_property
    def b2tfhi__qidiay(self):
        """ (nx,ny): Diamagnetic radial energy term (5/2) [W].
        Negative sign.
        """
        self._b2tfhi__qidiay = self._read_gdata_signal('b2tfhi__qidiay')


    @solps_property
    def b2tfhi__qidia(self):
        self._b2tfhi__qidia = np.zeros((self.nx,self.ny,2))
        self._b2tfhi__qidia[...,0] = self.b2tfhi__qidiax
        self._b2tfhi__qidia[...,1] = self.b2tfhi__qidiay





#    @property
#    def _recon_b2tfhi__qidiax(self):
#        """ Visually perfect reconstruction
#        Just for testing purposes.
#        """
#        volavg = self.map(self.vol, 'left')
#        hxavg =  self.map(self.hx, 'left')
#        sxavg =  volavg/hxavg
#        naavg =  self.map(self.b2npc_na, 'left')
#        tiavg =  self.map((self.b2nph_ti), 'left')
#        vecrb =  self.b2tfnb_vbecrbx.copy()
#        recon = 2.5*(sxavg.T*tiavg.T*naavg.T*vecrb.T).T
#        recon[np.isnan(recon)] = 0.0
#        return recon[...,1] #This is wrong, it should be sum over ion species.

    @property
    def _recon_b2tfhi__qidiay1(self):
        """ Visually perfect reconstruction
        Just for testing purposes.
        THIS IS THE CORRECT ONE to reconstruct e.g. V ExB flow.
        HY1 > HY
        """
        #naavg =  self.map(self.b2npc_na, 'bottom')
        #tiavg =  self.map((self.b2nph_ti), 'bottom')
        #vecrb =  self.b2tfnb_vbecrby.copy()
        #recon = 2.5*(self.syb1.T*tiavg.T*naavg.T*vecrb.T).T
        #recon[np.isnan(recon)] = 0.0
        #return recon[...,1] #This is wrong, it should be sum over ion species.


        #natiavg =  self.map((self.b2nph_ti.T*self.b2npc_na.T).T, 'bottom')
        #vecrb =  self.b2tfnb_vbecrby.copy()
        #recon = 2.5*(self.syb1.T*natiavg.T*vecrb.T).T
        #recon[np.isnan(recon)] = 0.0
        #return recon[...,1] #This is wrong, it should be sum over ion species.

        tiavg =  self.map(self.b2nph_ti, 'bottom')
        vdia =  self.b2tfnb_vadianay.copy()
        recon = 2.5*(self.syb1.T*tiavg.T*vdia.T).T
        recon[np.isnan(recon)] = 0.0
        return recon[...,1] #This is wrong, it should be sum over ion species.


































    ### ------------------------ CURRENT    EQUATION --------------------------
    @solps_property
    def b2npp_po(self):
        """ Electric potential.
        """
        self._b2npp_po = self._read_gdata_signal('b2npp*_po')

    @solps_property
    def b2npp_fchx(self):
        """ Total poloidal current times vol/hx.
        """
        self._b2npp_fchx = self._read_gdata_signal('b2npp*_fchx')

    @solps_property
    def b2npp_fchy(self):
        """ Total radial current times vol/hy.
        """
        self._b2npp_fchy = self._read_gdata_signal('b2npp*_fchy')


    @solps_property
    def b2tfhe__fch_px(self):
        """ Poloidal projection of the parallel current times vol/hx.
        """
        self._b2tfhe__fch_px = self._read_gdata_signal('b2tfhe__fch_px')



    @solps_property
    def b2tfhe__fchdiax(self):
        """ Poloidal diamagnetic (gradB) current times vol/hx.
        """
        self._b2tfhe__fchdiax = self._read_gdata_signal('b2tfhe__fchdiax')

    @solps_property
    def b2tfhe__fchdiay(self):
        """ Radial diamagnetic (gradB) current times vol/hy.
        """
        self._b2tfhe__fchdiay = self._read_gdata_signal('b2tfhe__fchdiay')

    @solps_property
    def b2tfhe__fchdia(self):
        self._b2tfhe__fchdia = np.zeros((self.nx,self.ny,2))
        self._b2tfhe__fchdia[...,0] = self.b2tfhe__fchdiax
        self._b2tfhe__fchdia[...,1] = self.b2tfhe__fchdiay


## ATTENTION!!
## Not yet ready
#    @solps_property
#    def fchwdiax(self):
#        """ Real poloidal diamagnetic (gradB) current times vol/hx.
#        Should contain za.
#        """
#        self._fchwdiax = (np.sum(self.za*self.qe*self.b2tfnb_wbdianax,2) -
#                          self.qe*self.wedianex)
#
#
#    @solps_property
#    def fchwdiay(self):
#        """ Real radial diamagnetic (gradB) current times vol/hy1.
#        """
#        self._fchwdiay = (np.sum(self.za*self.qe*self.b2tfnb_wbdianay,2) -
#                          self.qe*self.wedianey)
#
#    @solps_property
#    def fchwdia(self):
#        self._fchwdia = np.zeros((self.nx,self.ny,2))
#        self._fchwdia[...,0] = self.fchwdiax
#        self._fchwdia[...,1] = self.fchwdiay
#








    @solps_property
    def b2tfhe__fchinertx(self):
        """ Inertial current times vol/hx.
        """
        self._b2tfhe__fchinertx = self._read_gdata_signal('b2tfhe__fchinertx')

    @solps_property
    def b2tfhe__fchinerty(self):
        """ Inertial current times vol/hy.
        """
        self._b2tfhe__fchinerty = self._read_gdata_signal('b2tfhe__fchinerty')



    @solps_property
    def b2tfhe__fchvisparx(self):
        """ Poloidal current due to parallel viscosity, times vol/hx.
        """
        self._b2tfhe__fchvisparx = self._read_gdata_signal('b2tfhe__fchvisparx')

    @solps_property
    def b2tfhe__fchvispary(self):
        """ Radial current due to parallel viscosity, times vol/hy.
        """
        self._b2tfhe__fchvispary = self._read_gdata_signal('b2tfhe__fchvispary')



    @solps_property
    def b2tfhe__fchinx(self):
        """ Poloidal current due to ion-neutral collisions current
        times vol/hx.
        """
        self._b2tfhe__fchinx = self._read_gdata_signal('b2tfhe__fchinx')

    @solps_property
    def b2tfhe__fchiny(self):
        """ Radial  current due to ion-neutral collisions current
        times vol/hy.
        """
        self._b2tfhe__fchiny = self._read_gdata_signal('b2tfhe__fchiny')



    @solps_property
    def b2tfhe__csig_anx(self):
        """ Anomalous poloidal current coefficient.
        """
        self._b2tfhe__csig_anx = self._read_gdata_signal('b2tfhe__csig_anx')

    @solps_property
    def b2tfhe__csig_any(self):
        """ Anomalous radial  current coefficient
        """
        self._b2tfhe__csig_any = self._read_gdata_signal('b2tfhe__csig_any')



    @solps_property
    def b2tfhe__fchvispery(self):
        """ Radial current due to parallel viscosity, times vol/hy.
        """
        self._b2tfhe__fchvispery = self._read_gdata_signal('b2tfhe__fchvispery')



    @solps_property
    def b2tfhe__fchvisqy(self):
        """ Radial current due to viscosity associated with heat fluxes,
        times vol/hy.
        """
        self._b2tfhe__fchvisqy = self._read_gdata_signal('b2tfhe__fchvisqy')



    @solps_property
    def b2tfhe__fchstochy(self):
        """ Radial stochastic conductiviy current, times vol/hy.
        """
        self._b2tfhe__fchstochy = self._read_gdata_signal('b2tfhe__fchstochy')



    @solps_property
    def b2tfhe__fchanmlx(self):
        """ Poloidal anomalous current [A]
        """
        self._b2tfhe__fchanmlx = self._read_gdata_signal('b2tfhe__fchanmlx')

    @solps_property
    def b2tfhe__fchanmly(self):
        """ Poloidal anomalous current [A]
        """
        self._b2tfhe__fchanmly = self._read_gdata_signal('b2tfhe__fchanmly')





## Not yet assigned ----------------------------------------------------------
#    @solps_property
#    def na(self):
#        """ (nx,ny,ns): Fluid density [m^-3]
#        """
#        self._na = self._read_gdata_signal('na*')
#
    @solps_property
    def te(self):
        """ (nx,ny): Electron temperature [eV]
        """
        try:
            self._te = self._read_gdata_signal('te')
        except:
            self._te = self._read_gdata_signal('te_eV')











### ===========================================================================
### ===========================================================================
### ===========================================================================
class BalancePlot(object):
    def __init__(self, mother, variables, **kwargs):
        """
        Params
        ------
        variables: list(dicts)
            Each element of the list correspond to a line to be ploted
            in the radial balance.
        mask: (nx,ny), optional.
            Region in which the balance is performed.
        """
        self.mother = mother
        self.variables = variables

        self.fig     = priority(['fig','figure'], kwargs, None)
        self.canvas  = priority(['canvas','ax'], kwargs, None)
        self.figsize = priority(['figsize','fig_size'], kwargs, None) #Big!

        self.xlabel  = priority('xlabel', kwargs, '')
        self.ylabel  = priority('ylabel', kwargs, '')

        self.mask    = priority('mask',  kwargs, None)
        self.iups    = priority('iups',  kwargs, None)
        self.idwn    = priority('idwn',  kwargs, None)
        self.ird1    = priority('ird1',  kwargs, None)
        self.ird2    = priority('ird2',  kwargs, None)

        #if fig and not self.canvas:
        #    self.canvas = fig.get_canvas()
        #elif selfcanvas and not sel.fig:
        #    self.fig = canvas.get_fig()
        #elif not self.fig and not self.canvas:
        #    self.fig, self.canvas = plt.subplots(1, figsize=self.figsize)
        if not self.canvas:
            self.fig, self.canvas = plt.subplots(1, figsize=self.figsize)


        def _create_region(self):
            """
            Uses mask, if not, then use indices.

            """
            if self.mask:
                return self.mask


        def poloidal_int(self, var, mask):
            """ Is logic correct here?
            This should work for any dims on top of (nx,ny)
            """
            tmp = np.zeros(var.shape[1:])
            for iy in range(self.ny):
                for ix in range(self.nx):
                    if mask[ix,iy]:
                        tmp[iy] += var[ix,iy]
            return tmp

        def radial_int(self, var, mask):
            """ Is logic correct here?
            This should work for any dims on top of (nx,ny)
            """
            tmp = np.zeros(list(itertools.chain.from_iterable(
                    [[var.shape[0]], var.shape[2:]])))
            for ix in range(self.nx):
                for iy in range(self.ny):
                    if mask[ix,iy]:
                        tmp[ix] += var[ix,iy]
            return tmp


class RadialBalance(BalancePlot):
    def __init__(self, mother, variables, **kwargs):
        super(RadialBalance,self).__init__(mother, variables,**kwargs)
        embed()

    def _create_artist(self, dvar):
        """
        dict might contain
            'base': meaning of y axis (e.g., dS)
            'data': (nx,ny) array of the quantity.
            'label': label of quantity, can be in LaTeX.
            'color': color of line.
            'lw': Line width.
            'ls': Line style.
            'factor: Factor to multiply the quantity.
                     Can be scalar and/or surface
                     to change dimenions.
            'integrate': Boolean. Should it be poloidally integrated?

        Return 2DLine
        """
        integrate = priority('integrate', dvar, True)
        x = dvar['base']
        if istype(dvar['var'],str):
            var = getattr(self.mother,dvar['var'])
        else:
            var = dvar['var'].copy()


        line = mpl.line.Line2D(x,y)
        if 'color' in dvar:
            line.set_color(dvar['color'])
        if 'lw' in dvar:
            line.set_lw(dvar['lw'])
        if 'ls' in dvar:
            line.set_ls(dvar['ls'])
        if 'label' in dvar:
            line.set_label(dvar['label'])

        return line


    def plot():
        pass















###############################################################################
###############################################################################
###############################################################################
if __name__ == '__main__':
    qe = 1.6021766028e-19
    import scipy.io as io
    from solpspy import *
    from IPython import embed
    #path = '/ptmp1/scratch/ipar/solps-iter/runs/aug_ddnu_nepow_feedback/alt_Bt=-2.5T_kaveeva=1e-2_ip=1.0MA_inert=1.0_vispar=1.0_visper=1.0_i-n=1.0_tests/SI_33988m_5MW.D=3e22.Ar=1e19.N=2e19.test_20190313/case-b2fplasmf'


    path = '/ptmp1/scratch/ipar/solps-iter/runs/aug_ddnu_nepow_feedback_coreflux=1.00e20/alt_Bt=+2.5T_kaveeva=1e-2_ip=1.0MA_BCCON=22_BCENI=1_BCENE=1/test_nesepm=1.0_tecore=550eV_ticore=450eV'



    run = SolpsData(path)
    outputs = EquationsOutput(run)

    assert np.allclose(outputs.te, run.te)
    assert np.allclose(outputs.ne, run.ne)
    assert np.allclose(outputs.leftix, run.leftix)
    assert np.allclose(outputs.leftiy, run.leftiy)
    assert np.allclose(outputs.rightix, run.rightix)
    assert np.allclose(outputs.rightiy, run.rightiy)
    assert np.allclose(outputs.bottomix, run.bottomix)
    assert np.allclose(outputs.bottomiy, run.bottomiy)
    assert np.allclose(outputs.topix, run.topix)
    assert np.allclose(outputs.topiy, run.topiy)



    special_run_case = False
    if special_run_case:
        ## From b2npc11, thus ions
        nb1 = np.load(os.path.join(path, 'nb1.npy'))
        fnb1 = np.load(os.path.join(path, 'fnb1.npy'))
        snbpc1 = np.load(os.path.join(path, 'snb1.npy'))
        snb1 = snbpc1[...,0] + snbpc1[...,1]*nb1
        cont_rhs1 = np.load(os.path.join(path, 'cont_rhs1.npy'))

        rescb1 = np.load(os.path.join(path, 'rescb1.npy'))
        msk = np.logical_and( #Corners are set to 0.0
                np.logical_or(
                    outputs.bottomiy == -1, outputs.topiy==outputs.ny),
                np.logical_or(
                    outputs.leftix == -1, outputs.rightix==outputs.nx))
        rescb1[msk] = 0.0

        assert np.allclose(outputs.b2npc_na[...,1], nb1)
        assert np.allclose(outputs.b2npc_fna[...,1], fnb1)
        assert np.allclose(outputs.b2npc_sna[:,1:,1],
                snb1[:,1:], rtol=0.001) #why???
        assert np.allclose(outputs.b2npc_resco[...,1], rescb1)


        snb_b = np.load(os.path.join(path,'snb_before_saving.npy'))
        nb_b = np.load(os.path.join(path,'nb_before_saving.npy'))
        wrk01 = snb_b[...,0] + nb_b*snb_b[...,1]
        assert np.allclose(wrk01, outputs.b2npc_sna[...,1], rtol=0.001)


        #Stages of residual processses
        resfn1 = np.load(os.path.join(path, 'resfn_sfn.npy'))
        resfn2 = np.load(os.path.join(path, 'resfn_sfn_ffnx.npy'))
        resfn3 = np.load(os.path.join(path, 'resfn_sfn_ffnx_ffny.npy'))

        #Same inputs as taken from inside the routine in solps
        sfn_inside = np.load(os.path.join(path, 'sfn_inside.npy'))
        fun_inside = np.load(os.path.join(path, 'fun_inside.npy'))
        wrk0 = sfn_inside[...,0] +fun_inside*sfn_inside[...,1]
        wrk0 = np.repeat(wrk0[:,:,np.newaxis], 2, axis=2)
        ffn_inside = np.load(os.path.join(path, 'ffn_inside.npy'))

        #res = outputs.residuals(wrk0, ffn_inside)
        #assert np.allclose(res, outputs.b2npc_resco[...,1])



    #balance = io.netcdf_file(os.path.join(path,'balance.nc'))
    #fmo_flua = balance.variables['fmo_flua'].data.T
    #fmox_flua = fmo_flua[:,:,0].copy()
    #ofmox_flua = (outputs.b2npmo_fmox.T*outputs.hx.T/outputs.vol.T).T






    testing_drifts = False
    if testing_drifts:

        ## Reconstruction of ExB flux
        volavg = outputs.map(outputs.vol, 'left')
        hxavg = outputs.map(outputs.hx, 'left')
        sxavg = volavg/hxavg
        naavg = outputs.map(outputs.b2npc_na, 'left')
        vecrb = outputs.b2tfnb_vbecrbx.copy()
        recon = (sxavg.T*naavg.T*vecrb.T).T
        recon[np.isnan(recon)] = 0.0

        assert np.allclose(recon, outputs.b2tfnb_vaecrbnax)
        assert np.allclose(outputs._recon_b2tfnb_vaecrbnax,
                outputs.b2tfnb_vaecrbnax)

        ### THIS IS NOT A VALID RECONSTRUCTION
        #sx = outputs.vol/outputs.hx
        #na = outputs.b2npc_na[...,1]
        #recon2 = outputs.map(sx*na, 'left')*vecrb
        #assert np.allclose(recon2, outputs.b2tfnb_vaecrbnax)


        ## Reconstruction of Dia or gradB flux
        vdia = outputs.b2tfnb_vbdiax.copy()
        recon = (sxavg.T*naavg.T*vdia.T).T
        recon[np.isnan(recon)] = 0.0

        assert np.allclose(recon, outputs.b2tfnb_vadianax)


        ## Reconstruction of total POLOIDAL flux
        fnax = outputs.b2npc_fnax.copy()

        recon =  outputs.b2tfnb_bxuanax.copy()
        recon += outputs.b2tfnb_vaecrbnax.copy()
        recon += outputs.b2tfnb_dpccornax.copy()
        recon += outputs.b2tfnb_dPat_mdf_gradnax.copy()
        recon += outputs.b2tfnb_dgradpbx.copy()
        recon -= outputs.b2tfnb_fnbPSchx.copy()

        #This is because for iy=0 and ny-1,
        # fnax ==0 , but recon =! 0
        # For ions it is not the physical flow but gives the same
        # divergence as the physical flow.
        # Only truly works for D. For Ferdinand a=1, even with
        # rtol=0.1 there are (13,20) (49,32) and (86,0) that are wrong
        assert np.allclose(fnax[:,1:-2,1], recon[:,1:-2,1]) #For D


        # For neutrals, some pieces seem to be missing??
        #Almost all are correct here...
        #assert np.allclose(fnax[:,1:-2,0], recon[:,1:-2,0], rtol=0.01)



        ## Reconstruction of total Radial flux
        fnay = outputs.b2npc_fnay.copy()

        recon = outputs.b2tfnb_vaecrbnay.copy()
        recon +=  outputs.b2tfnb_cvlbnay.copy()
        recon +=  outputs.b2tfnb_dPat_mdf_gradnay.copy()
        recon += outputs.b2tfnb_dgradpby.copy()
        recon -= outputs.b2tfnb_fnbPSchy.copy()
        #Only main ion species:
        recon[...,1] = recon[...,1] + outputs.b2tfhe__csig_any.copy()/qe
        recon[...,1] = recon[...,1] + outputs.b2tfhe__fchinerty/qe
        recon[...,1] = recon[...,1] + outputs.b2tfhe__fchvispary/qe

        embed()
        ## Somewhat close in some cases but still quite far away.
        #assert np.allclose(fnay[:,1:-2,1], recon[:,1:-2,1])



        ## Reconstruct currents??





#            'base': meaning of y axis (e.g., dS)
#            'data': (nx,ny) array of the quantity.
#            'label': label of quantity, can be in LaTeX.
#            'color': color of line.
#            'lw': Line width.
#            'ls': Line style.
#            'factor: Factor to multiply the quantity.
#                     Can be scalar and/or surface
#                     to change dimenions.
#            'integrate': Boolean. Should it be poloidally integrated?
#




    testing_plot = False
    if testing_plot:
        variables = [
            {'data': outputs.b2npc_sna[...,1],
            'base':outputs.ds,
            'label': 'Sources',
            'color':'r',
            'lw':3.0,
            'ls':'-',
            'factor':1.0,
            'integrate':True,},

            {'data': outputs.div_flux(outputs.b2npc_fna)[...,1,1],
            'base':outputs.ds,
            'label':'Rad. div.',
            'color':'k',
            'lw':3.0,
            'ls':'-',
            'factor':1.0,
            'integrate':True,},

            {'data': outputs.div_flux(outputs.b2npc_fna)[...,0,1],
            'base':outputs.ds,
            'label':'Pol. div.',
            'color':'b',
            'lw':3.0,
            'ls':'-',
            'factor':1.0,
            'integrate':True,}
            ]

        tmp = RadialBalance(outputs, variables)



    testing_sum = True
    if testing_sum:
        a1 = np.sum(outputs.te[52:56,22])
        a2 = np.sum(outputs.te[52:54,23])
        gmask = np.zeros((outputs.nx, outputs.ny))
        gmask[52:56,22] = 1
        gmask[52:54,23] = 1
        tmp = outputs.sum(outputs.te, gmask)
        assert np.allclose(a1, tmp[22])
        assert np.allclose(a2, tmp[23])




    testing_flux = True
    if testing_flux:
        assert np.allclose(run.ua, outputs.b2npmo_ua)
        bxz = np.abs(outputs.bbx/outputs.bbz)
        bx = outputs.bbx/outputs.bb
        sy = outputs.vol/outputs.hx
        sx = outputs.vol/outputs.hy

        pbsx =  sy*bx
        assert np.allclose(run.parallel_surface_m, pbsx)
        pbsxz = sy*bxz

        uax = outputs.b2tfnb_bxuanax[...,1]/(pbsx*
                outputs.b2npc_na[...,1])
        uaxz = outputs.b2tfnb_bxuanax[...,1]/(pbsxz*
                outputs.b2npc_na[...,1])
        uapbx= outputs.b2tfnb_bxuanax[...,1]/(outputs.pbsx*
                outputs.b2npc_na[...,1])


        embed()













