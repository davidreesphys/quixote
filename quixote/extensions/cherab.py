
import os
import numpy as np

import quixote


class Cherab(object):
    def __init__(self, mother):
        """ Cherab class exposes the Quixote/Azalea/Solpspy API in a way
        that make the SolpsData object pluggeable in the Cherab pipeline.
        """
        self.mother = mother
        self.log = create_log('Cherab', {'ident':self.mother.name})
        self.config = mother.config
