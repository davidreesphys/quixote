.. _indices:

Indices and dimensions
======================


Now you have your data, but that is only half of the task:
knowing how to navigate it is as important.

Indices are variables that help you relate data to its 
position or meaning within the SOLPS data structure and grid.

.. note::

    Indices' names always start with **i**.

In general, the first dimensions of most variables are related
to its grid position. Usually, if the data is per species, the
very last dimension will be the species axis.
Any extra dimensions are usually sandwiched between these two
groups.

Therefore, for structured grids, most data have a shape of
(nx, ny), (nx, ny, ns), (nx, ny, ...), or (nx, ny, ..., ns).
In the case of unstructure data, the data are flattened into a
single "grid dimension" and (nx, ny) is replaced by one
of the many types of grid dimensions:

    * ncv: Number of all cell volumes.
    * nfc: Number of all cell faces.
    * nvx: Number of all cell vertices.
    * nfs: Number of flux surfaces.
    * nft: Number of flux tubes.
    * ...

So the ion density, na, will have dimensions (nx, ny, ns) or
(ncv, ns), depending on the version of the code.

The particle flux, fna, has dimensiones (nx, ny, 2, ns) 
(the third dimension being for x/y components of fna) or
(nfc, 2, ns).
Note that now all poloidal and radial faces, and every 
other face type in between are put together in the same
dimension, indistinguishable from each other.
Additionally, in cases with 9pt stencil, all faces have
an x and y flux components.

In both cases, ncv and nfc, as well as all the other grid
dimensions do not really follow a pattern in fully 
unstructured grids, and further index variables are required
to effectively navigate

.. note::

    Structured and unstructured data have different grid dimensionality.
    A nice trick to use the same code for both of them when one is only
    interested in manipulating other dimensions, e.g. the species index,
    is to use the Python elipsis ``...`` instead of ``:``.

    .. code-block:: python
        
        data.na[...,1]

    instead of 
        
    .. code-block:: python
        
        data.na[:,:,1] # Structured
        data.na[:,1] # Unstructured








Structured grids
----------------


.. jupyter-execute::
   :hide-code:
   :hide-output:

    import os
    import numpy as np
    import matplotlib.pyplot as plt

    import copy
    import quixote.tools as tools
    tools.create_log('root', level='critical')

    import quixote
    data = quixote.SolpsData('guides/examples/structured')





Unstructured grids
------------------


There are three important classes of index related variables:

    * Relational
    * Labelling
    * Positional



Relational index variables
^^^^^^^^^^^^^^^^^^^^^^^^^^

Relational index variables helps you relate different grid dimensions
with each other.

They all have the same name structure: **i** +
**what you have** +
**what you want** .

For example, I HAVE a cell volume, cv, and I WANT the cell faces, fc, therefore
the variable that i will use is icvfc:

.. jupyter-execute::
   :hide-code:
   :hide-output:

    data = quixote.SolpsData('guides/examples/wg')

.. jupyter-execute::
    
    my_cv = 100
    print(data.icvfc[my_cv])


and for the reverse operation (notice that we won't get only the initial
cell volume, but also the neighbor with which its shares a face):

.. jupyter-execute::
    
    print(data.ifccv[
        data.icvfc[my_cv]])


In principle, the most used  transitions have been implemented.
If for a niche case one transition is required and not implemented,
the user can always compose two of these relational index variables.

For example, iftcv[ifcft] is equivalent to ifccv, with extra steps
(and numerous solutions).



Labelling variables
^^^^^^^^^^^^^^^^^^^

.. warning::

    Names still follow the old criteria in the examples, but my intention
    is to change them to the new criteria as soon as possible.

Labelling variables give a name to a number, i.e., they label what
that number represents.

For example, ifsreg is a list with the corresponding index region of each 
flux surface. On the other hand, fsreg is a dictionary containing the meaning
of each 


.. jupyter-execute::

    print(data.fsreg)


.. jupyter-execute::

    print(data.fsregions)

.. jupyter-execute::

    print(data.fsregions[1])
    print(data.fsregions['core'])
