.. _installation:

Installation
============


**Requirements**

The updated version can be found in ``requirements.txt``.

*Common python packages*
 * numpy
 * scipy
 * matplotlib

*Specific packages (not installable via pip or conda)*
 * MDSplus

*For testing*
 * pytest

*For documentation*
 * sphinx
 * sphinx_rtd_theme
 * jupyter
 * nbsphinx
 * jupyter_sphinx


.. note::

   The following sections are a summary on how to clone
   and ``pip install -e .``  the ``setup.py``.
   If you already know how to do it, then you probably
   do not need to read them.

   There is no secret ingredient.


Download
--------

The package must **NOT** be in a place that can be found by ``PYTHONPATH``
(recommended) or at least the name of the package folder must not be
``quixote`` (so you would need to rename it after cloning but before
installing. Not recommended).
This is done to avoid conflicting names between a visible version and
the installed version.

Oh, how the times have changed!


In my case, I decided to create a new directory called ``python-packages``
in my ``repository`` directory.
Again, my ``python-packages`` is **NOT** added to ``PYTHONPATH``.


The public repository of ``quixote`` is hosted in Gitlab and can be cloned
via HTML::

    git clone https://gitlab.com/ipar/quixote.git

or via SSH (sometimes requires identification or keyprints)::

    git clone git@gitlab.com:ipar/quixote.git
    

Use the branch that better suits you:
    * **master**: stable version of the code.
      Recommended for most users.
      It is also the default cloned branch.

    * **dev**: development branch. The branch that should be copied and
      modified by potential collaborators. It also contains the latests
      updates, but it might be unstable.
      
To get  the ``dev`` branch and have it automatically tracking the gitlab
repository (as ``origin``), it should be enough for you to do::

    git checkout dev

``git`` is the most widely used version control protocol.
Therefore, further information is readily available on 
the Internet.


Install
-------

Go to the ``quixote`` package directory that you have just cloned (so NOT
``quixote/quixote``, just ``quixote``) and run ONE of the following
commands to install ``quixote`` either locally or globally.

.. note::

   Yes, there is a ``.`` at the end of some commands and it is important:
   it indicates where ``setup.py`` is located.





Using ``conda`` enviroments
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Most people nowadays, including users in most clusters (TOK, Triton, etc), use
``conda``, sometimes unknowingly.

You can install ``quixote`` in your ``conda env`` as usual.

For more information about ``conda``, please check `Conda <conda.rst>`_.

Once you have activated your ``conda env`` ::

    python -m pip install -e .

A soft link will be located in
``~/.conda/envs/my-env/lib/pythonx.x/site-packages/``.

If you work with two or more ``python`` versions, please check
:ref:`troubleshooting`.

If you ever need to uninstall the package, just run anywhere
when your ``conda env`` is activated::

    python -m pip uninstall quixote






Using only ``pip``
^^^^^^^^^^^^^^^^^^

Using only ``pip`` means that you will install ``quixote`` and all the
dependencies in your "base" enviroment.

If you do not have admin, root or sudo rights (most people),
then you are going to make a local/user installation
(the soft link will be located in ``~/.local/lib/pythonx.x/site-packages/``)::

    python -m pip install --user -e .

If you have sudo rights and you want to use them, you can perform a global
installation::

    python -m pip install -e .


If you work with two or more ``python`` versions, please check
:ref:`troubleshooting`.



If you ever need to uninstall the package, just run anywhere::

    python -m pip uninstall quixote 



.. _troubleshooting:

Troubleshooting
^^^^^^^^^^^^^^^

**I have two or more** ``python`` **versions on the same enviroment.**

So you have decided to make your life harder, huh.

Make sure that your ``python`` is the correct one::

    which python

or if you are also using ``python 2.7`` in the same enviroment (support ended
in Jan 2020, com'on)::

    which python3

This command will show you the path to the binnary file of ``python``.
Then, using that exact path with ``python`` at the end::

    path/to/whatever/python -m pip install -e .


or::

    path/to/whatever/python -m pip install --user -e .

as explained in the previous sections.








Testing
-------

.. note::
   It is highly recommended to all users to test the installation
   at least once.

To test the validity of your installation, please git clone the quixote test suite::

    git clone https://gitlab.com/ipar/quixote-tests.git

and inside the suite run::

    python -m pytest -x


If you do not get any errors (a big red E with a lot of red text), then
everything is fine. Skips (the little s) are normal.

See Testing_ for more information.

.. _Testing: guides/testing.ipynb


For coverage, if you want the HTML coverage report::

    python -m pytest -x --cov-report html --cov=quixote
    firefox htmlcov/index.html

if you want the terminal coverage report::

    python -m pytest -x --cov-report term --cov=quixote






Installing: The hacking way
---------------------------

.. warning::

   This is no longer the recommended way to install ``quixote``.

If ``pip install -e .`` does not work, i.e., does not make a soft link in your
site-packages and therefore available to import, then you can hack your way.

First, follow the Download subsection in the previous section.

In order to make ``quixote`` visible to Python, you will need to make the
package directory, ``quixote``, visible to ``PYTHONPATH`` enviromental variable,
so that ``quixote/quixote`` can be found.

This will defeat the entire purpose of the new structure, but the Zen states
that working is always better than not working.




An example for both the TOK and the Triton cluster are given below. For other clusters, adapt these steps to your needs.

TOK cluster
^^^^^^^^^^^

In the case of tcsh, to add the parent directory (``repository/python/quixote``) to the python path PYTHONPATH var (using the appropiate path)::

    setenv PYTHONPATH ${PYTHONPATH}:/afs/ipp-garching.mpg.de/home/i/ipar/repository/python/quixote

In order to be able to use the MDSplus package::

    module load mdsplus
    source $MDSPLUS_DIR/setup.csh

This can automatize by adding these lines of code to your loging or console file, e.g., ``~/.public/user.cshrc``.

The pytest package is necessary for testing, which is strongly recommended even for non-developers, to ensure a correct installation. In the TOK cluster, packages must be installed in the user space::

    pip install --user pytest




Triton cluster
^^^^^^^^^^^^^^

In the case of tcsh, to add the parent directory (``repository/python/quixote``) to the python path PYTHONPATH var (using the appropiate path)::

    ml purge
    ml anaconda

    export PYTHONPATH=/scratch/work/paradei1/repository/python/quixote:$PYTHONPATH

In order to be able to use the MDSplus package::

    module use /share/apps/spack/envs/fgci-centos7-haswell-dev/lmod/linux-centos7-x86_64/all
    module load mdsplus

I have added the following to my ~/.zshrc file::
This can automatize by adding these lines of code to your loging or console file, e.g., ``~/.zshrc``.
