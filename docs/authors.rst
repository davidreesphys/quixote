Authors and contributors
========================

**Ivan Paradela Perez**, original author and general developer.

**Ferdinand Hitzler**, main author of chords and spectroscopy.
